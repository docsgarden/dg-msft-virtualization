---
id: zCWFQh9WbQ462qQ44VEKvZo
title: >-
    WHvEmulatorDestoryEmulator method
desc: >-
    Learn about the WHvEmulatorDestoryEmulator method.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WHvEmulatorDestoryEmulator
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 97835248d5e985cd1db47c4b565fd346d83c45bf329cf94bc96fa2a6c33ba1b7
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WHvEmulatorDestoryEmulator.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/19/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WHvEmulatorDestoryEmulator.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WHvEmulatorDestoryEmulator).

# WHvEmulatorDestoryEmulator


## Syntax

```c
HRESULT
WINAPI
WHvEmulatorDestoryEmulator(
    _In_ WHV_EMULATOR_HANDLE Emulator
    );
```
### Parameters

`Partition`

Handle to the emulator instance that is destroyed.

## Remarks
Destroy an instance of the instruction emulator created by [[`WHvEmulatorCreateEmulator`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.whvemulatorcreateemulator]]