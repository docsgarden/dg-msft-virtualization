---
id: AXuLj9sVDFLjS4pc3gubMFc
title: >-
    HCS_EVENT_TYPE
desc: >-
    HCS_EVENT_TYPE
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HCS_EVENT_TYPE
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 85acf5842cbf083ca3055cb5413d66f68b9c020ecbe2b662c9e2a4b2eb118c70
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HCS_EVENT_TYPE.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HCS_EVENT_TYPE']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HCS_EVENT_TYPE.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HCS_EVENT_TYPE).

# HCS_EVENT_TYPE

## Description

Events indicated to callbacks registered by [[HcsSetComputeSystemCallback|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssetcomputesystemcallback]] or [[HcsSetProcessCallback|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssetprocesscallback]].

## Syntax

```cpp
typedef enum HCS_EVENT_TYPE
{
    HcsEventInvalid = 0x00000000,
    HcsEventSystemExited = 0x00000001,
    HcsEventSystemCrashInitiated = 0x00000002,
    HcsEventSystemCrashReport = 0x00000003,
    HcsEventSystemRdpEnhancedModeStateChanged = 0x00000004,
    HcsEventSystemSiloJobCreated = 0x00000005,
    HcsEventSystemGuestConnectionClosed = 0x00000006,
    HcsEventProcessExited = 0x00010000,
    HcsEventOperationCallback = 0x01000000,
    HcsEventServiceDisconnect = 0x02000000
} HCS_EVENT_TYPE;
```

## Constants

|Name|Description|
|---|---|
|`HcsEventInvalid`|The event is invalid.|
|`HcsEventSystemExited`|The notification of `HCS_SYSTEM` handle for system exited.|
|`HcsEventSystemCrashInitiated`|The notification of `HCS_SYSTEM` handle for crash initiated.|
|`HcsEventSystemCrashReport`|The notification of `HCS_SYSTEM` handle for crash report.|
|`HcsEventSystemRdpEnhancedModeStateChanged`|The notification of `HCS_SYSTEM` handle for Rdp enhanced mode state changed.|
|`HcsEventSystemSiloJobCreated`|Reserved For Future Use.|
|`HcsEventSystemGuestConnectionClosed`|The notification of `HCS_SYSTEM` handle for guest connection closed.|
|`HcsEventProcessExited`|The notification of `HCS_PROCESS` handle for process exited.|
|`HcsEventOperationCallback`|The notification of call-back operation.|
|`HcsEventServiceDisconnect`|The notification of service disconnect.|

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeDefs.h |