---
id: 69HU2VWJ8Lr2L27iqQs6ZtB
title: >-
    HdvWriteGuestMemory
desc: >-
    HdvWriteGuestMemory
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvWriteGuestMemory
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: a6af72354417d5aa5f8938fa7e8a3d7010d51c97e2e06ffd8b661f6bb1bd0eb6
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvWriteGuestMemory.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HdvWriteGuestMemory']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvWriteGuestMemory.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvWriteGuestMemory).

# HdvWriteGuestMemory

## Description

Writes the contents of the supplied buffer to guest primary memory (RAM).

## Syntax

```C++
HRESULT WINAPI
HdvWriteGuestMemory(
    _In_                        HDV_DEVICE Requestor,
    _In_                        UINT64     GuestPhysicalAddress,
    _In_                        UINT32     ByteCount,
    _In_reads_(ByteCount) const BYTE*      Buffer
    );
```

## Parameters

|Parameter|Description|
|---|---|---|---|---|---|---|---|
|`Requestor` |Handle to the device requesting memory access.|
|`GuestPhysicalAddress` |Guest physical address at which the write operation starts.|
|`ByteCount` |Number of bytes to write.|
|`Buffer` |Source buffer for the write operation.|

## Return Values

|Return Value     |Description|
|---|---|
|`S_OK` | Returned if function succeeds.|
|`HRESULT` | An error code is returned if the function fails.
|     |     |

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |