---
id: 4mvu3FpSjKzzx3eTnBSa7ET
title: >-
    HcnCreateLoadBalancer
desc: >-
    HcnCreateLoadBalancer
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnCreateLoadBalancer
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 387950c3facc548537788f923e0630c072dcc72e50f5857f44c80a27f604911e
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnCreateLoadBalancer.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnCreateLoadBalancer']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnCreateLoadBalancer.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnCreateLoadBalancer).

# HcnCreateLoadBalancer

## Description

Creates a load balancer.

## Syntax

```cpp
HRESULT
WINAPI
HcnCreateLoadBalancer(
    _In_ REFGUID Id,
    _In_ PCWSTR Settings,
    _Out_ PHCN_LOADBALANCER LoadBalancer,
    _Outptr_opt_ PWSTR* ErrorRecord
    );
```

## Parameters

`Id`

Id for the new load balancer.

`Settings`

JSON document specifying the settings of the [[load balancer|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.HNS_Schema#HostComputeLoadBalancer]].

`LoadBalancer`

Receives a handle to the newly created load balancer. It is the responsibility of the caller to release the handle using [[HcnCloseLoadBalancer|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcncloseloadbalancer]] once it is no longer in use.

`ErrorRecord`

Receives a JSON document with extended errorCode information. The caller must release the buffer using CoTaskMemFree.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |