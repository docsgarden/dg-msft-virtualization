---
id: 4zpGfdxUcqsieWbZUiXihnt
title: >-
    More on virtualizing SharePoint
desc: >-
    Since there was interest in the previous post about the UK services team's series on virtualizing SharePoint, I wanted to recommend the following video from October 2008.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090408-more-on-virtualizing-sharepoint
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: a05024221e5302548f1f953249424fc1ea77383e82568703947daa7bbcc87c51
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090408-more-on-virtualizing-sharepoint.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-04-08 17:37:00"
    ms.date: "04/08/2009"
    categories: "enterprise-strategy-group"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090408-more-on-virtualizing-sharepoint.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090408-more-on-virtualizing-sharepoint).

# More on virtualizing SharePoint

Since there was interest in the [previous post](https://blogs.technet.com/virtualization/archive/2009/04/06/Series-on-virtualizing-SharePoint.aspx "TechNet blog post") about the UK services team's series on virtualizing SharePoint, I wanted to recommend the following video from October 2008. The interview is with Michael Noel of Convergent Computing.  Michael is also a SharePoint MVP. In this video (6 minutes long), Michael talks about using System Center Virtual Machine Manager to deploy SharePoint.  He also talks about disaster recovery and quickly provisioning a SharePoint farm.

There's a high interest amongst our customers to adopt SharePoint, and to run it within a virtual machine. Mark at Enterprise Strategy Group [[his blog](http://esgblogs.typepad.com/marks_blog/ "Mark Bowker blog")] shared the following data with me [from a Dec 2008 research brief by ESG], that partially explains why there's interest.

 

> _In a recent survey of 1,191 IT and business managers, ESG found that one in three (33%) North American and Western European midmarket and enterprise companies are currently using Microsoft ’s SharePoint suite of collaboration and content management tools. __As adoption becomes more widespread, SharePoint is quickly becoming an organization-wide, critical business resource. As shown in Figure 1, 29% of current SharePoint users rate the implementation of SharePoint as one of their top three IT initiatives over the past 12-24 months and 82% place it in their top ten initiatives._
> 
> _ESG ’s research also reveals that SharePoint is viewed very favorably by current users, with 69% of current adopters saying they would recommend that organizations similar to their own implement SharePoint broadly._

  
[Michael Noel on Virtualization Part II](https://www.microsoft.com/video/en/us/details/c82a8f7a-2261-4f4c-8197-518649c8d4b4?vp_evt=eref&vp_video=Michael+Noel+on+Virtualization+Part+II)

BTW \- for those of you who read this blog on the Web (20-25% of you), I hope you like the new skins. Patrick