---
id: 7dmEpEcgfMEeUiq3Ya9AfQU
title: >-
    Windows Server “8” Beta – Hyper-V Replica
desc: >-
    Windows Server “8” Beta – Hyper-V Replica
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2012/20120309-windows-server-8-beta-hyper-v-replica
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 9f0eafbcbda4106d6c2ac5fa4ded86a1d89645d2f170f039910d4ba7bcb3f677
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2012/20120309-windows-server-8-beta-hyper-v-replica.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "03/09/2012"
    date: "2012-03-09 06:12:00"
    categories: "hvr"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2012/20120309-windows-server-8-beta-hyper-v-replica.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2012/20120309-windows-server-8-beta-hyper-v-replica).

# Windows Server “8” Beta – Hyper-V Replica

**Hyper-V Replica (HVR)** is a new feature in Windows Server “8” Beta that provides asynchronous replication of Hyper-V virtual machines for BCDR (Business Continuity and Disaster Recovery) scenarios.  Watch this space for in depth blogs from the Hyper-V Replica team on how-to’s, tips & tricks, sample PS cmdlets, commentary on some of the engineering decisions (eg: why do we support server names in the product and not IP addresses, why did we design resync and scenarios where it would be used). To know more about HVR, see [Understand and Troubleshoot Hyper-V Replica](https://www.microsoft.com/download/en/details.aspx?id=29016). A quick summary which demonstrates the feature capabilities is available [here](https://technet.microsoft.com/library/hh831759.aspx?ppud=4). We want this to be an interactive dialogue between you and the engineers that built this - so appreciate your comments/feedback ! Stay tuned and watch for posts with the **HVR** tag...   

\- Hyper-V Replication Team