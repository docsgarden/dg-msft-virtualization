---
id: 6EKUVjkH7vrznfgnFHJtBr3
title: >-
    Orchestrate containers with a gMSA
desc: >-
    How to orchestrate Windows containers with a Group Managed Service Account (gMSA).
canonicalUrl: https://docs.microsoft.com/virtualization/windowscontainers/manage-containers/gmsa-orchestrate-containers
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 4b80fa37fb0567dfeed9b05d4dd30125330e86e65cc0a3c9638b7831a9bab5f0
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/windowscontainers/manage-containers/gmsa-orchestrate-containers.md
    keywords: "docker, containers, active directory, gmsa, orchestration, kubernetes, group managed service account, group managed service accounts"
    author: "rpsqrd"
    ms.author: "jgerend"
    ms.date: "09/10/2019"
    ms.topic: "how-to"
    ms.assetid: "9e06ad3a-0783-476b-b85c-faff7234809c"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/windowscontainers/manage-containers/gmsa-orchestrate-containers.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/windowscontainers/manage-containers/gmsa-orchestrate-containers).

# Orchestrate containers with a gMSA

> Applies to: Windows Server 2022, Windows Server 2019

In production environments, you'll often use a container orchestrator such as the hosted Kubernetes service, Azure Kubernetes Service (AKS), to deploy and manage your apps and cluster services. Each orchestrator has its own management paradigms and is responsible for accepting credential specs to give to the Windows container platform.

When you're orchestrating containers with Group Managed Service Accounts (gMSAs), make sure that:

> [[!div class="checklist"]
> * All container hosts that can be scheduled to run containers with gMSAs are domain joined
> * The container hosts have access to retrieve the passwords for all gMSAs used by containers
> * The credential spec files are created and uploaded to the orchestrator or copied to every container host, depending on how the orchestrator prefers to handle them.
> * Container networks allow the containers to communicate with the Active Directory Domain Controllers to retrieve gMSA tickets

## Use gMSA with Kubernetes

You can use gMSA with AKS and also with AKS on Azure Stack HCI, which is the on-premises implementation of the AKS orchestrator. For more information about how to use gMSA with Kubernetes, see [Use gMSA on Azure Kubernetes Service in Windows Containers|dendron://dg-msft-virtualization/msft.virtualization.windowscontainers.manage-containers.gmsa-aks-ps-module]] and [Configure group Managed Service Account with AKS on Azure Stack HCI](https://docs.microsoft.com/en-us/azure-stack/aks-hci/prepare-windows-nodes-gmsa).

To read about the latest industry information about this feature, see [Configure gMSA for Windows pods and containers](https://kubernetes.io/docs/tasks/configure-pod-container/configure-gmsa).

## Use gMSA with Service Fabric

Service Fabric supports running Windows containers with a gMSA when you specify the credential spec location in your application manifest. You'll need to create the credential spec file and place in the **CredentialSpecs** subdirectory of the Docker data directory on each host so that Service Fabric can locate it. You can run the **Get-CredentialSpec** cmdlet, part of the [CredentialSpec PowerShell module](https://aka.ms/credspec), to verify if your credential spec is in the correct location.

See [Quickstart: Deploy Windows containers to Service Fabric](https://docs.microsoft.com/en-us/azure/service-fabric/service-fabric-quickstart-containers) and [Set up gMSA for Windows containers running on Service Fabric](https://docs.microsoft.com/en-us/azure/service-fabric/service-fabric-setup-gmsa-for-windows-containers) for more information about how to configure your application.

## How to use gMSA with Docker Swarm

To use a gMSA with containers managed by Docker Swarm, run the [docker service create](https://docs.docker.com/engine/reference/commandline/service_create/) command with the `--credential-spec` parameter:

```powershell
docker service create --credential-spec "file://contoso_webapp01.json" --hostname "WebApp01" <image name>
```

See the [Docker Swarm example](https://docs.docker.com/engine/reference/commandline/service_create/#provide-credential-specs-for-managed-service-accounts-windows-only) for more information about how to use credential specs with Docker services.

## Next steps

In addition to orchestrating containers, you can also use gMSAs to:

- [[Configure apps|dendron://dg-msft-virtualization/msft.virtualization.windowscontainers.manage-containers.gmsa-configure-app]]
- [[Run containers|dendron://dg-msft-virtualization/msft.virtualization.windowscontainers.manage-containers.gmsa-run-container]]

If you run into any issues during setup, check our [[troubleshooting guide|dendron://dg-msft-virtualization/msft.virtualization.windowscontainers.manage-containers.gmsa-troubleshooting]] for possible solutions.