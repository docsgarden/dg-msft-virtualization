---
id: 32sBXjHY584Tkb92bN6e6nJ
title: >-
    Linux Integration Services 4.1.3-2
desc: >-
    Learn about version 4.1.3-2 update of the Linux Integration Services.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2017/20170310-linux-integration-services-4-1-3-2
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 458a3781e9f73e5c63d8f15d54c0f44d29c8f5d22e62297c779c2adecf53724f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2017/20170310-linux-integration-services-4-1-3-2.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2017-03-10 22:27:17"
    ms.date: "03/10/2017"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2017/20170310-linux-integration-services-4-1-3-2.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2017/20170310-linux-integration-services-4-1-3-2).

# Linux Integration Services 4.1.3-2

Linux Integration Services has been update to version 4.1.3-2 and is available from <https://www.microsoft.com/en-us/download/details.aspx?id=51612> This is a minor update to correct the RPMs for a kernel ABI change in Red Hat Enterprise Linux, CentOS, and Oracle Linux's Red Hat Compatible Kernel version 7.3. Version 3.10.0-514.10.2.el7 of the kernel was sufficiently different for symbol conflicts to break the LIS kernel modules and create a situation where a VM would not start correctly. This version of the modules is compatible with the new kernel.