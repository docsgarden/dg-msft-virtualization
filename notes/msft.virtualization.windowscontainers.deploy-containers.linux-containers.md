---
id: 69JFvVgKxPM6RGu4ZP4LuUG
title: >-
    Linux Containers on Windows 10
desc: >-
    Learn about different ways you can use Hyper-V to run Linux containers on Windows 10 as if they're native.
canonicalUrl: https://docs.microsoft.com/virtualization/windowscontainers/deploy-containers/linux-containers
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 7d3217a22f54cceb588f2577b2b10c0660207d768133a55cd94bff77ad11d8df
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/windowscontainers/deploy-containers/linux-containers.md
    keywords: "linux containers, docker, containers, windows 10"
    author: "scooley"
    ms.author: "scooley"
    ms.date: "09/17/2019"
    ms.topic: "overview"
    ms.assetid: "edfd11c8-ee99-42d8-9878-efc126fe1826"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/windowscontainers/deploy-containers/linux-containers.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/windowscontainers/deploy-containers/linux-containers).

# Linux containers on Windows 10

Linux containers make up a huge percent of the overall container ecosystem and are fundamental to both developer experiences and production environments.  Since containers share a kernel with the container host, however, running Linux containers directly on Windows isn't an option. This is where virtualization comes into the picture.

## Linux containers in a Moby VM

To run Linux containers in a Linux VM, follow the instructions in [Docker's get-started guide](https://docs.docker.com/docker-for-windows/).

Docker has been able to run Linux containers on Windows desktop since it was first released in 2016 (before Hyper-V isolation or Linux containers on Windows were available) using a [LinuxKit](https://github.com/linuxkit/linuxkit) based virtual machine running on Hyper-V.

In this model, Docker Client runs on Windows desktop but calls into Docker Daemon on the Linux VM.

![Moby VM as the container host](https://docs.microsoft.com/virtualization/windowscontainers/deploy-containers/media/MobyVM.png)

In this model, all Linux containers share a single Linux-based container host and all Linux containers:

* Share a kernel with each other and the Moby VM, but not with the Windows host.
* Have consistent storage and networking properties with Linux containers running on Linux (since they are running on a Linux VM).

It also means the Linux container host (Moby VM) needs to be running Docker Daemon and all of Docker Daemon's dependencies.

To see if you're running with Moby VM, check Hyper-V Manager for Moby VM using either the Hyper-V Manager UI or by running `Get-VM` in an elevated PowerShell window.

## Next steps

> [[!div class="nextstepaction"]
> [Set up Linux Containers|dendron://dg-msft-virtualization/msft.virtualization.windowscontainers.quick-start.quick-start-windows-10-linux]]