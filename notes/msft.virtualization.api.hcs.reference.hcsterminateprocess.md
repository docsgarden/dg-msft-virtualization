---
id: 77r4YKfRCUbrk3fvU5KXsc5
title: >-
    HcsTerminateProcess
desc: >-
    HcsTerminateProcess
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsTerminateProcess
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 045739fcde120149146d5f02c1044bf689be4f9d4a37b3304dc70992fafd3f83
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsTerminateProcess.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsTerminateProcess']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsTerminateProcess.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsTerminateProcess).

# HcsTerminateProcess

## Description

Terminates a process in a compute system.

## Syntax

```cpp
HRESULT WINAPI
HcsTerminateProcess(
    _In_ HCS_PROCESS process,
    _In_ HCS_OPERATION operation,
    _In_opt_ PCWSTR options
    );
```

## Parameters

`process`

The handle to the process to terminate.

`operation`

The handle to the operation tracking the terminate operation.

`options`

Reserved for future use. Must be `NULL`.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

If the return value is `S_OK`, it means the operation started successfully. Callers are expected to get the operation's result using [[`HcsWaitForOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresultandprocessinfo]] or [[`HcsGetOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresultandprocessinfo]].


## Operation Results

The return value of [[`HcsWaitForOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresultandprocessinfo]] or [[`HcsGetOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresultandprocessinfo]] based on current operation listed as below.


| Operation Result Value | Description |
| -- | -- |
| `S_OK` | The process was terminated successfully |


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |