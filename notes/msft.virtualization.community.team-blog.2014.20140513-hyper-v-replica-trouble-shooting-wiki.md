---
id: 9Psq5BPmCPaH8qQScwm55Bv
title: >-
    Hyper-V Replica trouble-shooting wiki
desc: >-
    This article discusses the new Hyper-V Replica troubleshooting wiki.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2014/20140513-hyper-v-replica-trouble-shooting-wiki
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 25a5664a6edb242af86acbfba244708d5d12c9a1476655c3427f04d3af17cb09
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140513-hyper-v-replica-trouble-shooting-wiki.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "05/13/2014"
    date: "2014-05-13 00:18:55"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140513-hyper-v-replica-trouble-shooting-wiki.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2014/20140513-hyper-v-replica-trouble-shooting-wiki).

# Hyper-V Replica troubleshooting wiki

We are happy to announce the availability of Hyper-V Replica trouble-shooting Wiki here:

<https://social.technet.microsoft.com/wiki/contents/articles/21948.hyper-v-replica-troubleshooting-guide.aspx>

This guide contains links and resources to trouble-shoot some common Hyper-V Replica failure scenarios. We will be updating the guide over time!

We would like this to be a community effort to make it social and **_you are free to add the content_** to this guide.To add content follow this high level schema for the new articles [Please feel free to add other sections as appropriate]:

**a. Error Messages/Event Viewer details** – This section mentions what error messages customer will see on UI/PS/WMI and what event viewer messages are logged.

**b. Possible Causes** – This section explains list of scenarios(one or more) which might have led to failure. 

**c. Resolution** – This section lists down the actions admin has to take in his environment to resolve the failure

**d. Additional resources** – List of blogs/KB Articles/Documentation/other articles which contain more information for the customer about the failure.

If you are new to TechNet wiki, the guide on “ **How to contribute** ” is [here](https://social.technet.microsoft.com/wiki/contents/articles/145.wiki-how-to-contribute-content-to-technet-wiki.aspx).

Happy WIKI’ing ![Smile](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/metablogapi/wlEmoticon-smile_6C1D8731.png)