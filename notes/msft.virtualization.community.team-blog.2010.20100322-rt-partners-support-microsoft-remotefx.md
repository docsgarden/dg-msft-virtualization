---
id: 6yMjXB858UDs2jQfQFNoqCk
title: >-
    RT&#58; Partners Support Microsoft RemoteFX
desc: >-
    Partners Support Microsoft RemoteFX
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100322-rt-partners-support-microsoft-remotefx
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 23883084d811fa8ae24ca6a9a9a14713a47cbbaa45b789a687c75bc1f6f6392d
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100322-rt-partners-support-microsoft-remotefx.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "03/22/2010"
    date: "2010-03-22 17:04:00"
    categories: "calista-technologies"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100322-rt-partners-support-microsoft-remotefx.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100322-rt-partners-support-microsoft-remotefx).

# Partners Support Microsoft RemoteFX

Today Max Herrmann (who [posted on this site](https://blogs.technet.com/virtualization/archive/2010/03/17/explaining-microsoft-remotefx.aspx "Max's March 18 post") on March 18) posted a blog that calls out software and hardware partners' stated intentions to support Microsoft RemoteFX. You can read Max's post on the [RDS blog](https://techcommunity.microsoft.com/t5/security-compliance-and-identity/partners-support-microsoft-remotefx/ba-p/246982 "RDS blog on MSDN"). Here's an excerpt: 

> Today, I am very happy to confirm that the following hardware and software companies, among others, have engaged with us as RemoteFX partners with the goal to build a formidable ecosystem of first-class user experience solutions: AMD, AppliedMicro, Citrix, Cloudium Systems, Dell, DevonIT, HP, Nvidia, Quest, Texas Instruments, ThinLinx, Via, Wondermedia and Wyse.
> 
> Several of these companies have already come forward with their own RemoteFX announcements last Friday, one day after Microsoft RemoteFX was born: We have seen statements of planned support for RemoteFX from [AppliedMicro](http://investor.appliedmicro.com/phoenix.zhtml?c=78121&p=irol-newsArticle&ID=1403972&highlight=), [Citrix](https://www.citrix.com/blogs/2013/12/11/true-hardware-gpu-sharing-arrives/), [DevonIT](http://www.devonit.com/about-devon-it/devon-it-announces-plans-for-integration-of-thin-client-solutions-with-microsoft-remotefx-2), [Quest](http://www.quest.com/newsroom/news-releases-show.aspx?contentid=11210), [ThinLinx](http://www.thinlinx.com/about/about.html#about_news), [Via](http://www.via.com.tw/en/resources/pressroom/pressrelease.jsp?press_release_no=4627) and [Wyse](http://www.wyse.com/about/news/pr/2010/0319_MicrosoftRemoteFX.asp). [AMD expressed excitement about our RemoteFX announcement](http://links.amd.com/VirtualFuture) in one of their blogs, and [in a Network World article](http://www.networkworld.com/community/node/58722), HP announced future support for Microsoft RemoteFX to enhance the user multimedia experience for their thin clients. 
> 
> Jeff Groudan from HP commented that Microsoft’s recent announcements are consistent with HP’s ongoing efforts to simplify client virtualization, from the desktop-to-the-datacenter. “Integrating Microsoft RemoteFX enables future WES-based HP thin clients to deliver a richer, local-like user experience for Microsoft session-based desktop and virtual desktop infrastructure customers. By leveraging the power of virtualized graphics and advanced codecs, remote workers will be able to access any type of application or screen content, including rich media and 3D applications.”
> 
> I feel with these great companies on board, RemoteFX is heading into an exciting future. For more information on these RemoteFX partner companies, [visit our RDS partner page](https://www.microsoft.com/windowsserver2008/en/us/rds-partners.aspx). And if you want to learn more about RemoteFX, please check in often with my blog for more details as we continue to refine the technology for Windows Server 2008 R2 SP1.

 

Patrick