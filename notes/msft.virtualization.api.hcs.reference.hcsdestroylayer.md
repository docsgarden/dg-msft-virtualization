---
id: AvWFfhjVXPMoR33hkWoPN7D
title: >-
    HcsDestroyLayer
desc: >-
    HcsDestroyLayer
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsDestroyLayer
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: c231e2dbd3c851b1840dfabb9f7ff868bdbb2122e3905da58deb355a0c6dacc0
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsDestroyLayer.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsDestroyLayer']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsDestroyLayer.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsDestroyLayer).

# HcsDestroyLayer

## Description

Deletes a layer from the host.

## Syntax

```cpp
HRESULT WINAPI
HcsDestroyLayer(
    _In_ PCWSTR layerPath
    );
```

## Parameters

`layerPath`

Path of the layer to delete.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Remarks

**Be careful when using this API, it deletes directories using high privilege rights.**
This function deletes a layer from the host.

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeStorage.h |
| **Library** | ComputeStorage.lib |
| **Dll** | ComputeStorage.dll |