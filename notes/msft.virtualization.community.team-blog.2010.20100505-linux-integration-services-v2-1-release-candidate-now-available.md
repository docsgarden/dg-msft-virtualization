---
id: 3sraz5urvsdUhuBFJy7wDai
title: >-
    Linux Integration Services v2.1 Release Candidate Now Available
desc: >-
    The release candidate (RC) version of the Linux Integration Services for Microsoft Hyper-V.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100505-linux-integration-services-v2-1-release-candidate-now-available
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 67a6d18399ca84f24d14656296c84e97adea4ceeeb55c088dfc61bf06eca83a5
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100505-linux-integration-services-v2-1-release-candidate-now-available.md
    author: "mabriggs"
    ms.author: "mabrigg"
    date: "2010-05-05 11:15:00"
    ms.date: "05/05/2010"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100505-linux-integration-services-v2-1-release-candidate-now-available.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100505-linux-integration-services-v2-1-release-candidate-now-available).

# "Linux Integration Services v2.1 Release Candidate Now Available"

In March, we had announced the beta release of the Linux Integration Services for Microsoft Hyper-V, which added support for SMP-based virtual machines, timesync, and integrated shutdown. Today we're announcing the release candidate (RC) version of the integration services.

In addition to the features that were present in the beta release, the following features have been added for this release candidate:

·         **Heartbeat:** This provides a way for the host to detect if the guest is still running and responsive. **

**

·          **Pluggable Time Source:** A pluggable clock source module is included for a more accurate time source to the guest.

 **

** 

This version of the integration services for Hyper-V can be downloaded from [here](https://connect.microsoft.com/InvitationUse.aspx?ProgramID=1863&InvitationID=LNIS-T47Q-B7MP&SiteID=495), and supports Novell SUSE Linux Enterprise Server 10 SP3, SUSE Linux Enterprise Server 11, and Red Hat Enterprise Linux 5.2 / 5.3 / 5.4 / 5.5.