---
id: 8wFzApQgRUbziziita4tRRr
title: >-
    R2 Veeam Too
desc: >-
    Veeam Software is committed to fully supporting Microsoft Windows Server 2008 R2 Hyper-V and Microsoft Hyper-V Server 2008 R2
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090723-r2-veeam-too
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 56bf2ff4bbb7478362fb63fd3f2516f0d1dbb50825bef2e46958f66433d4651e
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090723-r2-veeam-too.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2009-07-23 18:36:00"
    ms.date: "07/23/2009"
    categories: "guest-blog-post"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090723-r2-veeam-too.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090723-r2-veeam-too).

# Veeam's Committed Support 

Hello, I’m Doug Hazelman and I’m the director of the Global Systems Engineers Group at Veeam Software. I’ve been with Veeam for almost two years, and prior to that I was with Aelita Software (which was acquired by Quest Software) where I worked with the same management team now behind Veeam Software. I’d like to thank Microsoft for this opportunity to guest blog on Veeam’s direction around Hyper-V. 

 

Veeam’s tagline is “listening to you, building the tools you need.”  We’ve heard your requests and we’d like to announce now that Veeam is committed to fully supporting Microsoft Windows Server 2008 R2 Hyper-V and Microsoft Hyper-V Server 2008 R2. While Veeam has continued to build some of the best software for data protection and management of VMware infrastructures, we realize that customers are now faced with more virtualization choices. By fully supporting Microsoft Hyper-V and VMware vSphere, Veeam can help you manage heterogeneous hypervisor deployments and clouds with the innovative solutions you’ve come to expect from Veeam. The management and R&D teams at Veeam have a long history of working with Microsoft going back to the Aelita days, and we’re all excited to be working with Microsoft again. 

 

The Veeam engineers are currently focusing a lot of time and energy on how we can provide the best support for Hyper-V. Look for more information in the coming weeks as we disclose more about Veeam’s plans for Hyper-V. And congratulations to Microsoft for releasing R2.

 

Thanks,

 

-doug

Blog: [http://www.VeeamMeUp.com](http://www.veeammeup.com/)

Twitter: @VMDoug or @Veeam