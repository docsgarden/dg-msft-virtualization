---
id: 5JmNbXywYq7GGpfzko6pSvG
title: >-
    TechEd North America 2014
desc: >-
    This article shares details about TechEd North America 2014.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2014/20140508-teched-north-america-2014
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: ad745dbfa020e4a93025ef49db30519fd51d0d4735e8fee31d496eedd5296cc7
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140508-teched-north-america-2014.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "05/08/2014"
    date: "2014-05-08 22:02:00"
    categories: "disaster-recovery"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140508-teched-north-america-2014.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2014/20140508-teched-north-america-2014).

# TechEd North America 2014
This year [TechEd North America](http://northamerica.msteched.com) is happening between 12 May and 15 May, at Houston. There are some interesting sessions around Backup and Disaster Recovery – so I would highly encourage you all to attend these sessions and interact with the folks presenting.

#### Sessions on May 12![May 12th sessions](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/metablogapi/image_thumb_11F22698.png)](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/metablogapi/image_4DAF2E9A.png)

![May 12 sessions](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/metablogapi/image_thumb_6C1E90E5.png)](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/metablogapi/image_6811B262.png)

#### Sessions on May 15

![May 15th session](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/metablogapi/image_thumb_36AAF022.png)](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/metablogapi/image_24CBCBA4.png)

#### ![2014 TechEd sessions](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/metablogapi/image_thumb_3EC4AD28.png)](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/metablogapi/image_0853FB26.png)

![Sessions at TechEd 2014](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/metablogapi/image_thumb_6DAC806E.png)](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/metablogapi/image_2DE00938.png)

 

Looking forward to seeing you all there!