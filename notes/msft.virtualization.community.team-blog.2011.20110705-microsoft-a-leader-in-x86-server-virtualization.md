---
id: 4TqzyvTJURC97RCDes2QmaA
title: >-
    Microsoft a Leader in x86 Server Virtualization
desc: >-
    Microsoft a Leader in x86 Server Virtualization
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110705-microsoft-a-leader-in-x86-server-virtualization
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: dcce1bed05147c28b945b216eb9e0706d2c0923c934aba4a9890490c4c7c0ddc
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110705-microsoft-a-leader-in-x86-server-virtualization.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "07/05/2011"
    date: "2011-07-05 19:27:59"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110705-microsoft-a-leader-in-x86-server-virtualization.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110705-microsoft-a-leader-in-x86-server-virtualization).

# Microsoft a Leader in x86 Server Virtualization

Great news to share with our virtualization blog readers - Gartner published the 2011 Magic Quadrant for x86 Server Virtualization Infrastructure, in which Microsoft is listed among the leaders, along with Citrix and VMware.  Microsoft’s Brad Anderson, corporate vice president of the Management & Security Division, discusses how Gartner’s results are changing the conversation with customers on the Cloud Power Blog. [Click here to read more!](http://bit.ly/ktLmpV)