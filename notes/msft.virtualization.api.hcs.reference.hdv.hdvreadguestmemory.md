---
id: 95273PXyRcCTrxtmG4nBgMm
title: >-
    HdvReadGuestMemory
desc: >-
    HdvReadGuestMemory
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvReadGuestMemory
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: f485e0594bdb5962e9924451d17226aad9de7297645f992b722e337de9a3797a
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvReadGuestMemory.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HdvReadGuestMemory']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvReadGuestMemory.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvReadGuestMemory).

# HdvReadGuestMemory

## Description

Reads guest primary memory (RAM) contents into the supplied buffer.

## Syntax

```C++
HRESULT WINAPI
HdvReadGuestMemory(
    _In_                    HDV_DEVICE Requestor,
    _In_                    UINT64     GuestPhysicalAddress,
    _In_                    UINT32     ByteCount,
    _Out_writes_(ByteCount) BYTE*      Buffer
    );
```

## Parameters

|Parameter|Description|
|---|---|---|---|---|---|---|---|
|`Requestor` |Handle to the device requesting memory access.|
|`GuestPhysicalAddress`|Guest physical address at which the read operation starts.|
|`ByteCount`|Number of bytes to read.|
|`Buffer`|Target buffer for the read operation. |
|    |    |

## Return Values

|Return Value     |Description|
|---|---|
|`S_OK` | Returned if function succeeds.|
|`HRESULT` | An error code is returned if the function fails.
|     |     |

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |