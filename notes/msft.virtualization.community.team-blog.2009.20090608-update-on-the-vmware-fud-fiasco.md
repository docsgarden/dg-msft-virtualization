---
id: 9fkWCpmZNMvo22bFwsFXW76
title: >-
    Update on the VMware FUD Fiasco.
desc: >-
    If you've been following the VMware FUD fiasco, I just wanted to provide a quick update.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090608-update-on-the-vmware-fud-fiasco
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: d8a2d26399a12ad98e8cfe95ce93e0079f2b77392b24812065304a1387ca7a8a
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090608-update-on-the-vmware-fud-fiasco.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-06-08 01:20:00"
    ms.date: "06/08/2009"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090608-update-on-the-vmware-fud-fiasco.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090608-update-on-the-vmware-fud-fiasco).

# Update on the VMware FUD Fiasco

Virtualization Nation, If you've been following the VMware FUD fiasco, I just wanted to provide a quick update. In the last post, we finally reached a point where VMware provided some basic information about their configuration and we immediately began working to reproduce the test. As I mentioned in the last blog, t **he Hyper-V team is fanatical about the stability of Hyper-V. If there's an issue, we're not going to hide. We will address the issue and provide a fix. Period.** **We are 100% customer focused and committed to world class support.** We've still got some more work to do, but I told you that I'd provide an update in early June so I want to do just that. **Testing Tidbit** On the testing front, I don't want to deliver incomplete information, but I'll give you this tidibt. We haven't found any Hyper-V crashes and we have found problems with VMware's tests. **The VMware Ask** Also in the last blog, we provided VMware the opportunity to resolve this matter and demonstrate partnership. Specifically, 

> **_To Bruce Herndon/VMware_**

> _In your blog you state that this issue "shows little sign of abating." This statement implies that you're interested in resolving this matter._

> _Finally, something we can agree on._

> **_If you indeed want to resolve this matter, then:_**

> 1. **_Take down the video._**
> 2. **_Send us a crashdump so we can take a look sooner rather than later. Send the crashdump to your Microsoft Technical Account Manager (TAM). If you need help, let us know and we'll follow-up._**
> 3. **_Pledge this won't happen again._**



Unfortunately, VMware has done none of the above and remained silent. Since we're going to spend a few more weeks testing, we'd like to again provide VMware the opportunity to take us up on this offer and demonstrate partnership. **Speaking of Partnership.** In the meantime, this seems like a great opportunity to mention a few of the things we've done to help our partners develop for both Windows and Hyper-V. I'll cover that in my next blog. Cheers, _Jeff Woolsey_ _Principal Group Program Manager_

_Windows Server, Hyper-V_