---
id: 9C4kFKUcS5f3ELeZRsdz9nr
title: >-
    Live Meeting for Influencers, 1.12.10 at 10&#58;00 am PST&#58; Moving from VMware to Microsoft
desc: >-
    Live Meeting for Influencers, 1.12.10 at 8;00 am PST; Moving from VMware to Microsoft
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100105-live-meeting-for-influencers-1-12-10-at-10-00-am-pst-moving-from-vmware-to-microsoft
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 59f2efb71aeac23934d684811434c327da6374b7d74bbfa20f1fdcb450b21fa7
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100105-live-meeting-for-influencers-1-12-10-at-10-00-am-pst-moving-from-vmware-to-microsoft.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "01/05/2010"
    date: "2010-01-05 18:19:08"
    categories: "community"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100105-live-meeting-for-influencers-1-12-10-at-10-00-am-pst-moving-from-vmware-to-microsoft.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100105-live-meeting-for-influencers-1-12-10-at-10-00-am-pst-moving-from-vmware-to-microsoft).

# Live Meeting for Influencers, 1.12.10 at 8:00 am PST; Moving from VMware to Microsoft

On **Tuesday, January 12, 2010 10:00 AM-11:00 AM.,** we are hosting a Live Meeting presentation for System Center influencers entitled, “So you Want To Move from a VMware Infrastructure to Microsoft,” presented by Kenon Owens, Microsoft Virtualization product manager.

Please note: 

  * This Live Meeting presentation is exclusive to members of the **System Center** **Influencers Program**.   Not a member?  Visit [the program information page on TechNet](https://technet.microsoft.com/ee532416.aspx) to learn more and apply to join. 
  * [Join information is available on the System Center Influencers Program portal](https://sharepoint.connect.microsoft.com/SystemCenterCommunity/Lists/Announcements/DispForm.aspx?ID=18&Source=https%3A%2F%2Fsharepoint%2Econnect%2Emicrosoft%2Ecom%2FSystemCenterCommunity%2FLists%2FAnnouncements%2FAllItems%2Easpx).  Members of the program will need to log into the Connect site to access the Live Meeting join information. 



> **Abstract:**
> 
> Many customers are making the decision every ** __** day to move away from their high-priced VMware Infrastructure to a dynamic infrastructure using Microsoft’s virtualization technology. They know the quality and capabilities of Microsoft Virtualization give them the platform they need, they just don’t know how to do it. There is a way to migrate from VMware, and a coexistence strategy during the transition is essential. Hear Microsoft’s vision and guidance when moving from a VMware infrastructure to Microsoft Virtualization, and in a real-time demo, watch how Microsoft can assist you in managing both your VMware and Microsoft environments. This session is also designed to hear your feedback and get your ideas on this popular topic.

\- dave //