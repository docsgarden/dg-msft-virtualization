---
id: oNbDsZXoDxg2uhH3pHgiCHG
title: >-
    Uncle TechEd Wants You!
desc: >-
    Uncle TechEd Wants You!
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110506-uncle-teched-wants-you
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 15e25e7552c4287ddfd9d1e96c3b0402bc0806ac326ea354be864309d138c7e0
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110506-uncle-teched-wants-you.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "05/06/2011"
    date: "2011-05-06 16:08:00"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110506-uncle-teched-wants-you.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110506-uncle-teched-wants-you).

# Uncle TechEd Wants You!

The Microsoft TechEd Twitter Army is looking for recruits to twitter off the show floor during our premier TechEd event running May 16-19 in Atlanta. If you’re going to the show and want your opinions heard, be sure to check in at the Social Media area in the Microsoft Server & Cloud Platform Booth on Monday afternoon at 12:30 pm.  Recruits who do well on the Twitter front lines will **compete for an Xbox 360 + Kinnect package and other prizes** to be handed out at a private Twitter Army event happening Thursday at 2 pm. Those that attend the 12:30pm Tweetup will also receive a special Twitter Army badge!   Don’t forget, and remember that Uncle TechEd Wants You!