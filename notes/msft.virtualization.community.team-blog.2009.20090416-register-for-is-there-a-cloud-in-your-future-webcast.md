---
id: fZGBGsqnfQ6Cr8z7tUtiQ6m
title: >-
    Register for 'Is There a Cloud in Your Future?' Webcast
desc: >-
    On Tuesday, April 28 at 11:00am PDT, Microsoft will continue its webcast series with 'Is There a Cloud in Your Future?'.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090416-register-for-is-there-a-cloud-in-your-future-webcast
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 52ef3d2f6b5aa965b8eeb9d811fdc9f860ec11792de2fe13cf0801a5b6a698d3
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090416-register-for-is-there-a-cloud-in-your-future-webcast.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-04-16 07:30:00"
    ms.date: "04/16/2009"
    categories: "cloud-computing"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090416-register-for-is-there-a-cloud-in-your-future-webcast.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090416-register-for-is-there-a-cloud-in-your-future-webcast).

# Register for 'Is There a Cloud in Your Future?' Webcast

On Tuesday, April 28 at 11:00am PDT, Microsoft will continue its webcast series on 'How to Invest in the IT Infrastructure You Have While Preparing for Tomorrow' with [Part 2](http://www2.eventsvc.com/academylive/register/c9fee1c9-b5ce-4349-a3f6-82858e358aa5): **"Is There a Cloud in Your Future?"**

Register [here](http://www2.eventsvc.com/academylive/register/c9fee1c9-b5ce-4349-a3f6-82858e358aa5)

 

Join Zane Adam, Microsoft Senior Director of System Center and Virtualization, Barry Briggs, Microsoft CTO for IT, and Chris Steffen, Principle Technical Architect, Kroll Factual Data, to discuss solutions and best practices that help you secure and manage IT environments today, while also looking to the future and integrate new and emerging IT models such as cloud computing. 

 

 **Part 1: "One Server Does Not Fit All"** is available for on-demand viewing  [here](https://www107.livemeeting.com/cc/msevents/view?id=CAL041409)

_Bill Laing, Microsoft CVP Windows Server Division, Al Gillen, program vice president, IDC, and a Microsoft Customer present best practices to keep your costs down during this economic downturn and solutions that help you both choose and make the most of your server._