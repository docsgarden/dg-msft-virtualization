---
id: AcA6QgWCbpKLf3DCNYkZrEg
title: >-
    LiveMeeting on Microsoft’s Integrated Virtualization Strategy&#58; Thursday, Nov. 19, 8&#58;00 AM PST
desc: >-
    LiveMeeting on Microsoft’s Integrated Virtualization Strategy; Thursday, Nov. 19, 8;00 AM PST
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20091117-livemeeting-on-microsofts-integrated-virtualization-strategy-thursday-nov-19-8-00-am-pst
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: f5ad2a0370047a5a623c90f8ed370ad4c0c399d127b32a2f2397394961c04cf9
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091117-livemeeting-on-microsofts-integrated-virtualization-strategy-thursday-nov-19-8-00-am-pst.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "11/17/2009"
    date: "2009-11-17 14:44:48"
    categories: "cloud-computing"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091117-livemeeting-on-microsofts-integrated-virtualization-strategy-thursday-nov-19-8-00-am-pst.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20091117-livemeeting-on-microsofts-integrated-virtualization-strategy-thursday-nov-19-8-00-am-pst).

# LiveMeeting on Microsoft’s Integrated Virtualization Strategy; Thursday, Nov. 19, 8:00 AM PST

Join us **Thursday, November 19, 8:00 AM PST** for a LiveMeeting session on  “Microsoft’s Integrated Virtualization Strategy, Products and Solutions” presented by Kenon Owens, Technical Product Manager. This LiveMeeting is exclusive to members of the _System Center Influencers Program_ (which is also open to virtualization enthusiasts).   Not a member?  Visit the [program overview on TechNet](https://technet.microsoft.com/ee532416.aspx) to learn more about the program and how to join. 

[Join Information Available on Influencer Portal](https://sharepoint.connect.microsoft.com/SystemCenterCommunity/Lists/Announcements/DispForm.aspx?ID=15&Source=https%3A%2F%2Fsharepoint%2Econnect%2Emicrosoft%2Ecom%2FSystemCenterCommunity%2FOffers%2Easpx).  Members of the program will need to log into the Connect site to access the LiveMeeting join information. 

> **Abstract:** Through this 200 Level presentation, learn about the Microsoft virtualization strategy from the client, to the datacenter, to the cloud--and how it will help you cut costs and build value. In this session we review Microsoft virtualization products and discuss how you can use them to solve today's IT issues (cost cutting, consolidation, business continuity, green IT), develop new computing solutions (VDI) and build a foundation for a more dynamic IT environment, including cloud computing. This session reviews all of the latest Microsoft virtualization products, including Application Virtualization (App-V), Microsoft Enterprise Desktop Virtualization (MED-V), Windows Server 2008 with Hyper-V, and Microsoft Hyper-V Server, as well as the System Center management platform (including Virtual Machine Manager 2008). Learn about the innovative pricing and licensing structure that allows further savings to lower both acquisition and ongoing ownership costs. Learn how you can enable IT to become a cost cutting mechanism with Microsoft virtualization and management technologies.

\- dave //