---
id: AeXLj5vW8hANLVmxBhTb5jX
title: >-
    WHV_EMULATOR_SET_VIRTUAL_PROCESSOR_REGISTERS_CALLBACK method
desc: >-
    Learn about the WHV_EMULATOR_SET_VIRTUAL_PROCESSOR_REGISTERS_CALLBACK method.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorSetVirtualProcessorRegistersCallback
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 293cef371b56d08e24f3b5f59772b78f0fed7151ef4dda45a53fe7df9acc3414
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorSetVirtualProcessorRegistersCallback.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/19/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorSetVirtualProcessorRegistersCallback.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorSetVirtualProcessorRegistersCallback).

# WHV_EMULATOR_SET_VIRTUAL_PROCESSOR_REGISTERS_CALLBACK


## Syntax

```c
typedef HRESULT (CALLBACK *WHV_EMULATOR_SET_VIRTUAL_PROCESSOR_REGISTERS_CALLBACK)(
    _In_ VOID* Context,
    _In_reads_(RegisterCount) const WHV_REGISTER_NAME* RegisterNames,
    _In_ UINT32 RegisterCount,
    _In_reads_(RegisterCount) const WHV_REGISTER_VALUE* RegisterValues
    );
```

## Remarks
Callback setting VP register state, similar to the WinHv API. This will only
be called right before a successful emulation call is about to return.