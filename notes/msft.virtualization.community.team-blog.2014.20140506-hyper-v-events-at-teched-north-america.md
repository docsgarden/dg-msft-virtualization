---
id: ARNMbKFL6hD9BmurLKb9BKw
title: >-
    Hyper-V events at TechEd North America
desc: >-
    This article shares the Hyper-V team's events at TechEd.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2014/20140506-hyper-v-events-at-teched-north-america
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 0dcbf5555e1fe0c3183081af7d403eb76f619255e60f3ff3d918d55ff8c58dea
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140506-hyper-v-events-at-teched-north-america.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "05/06/2014"
    date: "2014-05-06 13:57:48"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140506-hyper-v-events-at-teched-north-america.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2014/20140506-hyper-v-events-at-teched-north-america).

# The Hyper-V Team at TechEd

I'm excited to report we, the Hyper-V team, will have a record high presence this year at TechEd North America in Houston.  Come join us at the Hyper-V booth and for our Hyper-V sessions. **Sessions:** **Monday:** 11:00 - 12:00 FDN06 Transform the Datacenter: Making the Promise of Connected Clouds a Reality    
Speaker(s): Brian Hillger, Elden Christensen, Jeff Woolsey, Jeffrey Snover, Matt McSpirit 1:15 - 2:30  DCIM-B319 Building a Backup Strategy for Your Private Cloud  
Speaker(s): Doug Hazelman, Michael Jones, Shivam Garg, Taylor Brown, Vineeth Karinta 4:45 - 6:00  DCIM-B378 Converged Networking for Windows Server 2012 R2 Hyper-V  
Speaker(s): Don Stanwyck, Taylor Brown **Tuesday:** 8:30 - 9:45   DCIM-B379 Using VMware? The Advantages of Microsoft Cloud Fundamentals with Virtualization   
Speaker(s): Jeff Woolsey, Matt McSpirit **Wednesday:** 5:00 - 6:15   DCIM-B380 What’s New in Windows Server 2012 R2 Hyper-V   
Speaker(s): Jeff Woolsey **Thursday:** 2:45 - 4:00   DCIM-B219 Secure Design and Best Practices for Your Private Cloud  
Speaker(s): Patrick Lang, Sam Chandrashekar **Booth Info:** The Hyper-V booth will be in the center of the Expo floor with the Server and Cloud Tools booth block.   Come find us when you have a chance. I'll post bios for the Hyper-V attendees shortly. 

Cheers,  
Sarah