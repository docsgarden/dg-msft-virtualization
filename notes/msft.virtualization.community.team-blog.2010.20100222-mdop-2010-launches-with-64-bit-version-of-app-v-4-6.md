---
id: 7qH4iurLVmqM92vrdTEb3X6
title: >-
    MDOP 2010 Launches, with 64-bit version of App-V 4.6
desc: >-
    MDOP 2010 Launches, with 64-bit version of App-V 4.6
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100222-mdop-2010-launches-with-64-bit-version-of-app-v-4-6
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 6686cdce3b9e7e70fd8c8eaa118ed003feecfff08de5a86f5c18bf222c0b8f1f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100222-mdop-2010-launches-with-64-bit-version-of-app-v-4-6.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "02/22/2010"
    date: "2010-02-22 21:09:00"
    categories: "application-virtualization"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100222-mdop-2010-launches-with-64-bit-version-of-app-v-4-6.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100222-mdop-2010-launches-with-64-bit-version-of-app-v-4-6).

# MDOP 2010 Launches, with 64-bit version of App-V 4.6

Earlier today we [announced](http://windowsteamblog.com/blogs/business/default.aspx "MDOP blog post") important updates for IT pros considering Windows 7 deployment. Specifically, we released **Microsoft Desktop Optimization Pack (MDOP) 2010** , which includes **App-V 4.6** , supporting Windows Server 2008 R2 x64 for RDS and Office 2010. The App-V support for Office 2010 means you can do a one-touch deployment and don't have to wait for Office 2010 to deploy Windows 7.

 

Additionally we released the **MED-V 1.0 SP1 Release Candidate** , which creates managed virtual machines running previous versions of Windows and Internet Explorer so users can upgrade to Windows 7. The RTM version of MED-V 1.0 service pack 1 will be available in April, but you can start today by downloading the release candidate.

 

For more information about App-V and MED-V, check out the following blogs.

  * [Springboard site](http://windowsteamblog.com/blogs/springboard/default.aspx)

  * Karri offers some details of App-V 4.6 on the [MDOP team blog](https://blogs.technet.com/mdop/archive/2010/02/19/app-v-4-6-and-med-v-1-0-sp1-rc-are-here.aspx "MDOP team blog post") 

  * Ment Van Der Plas talks [App-V deployment with System Center Configuration Manager](https://docs.microsoft.com/en-us/mem/configmgr/apps/get-started/deploying-app-v-virtual-applications#:~:text=App%2DV%20virtual%20environments%20in,next%20evaluate%20their%20installed%20applications.) and [Sequencing Office 2010](http://www.softgridblog.com/).

  * Tim Managan talks about App-V and Windows7 integration on his [blog](http://www.tmurgent.com/TmBlog/?p=151).

  * Ruben Spruijt, App-V MVP, describes the power of App-V’s Shared Cache in his [blog](http://www.brianmadden.com/blog/rubenspruijt). 

  * Kevin Kaminskis discusses Sequencing 64-bit apps on his [blog](http://myitforum.com/cs2/blogs/kkaminski/default.aspx).

  * Rodney Medina talks about how App-V is helping his customers deploy Windows 7 in his [blog](http://www.softgridblog.com/)




 

Patrick O'Rourke