---
id: 4s8f4Rmi4mq8TRarhcLge2A
title: >-
    PSA - VMs with saved state from builds 10560-10567 will not restore on 10568 and above
desc: >-
    Details about how virtual machines with save state builds might not restore if they were created on Windows 8/1 or Windows 10 hosts.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2015/20151030-psa-vms-with-saved-state-from-builds-10560-10567-will-not-restore-on-10568-and-above
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 4c8aba7d819651aa0bb58b09456c9375456d447ea6060d8494698000d04d9da9
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2015/20151030-psa-vms-with-saved-state-from-builds-10560-10567-will-not-restore-on-10568-and-above.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2015-10-30 11:30:00"
    ms.date: "10/30/2015"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2015/20151030-psa-vms-with-saved-state-from-builds-10560-10567-will-not-restore-on-10568-and-above.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2015/20151030-psa-vms-with-saved-state-from-builds-10560-10567-will-not-restore-on-10568-and-above).

# PSA - VMs with saved state from builds 10560-10567 will not restore on 10568 and above

Windows Insiders might hit a bug if they try to restore a VM with recent saved state. This is especially true for VMs created on Windows 8.1 and Windows 10 hosts. We recommend that you take action before upgrading past 10567.

 **Problem**

Windows Insiders will not be able to restore VMs on build 10568 and above if the following conditions are met:

  1. The VM is configuration version 5 or 6.2. All other configuration versions are OK. Versions 5 and 6.2 are typically created on Windows 8.1/10 hosts. (You can check the configuration version by looking at the “Configuration Version” column in Hyper-V Manager.)
  2. The VM has saved state which was generated from builds 10560 through 10567



**Workaround**

If you haven’t upgraded past 10567, we recommend that you shut down your VMs before upgrading. You will then be able to start your VMs as usual after the upgrade.  If the upgrade is already complete, you must delete the saved state before starting the VM.

 **Note: Microsoft Emulator for Windows 10 Mobile**

If you use the Emulator for Windows 10 Mobile and are affected by this bug, we recommend that you delete the affected VMs and let Visual Studio recreate them on next launch.

Blog brought to you by 

Theo Thompson