---
id: 6QmB7Bg6ve5UWSEmx8ev25e
title: >-
    HCN_NAMESPACE
desc: >-
    HCN_NAMESPACE
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_NAMESPACE
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 31dcc746ce1ef078f7913307bdd5f9ac76be6d21c241a7dc577ca10f4c942323
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_NAMESPACE.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HCN_NAMESPACE']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_NAMESPACE.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_NAMESPACE).

# HCN_NAMESPACE

## Description

Context handle referencing a Namespace in HNS.


## Syntax

```cpp
typedef void* HCN_NAMESPACE;
```


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |