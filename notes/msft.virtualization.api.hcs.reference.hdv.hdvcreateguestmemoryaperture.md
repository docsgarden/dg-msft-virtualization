---
id: 6bNBwVq5zcunQGCGXmNgEvn
title: >-
    HdvCreateGuestMemoryAperture
desc: >-
    HdvCreateGuestMemoryAperture
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvCreateGuestMemoryAperture
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: f51d8f8e5ac0071c332ff41689539bc149bff12d96e7c30cb4054fc9a0afcfd9
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvCreateGuestMemoryAperture.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HdvCreateGuestMemoryAperture']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvCreateGuestMemoryAperture.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvCreateGuestMemoryAperture).

# HdvCreateGuestMemoryAperture

## Description

Creates a guest RAM aperture into the address space of the calling process.

## Syntax

```C++
HRESULT WINAPI
HdvCreateGuestMemoryAperture(
    _In_  HDV_DEVICE Requestor,
    _In_  UINT64     GuestPhysicalAddress,
    _In_  UINT32     ByteCount,
    _In_  BOOL       WriteProtected,
    _Out_ void**     MappedAddress
    );
```

## Parameters

|Parameter|Description|
|---|---|---|---|---|---|---|---|
|`Requestor` | Handle to the device requesting memory access.|
|`GuestPhysicalAddress` | Base physical address at which the aperture starts.|
|`ByteCount` | Size of the aperture in bytes.|
|`WriteProtected` | If TRUE, the process is only granted read access to the mapped memory.|
|`MappedAddress` | Receives the virtual address (in the calling process) at which the requested guest memory region has been mapped.|
|    |    |

## Return Values

|Return Value     |Description|
|---|---|
|`S_OK` | Returned if function succeeds.|
|`HRESULT` | An error code is returned if the function fails.
|     |     |

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |