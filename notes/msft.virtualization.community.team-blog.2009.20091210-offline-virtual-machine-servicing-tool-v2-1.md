---
id: 8QaHFpNbbfH9Tjrmqvgvzi9
title: >-
    Offline Virtual Machine Servicing Tool v2.1
desc: >-
    Offline Virtual Machine Servicing Tool v2.1
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20091210-offline-virtual-machine-servicing-tool-v2-1
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 0a2f61486d0f5f6bc236995bfd33dd3d9d80d4a6967a60c8726c7f28ee81bf7b
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091210-offline-virtual-machine-servicing-tool-v2-1.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "12/10/2009"
    date: "2009-12-10 07:20:00"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091210-offline-virtual-machine-servicing-tool-v2-1.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20091210-offline-virtual-machine-servicing-tool-v2-1).

# Offline Virtual Machine Servicing Tool v2.1

Virtualization affects how we plan, build, deploy, operate, and _service_ workloads.   Customers are creating large libraries of virtual machines containing various configurations.  The patch-state of these virtual machines are not always known.  Ensuring that offline virtual machines are properly patched and won’t become vulnerable the instant they come online is critical. 

 

I am therefore very pleased to state that the Offline Virtual Machine Servicing Tool v2.1 has now been released! 

 

Congratulations to the Solution Accelerator team for this release! 

 

The **[Offline Virtual Machine Servicing Tool 2.1](https://technet.microsoft.com/library/cc501231.aspx)** has free, tested guidance and automated tools to help customers keep their virtualized machines updated, without introducing vulnerabilities into their IT infrastructure.   

 

The tool combines the Windows Workflow programming model with the Windows PowerShell interface to automatically bring groups of virtual machines online, service them with the latest security updates, and return them to an offline state. 

 

 **What ’s New?

**

Release 2.1 is a direct response to customer and Microsoft field requests to support the R2 wave.  Offline Virtual Machine Servicing Tool 2.1 now supports the following products:

·        Hyper-V-R2

·        VMM 2008 R2

·        SCCM 2007 SP2

·        WSUS 3.0 SP2 

·        OVMST 2.1 also supports updates to Windows 7 and Windows Server 2008 R2 virtual machines.   

 

Bryon Surace

Senior Program Manager

Hyper-V