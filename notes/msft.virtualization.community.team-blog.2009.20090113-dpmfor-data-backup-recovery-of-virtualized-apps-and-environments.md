---
id: 8MeL5GWZ96R5SfvJcfBwAXr
title: >-
    DPM for data backup/recovery of virtualized apps and environments
desc: >-
    Congratulations to the Microsoft Storage Solutions team for releasing Service Pack 1 for System Center Data Protection Manager 2007.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090113-dpmfor-data-backup-recovery-of-virtualized-apps-and-environments
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: fdcb89a4226f30d33aed57fda9c861d474daef390fcdcdf645ea512af75b10c0
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090113-dpmfor-data-backup-recovery-of-virtualized-apps-and-environments.md
    keywords: "virtualization, virtual server, virtual pc, blog"
    author: "scooley"
    ms.author: "scooley"
    ms.date: "1/13/2009"
    ms.topic: "article"
    ms.assetid: "None"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090113-dpmfor-data-backup-recovery-of-virtualized-apps-and-environments.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090113-dpmfor-data-backup-recovery-of-virtualized-apps-and-environments).

# DPM for data backup/recovery of virtualized apps and environments

We want to congratulate the Microsoft Storage Solutions team for releasing Service Pack 1 for System Center Data Protection Manager 2007.

SP1 for DPM 2007 brings some great new capabilities for protecting Hyper-V environments.  Most notably, of course, is the ability to protect guests within Hyper-V environments, often without downtime (for those guests running a Windows operating system that supports VSS).  Also new for DPM with SP1 is the ability to run the DPM server on the Hyper-V host itself, so that the DPM server can protect the guests from the host viewpoint, within the same physical server - to disk, to tape and even to the cloud. 

And unlike other (shall-not-be-named) virtualization platforms’ backup mechanisms, DPM does _not_ require a SAN and does _not_ require 3rd party backup software or add-ons.   It’s an all Microsoft backup and recovery solution for Microsoft’s virtualization platform.

For more details on the SP1 release for DPM 2007, check out:

·         Bala’s executive [viewpoint on DPM 2007 sp1](https://blogs.technet.com/dpm/archive/2009/01/13/announcing-service-pack-1-for-dpm-2007.aspx)

·         Some early SP1 customers are also blogging about their experiences and you can see at these links – [ICON](https://blogs.technet.com/dpm/archive/2009/01/13/customer-blog-post-on-DPM-2007-SP1-ICON.aspx) & [Convergent Computing](https://blogs.technet.com/dpm/archive/2009/01/13/customer-blog-post-on-DPM-2007-SP1-RAND.aspx)

·         A feature overview of [DPM SP1 is located here](https://blogs.technet.com/dpm/archive/2009/01/13/Service-Pack-1-is-for-you.aspx)

The DPM folks have dedicated resources on “ ** _How to protect virtualized environments with DPM 2007_** ” – [https://www.microsoft.com/DPM/virtualization](https://www.microsoft.com/DPM/virtualization). And a very cool podcast showing how to protect Hyper-V with DPM 2007 SP1 - [https://edge.technet.com/Media/DPM-2007-SP1-Protecting-Hyper-V](https://edge.technet.com/Media/DPM-2007-SP1-Protecting-Hyper-V)

Congrats to the DPM crew – and nice job on backing up Hyper-V !!!

Patrick