---
id: EYWfvbHMe5iRhQQY5yCwN2z
title: >-
    Series on virtualizing SharePoint
desc: >-
    The Microsoft-UK services team recently posted an in-depth virtualizing SharePoint series.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090406-series-on-virtualizing-sharepoint
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 026b68c3d305d336db522d097800297cec2fe55b7f8021748c62b261ad7df50b
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090406-series-on-virtualizing-sharepoint.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-04-06 14:20:00"
    ms.date: "04/06/2009"
    categories: "disaster-recovery"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090406-series-on-virtualizing-sharepoint.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090406-series-on-virtualizing-sharepoint).

# Series on virtualizing SharePoint

The Microsoft-UK services team recently posted an in-depth virtualizing SharePoint series. It is a culmination of their experiences over the last couple years helping customers successfully host SharePoint in virtualized environments. 

 

·         [Introduction](https://blogs.msdn.com/uksharepoint/archive/2009/02/26/virtualizing-sharepoint-series-introduction.aspx)

·         [Optimizing the performance of a virtualized SharePoint environment](https://blogs.msdn.com/uksharepoint/archive/2009/03/04/topic-1-recommendations-for-optimizing-the-performance-of-a-virtualized-sharepoint-environment.aspx)

·         [SharePoint server role recommendations in the virtualized SharePoint environment](https://blogs.msdn.com/uksharepoint/archive/2009/03/08/virtualizing-sharepoint-series-recommendations-for-each-server-role-in-the-virtualized-sharepoint-environment.aspx)

·         [Monitoring and managing a virtualized SharePoint environment](https://blogs.msdn.com/uksharepoint/archive/2009/03/11/virtualizing-sharepoint-series-recommendations-for-monitoring-and-managing-a-virtualized-sharepoint-environments.aspx)

·         **[High availability and disaster recovery,  deployment best practices, common mistakes and summary](https://blogs.msdn.com/uksharepoint/archive/2009/03/23/virtualizing-sharepoint-series-other-recommendations-and-conclusions-for-your-virtualized-sharepoint-environment.aspx)**

 

This is fantastic guidance for those organisations currently running, or thinking of running SharePoint in virtualized environments. Enjoy!

 

Patrick