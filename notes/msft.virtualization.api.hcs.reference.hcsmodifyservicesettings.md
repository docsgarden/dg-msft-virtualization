---
id: 6nb4zpojdKXaQR8UPRMcmpj
title: >-
    HcsModifyServiceSettings
desc: >-
    HcsModifyServiceSettings
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsModifyServiceSettings
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 10b362d57d9f0978b18ab7dc9c020d64daf2dbee7f35553331ba23a0ad6fc679
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsModifyServiceSettings.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsModifyServiceSettings']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsModifyServiceSettings.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsModifyServiceSettings).

# HcsModifyServiceSettings

## Description

This function modifies the settings of the Host Compute System, see [[sample code|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.servicesample#modifyservicesettings]].

## Syntax

```cpp
HRESULT WINAPI
HcsModifyServiceSettings(
    _In_ PCWSTR settings,
    _Outptr_opt_ PWSTR* result
    );
```

## Parameters

`settings`

JSON document of [[ModificationRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ModificationRequest]] specifying the settings to modify.

`result`

On failure, it can optionally receive an error JSON document represented by a [[ResultError|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ResultError]]; it's not guaranteed to be always returned and depends on the property type that is being modified.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Remarks

The [[ModificationRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ModificationRequest]] JSON document has a property called `"Settings"` of type `Any`. In JSON, `Any` means an arbitrary object with no restrictions. Refer to the following table to know what JSON type HCS expects for each [[ModifyPropertyType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ModifyPropertyType]].

|`ModifyPropertyType`|`"Setting"` JSON Type|
|---|---|
|`"CpuGroup"`|[[HostProcessorModificationRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#HostProcessorModificationRequest]]|
|`"ContainerCredentialGuard"`|[[ContainerCredentialGuardOperationRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ContainerCredentialGuardOperationRequest]]|


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |