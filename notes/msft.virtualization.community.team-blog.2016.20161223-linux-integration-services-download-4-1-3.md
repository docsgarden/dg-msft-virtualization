---
id: 3SvmVhvm4GzfvgeJus6DBjc
title: >-
    Linux Integration Services Download 4.1.3
desc: >-
    Blog post that discusses the publication of the 4.1.3 update for the Linux Integration Services and outlines what is added in the update.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2016/20161223-linux-integration-services-download-4-1-3
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 5823a6c30fcc220e8354031637a62e4aa2bb84e00d2f1add198c5ab5fc51085a
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2016/20161223-linux-integration-services-download-4-1-3.md
    author: "scooley"
    ms.author: "scooley"
    date: "2016-12-23 06:07:14"
    ms.date: "03/20/2019"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2016/20161223-linux-integration-services-download-4-1-3.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2016/20161223-linux-integration-services-download-4-1-3).

# Linux Integration Services Download 4.1.3

We've just published an update for the Linux Integration Services download. This release includes a series of upstream updates and adds compatibility with Red Hat Enterprise Linux, CentOS, and Oracle Linux RHCK 7.3. The LIS 4.1.3 download is available from the [Microsoft Download Center](https://www.microsoft.com/en-us/download/details.aspx?id=51612). The LIS download is not required for Linux distributions that have built-in LIS, as described in ["Which Linux Integration Services should I use in my Linux VMs?"](https://blogs.technet.microsoft.com/virtualization/2016/07/12/which-linux-integration-services-should-i-use-in-my-linux-vms/) Linux Integration Services is an open source project that is part of the Linux Kernel, and we welcome public involvement with the LIS download on [github](https://github.com/LIS/lis-next).