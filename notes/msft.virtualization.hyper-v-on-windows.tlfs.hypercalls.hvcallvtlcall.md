---
id: Ay8MeqXAzRsGtUAZ7u94Srp
title: >-
    HvCallVtlCall
desc: >-
    HvCallVtlCall hypercall
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallVtlCall
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 06490b12b06d992446fb4e92374f0dbfcc38e4f971b6ebf9595357e98c9e3455
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallVtlCall.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallVtlCall.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallVtlCall).

# HvCallVtlCall

HvCallVtlCall initiates a "[[VTL call|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.vsm#vtl-call]]" and switches into the next highest VTL enabled on the VP.

## Interface

 ```c
HV_STATUS
HvCallVtlCall(
    VOID
    );
 ```

## Call Code

`0x0011` (Simple)