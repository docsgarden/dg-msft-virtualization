---
id: 4PCVsmFSzaRBR5nJpHuA4ai
title: >-
    Delivering business value with an app-centric approach to Private Cloud
desc: >-
    Delivering business value with an app-centric approach to Private Cloud
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110830-delivering-business-value-with-an-app-centric-approach-to-private-cloud
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: d9aa37cff85c83a6d7e1507eea8d750bb9c702eee2515bdfe0f3fee29394756a
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110830-delivering-business-value-with-an-app-centric-approach-to-private-cloud.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "08/30/2011"
    date: "2011-08-30 10:07:59"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110830-delivering-business-value-with-an-app-centric-approach-to-private-cloud.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110830-delivering-business-value-with-an-app-centric-approach-to-private-cloud).

# Delivering business value with an app-centric approach to Private Cloud

Hear more from Edwin Yuen, Director of Cloud and Virtualization Strategy, about Microsoft's approach to private cloud and how that differs from VMware's: Yesterday in his keynote, Paul Maritz’s said VMware’s “heart” is in infrastructure.  He also talked a lot about VMware’s focus on the cloud.  Well, if your heart is in infrastructure (read: virtualization), and you’re focused on the cloud, we think that leads to a virtualization-focused approach to cloud.  
    
At Microsoft our approach to private cloud delivers business value through a focus on the application, not the infrastructure. And, we believe economics is a [fundamental benefit of cloud](http://bit.ly/ogFRab) – it is one that customers should benefit from, not just us.  
    
VMware’s private cloud solution reflects that infrastructure/virtualization heart, and that heart beats pretty loudly in their licensing – which is per-vm and per product.  So, as your cloud grows, and density increases, your costs grow - right along with it.  That doesn’t seem very cloud friendly to me - especially when a VMware private cloud solution can cost 4 to nearly 10 times more than a comparable Microsoft private cloud solution.  If you’d like to take a look at the research behind these numbers take a look at the [new whitepaper](http://bit.ly/pKxRqv) my team just published. Your cloud, on your terms, built for the future, ready now – [take a look](http://bit.ly/oCbwd0) & let us know what you think. Thanks for reading, 

Edwin Yuen