---
id: 7Gr7TfBBfzrURSKTpyoKXje
title: >-
    HcsSetOperationContext
desc: >-
    HcsSetOperationContext
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsSetOperationContext
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 730f3d1eca7c2b24001e0c03e0396deee9f728a4322b5d978aa8dd71b853db7d
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsSetOperationContext.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsSetOperationContext']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsSetOperationContext.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsSetOperationContext).

# HcsSetOperationContext

## Description

Sets the context pointer on an operation.

## Syntax

```cpp
HRESULT WINAPI
HcsSetOperationContext(
    _In_     HCS_OPERATION operation
    _In_opt_ void*         context
    );


```

## Parameters

`operation`

The handle to an active operation.

`context`

Optional pointer to a context that is passed to the callback.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |