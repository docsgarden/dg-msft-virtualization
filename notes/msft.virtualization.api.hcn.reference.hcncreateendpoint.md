---
id: 66DNBPhpFsyJgGCchQyquum
title: >-
    HcnCreateEndpoint
desc: >-
    HcnCreateEndpoint
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnCreateEndpoint
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 1b452eb3e412bbc2d8379cb5d2acb1da54256d53dcfea6fb4b62ccefbc5d0f80
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnCreateEndpoint.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnCreateEndpoint']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnCreateEndpoint.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnCreateEndpoint).

# HcnCreateEndpoint

## Description

Creates an endpoint.

## Syntax

```cpp
HRESULT
WINAPI
HcnCreateEndpoint(
    _In_ HCN_NETWORK Network,
    _In_ REFGUID Id,
    _In_ PCWSTR Settings,
    _Out_ PHCN_ENDPOINT Endpoint,
    _Outptr_opt_ PWSTR* ErrorRecord
    );
```

## Parameters

`Network`

Network for the new endpoint.

`Id`

Id for the new endpoint.

`Settings`

JSON document specifying the settings of the [[endpoint|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.HNS_Schema#HostComputeEndpoint]].

`Endpoint`

Receives a handle to the newly created endpoint. It is the responsibility of the caller to release the handle using [[HcnCloseEndpoint|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcncloseendpoint]] once it is no longer in use.

`ErrorRecord`

Receives a JSON document with extended errorCode information. The caller must release the buffer using CoTaskMemFree.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |