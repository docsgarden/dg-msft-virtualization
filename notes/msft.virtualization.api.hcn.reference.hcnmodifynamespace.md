---
id: 6t6vq85iKoni7HLr4yrdKiE
title: >-
    HcnModifyNamespace
desc: >-
    HcnModifyNamespace
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnModifyNamespace
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: b1a964ee7a7e9cd3f036bc605e98babaa54927853e776e600b88df3db50177ff
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnModifyNamespace.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnModifyNamespace']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnModifyNamespace.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnModifyNamespace).

# HcnModifyNamespace

## Description

Modifies an namespace.

## Syntax

```cpp
HRESULT
WINAPI
HcnModifyNamespace(
    _In_ HCN_NAMESPACE Namespace,
    _In_ PCWSTR Settings,
    _Outptr_opt_ PWSTR* ErrorRecord
    );

```

## Parameters

`Id`

The [[HCN\_NAMESPACE|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_namespace]] to modify.

`Settings`

JSON document specifying the settings of the [[namespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.HNS_Schema#HostComputeNamespace]].

`ErrorRecord`

Receives a JSON document with extended errorCode information. The caller must release the buffer using CoTaskMemFree.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |