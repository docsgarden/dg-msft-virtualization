---
id: 9bodHUXXyXsKe4te6s8Cjb3
title: >-
    HcnOpenNetwork
desc: >-
    HcnOpenNetwork
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnOpenNetwork
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: a211557600567c6e814ad2a9db4e086c866b8a999a769fe34a2d9691500b6ed3
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnOpenNetwork.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnOpenNetwork']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnOpenNetwork.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnOpenNetwork).

# HcnOpenNetwork

## Description

Opens a network.

## Syntax

```cpp
HRESULT
WINAPI
HcnOpenNetwork(
    _In_ REFGUID Id,
    _Out_ PHCN_NETWORK Network,
    _Outptr_opt_ PWSTR* ErrorRecord
    );
```

## Parameters

`Id`

Id of the network.

`Network`

Receives a handle to the network. It is the responsibility of the caller to release the handle using [[HcnCloseNetwork|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnclosenetwork]] once it is no longer in use.

`ErrorRecord`

Receives a JSON document with extended errorCode information. The caller must release the buffer using CoTaskMemFree.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |