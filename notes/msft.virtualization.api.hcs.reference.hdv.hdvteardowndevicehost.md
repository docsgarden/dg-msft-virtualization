---
id: 9tZ6LCPQfhJ6Gjtp9yKJGWy
title: >-
    HdvTeardownDeviceHost
desc: >-
    HdvTeardownDeviceHost
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvTeardownDeviceHost
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: b731832c1fe418ae13a11fea55f6f4a86e22532d3d0a8c9d71919a302d4075cd
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvTeardownDeviceHost.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HdvTeardownDeviceHost']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvTeardownDeviceHost.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvTeardownDeviceHost).

# HdvTeardownDeviceHost

## Description

Tears down the device emulator host in the caller's process. All device instances associated to the device host become non-functional.

## Syntax

```C++
HRESULT WINAPI
HdvTeardownDeviceHost(
    _In_ HDV_HOST DeviceHost
    );
```

## Parameters

|Parameter|Description|
|---|---|---|---|---|---|---|---|
|`DeviceHost` |Handle to the device host to tear down.|
|    |    |

## Return Values

|Return Value     |Description|
|---|---|
|`S_OK` | Returned if function succeeds.|
|`HRESULT` | An error code is returned if the function fails.
|     |     |

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |