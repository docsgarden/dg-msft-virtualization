---
id: AyAFgTDAWioeVYQXHWzSM2Y
title: >-
    HCS_OPERATION_TYPE
desc: >-
    HCS_OPERATION_TYPE
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HCS_OPERATION_TYPE
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: c7a13696d99447a4420758fa1cd8e4b455af46ed7f9d42aa896857d6b634c67f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HCS_OPERATION_TYPE.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HCS_OPERATION_TYPE']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HCS_OPERATION_TYPE.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HCS_OPERATION_TYPE).

# HCS_OPERATION_TYPE

## Description

Defines the type of an operation, which is used as return value in [[HcsGetOperationType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationtype]]. The operation type is determined by the called HCS API that leverages an hcs operation for asynchronous tracking.

## Syntax

```cpp
typedef enum HCS_OPERATION_TYPE
{
    HcsOperationTypeNone = -1,
    HcsOperationTypeEnumerate = 0,
    HcsOperationTypeCreate = 1,
    HcsOperationTypeStart = 2,
    HcsOperationTypeShutdown = 3,
    HcsOperationTypePause = 4,
    HcsOperationTypeResume = 5,
    HcsOperationTypeSave = 6,
    HcsOperationTypeTerminate = 7,
    HcsOperationTypeModify = 8,
    HcsOperationTypeGetProperties = 9,
    HcsOperationTypeCreateProcess = 10,
    HcsOperationTypeSignalProcess = 11,
    HcsOperationTypeGetProcessInfo = 12,
    HcsOperationTypeGetProcessProperties = 13,
    HcsOperationTypeModifyProcess = 14,
    HcsOperationTypeCrash = 15
} HCS_OPERATION_TYPE;
```

## Constants

|Name|Description|
|---|---|
|`HcsOperationTypeNone`|Return if the operation has not yet been used to track a function call.|
|`HcsOperationTypeEnumerate`|Return if the operation is tracking function [[HcsEnumerateComputeSystems|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsenumeratecomputesystems]].|
|`HcsOperationTypeCreate`|Return if the operation is tracking function [[HcsCreateComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscreatecomputesystem]].|
|`HcsOperationTypeStart`|Return if the operation is tracking function [[HcsStartComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsstartcomputesystem]].|
|`HcsOperationTypeShutdown`|Return if the operation is tracking function [[HcsShutDownComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsshutdowncomputesystem]].|
|`HcsOperationTypePause`|Return if the operation is tracking function [[HcsPauseComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcspausecomputesystem]].|
|`HcsOperationTypeResume`|Return if the operation is tracking function [[HcsResumeComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsresumecomputesystem]].|
|`HcsOperationTypeSave`|Return if the operation is tracking function [[HcsSaveComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssavecomputesystem]].|
|`HcsOperationTypeTerminate`|Return if the operation is tracking function [[HcsTerminateComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsterminatecomputesystem]].|
|`HcsOperationTypeModify`|Return if the operation is tracking function [[HcsModifyComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsmodifycomputesystem]].|
|`HcsOperationTypeGetProperties`|Return if the operation is tracking function [[HcsGetComputeSystemProperties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetcomputesystemproperties]].|
|`HcsOperationTypeCreateProcess`|Return if the operation is tracking function [[HcsCreateProcess|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscreateprocess]].|
|`HcsOperationTypeSignalProcess`|Return if the operation is tracking function [[HcsSignalProcess|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssignalprocess]].|
|`HcsOperationTypeGetProcessInfo`|Return if the operation is tracking function [[HcsGetProcessInfo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetprocessinfo]].|
|`HcsOperationTypeGetProcessProperties`|Return if the operation is tracking function [[HcsGetProcessProperties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetprocessproperties]].|
|`HcsOperationTypeModifyProcess`|Return if the operation is tracking function [[HcsModifyProcess|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsmodifyprocess]].|
|`HcsOperationTypeCrash`|Return if the operation is tracking function [[HcsCrashComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscrashcomputesystem]].|

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeDefs.h |