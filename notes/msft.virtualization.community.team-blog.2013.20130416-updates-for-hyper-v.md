---
id: 9p2uhPDaQX4ZnumR7Nea9ZK
title: >-
    Updates for Hyper-V
desc: >-
    Updates for Hyper-V
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2013/20130416-updates-for-hyper-v
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 3a2123122c210a2b1fca8890578e66ac82b0dc15e6ce610c7002985454a3e9b8
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20130416-updates-for-hyper-v.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/16/2013"
    date: "2013-04-16 00:20:00"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20130416-updates-for-hyper-v.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2013/20130416-updates-for-hyper-v).

# Updates for Hyper-V

A new article has appeared on the TechNet Wiki that provides a complete list of updates for Hyper-V on Windows Server 2012: <https://social.technet.microsoft.com/wiki/contents/articles/15576.hyper-v-update-list-for-windows-server-2012.aspx> I am really happy to see this online, as we have received lots of positive feedback about how useful the existing documentation is for updates that are available for Hyper-V on [Windows Server 2008 R2](https://social.technet.microsoft.com/wiki/contents/articles/1349.hyper-v-update-list-for-windows-server-2008-r2.aspx "https://social.technet.microsoft.com/wiki/contents/articles/1349.hyper-v-update-list-for-windows-server-2008-r2.aspx") and on [Windows Server 2008](https://technet.microsoft.com/library/dd430893\(WS.10\).aspx "https://technet.microsoft.com/library/dd430893\(WS.10\).aspx").  Hopefully this will help everyone in planning and managing their Hyper-V deployments. 

Cheers,  
Ben