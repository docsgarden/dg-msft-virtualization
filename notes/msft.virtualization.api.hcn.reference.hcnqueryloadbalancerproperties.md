---
id: 5ufnKXNeNUhGom7mMt3VnGh
title: >-
    HcnQueryLoadBalancerProperties
desc: >-
    HcnQueryLoadBalancerProperties
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnQueryLoadBalancerProperties
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: d2dc6d203014b72f3fe17a32ef25b8c5d091c51edc46205bf10baa0a0e13f1b9
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnQueryLoadBalancerProperties.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnQueryLoadBalancerProperties']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnQueryLoadBalancerProperties.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnQueryLoadBalancerProperties).

# HcnQueryLoadBalancerProperties

## Description

Queries the properties of a load balancer.

## Syntax

```cpp
HRESULT
WINAPI
HcnQueryLoadBalancerProperties(
    _In_ HCN_LOADBALANCER LoadBalancer,
    _In_ PCWSTR Query,
    _Outptr_ PWSTR* Properties,
    _Outptr_opt_ PWSTR* ErrorRecord
    );
```

## Parameters

`LoadBalancer`

Handle to an load balancer [[`HCN_LOADBALANCER`|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_loadbalancer]]

`Query`

Optional JSON document of [[HostComputeQuery|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.HNS_Schema#HostComputeLoadBalancer]].

`Properties`

The properties in the form of a JSON document of [[HostComputeLoadBalancer|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.HNS_Schema#HostComputeLoadBalancer]].

`ErrorRecord`

Receives a JSON document with extended errorCode information. The caller must release the buffer using CoTaskMemFree.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |