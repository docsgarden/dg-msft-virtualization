---
id: 7GSZxspnf8ycsJ7DvCeXqig
title: >-
    VDI reference architecture with HP and Citrix
desc: >-
    At HP's Tech Forum conference, HP started to market a VDI reference architecture, and deployment offer, with us and Citrix.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100622-vdi-reference-architecture-with-hp-and-citrix
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 74457291a49123607c88f3fb5fa407687a587cd54a54e4ebc889123820924e27
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100622-vdi-reference-architecture-with-hp-and-citrix.md
    author: "scooley"
    ms.author: "scooley"
    date: "2010-06-22 15:08:57"
    ms.date: "06/22/2010"
    categories: "citrix"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100622-vdi-reference-architecture-with-hp-and-citrix.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100622-vdi-reference-architecture-with-hp-and-citrix).

# VDI reference architecture with HP and Citrix

At [HP's Tech Forum conference](http://h30406.www3.hp.com/campaigns/2010/events/hptechforum/index.php "HP Tech Forum website"), HP started to market a VDI reference architecture, and deployment offer, with us and Citrix. The brochure can be seen [here](http://h20195.www2.hp.com/V2/GetPDF.aspx/4AA2-1061ENW.pdf "HP brochure on VDI ref architecture"). The VDI offer can be seen [here](http://www.citrixandmicrosoft.com/ "VDI offer site"). The nuts and bolts of the architecture includes: 

* HP ProLiant BL460c server blades
* HP StorageWorks P4800 SAN
* HP Insight Control for System Center
* Citrix XenDesktop 4.0 enterprise edition
* Hyper-V R2

There are new HP infrastructure services for VDI to help you get started. HP's Dave Donatelli spoke of this VDI architecture (and more) toward the end of [his keynote today](http://h18004.www1.hp.com/products/solutions/whatsnew.html "HP Tech Forum keynote on June 22"). It starts around 1:08:00 of Donatelli's keynote. 

Patrick