---
id: 6DeBrzGTvyTmWEupppFGu8J
title: >-
    Live Migration and Host Clustering available at no charge in Microsoft Hyper-V Server 2008 R2
desc: >-
    Microsoft continues to add more features and value to the Microsoft Hyper-V Server in the upcoming release, Microsoft Hyper-V Server 2008 R2.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090420-live-migration-and-host-clustering-available-at-no-charge-in-microsoft-hyper-v-server-2008-r2
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 3be14dcac4b6ed12b3dab5c8bcf4aa397d6c27e539b3e9f217bdb8a24c1b200a
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090420-live-migration-and-host-clustering-available-at-no-charge-in-microsoft-hyper-v-server-2008-r2.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-04-20 18:13:00"
    ms.date: "04/20/2009"
    categories: "disaster-recovery"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090420-live-migration-and-host-clustering-available-at-no-charge-in-microsoft-hyper-v-server-2008-r2.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090420-live-migration-and-host-clustering-available-at-no-charge-in-microsoft-hyper-v-server-2008-r2).

# Live Migration and Host Clustering available at no charge in Microsoft Hyper-V Server 2008 R2

I’m Zane Adam, senior director of virtualization and System Center.  It’s been a while since [my last post](https://blogs.technet.com/virtualization/archive/2008/10/21/system-center-virtual-machine-manager-2008-rtms-and-what-i-m-hearing-from-customers-and-partners-about-microsoft-s-virtualization-solutions.aspx), and wanted to update you on our standalone hypervisor, Microsoft Hyper-V Server 2008 R2

Last Fall [we released](https://blogs.technet.com/virtualization/archive/2008/10/01/Bare-metal-hypervisor-is-here_2C00_-along-with-new-training_2C00_-services.aspx) Microsoft Hyper-V Server 2008, a standalone hypervisor-based virtualization product that is available for free. We continue to add more features and value to this product in the upcoming release, Microsoft Hyper-V Server 2008 R2.  Our core strategy is to ensure that our customers can virtualize their IT environment in the most cost effective manner, and at the same time, have access to enterprise features like live migration and clustering features for high availability.  So in addition to scalability and performance improvements in this version, customers can get live migration and host clustering capabilities and high availability (up to 16 nodes) at no charge.

Microsoft Hyper-V Server 2008 R2 will continue to be free, and now will include live migration and host clustering capabilities. Customers won’t need to pay thousands of dollars for alternate virtualization platforms to get these features. With Microsoft Hyper-V Server 2008 R2, customers have a solution for both planned and unplanned downtime and can use it for scenarios like server consolidation, branch server consolidation, high availability, and VDI.

These same features also will be available in Windows Server 2008 R2 Hyper-v, which provides our customers with core virtualization features as part of Windows Server offering.  With Windows Server 2008 R2, customers also get flexible virtualization rights (e.g., 4 free virtual instances with Windows Server Enterprise Edition and unlimited virtual instances with Windows Server Datacenter Edition).

You can download Microsoft Hyper-V Server 2008 R2 beta [here](https://www.microsoft.com/downloads/details.aspx?familyid=E464E255-CDD5-44B2-84E6-3233EAE3F356&displaylang=en). Microsoft Hyper-V Server 2008 R2 can be managed by System Center Virtual Machine Manager 2008 R2. You can download the beta [here](https://www.microsoft.com/systemcenter/virtualmachinemanager/en/us/r2-beta.aspx).

You can find more info on Microsoft’s virtualization products and solutions at [http://www.microsoft.com/virtualization](https://www.microsoft.com/virtualization).

Zane