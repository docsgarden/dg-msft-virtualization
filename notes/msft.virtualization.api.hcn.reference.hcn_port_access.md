---
id: 7GBAmjzo7xnsepBJumjNNjf
title: >-
    HCN_PORT_ACCESS
desc: >-
    HCN_PORT_ACCESS
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_PORT_ACCESS
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 192e90ce122488a6e0e9fa7e6e7f34b29c2157f6df52e9625c0a8e24157002a9
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_PORT_ACCESS.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HCN_PORT_ACCESS']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_PORT_ACCESS.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_PORT_ACCESS).

# HCN\_PORT\_ACCESS

## Description

The type of access required for a port reservation.

## Syntax

```cpp
typedef enum tagHCN_PORT_ACCESS
{
    HCN_PORT_ACCESS_EXCLUSIVE = 0x01,
    HCN_PORT_ACCESS_SHARED = 0x02
} HCN_PORT_ACCESS;
```


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |