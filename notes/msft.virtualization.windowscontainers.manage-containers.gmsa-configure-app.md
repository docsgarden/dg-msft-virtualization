---
id: 3VJKg5Z5fAq7btznxpfXfSj
title: >-
    Configure your app to use a Group Managed Service Account
desc: >-
    How to configure apps to use Group Managed Service Accounts (gMSAs) for Windows containers.
canonicalUrl: https://docs.microsoft.com/virtualization/windowscontainers/manage-containers/gmsa-configure-app
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 69f4ae0ded2ee142c8b1a7a6ed801ecb45630b9459e5ab89b44ff2df0979502f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/windowscontainers/manage-containers/gmsa-configure-app.md
    keywords: "docker, containers, active directory, gmsa, apps, applications, group managed service account, group managed service accounts, configuration"
    author: "rpsqrd"
    ms.author: "ryanpu"
    ms.date: "09/10/2019"
    ms.topic: "how-to"
    ms.assetid: "9e06ad3a-0783-476b-b85c-faff7234809c"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/windowscontainers/manage-containers/gmsa-configure-app.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/windowscontainers/manage-containers/gmsa-configure-app).

# Configure your app to use a gMSA

> Applies to: Windows Server 2022, Windows Server 2019

In the typical configuration, a container is only given one Group Managed Service Account (gMSA) that is used whenever the container computer account tries to authenticate to network resources. This means your app will need to run as **Local System** or **Network Service** if it needs to use the gMSA identity. Containers can also be configured with additional gMSAs, in case you want to run a service or application in the container as a different identity from the container computer account.

## Run an IIS app pool as Network Service

If you're hosting an IIS website in your container, all you need to do to leverage the gMSA is set your app pool identity to **Network Service**. You can do that in your Dockerfile by adding the following command:

```dockerfile
RUN %windir%\system32\inetsrv\appcmd.exe set AppPool DefaultAppPool -'processModel.identityType':NetworkService
```

If you previously used static user credentials for your IIS App Pool, consider the gMSA as the replacement for those credentials. You can change the gMSA between dev, test, and production environments and IIS will automatically pick up the current identity without having to change the container image.

## Run a Windows service as Network Service

If your containerized app runs as a Windows service, you can set the service to run as **Network Service** in your Dockerfile:

```dockerfile
RUN sc.exe config "YourServiceName" obj= "NT AUTHORITY\NETWORK SERVICE" password= ""
```

## Run arbitrary console apps as Network Service

For generic console apps that are not hosted in IIS or Service Manager, it is often easiest to run the container as **Network Service** so the app automatically inherits the gMSA context. This feature is available as of Windows Server version 1709.

Add the following line to your Dockerfile to have it run as Network Service by default:

```dockerfile
USER "NT AUTHORITY\NETWORK SERVICE"
```

You can also connect to a container as Network Service on a one-off basis with `docker exec`. This is particularly useful if you're troubleshooting connectivity issues in a running container when the container does not normally run as Network Service.

```powershell
# Opens an interactive PowerShell console in the container (id = 85d) as the Network Service account
docker exec -it --user "NT AUTHORITY\NETWORK SERVICE" 85d powershell
```

## Next steps

In addition to configuring apps, you can also use gMSAs to:

- [[Run containers|dendron://dg-msft-virtualization/msft.virtualization.windowscontainers.manage-containers.gmsa-run-container]]
- [[Orchestrate containers|dendron://dg-msft-virtualization/msft.virtualization.windowscontainers.manage-containers.gmsa-orchestrate-containers]]

If you run into any issues during setup, check our [[troubleshooting guide|dendron://dg-msft-virtualization/msft.virtualization.windowscontainers.manage-containers.gmsa-troubleshooting]] for possible solutions.