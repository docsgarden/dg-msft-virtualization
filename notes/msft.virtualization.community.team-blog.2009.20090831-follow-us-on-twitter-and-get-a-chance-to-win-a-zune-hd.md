---
id: 6yHZVnX3FPCELjJX8sH2yLf
title: >-
    Follow us on Twitter and get a chance to win a Zune HD
desc: >-
    Follow us on Twitter and get a chance to win a Zune HD
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090831-follow-us-on-twitter-and-get-a-chance-to-win-a-zune-hd
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: b9ee572f16658a2a3b24c6b6bfe9cfffe2870f84129e93e628529351374282c4
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090831-follow-us-on-twitter-and-get-a-chance-to-win-a-zune-hd.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "08/31/2009"
    date: "2009-08-31 09:09:00"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090831-follow-us-on-twitter-and-get-a-chance-to-win-a-zune-hd.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090831-follow-us-on-twitter-and-get-a-chance-to-win-a-zune-hd).

# Follow us on Twitter and get a chance to win a Zune HD

I now have the final list of sessions that I will be attending at VMworld. As I [noted last time](https://blogs.technet.com/virtualization/archive/2009/08/26/vmworld-2009-an-oppurtunity-to-meet-our-customers.aspx) I had a lot of waitlists which it now appears I have not made the cut for most of the sessions. That means I will be at our booth (#2422) almost all of today (Aug 31, 2009) and at the times when I am not attending a session. <?xml:namespace prefix = o ns = "urn:schemas-microsoft-com:office:office" />  There is a great opportunity to win a [32GB Zune HD](https://store.microsoft.com/microsoft/Zune-HD-32GB/product/41941DC9). All you have to do is to follow the [Microsoft Virtualization experts](https://events.microsoft.com/en-us/allevents/?language=English&clientTimeZone=1) on Twitter. By the way this is open to folks who are not attending VMworld as well. So follow along with us!! You can follow me [here](https://twitter.com/Vtango).  I am heading over to the Moscone Center to register and see if I can get into some of the labs being offered today. 

Vijay Tewari

Principal Program Manager, Windows Server Virtualization