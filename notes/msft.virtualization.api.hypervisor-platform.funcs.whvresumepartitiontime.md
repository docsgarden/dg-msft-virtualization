---
id: 6JTQzGdGr33Eqypt4grq8Kh
title: >-
    WHvResumePartitionTime
desc: >-
    Description on working with WHvResumePartitionTime and understanding its parameters, return value, remarks, and requirements.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvResumePartitionTime
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 89c594bf65da59993558578420509ac2f0f97f6642e31ce96465a1e0435a22fe
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvResumePartitionTime.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "03/15/2019"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvResumePartitionTime.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvResumePartitionTime).

# WHvResumePartitionTime

## Syntax

```C
HRESULT
WINAPI
WHvResumePartitionTime(
    _In_ WHV_PARTITION_HANDLE Partition
    );
```

### Parameters

`Partition`

Handle to the partition object.

## Return Value

If the function succeeds, the return value is `S_OK`.  

## Remarks

Resumes time for a partition suspended by [[`WHvSuspendPartitionTime`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-platform.funcs.whvsuspendpartitiontime]].

## Requirements

Minimum supported build:    Insider Preview Builds (19H1)