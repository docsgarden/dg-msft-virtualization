---
id: 7eFYgogUbFJ9cDWJNwqzfbb
title: >-
    HcsModifyComputeSystem
desc: >-
    HcsModifyComputeSystem
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsModifyComputeSystem
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: c84eff3582c2a3271989a5d3d3f7d6b85dae3eea378b84a4f4c7870195fa8a22
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsModifyComputeSystem.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsModifyComputeSystem']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsModifyComputeSystem.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsModifyComputeSystem).

# HcsModifyComputeSystem

## Description

Modifies settings of a compute system, see [[sample code|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.tutorial]] for simple example.

## Syntax

```cpp
HRESULT WINAPI
HcsModifyComputeSystem(
    _In_ HCS_SYSTEM computeSystem,
    _In_ HCS_OPERATION operation,
    _In_ PCWSTR configuration,
    _In_opt_ HANDLE identity
    );
```

## Parameters

`computeSystem`

Handle the compute system to modify.

`operation`

Handle to the operation that tracks the modify operation.

`configuration`

JSON document of [[ModifySettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ModifySettingRequest]] specifying the settings to modify.

`identity`

Optional handle to an access token that is used when applying the settings.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

If the return value is `S_OK`, it means the operation started successfully. Callers are expected to get the operation's result using [[`HcsWaitForOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]] or [[`HcsGetOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]].

## Operation Results

The return value of [[`HcsWaitForOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]] or [[`HcsGetOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]] based on current operation listed as below.

| Operation Result Value | Description |
| -- | -- |
| `S_OK` | The compute system was modified successfully |

## Remarks

The [[ModifySettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ModifySettingRequest]] JSON document has a property called `"Settings"` of type `Any`. In JSON, `Any` means an arbitrary object with no restrictions. Refer to the following table to know what JSON type HCS expects for each `"ResourcePath"`.

|`"ResourcePath"`|`"Settings"` Type|Valid `"RequestType"` in [[ModifyRequestType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ModifyRequestType]]|
|---|---|---|
|L"VirtualMachine/ComputeTopology/Processor/CpuGroup"|[[CpuGroup|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#CpuGroup]]|No Limit|
|L"VirtualMachine/ComputeTopology/Processor/IdledProcessors"|[[IdleProcessorsRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#CpuGroup]]|Only "Update"|
|L"VirtualMachine/ComputeTopology/Processor/CpuFrequencyPowerCap"|ULONG|No Limit|
|L"VirtualMachine/Devices/FlexibleIov/`<Identifier>`"<br>`Identifier` is expected as uniq name to represent the flexible IOV device|[[FlexibleIoDevice|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#FlexibleIoDevice]]|Only "Add"|
|L"VirtualMachine/ComputeTopology/Gpu"|[[GpuConfiguration|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#GpuConfiguration]]|Only "Update"|
|L"VirtualMachine/Devices/MappedPipes/`<Identifier>`"<br>`Identifier` is expected as uniq name to represent the host named pipe to be mapped|`Settings` should be empty|"Add" or "Remove"|
|L"VirtualMachine/ComputeTopology/Memory/SizeInMB"|UINT64, meaning new memory size in MB|No Limit|
|L"VirtualMachine/Devices/NetworkAdapters/`<Identifier`>"<br>`Identifier` is expected as uniq name to represent the network adapter|[[NetworkAdapter|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#CpuGroup]]|No Limit|
|L"VirtualMachine/Devices/Plan9/Shares"|[[Plan9Share|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#Plan9Share]]|No Limit|
|L"VirtualMachine/Devices/Scsi/`<Identifier>`/Attachments/`<UnsignedInt>`"<br>`Identifier` is expected as uniq name to represent the scsi device; `UnsignedInt` is expected as the unsigned int value to represent the lun of the disk|[[Attachment|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#Attachment]]|No Limit<br>`Settings` is ignored when type is "Remove"|
|L"VirtualMachine/Devices/ComPorts/`<UnsignedInt>`"<br>`UnsignedInt` is expected to represent the serial ID which is not larger than 1|[[comPort|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ComPort]]|No Limit(check c_SerialResourceRegex???)|
|L"VirtualMachine/Devices/SharedMemory/Regions"|[[SharedMemoryRegion|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#SharedMemoryRegion]]|No Limit|
|L"VirtualMachine/Devices/VirtualPMem/Devices/`<UnsignedInt>`"<br>`UnsignedInt` is expected to represent the number identifier of the VPMEM device|[[VirtualPMemDevice|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#VirtualPMemDevice]]|"Add" or "Remove"<br>`Settings` is ignored when type is "Remove"|
|L"VirtualMachine/Devices/VirtualPMem/Devices/`<UnsignedInt>`/Mappings/`<UnsignedInt>`"<br>First `UnsignedInt` is expected to represent the number identifier of the VPMEM device; Second `UnsignedInt` is expected to represent the offset indicating which Mapping to modify|[[VirtualPMemMapping|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#VirtualPMemMapping]]|"Add" or "Remove"<br>`Settings` is ignored when type is "Remove"|
|L"VirtualMachine/Devices/VirtualSmb/Shares"|[[VirtualSmbShare|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#VirtualSmbShare]]|No Limit|
|L"VirtualMachine/Devices/VirtualPci/" + c_Identifier|[[VirtualPciDevice|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#VirtualSmbShare]]|"Add" or "Remove"<br>`Settings` is ignored when type is "Remove"|


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |