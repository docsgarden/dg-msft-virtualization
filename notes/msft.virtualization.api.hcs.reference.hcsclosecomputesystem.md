---
id: 6e4YwMUUQXMHzvEfezmqq5i
title: >-
    HcsCloseComputeSystem
desc: >-
    HcsCloseComputeSystem
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsCloseComputeSystem
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 9f0552744eaf6cc301973ebee9ad5d7109305bc96761625f15923cc775598d19
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsCloseComputeSystem.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsCloseComputeSystem']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsCloseComputeSystem.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsCloseComputeSystem).

# HcsCloseComputeSystem

## Description

Closes a handle to a compute system, see [[sample code|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.computesystemsample#saveclosecs]].

## Syntax

```cpp
void WINAPI
HcsCloseComputeSystem(
    _In_ HCS_SYSTEM computeSystem
    );
```

## Parameters

`computeSystem`
The handle to the compute system.

## Return Values

None.

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |