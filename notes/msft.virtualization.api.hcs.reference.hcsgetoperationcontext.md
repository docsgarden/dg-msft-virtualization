---
id: 6NJjDyvw8hALwv2qXCw694Y
title: >-
    HcsGetOperationContext
desc: >-
    HcsGetOperationContext
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetOperationContext
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 692287edf04aeab824e80540ffe500e41af1cad71bc407eea76a432e7a775d6c
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetOperationContext.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsGetOperationContext']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetOperationContext.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetOperationContext).

# HcsGetOperationContext

## Description

Gets the context pointer of an operation.

## Syntax

```cpp
void* WINAPI
HcsGetOperationContext(
    _In_ HCS_OPERATION operation
    );

```

## Parameters

`operation`

The handle to an operation.

## Return Values

Returns the context pointer stored in the operation as a `void*` type.

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |