---
id: 7izGdesdwF6ENLSkd9BYFk2
title: >-
    Server Virtualization Sessions @ Build 2015
desc: >-
    An announcement for the Build 2015 event and what sessions are related to Server Virtualization and Server Application development.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2015/20150430-server-virtualization-sessions-build-2015
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 58a4f97046e9481b1d8c3054f301c08e3bb48edc6756e47b71541b814f00fc4d
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2015/20150430-server-virtualization-sessions-build-2015.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2015-04-30 13:39:18"
    ms.date: "04/30/2015"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2015/20150430-server-virtualization-sessions-build-2015.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2015/20150430-server-virtualization-sessions-build-2015).

# Server Virtualization Sessions @ Build 2015

If you are interested in Server Virtualization and Server Application development - today is a good day to be at Build 2015 (or following it online).

Two big sessions to recommend are:

 **Nano Server: A Cloud Optimized Windows Server for  Developers**  
Time: April 30, 2015 from 2:00PM to 3:00PM  
Online: <https://channel9.msdn.com/Events/Build/2015/2-755>

**Windows Containers: What, Why and  How**  
Time: April 30, 2015 from 5:00PM to 6:00PM  
Online: <https://channel9.msdn.com/Events/Build/2015/2-704>

Cheers,  
Ben