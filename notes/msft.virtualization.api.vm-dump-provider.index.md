---
id: 7coNuRmo4kTU8K5Q92o3pr8
title: >-
    VM (Virtual Machine) Saved State Dump Provider API
desc: >-
    The saved state format of a VM can be accessed via VmSavedStateDumpProvider DLL.
canonicalUrl: https://docs.microsoft.com/virtualization/api/vm-dump-provider/index
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 67a9e4fa6e7ca549e1672974eb31e71bbc86a22074fa82c904404ac95193114c
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/index.md
    ms.date: "04/18/2022"
    author: "mattbriggs"
    ms.author: "mabrigg"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/index.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/vm-dump-provider/index).

# VM (Virtual Machine) Saved State Dump Provider API

**Note: These APIs are not yet publicly available and will be available in the latest release of the Windows SDK.**

The saved state format of a VM can be accessed via VmSavedStateDumpProvider DLL, which abstracts away the format to provide an API to extract dump-related content from a virual machine's saved state. The DLL exports a set of C-style Windows API functions.