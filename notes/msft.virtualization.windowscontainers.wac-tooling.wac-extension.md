---
id: 7aRQq9HGkjZi6gYtBVQNxzf
title: >-
    Configure the Containers extension on Windows Admin Center
desc: >-
    Container extension on Windows Admin Center
canonicalUrl: https://docs.microsoft.com/virtualization/windowscontainers/wac-tooling/wac-extension
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: d6675668d41417a286c54db456055edf4692592a0944d32005a75e2b6ed02ffa
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/windowscontainers/wac-tooling/wac-extension.md
    keywords: "docker, containers, Windows Admin Center"
    author: "vrapolinario"
    ms.author: "viniap"
    ms.date: "12/23/2020"
    ms.topic: "quickstart"
    ms.assetid: "bb9bfbe0-5bdc-4984-912f-9c93ea67105f"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/windowscontainers/wac-tooling/wac-extension.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/windowscontainers/wac-tooling/wac-extension).

# Configure the Containers extension on Windows Admin Center

This topic describes how to configure the Containers extension on Windows Admin Center. For more information on how to install and configure Windows Admin Center as well as how to target remote servers, see the [Windows Admin Center](https://docs.microsoft.com/en-us/windows-server/manage/windows-admin-center/overview) documentation.

## Install the Containers extension on Windows Admin Center

1. On your Windows Admin Center instance, select the **Settings** button in the top-right, and then, in the left pane, under **Gateway**, select **Extensions**.
2. On the **Available extensions** tab, select the **Containers** extension from the list.
3. Click **Install** to install the extension.

   ![Containers extension]([Containers extension|dendron://dg-msft-https://docs.microsoft.com/virtualization/msft.https://docs.microsoft.com/virtualization/windowscontainers/wac-tooling/media/wac-extension.png]]) 

Windows Admin Center will refresh and, once completed, the extension will appear under the **Installed extensions** tab.

## Next steps

> [[!div class="nextstepaction"]
> [Manage container images on Windows Admin Center|dendron://dg-msft-virtualization/msft.virtualization.windowscontainers.wac-tooling.wac-manage]]