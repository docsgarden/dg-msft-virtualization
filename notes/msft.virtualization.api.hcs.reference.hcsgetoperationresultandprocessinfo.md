---
id: 5ns7DGL8KnyrSxLeVdkhFju
title: >-
    HcsGetOperationResultAndProcessInfo
desc: >-
    HcsGetOperationResultAndProcessInfo
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetOperationResultAndProcessInfo
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: eb0140d38459b1831a9338b780e5b041315f5ec41b88caaeea383a86aa6f1970
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetOperationResultAndProcessInfo.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsGetOperationResultAndProcessInfo']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetOperationResultAndProcessInfo.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetOperationResultAndProcessInfo).

# HcsGetOperationResultAndProcessInfo

## Description

Gets the result of the operation used to track [[`HcsCreateProcess`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscreateprocess]] or [[`HcsGetProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetprocessinfo]]; optionally returns a JSON document associated to such tracked operation and/or the process information.

## Syntax

```cpp
HRESULT WINAPI
HcsGetOperationResultAndProcessInfo(
    _In_ HCS_OPERATION operation,
    _Out_opt_ HCS_PROCESS_INFORMATION* processInformation,
    _Outptr_opt_ PWSTR* resultDocument
    );
```

## Parameters

`operation`

The handle to an active operation.

`processInformation`

If the return value is `S_OK` and this parameter has been provided a valid pointer by the caller, it returns the [[HCS_PROCESS_INFORMATION|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_process_information]] associated to the HCS process created with [[`HcsCreateProcess`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscreateprocess]].

`resultDocument`

If the operation finished, regardless of success or failure, receives the result document of the operation. The returned result document's JSON document is dependent on the HCS function that was being tracked by this operation. Not all functions that are tracked with operations return a result document. Refer to the remarks on the documentation for the HCS functions that use hcs operations for asynchronous tracking.


On failure, it can optionally receive an error JSON document represented by a [[ResultError|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ResultError]]; it's not guaranteed to be always returned and depends on the function call the operation was tracking.


The caller is responsible for releasing the returned string using [`LocalFree`](https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-localfree).

## Return Values

|Value|Description|
|---|---|
|`S_OK`|The operation has completed successfully.|
|`HCS_E_OPERATION_NOT_STARTED`|The operation has not been started. This is expected when the operation has not been used yet in an HCS function that expects an `HCS_OPERATION` handle.|
|`HCS_E_OPERATION_PENDING`|The operation is still in progress and hasn't been completed, regardless of success or failure.|
|`E_INVALIDARG`|It's possible the operation's type is different from `HcsOperationTypeCreateProcess` and `HcsOperationTypeGetProcessInfo`. Note that invalid argument return code could be also returned by the HCS function that was being tracked by this operation.|
|Any other failure [[`HRESULT`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]] value|The operation completed with failures. The returned `HRESULT` is dependent on the HCS function thas was being tracked.|


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |