---
id: 4ConMgJUk5hqRF6UBWUoPHx
title: >-
    From 2010 to 2011&#58; Walking the Cloud Talk
desc: >-
    The shift to cloud computing in 2010 and predictions about 2011
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20101210-from-2010-to-2011-walking-the-cloud-talk
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 9d877dbecf02da8677131089951867df9e1a6f5748ce8fb110cc59fe15d33d49
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20101210-from-2010-to-2011-walking-the-cloud-talk.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2010-12-10 08:55:55"
    ms.date: "12/10/2010"
    categories: "blogs"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20101210-from-2010-to-2011-walking-the-cloud-talk.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20101210-from-2010-to-2011-walking-the-cloud-talk).

# From 2010 to 2011: Walking the Cloud Talk
My colleague David Greschler wrote a blog post for David Marshall's blog, [VM Blog](http://vmblog.com/home.aspx). DavidGr's blog post is part of a series this month on VM Blog where vendor representatives write about 2011 predictions and looking ahead. See DavidGr's post [here](http://vmblog.com/archive/2010/12/08/microsoft-from-2010-to-2011-walking-the-cloud-talk.aspx). Following is an excerpt: 

2010 was the year of the cloud. We saw some massive changes across the industry as IT decision makers and technology vendors wrestled with the shift to cloud computing. In particular, the industry had to grapple with many differing - and often conflicting - definitions of cloud computing. Certainly virtualization was often part of the discussion; however, 2010 brought a broader understanding that virtualization was no longer the end of the road, but instead a helpful stepping stone to the agile, responsive world of cloud computing. 

With an understanding of the cloud possibilities established, I believe 2011 is the year that IT departments will really begin to develop their cloud plans for implementation. [Gartner](http://www.gartner.com/it/page.jsp?id=1465614) has estimated that worldwide cloud services revenue (including public and private services) will reach $148.8 billion in 2014. 

As I see it, virtualization experts are poised to help their companies make that shift from virtualization to cloud computing and shape the cloud computing strategy that matches their needs. Patrick