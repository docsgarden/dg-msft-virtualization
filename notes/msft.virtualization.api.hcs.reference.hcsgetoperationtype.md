---
id: 6F9izPNXEurfnWTSxp4JoEZ
title: >-
    HcsGetOperationType
desc: >-
    HcsGetOperationType
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetOperationType
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 4911356ddd45901d440c07f2569b58700232838141e0b1b7fcf790450cabfbcb
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetOperationType.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsGetOperationType']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetOperationType.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetOperationType).

# HcsGetOperationType

## Description

Get the type of the operation, this corresponds to the API call the operation was issued with.

## Syntax

```cpp
HCS_OPERATION_TYPE WINAPI
HcsGetOperationType(
    _In_ HCS_OPERATION operation
    );
```

## Parameters

`operation`
The handle to an active operation.

## Return Values

If the function succeeds, the return value is [[HCS_OPERATION_TYPE|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_operation_type]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |