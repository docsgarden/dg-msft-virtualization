---
id: 73WZVa3e3sDejcEVrVLRArE
title: >-
    The GetPagingMode function
desc: >-
    Queries for the current Paging Mode in use by the virtual processor.
canonicalUrl: https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/GetPagingMode
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: e85e78709621d8ca79ac86600f208e763cf2d54c3ec390b0a38d0c5977ae970b
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/GetPagingMode.md
    ms.date: "04/19/2022"
    author: "mattbriggs"
    ms.author: "mabrigg"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/GetPagingMode.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/GetPagingMode).

# GetPagingMode function

Queries for the current Paging Mode in use by the virtual processor at the time the
saved state file was generated.

## Syntax

```C
HRESULT
WINAPI
GetPagingMode(
    _In_    VM_SAVED_STATE_DUMP_HANDLE      VmSavedStateDumpHandle,
    _In_    UINT32                          VpId,
    _Out_   PAGING_MODE*                    PagingMode
    );
```

## Parameters

`VmSavedStateDumpHandle`

Supplies a handle to a dump provider instance.

`VpId`

Supplies the Virtual Processor Id.

`PagingMode`

Returns the paging mode.

## Return Value

If the operation completes successfully, the return value is `S_OK`.

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |