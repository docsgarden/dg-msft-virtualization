---
id: 3FYoYKqjbK94ad6comTHuvq
title: >-
    MED-V 2.0 beta is available - for Windows 7 migrations
desc: >-
    You'll be interested to know that MED-V 2.0 beta is available to download.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20101012-med-v-2-0-beta-is-available-for-windows-7-migrations
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: c781b352e6f699f8ae614906363a5cfc917e0599ebea6b4b09016b85aada4b56
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20101012-med-v-2-0-beta-is-available-for-windows-7-migrations.md
    author: "scooley"
    ms.author: "scooley"
    date: "2010-10-12 15:06:11"
    ms.date: "10/12/2010"
    categories: "application-virtualization"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20101012-med-v-2-0-beta-is-available-for-windows-7-migrations.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20101012-med-v-2-0-beta-is-available-for-windows-7-migrations).

# MED-V 2.0 beta is available - for Windows 7 migrations

You'll be interested to know that MED-V 2.0 beta is available [to download](https://connect.microsoft.com/medv/Survey/NominationSurvey.aspx?SurveyID=11699&ProgramID=6452) today. Karri blogged about it over at the Windows team blog [[here](http://windowsteamblog.com/windows/b/business/archive/2010/10/12/how-med-v-helps-your-migration-to-windows-7.aspx)]. Here's an excerpt from Karri's blog: 

**_With MED-V 2.0 Beta_** , we have been listening to customer feedback to further simplify the MED-V experience by enabling one-time sign on to the MED-V workspace. The only time you ever re-enter your password is when you change your Windows password.  In addition, we recognize that more infrastructure is not always better so we’ve enabled you to leverage your existing software distribution system as the delivery mechanism for the MED-V workspaces. This means that System Center Configuration Manager integration was a priority for us as we want to extend the value of the infrastructure you already have. Last but certainly not least, customers told us that they are moving toward virtualization of their applications as their primary strategy for the future. Naturally, they want to be able to use App-V in conjunction with MED-V so that they have a consistent packaging methodology, regardless of the target. Now, App-V and MED-V are fully tested together and virtual apps can be deployed and managed within the MED-V 2.0 Beta workspaces.

For more information about other features, and there are many more, please visit the [MDOP Blog](https://blogs.technet.com/b/mdop/default.aspx?wa=wsignin1.0). For more on how Harbor Wholesale and other customers are taking advantage of MED-V in their environments, check out the [case studies](https://www.microsoft.com/casestudies/Case_Study_Search_Results.aspx?Type=1&ProTaxID=11574,11575&LangID=46). Patrick