---
id: 6os9AusTxPuNfHLhnptoMku
title: >-
    Hear what NetApp and Cisco are Saying After Joining the Hyper-V Fast Track Program
desc: >-
    Hear what NetApp and Cisco are Saying After Joining the Hyper-V Fast Track Program
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110517-hear-what-netapp-and-cisco-are-saying-after-joining-the-hyper-v-fast-track-program
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 7fb0ca197f5f79eabe2cfb841634df0a15ef5e3e858dfaf1b0bc1576ecf1b7e2
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110517-hear-what-netapp-and-cisco-are-saying-after-joining-the-hyper-v-fast-track-program.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "05/17/2011"
    date: "2011-05-17 05:49:06"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110517-hear-what-netapp-and-cisco-are-saying-after-joining-the-hyper-v-fast-track-program.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110517-hear-what-netapp-and-cisco-are-saying-after-joining-the-hyper-v-fast-track-program).

# Hear what NetApp and Cisco are Saying After Joining the Hyper-V Fast Track Program

As we announced yesterday we are proud to have [NetApp and Cisco](https://blogs.technet.com/b/virtualization/archive/2011/05/16/new-hyper-v-cloud-fast-track-partners-double-down-on-private-cloud-vision.aspx) participating in the [Hyper-V Cloud Fast Track](https://www.microsoft.com/virtualization/en/us/hyperv-cloud-fasttrack.aspx) program and delivering pre-validated private cloud solutions for customers. The new [NetApp Hyper-V Cloud Fast Track with Cisco](https://www.netapp.com/microsoftcloud) builds upon the [reference architecture](https://download.microsoft.com/download/B/7/9/B7931A5A-18F6-41EA-B603-975EF281587F/Hyper-V_Cloud_Fast_Track_White_Paper.pdf) and layers on the additional capabilities inherent in the NetApp and Cisco infrastructure.  This joint solution exemplifies a key tenet of Fast Track which is the extensible nature of the architecture.    Check out what [NetApp](https://blogs.netapp.com/msenviro/2011/05/netapp-hyperv-cloud-fast-track-with-cisco.html) is saying about this solution as well as [Cisco](http://blogs.cisco.com/category/datacenter/).  _If you happen to be at Tech Ed_ , Alex Jauch from NetApp, will be demonstrating some of the capabilities of their offering in Bryon Surace’s VIR327 session on Hyper-V Cloud Fast Track on Tuesday morning at 10:15 to 11:30 in Room B211 – hope to see you there.    

Scott Rosenbloom  
Sr. Product Manager  
Server & Cloud Division  
Microsoft Corp.