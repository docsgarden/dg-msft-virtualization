---
id: 35XZMdQq4Yz7DzTLuMbgJxk
title: >-
    Go vote
desc: >-
    Who has the better virtualization platform, Microsoft or VMWare?
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100810-go-vote
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: c9380a45228ff2509502b996a3c0837e3d9d95c2097bc7347e31de7389b7f8c7
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100810-go-vote.md
    author: "scooley"
    ms.author: "scooley"
    date: "2010-08-10 16:39:18"
    ms.date: "08/10/2010"
    categories: "citrix"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100810-go-vote.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100810-go-vote).

# Go vote

The savvy editors of NetworkWorld chose 3 weeks before VMworld 2010 conference to publish an online tech debate between Microsoft and VMware. The topic: who has the better virtualization platform. The editorial summary reads: 

_VMware is the kingpin of virtualization, but the game is changing fast and Microsoft is baking the technology into the very core of many products. Which company has the best approach?_ Read  the debate [here](http://www.networkworld.com/community/node/64734 "NetworkWorld Tech Debate"). Following is an excerpt:

_There was a perception among early adopters of server virtualization that Microsoft didn't have a rich feature set. That's not the case. More than a year ago we further simplified and expanded clustering nodes, and added live migration for zero-downtime migrations of virtual machines between Hyper-V servers._

_Hyper-V also provides high availability with transparent and automatic failover of virtual machines. With service pack 1 of Windows Server 2008 R2, we're adding Dynamic Memory and a new high-fidelity remote desktop protocol._

_Lastly, you should read_[ _Enterprise Strategy Group's_](http://www.enterprisestrategygroup.com/2010/07/microsoft-hyper-v-r2-scalable-native-server-virtualization-for-the-enterprise/) _lab results that show Hyper-V performance versus physical devices, with 95% to 99% of the performance of physical disks, and 89% to 98% of performance of the tested workloads compared to what can be achieved on physical machines._

_At Microsoft we believe virtualization is so critical we've made it part of our server OS, our management tools and our cloud strategy. As a result, VMware is missing critical features: the ability to manage both physical and virtual machines; the ability to get information about the application running within the virtual machine located on-premises or cloud; the ability to manage virtual machines from Microsoft, VMware and soon Citrix._ Go vote (for Microsoft) and make yourself heard by  leaving a comment. If you're attending VMworld in San Francisco, stop by the Microsoft booth (#1431). It'll be easy to miss us in the 10x10 booth ;-), so check back here and [our Twitter feed](https://twitter.com/virtualization "twitter feed") for more details on booth demos. Patrick