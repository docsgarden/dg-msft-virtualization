---
id: 56hKYQUxJtmMD2jHQg7TApz
title: >-
    Networking 101 for Disaster Recovery to Microsoft Azure using Site Recovery
desc: >-
    Learn the basics of networking for Disaster Recovery when using Microsoft Azure Site Recovery.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2014/20140909-networking-101-for-disaster-recovery-to-microsoft-azure-using-site-recovery
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 931f5d5afe9fc01acded74dfe88e822f19abf0f1c79342cc81681370c485c5c5
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140909-networking-101-for-disaster-recovery-to-microsoft-azure-using-site-recovery.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2014-09-09 03:29:00"
    ms.date: "09/09/2014"
    categories: "azure"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140909-networking-101-for-disaster-recovery-to-microsoft-azure-using-site-recovery.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2014/20140909-networking-101-for-disaster-recovery-to-microsoft-azure-using-site-recovery).

# Networking 101 for Disaster Recovery to Microsoft Azure using Site Recovery

Getting the networking requirements right is a critical piece in ensuring Disaster Recovery readiness of your business critical workloads.  When an administrator evaluates the disaster recovery capabilities that she needs for her application(s), she needs to think through and develop a robust networking infrastructure that ensures that the application is accessible to end users once it has been failed over and that the application downtime is minimized – RTO optimization is of key importance.

<!-- ![ ](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/E2ANetworking.png)](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/E2ANetworking.png)   -->


Head over to the Microsoft Azure blog to read our new blog that shows you how you can accomplish [Networking Infrastructure Setup for Microsoft Azure as a Disaster Recovery Site](https://aka.ms/virtualization_blog_e2a_networking). Using the example of a multi-tier application we show you how to setup the required networking infrastructure, establish network connectivity between on-premises and Azure and then conclude the post with more details on Test Failover and Planned Failover.