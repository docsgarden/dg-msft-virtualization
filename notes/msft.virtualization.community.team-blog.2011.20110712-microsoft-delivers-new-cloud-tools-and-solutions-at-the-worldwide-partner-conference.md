---
id: PkanSesAK34xuiFjqe59k9M
title: >-
    Microsoft Delivers New Cloud Tools and Solutions at the Worldwide Partner Conference
desc: >-
    Microsoft Delivers New Cloud Tools and Solutions at the Worldwide Partner Conference
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110712-microsoft-delivers-new-cloud-tools-and-solutions-at-the-worldwide-partner-conference
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: bf846af5d9a1b38832c871706871ec08786746985425af7e1d13c4bf678fb047
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110712-microsoft-delivers-new-cloud-tools-and-solutions-at-the-worldwide-partner-conference.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "07/12/2011"
    date: "2011-07-12 12:10:16"
    categories: "microsoft-system-center-desktop-error-monitoring"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110712-microsoft-delivers-new-cloud-tools-and-solutions-at-the-worldwide-partner-conference.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110712-microsoft-delivers-new-cloud-tools-and-solutions-at-the-worldwide-partner-conference).

# Microsoft Delivers New Cloud Tools and Solutions at the Worldwide Partner Conference

Cloud computing is as big a transformation, and opportunity, as the technology industry has ever seen. Partners and customers can look to Microsoft for the most comprehensive cloud strategy and offerings, in order to improve their business agility, focus and economics. Today, at the Worldwide Partner Conference, Microsoft announced tools and solutions to help partners capitalize on the opportunities, as well as examples of partners and customers already finding success.

[Read more](http://bit.ly/nUXFis)