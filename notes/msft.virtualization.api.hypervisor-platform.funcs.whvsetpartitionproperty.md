---
id: 38aM59z4abMdNG98tExRAUT
title: >-
    WHvSetPartitionProperty
desc: >-
    Understaning how to work with WHvSetPartitionProperty and its parameters, syntax, and return value
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvSetPartitionProperty
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 5ae77a000a71cdfb6ed3ab2f0092cb5aef12a868798d927f1746d84116460260
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvSetPartitionProperty.md
    author: "Juarezhm"
    ms.author: "hajuarez"
    ms.date: "05/31/2019"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvSetPartitionProperty.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvSetPartitionProperty).

# WHvSetPartitionProperty


## Syntax
```C
HRESULT
WINAPI
WHvSetPartitionProperty(
    _In_ WHV_PARTITION_HANDLE Partition,
    _In_ WHV_PARTITION_PROPERTY_CODE PropertyCode,
    _In_reads_bytes_(PropertyBufferSizeInBytes) const VOID* PropertyBuffer,
    _In_ UINT32 PropertyBufferSizeInBytes
    );
```
### Parameters

`Partition`

Handle to the partition object. 

`PropertyCode`

Specifies the property that is being set.

`PropertyBuffer`

Specifies the input buffer that provides the property value. 

`PropertyBufferSizeInBytes`

Specifies the size of the input buffer, in bytes. 

## Return Value

If the operation completed successfully, the return value is `S_OK`. 

The function returns `WHV_E_UNKNOWN_PROPERTY` for attempts to configure a property that is not available on the current system. 

The function returns `E_INVALIDARG` if the property cannot be modified in the current state of the partition, particularly for attempts to set a property prior to the [[`WHvSetupPartition`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-platform.funcs.whvsetuppartition]] function. Starting in Insider Preview Builds (19H2), the following properties can be modified after the [[`WHvSetupPartition`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-platform.funcs.whvsetuppartition]] function:
`WHvPartitionPropertyCodeExtendedVmExits`
`WHvPartitionPropertyCodeExceptionExitBitmap`
`WHvPartitionPropertyCodeX64MsrExitBitmap`
`WHvPartitionPropertyCodeCpuidExitList`