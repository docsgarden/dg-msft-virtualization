---
id: ErgSRGgtjbNnffUHGyueZEn
title: >-
    The GuestVirtualAddressToPhysicalAddress function
desc: >-
    Translates a virtual address to a physical address using information found in the guest's memory and processor's state.
canonicalUrl: https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/GuestVirtualAddressToPhysicalAddress
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: b0e7ace6967467189136c434681b60267a3694862d2565e9295173e8e2f0e082
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/GuestVirtualAddressToPhysicalAddress.md
    ms.date: "04/19/2022"
    author: "mattbriggs"
    ms.author: "mabrigg"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/GuestVirtualAddressToPhysicalAddress.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/GuestVirtualAddressToPhysicalAddress).

# GuestVirtualAddressToPhysicalAddress function

Translates a virtual address to a pysical address using information found in the guest's memory and processor's state.

## Syntax

```C
HRESULT
WINAPI
GuestVirtualAddressToPhysicalAddress(
    _In_    VM_SAVED_STATE_DUMP_HANDLE      VmSavedStateDumpHandle,
    _In_    UINT32                          VpId,
    _In_    const GUEST_VIRTUAL_ADDRESS     VirtualAddress,
    _Out_   GUEST_PHYSICAL_ADDRESS*         PhysicalAddress
    );
```

## Parameters

`VmSavedStateDumpHandle`

Supplies a handle to a dump provider instance.

`VpId`

Supplies the VP from where the virtual address is read.

`VirtualAddress`

Supplies the virtual address to translate.

`PhysicalAddress`

Returns the physical address assigned to the supplied virtual address.

## Return Value

If the operation completes successfully, the return value is `S_OK`.

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |