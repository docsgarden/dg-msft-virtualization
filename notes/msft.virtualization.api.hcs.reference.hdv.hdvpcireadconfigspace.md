---
id: 8osudsiP25U7ruJzpPwdPAf
title: >-
    HDV_PCI_READ_CONFIG_SPACE
desc: >-
    HDV_PCI_READ_CONFIG_SPACE
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciReadConfigSpace
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: ea7b36d4d771ca1798a624e9eb605264bb65188ada1eea14112c07ac27ac8736
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciReadConfigSpace.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HDV_PCI_READ_CONFIG_SPACE']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciReadConfigSpace.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciReadConfigSpace).

# HDV_PCI_READ_CONFIG_SPACE

## Description

Function called to execute a read into the emulated device's PCI config space.

## Syntax

```C++
typedef HRESULT (CALLBACK *HDV_PCI_READ_CONFIG_SPACE)(
    _In_opt_ void*   DeviceContext,
    _In_     UINT32  Offset,
    _Out_    UINT32* Value
    );
```

## Parameters

|Parameter|Description|
|---|---|---|---|---|---|---|---|
|`DeviceContext` |Context pointer that was supplied to HdvCreateDeviceInstance.|
|`Offset` |Offset in bytes from the base of the bar to read.|
|`Value` |Receives the read value.|
|    |    |

## Return Values

|Return Value     |Description|
|---|---|
|`S_OK` | Returned if function succeeds.|
|`HRESULT` | An error code is returned if the function fails.
|     |     |

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |