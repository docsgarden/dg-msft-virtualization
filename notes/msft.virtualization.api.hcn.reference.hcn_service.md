---
id: 8mwVYNxGn5RZzTAsaSgmsUM
title: >-
    HCN_SERVICE
desc: >-
    HCN_SERVICE
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_SERVICE
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 0353d0b70119d8dbb9cd52a6a2cebd5872d2db6208ca06fcb4c92782b098a061
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_SERVICE.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HCN_SERVICE']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_SERVICE.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_SERVICE).

# HCN\_SERVICE

## Description

Context handle referencing the HNS service.


## Syntax

```cpp
typedef void* HCN_SERVICE;
```


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |