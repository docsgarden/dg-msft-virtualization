---
id: MB9a7V5hCw83MGi33FkHZ7U
title: >-
    WHV_EMULATOR_STATUS method
desc: >-
    Learn about the WHV_EMULATOR_STATUS method.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorStatus
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 3f546b44ebf3e666a154110a583d5122ad542844e3d29735cbee8000a8607c28
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorStatus.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/19/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorStatus.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorStatus).

# WHV_EMULATOR_STATUS


## Syntax

```c
typedef union _WHV_EMULATOR_STATUS
{
    struct
    {
        UINT32 EmulationSuccessful : 1;
        UINT32 InternalEmulationFailure : 1;
        UINT32 IoPortCallbackFailed : 1;
        UINT32 MemoryCallbackFailed : 1;
        UINT32 TranslateGvaPageCallbackFailed : 1;
        UINT32 TranslateGvaPageCallbackGpaPageIsNotAligned : 1;
        UINT32 GetVirtualProcessorRegistersCallbackFailed : 1;
        UINT32 SetVirtualProcessorRegistersCallbackFailed : 1;
        UINT32 InterruptCausedIntercept : 1;
        UINT32 GuestCannotBeFaulted : 1;
        UINT32 Reserved : 21;
    };

    UINT32 AsUINT32;
} WHV_EMULATOR_STATUS;
```
## Remarks
Describes extended return status information from a given emulation call.