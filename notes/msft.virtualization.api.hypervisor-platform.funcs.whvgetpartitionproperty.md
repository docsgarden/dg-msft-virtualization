---
id: 3wGcsdwnc9ZvnBN8YnKaKwj
title: >-
    WHvGetPartitionProperty
desc: >-
    Explains different parameters and the return value for the GetPartitionProperty, including a syntax sample.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvGetPartitionProperty
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: a5d0cd759c7e718940b8416a792256244388c91333375c047a77156e7f9605b2
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvGetPartitionProperty.md
    author: "Juarezhm"
    ms.author: "hajuarez"
    ms.date: "07/18/2018"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvGetPartitionProperty.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvGetPartitionProperty).

# WHvGetPartitionProperty


## Syntax
```C
HRESULT
WINAPI
WHvGetPartitionProperty(
    _In_ WHV_PARTITION_HANDLE Partition,
    _In_ WHV_PARTITION_PROPERTY_CODE PropertyCode,
    _Out_writes_bytes_to_(PropertyBufferSizeInBytes, *WrittenSizeInBytes) VOID* PropertyBuffer,
    _In_ UINT32 PropertyBufferSizeInBytes,
    _Out_opt_ UINT32 *WrittenSizeInBytes
    );
```
### Parameters

`Partition`

Handle to the partition object.

`PropertyCode`

Specifies the property that is queried. `WHvPartitionPropertyCodeCpuidExitList` and `WHvPartitionPropertyCodeCpuidResultList` are not supported.

`PropertyBuffer`

Specifies the output buffer that receives the value of the requested property. 

`PropertyBufferSizeInBytes`

Specifies the size of the output buffer, in bytes. For the currently available set of properties, the buffer should be large enough to hold a 64-bit value.

`WrittenSizeInBytes`

Receives the written size in bytes of the `PropertyBuffer`.

## Return Value
If the operation completed successfully, the return value is `S_OK`.

The function returns `WHV_E_UNKNOWN_PROPERTY` if an unknown `PropertyCode` is requested or `ERROR_NOT_SUPPORTED` when called with an unsupported property code.