---
id: 5QXC4zASWRwg5RrjZgtsAc9
title: >-
    TechEd Europe 2010 Day 1&#58; Voices from the Community
desc: >-
    We\’ve been seeing quite a bit of chatter on the \“internets\” about the goings-on at TechEd Europe 2010.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20101109-teched-europe-2010-day-1-voices-from-the-community
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: a9c7fb1b1f2ce1fa9aae64e4b0981ec4346a2809d7711102e7e1169c30450aaf
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20101109-teched-europe-2010-day-1-voices-from-the-community.md
    author: "scooley"
    ms.author: "scooley"
    date: "2010-11-09 18:56:33"
    ms.date: "11/09/2010"
    categories: "blogs"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20101109-teched-europe-2010-day-1-voices-from-the-community.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20101109-teched-europe-2010-day-1-voices-from-the-community).

# TechEd Europe 2010 Day 1: Voices from the Community

We’ve been seeing quite a bit of chatter on the “internets” about the goings-on at [TechEd Europe 2010](https://www.microsoft.com/europe/teched) in Berlin, and it’s great to see the diverse impressions of attendees as the event happens. 

##### Net Noteworthies

The announcements and news about a range of things—including [Hyper-V Cloud](https://www.microsoft.com/virtualization/en/us/private-cloud.aspx), the [VMM SSP 2.0](https://www.microsoft.com/systemcenter/en/us/virtual-machine-manager/vmm-self-service-portal.aspx) release, and RC availability of [Forefront Endpoint Protection 2010](https://www.microsoft.com/fep)—garnered comment and tweets aplenty. Here are a few from the community that stood out for us among the hubbub of official announcements and journalist/analyst reports (in no particular order)…. [Check out the full post on the System Center Nexus blog for the rest of the story](https://blogs.technet.com/b/systemcenter/archive/2010/11/10/teched-europe-2010-day-1-voices-from-the-community.aspx). 

\- Server & Cloud Platform Team