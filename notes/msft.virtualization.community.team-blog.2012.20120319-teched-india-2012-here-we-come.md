---
id: 8LM8bhZDjfQbhiukgKT2mAe
title: >-
    TechEd India 2012, here we come!
desc: >-
    TechEd India 2012, here we come!
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2012/20120319-teched-india-2012-here-we-come
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 0485b170e52ad7048e74994a7c39c1af4c5bcc659a2ea888caad55b9c3c5fc65
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2012/20120319-teched-india-2012-here-we-come.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "03/19/2012"
    date: "2012-03-19 04:57:00"
    categories: "hvr"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2012/20120319-teched-india-2012-here-we-come.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2012/20120319-teched-india-2012-here-we-come).

# TechEd India 2012, here we come!

If you are in Bangalore, India between March 21st and March 23rd, visit us at [TechEd, India](http://india.msteched.com/ "http://india.msteched.com/")! We have a [product tent](http://india.msteched.com/#events "http://india.msteched.com/#events") and a booth for Hyper-V Replica - drop in, meet us and learn more about HVR. 

\- Praveen