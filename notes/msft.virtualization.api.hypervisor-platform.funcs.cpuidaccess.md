---
id: 6wnf5HQmytA6uRdbEKygXMM
title: >-
    CPUID access call
desc: >-
    Learn about context data for an exit caused by CPUID call.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/CPUIDAccess
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: cda45149c7df501bd3245b6650fd3b359622d98d8237a28627fc4bb3beae6aa6
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/CPUIDAccess.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/20/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/CPUIDAccess.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/CPUIDAccess).

# CPUID Access


## Syntax
```C
//
// Context data for an exit caused by a CPUID call (WHvRunVpExitReasonX64CPUID)
//
typedef struct WHV_X64_CPUID_ACCESS_CONTEXT
{
    // CPUID access info
    UINT64 Rax;
    UINT64 Rcx;
    UINT64 Rdx;
    UINT64 Rbx;
    UINT64 DefaultResultRax;
    UINT64 DefaultResultRcx;
    UINT64 DefaultResultRdx;
    UINT64 DefaultResultRbx;
} WHV_X64_CPUID_ACCESS_CONTEXT;
```

## Return Value
Information about exits caused by the virtual processor executing the `CPUID` instruction is provided in the `WHV_X64_CPUID_ACCESS_CONTEXT` structure. The `DefaultResultRax-Rbx` members of the structure provide the values of the requested `CPUID` values that the hypervisor would return based on the partition properties and the capabilities of the host.  

Exits for `CPUID` accesses are only generated if they are enabled by setting the `WHV_EXTENDED_VM_EXITS.CpuidExit` property for the partition.