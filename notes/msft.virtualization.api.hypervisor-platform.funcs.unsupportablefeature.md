---
id: 8HGqXAA2oaZoUprNzW4iRRy
title: >-
    Exit caused by an unsupported processor feature
desc: >-
    Learn about context data for an exit caused by an unsupported processor feature.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/UnsupportableFeature
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: deeb933ea9df7fe4485ed3fb3172f3f6d5b45eef4c455fc5a12810cdf8ef71c8
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/UnsupportableFeature.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/20/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/UnsupportableFeature.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/UnsupportableFeature).

# Unsupported Feature


## Syntax
```C
//
// Context data for an exit caused by the use of an unsupported processor feature
// (WHvRunVpExitReasonUnsupportedFeature)
//
typedef enum WHV_X64_UNSUPPORTED_FEATURE_CODE
{
    WHvUnsupportedFeatureIntercept     = 1,
    WHvUnsupportedFeatureTaskSwitchTss = 2
} WHV_X64_UNSUPPORTED_FEATURE_CODE;

typedef struct WHV_X64_UNSUPPORTED_FEATURE_CONTEXT
{
    WHV_X64_UNSUPPORTED_FEATURE_CODE FeatureCode;
    UINT64 FeatureParameter;
} WHV_X64_UNSUPPORTED_FEATURE_CONTEXT;
```

## Return Value
An exit for an unsupported feature is caused by the virtual processor accesses a feature of the architecture that is not properly virtualized by the hypervisor.