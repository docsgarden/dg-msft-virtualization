---
id: 6M4q6pfbrK9nokXjDfQE8XZ
title: >-
    HcsGetComputeSystemProperties
desc: >-
    HcsGetComputeSystemProperties
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetComputeSystemProperties
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 2e206c3ec18ef7952b84895f54d8026b011c0d303c2dca36d9fcd632f09cc69f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetComputeSystemProperties.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsGetComputeSystemProperties']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetComputeSystemProperties.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetComputeSystemProperties).

# HcsGetComputeSystemProperties

## Description

Returns properties of a compute system, see [[sample code|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.tutorial]] for simple example.

## Syntax

```cpp
HRESULT WINAPI
HcsGetComputeSystemProperties(
    _In_  HCS_SYSTEM computeSystem,
    _In_  HCS_OPERATION operation,
    _In_opt_ PCWSTR propertyQuery
    );
```

## Parameters

`computeSystem`

The handle to the compute system to query.

`operation`

The handle to the operation that tracks the query operation.

`propertyQuery`

Optional JSON document of [[System_PropertyQuery|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#System_PropertyQuery]] specifying the properties to query.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

If the return value is `S_OK`, it means the operation started successfully. Callers are expected to get the operation's result using [[`HcsWaitForOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]] or [[`HcsGetOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]]

## Operation Results

The return value of [[`HcsWaitForOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]] or [[`HcsGetOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]] based on current operation listed as below.

| Operation Result Value | Description |
| -- | -- |
| `S_OK` | The operation was finished successfully, the result document returned by the hcs operation is a JSON document representing a compute system's [[Properties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#Properties]]. Compute system Id and [[SystemType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#SystemType]] will always be returned.|

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |