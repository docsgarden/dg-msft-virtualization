---
id: 5tdv7mnihbut5V3bg4Gq8nm
title: >-
    Microsoft Management Summit&#58;  Helping IT Organizations Empower Employee Productivity
desc: >-
    Microsoft Management Summit;  Helping IT Organizations Empower Employee Productivity
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110323-microsoft-management-summit-helping-it-organizations-empower-employee-productivity
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 3c1a4dfb52234f6396aed993992e4bb84abf05f6c66d9e3402e11719d443b298
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110323-microsoft-management-summit-helping-it-organizations-empower-employee-productivity.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "03/23/2011"
    date: "2011-03-23 01:30:00"
    categories: "system-center-2012"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110323-microsoft-management-summit-helping-it-organizations-empower-employee-productivity.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110323-microsoft-management-summit-helping-it-organizations-empower-employee-productivity).

# Microsoft Management Summit;  Helping IT Organizations Empower Employee Productivity

Today Microsoft announces new IT solutions to streamline PC and device management, empower productivity and enable the modern enterprise, highlighting the release of Windows Intune, [System Center Configuration Manager 2012 Beta 2](https://technet.microsoft.com/evalcenter/ff657840.aspx) and the next version of Microsoft Desktop Optimization Pack (MDOP). 

See the full [press release here](https://www.microsoft.com/presspass/presskits/cloud/default.aspx) \- and download [System Center Configuration Manager 2012 Beta 2](https://technet.microsoft.com/evalcenter/ff657840.aspx) today!