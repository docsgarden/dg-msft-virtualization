---
id: AppuyS8EyJZg2CT6zVodjWp
title: >-
    Host Compute System Overview
desc: >-
    Host Compute System Overview
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Overview
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: c4380545a9837f06a806fbb806e288610b49150d9f54069ff6eab1923aaf6bed
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Overview.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['Host Compute System Overview']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Overview.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Overview).

# Host Compute System Overview

>**Applies to: Windows Server 2019, Windows Server 2022, Azure Stack HCI 20H2, Azure Stack 21H2, Microsoft Windows 10 October 2018 Update**

## What is the HCS?
The Host Compute System API provides the functionality to start and control VMs and containers. 

A compute system represents a VM or container that was created through the API. Operations are performed on handles to a compute system. 
Compute systems are ephemeral, meaning that once a VM or container stopped (e.g., a VM was shut down), the compute system is cleaned up and no further operations can be executed on existing handles to the compute system (the only exception is the ability to query the exit status of the compute system). It is the application’s responsibility to create a new compute system to start the VM or container again. 

The operations supported on compute systems include creating, starting and stopping a VM or container. For VMs, additional operations such pause, resume, save and restore are supported. 

For both VMs and containers, the API provides the ability to start and interact with process in the compute system. For containers, this functionality is the primary way that applications create and interact with the workload in the container. 

## Life Cycle Management
A client must configure the necessary resources in the host environment prior to calling the HCS APIs to create and configure the virtual machine. The HCS will not provide functionality to setup resources when creating the virtual machine.  

The three main components include creating the virtual hard disk file (VHD) to act as the VM's disk, [configuring the networking](https://docs.microsoft.com/en-us/windows-server/networking/technologies/hcn/hcn-top), and [creating any Hyper-V sockets](https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/user-guide/make-integration-service#create-a-hyper-v-socket). The application configurations and properties will be stored in a JSON file which will then be passed through the HCS APIs to create the compute system. The following sections describes the necessary components and workflow. 

## API References

For a list of the HCS functions, please see [[API Reference|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.apioverview]].

## JSON Schema

The overview of Schema-based configuration documents is in [[JSON Schema Overview|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemaoverview]]

For a list of JSON Schema References, please see [[Schema References|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference]].

For JSON Schema file used to generate other languages and the example of generated Schema code, please see [[Schema Sample|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemasample]]. 

## Samples

[[Sample codes|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.tutorial]] lists the simple workflow by using HCS API to create and manage the virtual machine.

The sample code of how callback functions are used as both synchronous and asynchronous way shows in [[Operation Completion Samples|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.operationcompletionsample]]