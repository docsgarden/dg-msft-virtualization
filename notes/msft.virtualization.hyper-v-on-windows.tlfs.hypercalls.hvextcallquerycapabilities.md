---
id: 3X4qDgrjtEs4nAMzMdK2CNy
title: >-
    HvExtCallQueryCapabilities
desc: >-
    HvExtCallQueryCapabilities hypercall
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvExtCallQueryCapabilities
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: a376c04da367a3adbc5313a579c0ffbe1d5a712c0e981d06bf12a4bf5e1b929c
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvExtCallQueryCapabilities.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvExtCallQueryCapabilities.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvExtCallQueryCapabilities).

# HvExtCallQueryCapabilities

This hypercall reports the availability of extended hypercalls.

The availability of this hypercall must be queried using the EnableExtendedHypercalls partition privilege flag.

## Interface

 ```c
HV_STATUS
HvExtCallQueryCapabilities(
    _Out_ UINT64 Capabilities
    );
 ```

## Call Code

`0x8001` (Simple)

## Output Parameters

| Name                    | Offset     | Size     | Information Provided                      |
|-------------------------|------------|----------|-------------------------------------------|
| `Capabilities`          | 0          | 8        | Bitmask of supported extended hypercalls                          |

The bitmask of supported extended hypercalls has the following format:

| Bit     | Extended Hypercall                                          |
|---------|-------------------------------------------------------------|
| 0       | HvExtCallGetBootZeroedMemory                                |
| 1       | HvExtCallMemoryHeatHint                                     |
| 2       | HvExtCallEpfSetup                                           |
| 3       | HvExtCallSchedulerAssistSetup                               |
| 4       | HvExtCallMemoryHeatHintAsync                                |
| 63-5    | Reserved                                                    |