---
id: 4uEUCXY75a35fg8cAKtdPv7
title: >-
    HcsGetProcessFromOperation
desc: >-
    HcsGetProcessFromOperation
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetProcessFromOperation
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 99062014d06f10cfd35f4d2d91d6dd8d7ab6f3b927f23e28c4191d1efd237688
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetProcessFromOperation.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsGetProcessFromOperation']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetProcessFromOperation.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetProcessFromOperation).

# HcsGetProcessFromOperation

## Description

Returns the handle to the process associated with an operation.

## Syntax

```cpp
HCS_PROCESS WINAPI
HcsGetProcessFromOperation(
    _In_ HCS_OPERATION operation
    );

```

## Parameters

`operation`
The handle to an active operation.

## Return Values

If the function succeeds, the return value is `HCS_PROCESS`.

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |