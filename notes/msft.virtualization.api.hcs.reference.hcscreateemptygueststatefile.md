---
id: 4ppNLvUUsEeiDTBXSaUHgT3
title: >-
    HcsCreateEmptyGuestStateFile
desc: >-
    HcsCreateEmptyGuestStateFile
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsCreateEmptyGuestStateFile
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: ae78d4c95977d54fbfc978bbc02fbdfaa375c1b867806104167e15f2fcd546a7
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsCreateEmptyGuestStateFile.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsCreateEmptyGuestStateFile']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsCreateEmptyGuestStateFile.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsCreateEmptyGuestStateFile).

# HcsCreateEmptyGuestStateFile

## Description

This function creates an empty guest-state file (.vmgs) for a VM. This file is required in cases where the VM is expected to be persisted and restarted multiple times. It is configured in the 'GuestState' property of the settings document for a VM. See [[sample code|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.utilityfunctionsample#createfilesgrantaccess]].

## Syntax

```cpp
HRESULT WINAPI
HcsCreateEmptyGuestStateFile(
    _In_ PCWSTR guestStateFilePath
    );
```

### Parameters

`guestStateFilePath`

Full path to the guest-state file to create.

### Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |