---
id: 4nKwhMK6dVdkTqJPZAhQ4pQ
title: >-
    HDV_DEVICE_TYPE
desc: >-
    HDV_DEVICE_TYPE
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvDeviceType
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 4a8d78dec26e170171960da8bfac35d0c403886f44e03c4cf5bccd141b0a1659
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvDeviceType.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HDV_DEVICE_TYPE']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvDeviceType.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvDeviceType).

# HDV_DEVICE_TYPE

## Description

Discriminator for the Emulated device type.

## Syntax

```C++
typedef enum HDV_DEVICE_TYPE
{
    HdvDeviceTypeUndefined = 0,
    HdvDeviceTypePCI
} HDV_DEVICE_TYPE;
```

## Constants

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| HdvDeviceTypeUndefined | null |
| HdvDeviceTypePCI | null |
|    |    |

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |