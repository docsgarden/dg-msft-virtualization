---
id: 9XRZ6mPT8YmSTfPiy9qLuKs
title: >-
    How to give us feedback
desc: >-
    Find out how to send us feedback to continue to improve your virtual machine management.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2017/20170210-how-to-give-us-feedback
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: def37f670e6b9f961e9ce720ba1a894c81eba596ce15d43ace826b568918acdc
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2017/20170210-how-to-give-us-feedback.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2017-02-10 21:21:24"
    ms.date: "02/10/2017"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2017/20170210-how-to-give-us-feedback.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2017/20170210-how-to-give-us-feedback).

# How to give us feedback

We love hearing from you. So what's the best way to give us feedback? The best way to report an issue or give a quick suggestion is the Feedback Hub on Windows 10 (Windows key + F to open it quickly). The feedback hub lets the product team see all of your feedback in one place, and allows other users to upvote and provide further comments. It's also tightly integrated with our bug tracking and engineering processes, so that we can keep an eye on what users are saying and use this data to help prioritize fixes and feature requests, and so that you can follow up and see what we're doing about it. In the latest build, we have reintroduced the Hyper-V feedback category. After typing your feedback, selecting "Show category suggestions" should help you find the Hyper-V category under Apps and Games. It looks like a couple people have already discovered the new category:   ![Hyper-V feedback](https://msdnshared.blob.core.windows.net/media/2017/02/feedback10.png)](https://msdnshared.blob.core.windows.net/media/2017/02/feedback10.png) When you put your feedback in the Hyper-V category, we are also able to collect relevant event logs to help diagnose issues. To provide more information about a problem that you can reproduce, hit "begin monitoring", reproduce the issue, and then "stop monitoring". This allows us to collect relevant diagnostic information to help reproduce and fix the problem. ![Begin monitoring](https://msdnshared.blob.core.windows.net/media/2017/02/feedback11.png)](https://msdnshared.blob.core.windows.net/media/2017/02/feedback11.png) We also love to hear from you in our forums if there are any issues you are running into. This is a good place to get direct help from the product group as well as community members. [Hyper-V Forums](https://social.technet.microsoft.com/Forums/windowsserver/en-US/home?forum=winserverhyperv) ![Hyper-V Forums](https://msdnshared.blob.core.windows.net/media/2017/02/feedback12.png)](https://msdnshared.blob.core.windows.net/media/2017/02/feedback12.png) That's all for now. Looking forward to seeing your feedback! Cheers, Andy