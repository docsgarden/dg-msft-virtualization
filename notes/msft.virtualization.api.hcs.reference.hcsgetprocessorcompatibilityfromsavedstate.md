---
id: 72FdAtBcLhyPR3bt2nAWRqd
title: >-
    HcsGetProcessorCompatibilityFromSavedState
desc: >-
    HcsGetProcessorCompatibilityFromSavedState
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetProcessorCompatibilityFromSavedState
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 8438229d5bc5fd51027bc387608be48aecf66070ffbef5e85d3a00d10664cf7f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetProcessorCompatibilityFromSavedState.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "12/21/2021"
    api_name: "['HcsGetProcessorCompatibilityFromSavedState']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetProcessorCompatibilityFromSavedState.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetProcessorCompatibilityFromSavedState).

# HcsGetProcessorCompatibilityFromSavedState

## Description

Returns processor compatibility fields in JSON string format.

## Syntax

```cpp
HRESULT WINAPI
HcsGetProcessorCompatibilityFromSavedState(
    PCWSTR RuntimeFileName,
    _Outptr_opt_ PCWSTR* ProcessorFeaturesString
    );
```

## Parameters

`RuntimeFileName`

The path to the vmrs file.

`ProcessorFeaturesString`

JSON document of the processor compatibilities as [[VmProcessorRequirements|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#VmProcessorRequirements]].

The caller is responsible for releasing the returned string using [`LocalFree`](https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-localfree).

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 2104|
| **Minimum supported server** | Windows Server 2022 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |