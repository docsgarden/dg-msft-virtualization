---
id: 4WvtcntB23gidoKweGdTUF9
title: >-
    Microsoft Virtualization&#58; Best Choice for MS Server Applications
desc: >-
    Microsoft Virtualization; Best Choice for MS Server Applications
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090902-microsoft-virtualization-best-choice-for-ms-server-applications
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 6420f4e3e0432fd94489fb36dece4db105b40e778e6d95837659b1ee7dc0307a
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090902-microsoft-virtualization-best-choice-for-ms-server-applications.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "09/02/2009"
    date: "2009-09-02 12:21:00"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090902-microsoft-virtualization-best-choice-for-ms-server-applications.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090902-microsoft-virtualization-best-choice-for-ms-server-applications).

# Microsoft Virtualization; Best Choice for MS Server Applications

Hello, I’m Zane Adam, general manager of Virtualization and Systems Management.  As more and more customers are looking at virtualization to help reduce cost and decrease complexity in their infrastructure, we are seeing lots of questions around virtualizing MS server applications such as Exchange, SQL and SharePoint.  What benefits should I expect to see? What do I need to consider when virtualizing these mission critical business application?  Should I virtualize all roles within a server application? 

 

We have worked across MS application groups to test our products with hyper-v and we believe that MS virtualization is the best choice for MS server applications.  First, MS server applications are built for Windows Server, and because hyper-v is part of the operating system, there is no need to add complexity of support and interoperability by adding a 3rd party virtualization layer.  This means there is just one stop for all of your support needs and also assurance that the stack of applications and hypervisor works well together.  Second, to help ensure optimal performance Microsoft has specific deployment guidance for virtualizing MS server applications which results in a mixed physical and virtual environment.  System Center is the management solution that allows you to view both physical and virtual instances from one console.  Third, as you know MS virtualization is up to six times less expensive than other virtualization solutions in the market, which makes running your server applications even that more affordable.

 

The folks who developed the MS server applications have developed specific guidance on virtualizing individual applications based on deployment scenarios.  You can hear more thoughts from them on the following blogs:

Exchange Server: [https://msexchangeteam.com/](https://msexchangeteam.com/)

SQL Server: [https://blogs.technet.com/dataplatforminsider/default.aspx](https://blogs.technet.com/dataplatforminsider/default.aspx)

SharePoint: [https://blogs.msdn.com/sharepoint](https://techcommunity.microsoft.com/t5/microsoft-sharepoint-blog/bg-p/SPBlog)

 

Also, for more information please visit    [www.microsoft.com/virtualization/solutions/business-critical-applications](https://www.microsoft.com/virtualization/solutions/business-critical-applications/default.mspx) . 

Thanks-

 

Zane