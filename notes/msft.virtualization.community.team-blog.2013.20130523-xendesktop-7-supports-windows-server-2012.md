---
id: HqDeK9JZoENjVhEQsNPrdnC
title: >-
    XenDesktop 7 Supports Windows Server 2012
desc: >-
    XenDesktop 7 Supports Windows Server 2012
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2013/20130523-xendesktop-7-supports-windows-server-2012
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 96548634cf68a31be5e9782d5bf042ee05b09e9b85c837aaad6cf3e7383799e5
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20130523-xendesktop-7-supports-windows-server-2012.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "05/23/2013"
    date: "2013-05-23 09:00:00"
    categories: "citrix"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20130523-xendesktop-7-supports-windows-server-2012.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2013/20130523-xendesktop-7-supports-windows-server-2012).

# XenDesktop 7 Supports Windows Server 2012

We are excited to see [the release of XenDesktop 7](http://www.citrix.com/news/announcements/may-2013/citrix-extends-enterprise-mobility-strategy-with-xendesktop-7.html) and support our partner Citrix. XenDesktop 7 brings together both XenApp and XenDesktop functionality into a common release and now brings support for Windows Server 2012.  XenDesktop can easily be deployed on [Hyper-V](https://www.microsoft.com/en-us/server-cloud/windows-server/server-virtualization.aspx?WT.mc_id=BlogVirt_HyperV_WS2012) and take full advantage of Windows Server 2012 to increase agility, reduce cost, and provide a scalable and robust platform for desktop virtualization.