---
id: 6Rw2bcZJBaEbwAyiRWEzboA
title: >-
    Windows Server Containers and Hyper-V Containers Debut at Ignite and Build
desc: >-
    Learn about supported Ignite and Build sessions for Windows Server Containers and Hyper-V Containers.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2015/20150511-windows-server-containers-and-hyper-v-containers-debut-at-ignite-and-build
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: a1b54395c44ce0ce923f1023f882e531e1cfcdee96789e7cf3bbd7a8ceada823
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2015/20150511-windows-server-containers-and-hyper-v-containers-debut-at-ignite-and-build.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2015-05-11 16:34:38"
    ms.date: "05/11/2015"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2015/20150511-windows-server-containers-and-hyper-v-containers-debut-at-ignite-and-build.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2015/20150511-windows-server-containers-and-hyper-v-containers-debut-at-ignite-and-build).

# Windows Server Containers and Hyper-V Containers Debut at Ignite and Build

Build and Ignite were packed with information about Windows Server Containers (announced in October) and Hyper-V Containers (announced in April).  We also launched the official Windows containers documentation site here:  
[https://msdn.microsoft.com/virtualization/windowscontainers/](https://msdn.microsoft.com/virtualization/windowscontainers/)  
Ignite and Build sessions as well as Channel9 interviews are available on the front page and in the Community section.   Keep checking back in for the latest announcements, community tools, and information as we move forward. Right now you can read about Windows containers, the container ecosystem, and our most frequently asked questions. Cheers,  
Sarah