---
id: 8tAF3ZwjyvsHL4qgZMasJ4D
title: >-
    Hyper-V Network Command Line Tool NVSPBIND Now Available Externally
desc: >-
    Hyper-V Network Command Line Tool NVSPBIND Now Available Externally
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100125-hyper-v-network-command-line-tool-nvspbind-now-available-externally
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: fc81bd49cd6ecf7f69732a42a903c725e6adc4a3a52fbcabb22967b314d28f67
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100125-hyper-v-network-command-line-tool-nvspbind-now-available-externally.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "01/25/2010"
    date: "2010-01-25 16:51:00"
    categories: "community"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100125-hyper-v-network-command-line-tool-nvspbind-now-available-externally.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100125-hyper-v-network-command-line-tool-nvspbind-now-available-externally).

# Hyper-V Network Command Line Tool NVSPBIND Now Available Externally

Hello all,

 

I’m Paul Despe and I work as a program manager on the Microsoft Hyper-V team.

 

Until now, Hyper-V administrators using Server Core installs of Windows Server 2008 or Microsoft Hyper-V Server were unable to enable and disable protocol bindings from the parent partition due to the lack of a graphical network control panel only available in full installs of Windows Server.

 

I'm pleased to announce one of our developers on the Hyper-V team, Keith Mange, recently addressed this gap with the release of his (previously Microsoft internal-only) networking tool NVSPBIND.EXE publicly on. See it [here](https://code.msdn.microsoft.com/nvspbind "tool on MSDN"). 

< p style="MARGIN: 0in 0in 0pt" class="MsoNormal">   

This command line tool gives users easier granular control over network items they want enabled for their physical NICs and helps fills a gap in troubleshooting, optimizing and hardening networking configurations on Server Core installs or Hyper-V Server.

 

See John Howard’s blog [[here](https://docs.microsoft.com/en-us/archive/blogs/jhoward/announcing-nvspbind "John Howard blog")] for more details:

  Paul Despe