---
id: 5CQWqUe5WpRk43LXGGBZuGF
title: >-
    HvCallEnablePartitionVtl
desc: >-
    HvCallEnablePartitionVtl hypercall
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallEnablePartitionVtl
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 93396a8fc29a07648f9a16612f638f1e7471412ce3ce3683a875237ac2395319
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallEnablePartitionVtl.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallEnablePartitionVtl.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallEnablePartitionVtl).

# HvCallEnablePartitionVtl

The HvCallEnablePartitionVtl hypercall enables a virtual trust level for a specified partition. It should be used in conjunction with HvCallEnableVpVtl to initiate and use a new VTL.

## Interface

 ```c

typedef union
{
    UINT8 AsUINT8;
    struct {
        UINT8 EnableMbec:1;
        UINT8 Reserved:7;
    };
} HV_ENABLE_PARTITION_VTL_FLAGS;

HV_STATUS
HvCallEnablePartitionVtl(
    _In_ HV_PARTITION_ID TargetPartitionId,
    _In_ HV_VTL TargetVtl,
    _In_ HV_ENABLE_PARTITION_VTL_FLAGS Flags
    );
 ```

### Restrictions

- A launching VTL can always enable a target VTL if the target VTL is lower than the launching VTL.
- A launching VTL can enable a higher target VTL if the launching VTL is the highest VTL enabled for the partition that is lower than the target VTL.

## Call Code

`0x000D` (Simple)

## Input Parameters

| Name                    | Offset     | Size     | Information Provided                      |
|-------------------------|------------|----------|-------------------------------------------|
| `TargetPartitionId`     | 0          | 8        | Supplies the partition ID of the partition this request is for. |
| `TargetVtl`             | 8          | 1        | Specifies the VTL to be enabled by this hypercall. |
| `Flags`                 | 9          | 1        | Specifies a mask to enable VSM related features.|
| RsvdZ                   | 10         | 6        |                                           |