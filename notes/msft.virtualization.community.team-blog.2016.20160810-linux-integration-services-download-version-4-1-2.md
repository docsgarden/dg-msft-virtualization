---
id: 636Wqzs9EdeKQ4prQR67Brh
title: >-
    Linux Integration Services download Version 4.1.2
desc: >-
    Blog post that announces the availability of Linux Integration Services 4.1.2 and provides resources with information about the new versions.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2016/20160810-linux-integration-services-download-version-4-1-2
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: e60ab91d4658ed993cfc50649ef4d9747db836e641ec1b4a7ac4b1a4673af1f3
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2016/20160810-linux-integration-services-download-version-4-1-2.md
    author: "scooley"
    ms.author: "scooley"
    date: "2016-08-10 17:04:38"
    ms.date: "07/31/2019"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2016/20160810-linux-integration-services-download-version-4-1-2.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2016/20160810-linux-integration-services-download-version-4-1-2).

# Linux Integration Services download Version 4.1.2

We are pleased to announce the availability of Linux Integration Services (LIS) 4.1.2. This point release of the LIS download expands supported releases to Red Hat Enterprise Linux, CentOS, and Oracle Linux with Red Hat Compatible Kernel 6.8. This release also includes upstream bug fixes and performance improvements not included in previous LIS downloads. See the separate PDF file "Linux Integration Services v4-1c.pdf" for more information. The LIS download is an optional way to get Linux Integration Services updates for certain versions of Linux. To determine if you want to download LIS refer to the blog post ["Which Linux Integration Services should I use in my Linux VMs?"](https://blogs.technet.microsoft.com/virtualization/2016/07/12/which-linux-integration-services-should-i-use-in-my-linux-vms/) **Download Location** The Linux Integration Services download is available either as a disk image (ISO) or gzipped tar file. The disk image can be attached to a virtual machine, or the tar file can upload and expanded to install these kernel modules. Refer to the instruction PDF available separately from the download named "Linux Integration Services v4-1c.pdf" <https://www.microsoft.com/en-us/download/details.aspx?id=51612> **Linux Integration Services documentation** See also the TechNet article “Linux and FreeBSD Virtual Machines on Hyper-V” for a comparison of LIS features and best practices for use here: <https://technet.microsoft.com/library/dn531030.aspx> **Source Code** Linux Integration Services code is open source released under the GNU Public License version 2 (GPLv2) and is freely available at the LIS GitHub project here: <https://github.com/LIS> and in the upstream Linux kernel: <https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/log/>