---
id: 3eTdNaYYoa784v7YgzEHx2z
title: >-
    Create a new virtual processor in a partition
desc: >-
    Learn about the WHvCreateVirtualProcessor function that creates a new virtual processor in a partition.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvCreateVirtualProcessor
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: ecae791fe1c8934b143a803fa48bf0bbd046156e5725bd637e91ba53151083e8
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvCreateVirtualProcessor.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/20/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvCreateVirtualProcessor.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvCreateVirtualProcessor).

# WHvCreateVirtualProcessor

## Syntax

```C
HRESULT
WINAPI
WHvCreateVirtualProcessor(
    _In_ WHV_PARTITION_HANDLE Partition,
    _In_ UINT32 VpIndex,
    _In_ UINT32 Flags
    );
```

### Parameters

`Partition`

Handle to the partition object

`VpIndex`

 Specifies the index of the new virtual processor

`Flags`

Unused, must be zero
  

## Remarks

The `WHvCreateVirtualProcessor` function creates a new virtual processor in a partition. The index of the virtual processor is used to set the APIC ID of the processor.