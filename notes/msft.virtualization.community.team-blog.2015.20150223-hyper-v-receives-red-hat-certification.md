---
id: APSyktPCRaHebmGGZr5pgSS
title: >-
    Hyper-V receives Red Hat certification
desc: >-
    Learn about how Hyper-V now supports Red Hat certificates.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2015/20150223-hyper-v-receives-red-hat-certification
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 32f9085e3c5cd46c065e4c476713fe87617e8812f0d63d5315312f302c3c6b21
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2015/20150223-hyper-v-receives-red-hat-certification.md
    author: "kelleymh"
    ms.author: "mikelley"
    date: "2015-02-23 09:25:00"
    ms.date: "02/23/2015"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2015/20150223-hyper-v-receives-red-hat-certification.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2015/20150223-hyper-v-receives-red-hat-certification).

# Hyper-V receives Red Hat certification

If you run Red Hat Enterprise Linux (RHEL) as a guest OS on Hyper-V, you will be interested to know that Red Hat certifies Hyper-V as a supported hypervisor for running RHEL.  The support services you get from Red Hat via your RHEL subscription are fully available when running RHEL in an on-premises installation of Hyper-V.

Microsoft recently obtained certification for the RHEL 7.0 release on Windows Server 2008 R2 Hyper-V, Windows Server 2012 Hyper-V, and Windows Server 2012 R2 Hyper-V.  The 2012 R2 Hyper-V certification includes both Generation 1 and Generation 2 VMs.

Earlier versions of RHEL are also certified by Red Hat, including RHEL 5.9 and later, and RHEL 6.4 and later.   Both the 32-bit and 64-bit versions are certified.

See [https://access.redhat.com/ecosystem/search/#/category/Server?query=Microsoft](https://access.redhat.com/ecosystem/search/#/category/Server?query=Microsoft) for the Red Hat web page listing the certifications.  Scroll down to get to the latest versions of Hyper-V.  


\- Michael Kelley