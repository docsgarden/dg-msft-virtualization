---
id: 6BpfsQqz8GFYxvza5jHft35
title: >-
    Microsoft customers showcased at VMworld Europe 2009
desc: >-
    This week at VMworld Europe 2009 we're showcasing a number of European customers who have deployed Windows Server Hyper-V, System Center and App-v 4.5.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090223-microsoft-customers-showcased-at-vmworld-europe-2009
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 7277d69fa8664f7025eb2037bd6f000417464eb0c838a406677c3df04023b7b0
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090223-microsoft-customers-showcased-at-vmworld-europe-2009.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-02-23 07:09:00"
    ms.date: "02/23/2009"
    categories: "dell"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090223-microsoft-customers-showcased-at-vmworld-europe-2009.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090223-microsoft-customers-showcased-at-vmworld-europe-2009).

# Microsoft customers showcased at VMworld Europe 2009

This week at VMworld Europe 2009 we're showcasing a number of European customers who have deployed Windows Server Hyper-V, System Center and App-v 4.5. You can watch two videos below. One is of Bouygues Construction based in France. Bouygues does civil engineering, electrical contracting, and maintenance, employs 51,100 people and operates in more than 80 countries. This video is in French with English subtitles, and includes discussion of their Hyper-V and SCVMM deployment on Dell servers with EMC storage. 

 

The second is Saxo Bank of Denmark. They’re an investment bank, and they used a Web-based trading platform and excellent customer service to drive rapid growth over 10 years, which in turn has led to a rapid expansion in the number of servers required to handle the bank’s data-intensive operations. Saxo Bank has deployed Windows Server 2008 Hyper-V and System Center Virtual Machine Manager to consolidate servers in the datacenter. Saxo plans to virtualize nearly all applications over the next few years, with the project expected to produce a 3-year internal rate of return of 150% and a net present value of 5.8 million Danish kroner (~U.S.$1.2 million).

Enjoy. A bientot.  Patrick