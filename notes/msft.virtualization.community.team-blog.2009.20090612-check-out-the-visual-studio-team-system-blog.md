---
id: 5PYujpA6TdTRaCE4NAsN3gX
title: >-
    Check out the Visual Studio Team System blog
desc: >-
    Many of you will be interested to learn more about forthcoming lab management capabilities in Visual Studio Team System 2010.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090612-check-out-the-visual-studio-team-system-blog
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 9caf62da8ea6858365b693a2e2d551680737127b93023df51a152ee95b7ef96f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090612-check-out-the-visual-studio-team-system-blog.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-06-12 13:54:00"
    ms.date: "06/12/2009"
    categories: "net"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090612-check-out-the-visual-studio-team-system-blog.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090612-check-out-the-visual-studio-team-system-blog).

# Check out the Visual Studio Team System blog

Many of you will be interested to learn more about forthcoming lab management capabilities in Visual Studio Team System 2010. Or you may have lunch or meetings with a dev who _you_ _want_ to know more about VSTS 2010 Lab Management. That last part is a nod to ["that darn reality" post](http://blogs.computerworld.com/that_darn_reality "ComputerWorld's sharktank") at SharkTank. So check the [VSTS Lab Management team blog](https://blogs.msdn.com/lab_management/default.aspx "VSTS Lab Management team blog"). 

 Recent posts include:

  * [Creating and working in virtual environments](https://blogs.msdn.com/lab_management/archive/2009/06/02/creation-and-working-with-virtual-environments.aspx)
  * [Application build, deploy and test automation in Lab Management](https://blogs.msdn.com/lab_management/archive/2009/05/27/application-build-deploy-and-test-automation-in-lab-management.aspx)
  * [Enable lab management features for existing team projects](https://blogs.msdn.com/lab_management/archive/2009/05/22/enable-lab-management-features-for-existing-team-projects.aspx)
  * [VSTS 2010 beta 1 is released](https://blogs.msdn.com/lab_management/archive/2009/05/19/vsts-2010-beta1-is-released.aspx)



Patrick