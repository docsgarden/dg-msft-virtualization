---
id: 3RUQX4ZgD9KSM6hoCWicC7E
title: >-
    HcsWaitForProcessExit
desc: >-
    HcsWaitForProcessExit
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsWaitForProcessExit
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 41eb70b4f49e2228d71b251ad645abb766e6fec0e1fa596fd983578a22b158d6
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsWaitForProcessExit.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "12/21/2021"
    api_name: "['HcsWaitForProcessExit']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsWaitForProcessExit.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsWaitForProcessExit).

# HcsWaitForProcessExit

## Description

Waits for a process in a compute system to exit.

## Syntax

```cpp
HRESULT WINAPI
HcsWaitForProcessExit(
    _In_ HCS_PROCESS process,
    _In_ DWORD timeoutMs,
    _Outptr_opt_ PWSTR* result
    );
```

## Parameters

`process`

The handle to the process to exit.

`timeoutMs`

Time to wait in milliseconds for the process to exit.

`result`

JSON document of [[ProcessStatus|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ProcessStatus]].

The caller is responsible for releasing the returned string using [`LocalFree`](https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-localfree).

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 2104|
| **Minimum supported server** | Windows Server 2022 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |