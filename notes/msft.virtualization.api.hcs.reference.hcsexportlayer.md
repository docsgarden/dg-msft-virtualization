---
id: fzocXH8egE5w9yXNGxpdgag
title: >-
    HcsExportLayer
desc: >-
    HcsExportLayer
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsExportLayer
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: ae82092fddc0ca2136d4a30b9963233e223c864889acb5ea0abaac96a24ae8ff
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsExportLayer.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsExportLayer']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsExportLayer.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsExportLayer).

# HcsExportLayer

## Description

This function exports a container layer. This function is used by an application to create a representation of a layer in a transport format that can be copied to another host or uploaded to a container registry.

## Syntax

```cpp
HRESULT WINAPI
HcsExportLayer(
    _In_ PCWSTR layerPath,
    _In_ PCWSTR exportFolderPath,
    _In_ PCWSTR layerData,
    _In_ PCWSTR options
    );
```

## Parameters

`layerPath`

Path of the layer to export.

`exportFolderPath`

Destination folder for the exported layer.

`layerData`

JSON document of [[layerData|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#LayerData]] providing the locations of the antecedent layers that are used by the exported layer.

`options`

JSON document of [[ExportLayerOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ExportLayerOptions]] describing the layer to export.


## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeStorage.h |
| **Library** | ComputeStorage.lib |
| **Dll** | ComputeStorage.dll |