---
id: 86CdiUyQJdgVp9qYHnNFDWc
title: >-
    Hyper-V Replica and Riverbed
desc: >-
    Hyper-V Replica and Riverbed
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2013/20130311-hyper-v-replica-and-riverbed
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: d73e89a636e949a4153daad1b8a1377050912f2fb15abddf89987267353603ea
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20130311-hyper-v-replica-and-riverbed.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "03/11/2013"
    date: "2013-03-11 09:51:00"
    categories: "hvr"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20130311-hyper-v-replica-and-riverbed.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2013/20130311-hyper-v-replica-and-riverbed).

# Riverbed and Hyper-V Replica

As part of setting up a replication relationship, Hyper-V Replica transfers the initial VHDs over the network – depending on the size of the VHD, this operation can be time consuming and also impacts your production network. Once initial replication is completed, Hyper-V Replica tracks the changes to the virtual machine and frequently sends the changes to the replica site. This can also impact the network as well based on the workload characteristics. 

While Hyper-V Replica has an inbuilt compression engine, customers have repeatedly asked about the product’s co-existence with WAN optimizers. Many organizations have WAN optimizers between their two sites and are curious to understand impact of WAN optimizers on the replication traffic. We partnered with the **Microsoft Enterprise Engineering Center (**[ **EEC**](https://www.microsoft.com/en-us/eec/default.aspx) **)** and [**Riverbed**](http://www.riverbed.com/us/) **®** WAN optimizers to run a series of experiments which captures the network bandwidth optimization delivered by Riverbed in a Hyper-V Replica deployment. The results of these runs is captured in a whitepaper which is available here - [**http://www.microsoft.com/en-us/download/details.aspx?id=36786**](https://www.microsoft.com/en-us/download/details.aspx?id=36786 "http://www.microsoft.com/en-us/download/details.aspx?id=36786") 

  Key Takeaways:

  * Hyper-V Replica and the Riverbed devices did not require any specialized/extra configuration to interact with each other. Each component “just-worked” out of the box!
  * The bandwidth optimizations are a function of the VHD size, sparseness etc. When HVR’s inbuilt compression was turned off, the Riverbed devices were able to optimize the network traffic significantly across different VHDs, VMs. Even after enabling HVR’s inbuilt compression, the Riverbed devices were able to further optimize the compressed traffic.