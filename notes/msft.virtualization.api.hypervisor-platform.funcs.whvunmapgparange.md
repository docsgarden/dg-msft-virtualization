---
id: 5p9mXtHTbEZoqoAdFQ8fhnK
title: >-
    WHvUnmapGpaRange
desc: >-
    Describes WHvUnmapGpaRange and provides syntax, parameters, remarks, and feedback. Parameters include Partition, GuestAddress, and SizeInBytes.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvUnmapGpaRange
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 1fa1ef405149a47dc2b5ebdd237b89c99fd31d4c1457345be79693b09f6c214f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvUnmapGpaRange.md
    author: "juarezhm"
    ms.author: "hajuarez"
    ms.date: "06/22/2018"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvUnmapGpaRange.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvUnmapGpaRange).

# WHvUnmapGpaRange

## Syntax
```C
HRESULT
WINAPI
WHvUnmapGpaRange(
    _In_ WHV_PARTITION_HANDLE Partition,
    _In_ WHV_GUEST_PHYSICAL_ADDRESS GuestAddress,
    _In_ UINT64 SizeInBytes
    );
```
### Parameters

`Partition`

Handle to the partition object

`GuestAddress`

Specifies the start address of the region in the VM’s physical address space that is unmapped

`SizeInBytes`

Specifies the number of bytes that are to be unmapped

## Remarks

Unmapping a previously mapped GPA range makes the memory range unavailable to the partition. Any further access by a virtual processor to the range will result in a memory access exit.