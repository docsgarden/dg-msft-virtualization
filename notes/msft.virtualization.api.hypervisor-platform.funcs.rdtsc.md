---
id: 7W8MVuUoTZ3Wo3bQM9CCoN7
title: >-
    Exit caused by an rdtsc(p) instruction
desc: >-
    Learn about context data for an exit caused by an rdtsc(p) instruction.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/Rdtsc
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: a8c3d1bba5ce6899a1eb006f24689ab99105048856a9fb10a3d86dd6ea06c982
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/Rdtsc.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/20/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/Rdtsc.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/Rdtsc).

# RDTSC(P)


## Syntax
```C
//
// Context data for an exit caused by an rdtsc(p) instruction
// (WHvRunVpExitReasonX64Rdtsc)
//
typedef union WHV_X64_RDTSC_INFO
{
    struct
    {
        UINT64 IsRdtscp:1;
        UINT64 Reserved:63;
    };

    UINT64 AsUINT64;
} WHV_X64_RDTSC_INFO;

typedef struct WHV_X64_RDTSC_CONTEXT
{
    UINT64 TscAux;
    UINT64 VirtualOffset;
    UINT64 Reserved[2];
    WHV_X64_RDTSC_INFO RdtscInfo;
} WHV_X64_RDTSC_CONTEXT;
```

## Return Value
Information about a rdtsc(p) instruction from the virtual processor is provided in the `WHV_VP_EXCEPTION_CONTEXT` structure. 

Exits for exceptions are only generated if they are enabled by setting the `WHV_EXTENDED_VM_EXITS.X64RdtscExit` property for the partition.

## Requirements

Minimum supported build:    Insider Preview Builds (19H2) Experimental