---
id: 5gisSduAChdSj7n2QSqCjem
title: >-
    HcsCloseOperation
desc: >-
    HcsCloseOperation
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsCloseOperation
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 238a15a321a25d7f9e78c45f5d4c5fb5f0c2c43c6e07ff27edf7e8974b957f89
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsCloseOperation.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsCloseOperation']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsCloseOperation.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsCloseOperation).

# HcsCloseOperation

## Description

Close the operation, freeing any tracking resources associated with the operation. An operation can be closed from within a callback.

## Syntax

```cpp
void WINAPI
HcsCloseOperation(
    _In_ HCS_OPERATION operation
    );
```

## Parameters

`operation`

Handle to an operation.

## Return Values

None.

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |