---
id: cDTMSdDkhxpmJUxN8ei9gLn
title: >-
    Update to private cloud toolkit for System Center
desc: >-
    A couple updates were announced Monday at our Worldwide Partner Conference.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100714-update-to-private-cloud-toolkit-for-system-center
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 14559e706160e0b83a47f0f6f4bda4e3bebf9b7933567deb697ced66babc8f8c
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100714-update-to-private-cloud-toolkit-for-system-center.md
    author: "scooley"
    ms.author: "scooley"
    date: "2010-07-14 10:43:30"
    ms.date: "07/14/2010"
    categories: "cloud-computing"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100714-update-to-private-cloud-toolkit-for-system-center.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100714-update-to-private-cloud-toolkit-for-system-center).

# Update to private cloud toolkit for System Center

Back in February we started working with partners and TAP customers on a toolkit to create private cloud environment.  See our [February blog](https://blogs.technet.com/b/ddcalliance/archive/2010/02/16/dynamic-infrastructure-toolkit-for-system-center-dit-sc-sneak-peek-into-on-boarding.aspx) about the Dynamic Infrastructure Toolkit for System Center.

A couple updates were announced Monday at our [Worldwide Partner Conference](http://digitalwpc.com/). First, the tool has a new name: [System Center Virtual Machine Manager Self-Service Portal 2.0](https://www.microsoft.com/virtualization/en/us/private-cloud.aspx) (the portal). Second, we reached the release candidate milestone. You can download it [here](https://www.microsoft.com/downloads/details.aspx?FamilyID=fef38539-ae5a-462b-b1c9-9a02238bb8a7&displaylang=en). _A release candidate milestone means it ’s feature complete, and is nearing completion._ We expect the portal to be finalized and released to the web in calendar Q4.

You can read more about the self-service portal tool over at the System Center team blog ([here](https://blogs.technet.com/b/systemcenter/archive/2010/07/13/microsoft-system-center-virtual-machine-manager-self-service-portal-2-0-release-candidate-now-available.aspx "System Center team blog about SCVMM SSP")). Here's an excerpt:

_The self-service portal provides the following features that are exposed through a web-based user interface:_

  * _**Configuration and allocation of datacenter resources:** Store management and configuration information related to compute, network and storage resources as assets in the VMMSSP database. _
  * _**Customization of virtual machine actions:** Provide a simple web-based interface to extend the default virtual machine actions; for example, you can add scripts that interact with Storage Area Networks for rapid deployment of virtual machines. _
  * _**Business unit on-boarding:** Standardized forms and a simple workflow for registering and approving or rejecting business units to enroll in the portal. _
  * _**Infrastructure request and change management:** Standardized forms and human-driven workflow that results in reducing the time needed to provision infrastructures in your environment. _
  * _**Self-Service provisioning:** Supports bulk creation of virtual machines on provisioned infrastructure through the web-based interface.Helps business units to manage their virtual machines based on delegated roles_



  * 


Patrick