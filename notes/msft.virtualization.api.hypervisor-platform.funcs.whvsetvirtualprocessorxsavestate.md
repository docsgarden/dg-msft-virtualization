---
id: 7xiQk6zx6BPhSnhzuSJLFdM
title: >-
    WHvSetVirtualProcessorXsaveState
desc: >-
    Describes the virtual processor and provides syntax, parameters, and return value. Parameters include Partition, VpIndex, Buffer, BufferSizeInBytes.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvSetVirtualProcessorXSaveState
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: f663d58150a64aaacdb3fdedf89e37e5f437c4382f17f01cf419e4ff1e1a3a74
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvSetVirtualProcessorXSaveState.md
    author: "jstarks"
    ms.author: "jostarks"
    ms.date: "06/06/2019"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvSetVirtualProcessorXSaveState.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvSetVirtualProcessorXSaveState).

# WHvSetVirtualProcessorXsaveState

## Syntax

```
HRESULT
WINAPI
WHvSetVirtualProcessorXsaveState(
    _In_ WHV_PARTITION_HANDLE Partition,
    _In_ UINT32 VpIndex,
    _In_reads_bytes_(BufferSizeInBytes) const VOID* Buffer,
    _In_ UINT32 BufferSizeInBytes
    );
```

### Parameters

`Partition`

Specifies the partition of the virtual processor.

`VpIndex`

Specifies the index of the virtual processor whose XSAVE state should be set.

`Buffer`

Specifies the buffer containing the XSAVE state.

`BufferSizeInBytes`

Specifies the size of the buffer, in bytes.

## Return Value

If the function succeeds, the return value is `S_OK`.