---
id: 47q683PCWYXVaY9QjhuGqEY
title: >-
    HDV_PCI_DEVICE_SET_CONFIGURATION
desc: >-
    HDV_PCI_DEVICE_SET_CONFIGURATION
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciDeviceSetConfiguration
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 02c2a4fd467c102553c9134f2dd9db6cb3cea1591326633a58ded3323a0a1615
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciDeviceSetConfiguration.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HDV_PCI_DEVICE_SET_CONFIGURATION']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciDeviceSetConfiguration.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciDeviceSetConfiguration).

# HDV_PCI_DEVICE_SET_CONFIGURATION

## Description

Function invoked to set the configuration of the emulated device.

## Syntax

```C++
typedef HRESULT (CALLBACK *HDV_PCI_DEVICE_SET_CONFIGURATION)(
    _In_opt_ void*         DeviceContext,
    _In_     UINT32        ConfigurationValueCount,
    _In_     const PCWSTR* ConfigurationValues
    );
```

## Parameters

|Parameter|Description|
|---|---|---|---|---|---|---|---|
|`DeviceContext` |Context pointer that was supplied to HdvCreateDeviceInstance|
|`ConfigurationValueCount` |Number of elements in the ConfigurationValues array|
|`ConfigurationValues` |Array with strings representing the configurations values. These strings are provided in the VM's configuration.|
|    |    |

## Return Values

|Return Value     |Description|
|---|---|
|`S_OK` | Returned if function succeeds.|
|`HRESULT` | An error code is returned if the function fails.
|     |     |

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |