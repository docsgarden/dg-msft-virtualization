---
id: hEkVFgiq336gW5dWmDn9Cpw
title: >-
    HV_DEVICE_INTERRUPT_TARGET
desc: >-
    HV_DEVICE_INTERRUPT_TARGET
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_device_interrupt_target
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: c393c8d2bca2bca2f01afc5b0ac8a541e4fc784d4d99ec55b264a939e02924c7
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_device_interrupt_target.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_device_interrupt_target.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_device_interrupt_target).

# HV_DEVICE_INTERRUPT_TARGET

## Syntax

 ```c

#define HV_DEVICE_INTERRUPT_TARGET_MULTICAST 1
#define HV_DEVICE_INTERRUPT_TARGET_PROCESSOR_SET 2

typedef struct
{
    HV_INTERRUPT_VECTOR Vector;
    UINT32 Flags;
    union
    {
        UINT64 ProcessorMask;
        UINT64 ProcessorSet[];
    };
} HV_DEVICE_INTERRUPT_TARGET;
 ```

“Flags” supplies optional flags for the interrupt target. “Multicast” indicates that the interrupt is sent to all processors in the target set. By default, the interrupt is sent to an arbitrary single processor in the target set.