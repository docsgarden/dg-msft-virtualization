---
id: 79oR4sSsJWTmdbdrdrgWYKn
title: >-
    HvCallEnableVpVtl
desc: >-
    HvCallEnableVpVtl hypercall
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallEnableVpVtl
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 4ff0cbe42aca140d71ac2f358e4d000a61cd7139e6b0e26d06a1dc34d18bac0e
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallEnableVpVtl.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallEnableVpVtl.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallEnableVpVtl).

# HvCallEnableVpVtl

HvCallEnableVpVtl enables a VTL to run on a VP. This hypercall should be used in conjunction with HvCallEnablePartitionVtl to enable and use a VTL. To enable a VTL on a VP, it must first be enabled for the partition. This call does not change the active VTL.

## Interface

 ```c

HV_STATUS
HvEnableVpVtl(
    _In_ HV_PARTITION_ID TargetPartitionId,
    _In_ HV_VP_INDEX VpIndex,
    _In_ HV_VTL TargetVtl,
    _In_ HV_INITIAL_VP_CONTEXT VpVtlContext
    );
 ```

### Restrictions

In general, a VTL can only be enabled by a higher VTL. There is one exception to this rule: the highest VTL enabled for a partition can enable a higher target VTL.

Once the target VTL is enabled on a VP, all other calls to enable the VTL must come from equal or greater VTLs.
This hypercall will fail if called to enable a VTL that is already enabled for a VP.

## Call Code
`0x000F` (Simple)

## Input Parameters

| Name                    | Offset     | Size     | Information Provided                      |
|-------------------------|------------|----------|-------------------------------------------|
| `TargetPartitionId`     | 0          | 8        | Supplies the partition ID of the partition this request is for. |
| `VpIndex`               | 8          | 4        | Specifies the index of the virtual processor on which to enable the VTL. |
| `TargetVtl`             | 12         | 1        | Specifies the VTL to be enabled by this hypercall. |
| RsvdZ                   | 13         | 3        |                                           |
| `VpVtlContext`          | 16         | 224      | Specifies the initial context in which the VP should start upon the first entry to the target VTL. |

## See also

[[HV_INITIAL_VP_CONTEXT|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.datatypes.HV_INITIAL_VP_CONTEXT]]