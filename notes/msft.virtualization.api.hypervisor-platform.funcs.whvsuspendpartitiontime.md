---
id: 9ZtjLKBvsXLA2wVB8sNWHgb
title: >-
    WHvSuspendPartitionTime
desc: >-
    Describes virtual processor SuspendPartitionTime and provides parameters, return value, remarks, and requirements.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvSuspendPartitionTime
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 32cb4d096e8d812e9faf9d7bbbddedf5b3e40c36e1187d22ca6024ddd519ee4c
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvSuspendPartitionTime.md
    author: "nschonni"
    ms.author: "mabrigg"
    ms.date: "06/03/2019"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvSuspendPartitionTime.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvSuspendPartitionTime).

# WHvSuspendPartitionTime

## Syntax

```C
HRESULT
WINAPI
WHvSuspendPartitionTime(
    _In_ WHV_PARTITION_HANDLE Partition
    );
```

### Parameters

`Partition`

Handle to the partition object.

## Return Value

If the function succeeds, the return value is `S_OK`.  

## Remarks

Suspends time for the partition.

No virtual processor may be running when this is called.  Time will resume when [[`WHvResumePartitionTime`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-platform.funcs.whvresumepartitiontime]] or
[[`WHvRunVirtualProcessor`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-platform.funcs.whvrunvirtualprocessor]] is called.

## Requirements

Minimum supported build:    Insider Preview Builds (19H1)