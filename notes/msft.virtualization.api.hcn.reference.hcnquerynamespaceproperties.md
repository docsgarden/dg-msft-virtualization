---
id: A7PZiBcesRSDJG5yvNoRJ7D
title: >-
    HcnQueryNamespaceProperties
desc: >-
    HcnQueryNamespaceProperties
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnQueryNamespaceProperties
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 137cfaa9599a90d78ba83b3a5154df8891eafc2f11e887375bba271e8fa121f9
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnQueryNamespaceProperties.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnQueryNamespaceProperties']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnQueryNamespaceProperties.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnQueryNamespaceProperties).

# HcnQueryNamespaceProperties

## Description

Queries the properties of a namespace.

## Syntax

```cpp
HRESULT
WINAPI
HcnQueryNamespaceProperties(
    _In_ HCN_NAMESPACE Namespace,
    _In_ PCWSTR Query,
    _Outptr_ PWSTR* Properties,
    _Outptr_opt_ PWSTR* ErrorRecord
    );
```

## Parameters

`Namespace`

Handle to an namespace [[`HCN_NAMESPACE`|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_namespace]]

`Query`

Optional JSON document of [[HostComputeQuery|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.HNS_Schema#HostComputeQuery]].

`Properties`

The properties in the form of a JSON document of [[HostComputeNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.HNS_Schema#HostComputeNamespace]].

`ErrorRecord`

Receives a JSON document with extended errorCode information. The caller must release the buffer using CoTaskMemFree.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |