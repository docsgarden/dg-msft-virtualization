---
id: 4EVhGyY6rVVYCVTe6GsGppz
title: >-
    HDV_PCI_DEVICE_GET_DETAILS
desc: >-
    HDV_PCI_DEVICE_GET_DETAILS
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciDeviceGetDetails
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 69ec5143945f7c1e5ae713701a7e14f911269fc605d7b7a6434ed8d233223aa9
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciDeviceGetDetails.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HDV_PCI_DEVICE_GET_DETAILS']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciDeviceGetDetails.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciDeviceGetDetails).

# HDV_PCI_DEVICE_GET_DETAILS

## Description

Function invoked to query the PCI description of the emulated device. This information is used when presenting the device to the guest partition.

## Syntax

```C++
typedef HRESULT (CALLBACK *HDV_PCI_DEVICE_GET_DETAILS)(
    _In_opt_ void*           DeviceContext,
    _Out_    PHDV_PCI_PNP_ID PnpId,
    _Out_    UINT32          ProbedBars[HDV_PCI_BAR_COUNT]
    );
```

## Parameters

|Parameter|Description|
|---|---|---|---|---|---|---|---|
|`DeviceContext` |Context pointer that was supplied to HdvCreateDeviceInstance|
|`PnpId` |Receives the vendor / device ID / ... information about the device|
|`BarCount`|Specifies the size of the output buffer provided in probedBars. It should be large enough to receive `HDV_MAX_PCI_BAR_COUNT` elements.|
|`ProbedBars` |Receives the results for probing the MMIO BARs.|
|    |    |

## Return Values

|Return Value     |Description|
|---|---|
|`S_OK` | Returned if function succeeds.|
|`HRESULT` | An error code is returned if the function fails.
|     |     |

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |