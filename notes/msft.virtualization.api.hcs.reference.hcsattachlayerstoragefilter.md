---
id: Ac3U4uL9SYGuQ9ZNLK2JKq4
title: >-
    HcsAttachLayerStorageFilter
desc: >-
    HcsAttachLayerStorageFilter
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsAttachLayerStorageFilter
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 7d3ef6a0ef3e5588861b9058d5b4fa9e93bcca9ab43f6678d4f78b6dae3afc1a
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsAttachLayerStorageFilter.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsAttachLayerStorageFilter']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsAttachLayerStorageFilter.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsAttachLayerStorageFilter).

# HcsAttachLayerStorageFilter

## Description

This function sets up the container storage filter on a layer directory. The storage filter provides the unified view to the layer and its antecedent layers.

## Syntax

```cpp
HRESULT WINAPI
HcsAttachLayerStorageFilter(
    _In_ PCWSTR layerPath,
    _In_ PCWSTR layerData
    );
```

## Parameters

`layerPath`

Full path to the root directory of the layer.

`layerData`

JSON document of [[layerData|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#LayerData]] providing the locations of the antecedent layers that are used by the layer.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeStorage.h |
| **Library** | ComputeStorage.lib |
| **Dll** | ComputeStorage.dll |