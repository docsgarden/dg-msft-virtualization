---
id: 6xji6VZPV3XyDSEwP2CUZWG
title: >-
    Update&#58; Capacity Planner for Hyper-V Replica
desc: >-
    This article discusses the updates made to the Capacity Planner for Hyper-V Replica on Windows Server 2012.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2014/20140121-update-capacity-planner-for-hyper-v-replica
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 223294083d6007c1f58c0febe0b6df18f733786fd27433a1792b049ffbb7bb09
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140121-update-capacity-planner-for-hyper-v-replica.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "01/21/2014"
    categories: "hvr"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140121-update-capacity-planner-for-hyper-v-replica.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2014/20140121-update-capacity-planner-for-hyper-v-replica).

# Updates to the Capacity Planner for Hyper-V Replica

In May 2013, we [released the first version](https://blogs.technet.com/b/virtualization/archive/2013/05/23/hyper-v-replica-capacity-planner.aspx) of the Capacity Planner for Hyper-V Replica on Windows Server 2012. It allowed administrators to plan their Hyper-V Replica deployments based on the workload, storage, network, and server characteristics. While it is always possible to monitor every single perfmon counter to make an informed decision, a readymade tool always makes life simpler and easier. The big plus comes from the fact that the guidance is based on actual workload and server characteristics, which makes it a level better than static input-based planning models. The tool picks the right counters to monitor, automates the metrics collection process, and generates an easily consumable report. The tool and documentation have been **updated for Windows Server 2012 R2** and can be download from here: <https://www.microsoft.com/en-us/download/details.aspx?id=39057> 

#### What’s new

We received feedback from our customers on how the tool can be made better, and we threw in a few improvements of our own. Here is what the updated Capacity Planner tool has: 

  1. Support for Windows Server 2012 and Windows Server 2012 R2 in a single tool
  2. Support for Extended Replication
  3. Support for virtual disks placed on NTFS, CSVFS, and SMB shares
  4. Monitoring of multiple standalone hosts simultaneously
  5. Improved performance and scale – up to 100 VMs in parallel
  6. Replica site input is optional – for those still in the planning stage of a DR strategy
  7. Report improvements – e.g.: reporting the peak utilization of resources also
  8. Improved guidance in documentation
  9. Improved workflow and user experience

In addition, the documentation has a section on how the tool can be used for capacity planning of [Hyper-V Recovery Manager](http://www.windowsazure.com/en-us/services/recovery-manager/) based on the [‘cloud’ construct](https://technet.microsoft.com/library/gg610625.aspx) of System Center Virtual Machine Manager.   So go ahead, use the tool in your virtual infrastructure and share your feedback and questions through this blog post. We would love to hear your comments! 

_**28-Feb-2014 update:**   _Keith Mayer has an excellent guided hands-on lab demo that can be found [here](https://blogs.technet.com/b/keithmayer/archive/2014/02/27/guided-hands-on-lab-capacity-planner-for-windows-server-2012-hyper-v-replica.aspx).