---
id: 9N9kLCQAYg6oKq9jVBmgHgG
title: >-
    Delete a partition and release the resources that the partition was using
desc: >-
    Learn about the WHvDeletePartition function that lets you delete a partition, remove the partition object, and release the resources that the partition was using.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvDeletePartition
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 840aa673bee270df6080c99ed63dd0c36849d864249d4ec1ba0ae6f035fff4ee
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvDeletePartition.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/20/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvDeletePartition.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvDeletePartition).

# WHvDeletePartition


## Syntax
```C
HRESULT
WINAPI
WHvDeletePartition(
    _In_ WHV_PARTITION_HANDLE Partition
    );
```
### Parameters

`Partition`

Handle to the partition object that is deleted.
  

## Remarks

Deleting a partition tears down the partition object and releases all resource that the partition was using.