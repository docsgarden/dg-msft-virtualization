---
id: 6mbFJWnWdT4rYJVyUC4YBDv
title: >-
    Migrate Windows Server 2012R2 Virtualized Workloads to Microsoft Azure with Azure Site Recovery
desc: >-
    Learn about migrating virtualized workloads from Windows Server 2012R2 to Microsoft Azure using Azure Site Recovery.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2014/20140813-migrate-windows-server-2012r2-virtualized-workloads-to-microsoft-azure-with-azure-site-recovery
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 62f9965223a02268f422d1cab00ca5ac37e7750f5a84490d827b92844a288855
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140813-migrate-windows-server-2012r2-virtualized-workloads-to-microsoft-azure-with-azure-site-recovery.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2014-08-13 22:19:30"
    ms.date: "08/13/2014"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140813-migrate-windows-server-2012r2-virtualized-workloads-to-microsoft-azure-with-azure-site-recovery.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2014/20140813-migrate-windows-server-2012r2-virtualized-workloads-to-microsoft-azure-with-azure-site-recovery).

# Migrate Windows Server 2012R2 Virtualized Workloads to Microsoft Azure with Azure Site Recovery

[Azure Site Recovery](https://aka.ms/rg1_Azure_Site_Recovery) not only enables a low-cost, high-capability, [CAPEX-saving, and OPEX-optimizing](https://aka.ms/rg1_disaster_recovery_to_azure) Disaster Recovery strategy for your IT Infrastructure, it can also help you quickly and easily spin-off additional development and testing environments or migrate on-premise virtual machines to Microsoft Azure. For customers who want a unified solution that reduces downtime of their production workloads during migration and that also enables verification of their applications in Azure without any impact to production, Azure Site Recovery with its in-built features makes migrating to Azure simple, reliable, and quick. The flowchart below describes a typical migration flow using Azure Site Recovery. For more information, please visit the following detailed blog on [how to migrate on-premise virtualized workloads to Azure using Azure Site Recovery](https://aka.ms/rg1_Migration_Blog_Azure_Blog).

![Flowchart describing workflow for migrating on-premise virtualized workloads to Azure using Azure Site Recovery.](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/metablogapi/ASR_Migration_Solution_thumb_6CEDC22C.png)](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/metablogapi/ASR_Migration_Solution_398F6E1F.png)