---
id: AH96ZK4nwiGmuZkNQKMcjsq
title: >-
    The ReadGuestRawSavedMemory function
desc: >-
    Reads raw memory from the saved state file. This function reads raw memory from the saved state file as if it were a flat memory layout, regardless of the guest memory layout.
canonicalUrl: https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/ReadGuestRawSavedMemory
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 28ee049e6bd04e37887b26c6349bb4402446b0ad86d2f8b6f2144044c9f4aca3
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/ReadGuestRawSavedMemory.md
    ms.date: "04/19/2022"
    author: "mattbriggs"
    ms.author: "mabrigg"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/ReadGuestRawSavedMemory.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/ReadGuestRawSavedMemory).

# ReadGuestRawSavedMemory function

Reads raw memory from the saved state file. This function reads raw memory from the saved state file as if it were a flat memory layout, regardless of the guest memory layout. If BytesRead returns something lower than BufferSize, then the end of memory has been reached.

## Syntax

```C
HRESULT
WINAPI
ReadGuestRawSavedMemory(
    _In_        VM_SAVED_STATE_DUMP_HANDLE  VmSavedStateDumpHandle,
    _In_        UINT64                      RawSavedMemoryOffset,
    _Out_writes_bytes_(BufferSize) LPVOID   Buffer,
    _In_        UINT32                      BufferSize,
    _Out_opt_   UINT32*                     BytesRead
    );
```

## Parameters

`VmSavedStateDumpHandle`

Supplies a handle to a dump provider instance.

`RawSavedMemoryOffset`

Byte offset on the raw saved memory from where to start reading.

`Buffer`

Returns the raw memory read on the current raw memory offset.

`BufferSize`

Supplies the requested byte count to read.

`BytesRead`

Optionally returns the bytes actually read.

## Return Value

If the operation completes successfully, the return value is `S_OK`.

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |