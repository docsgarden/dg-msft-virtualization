---
id: 87mcoVi2AxkYa5JLwAtCcZV
title: >-
    HcnReserveGuestNetworkServicePort
desc: >-
    HcnReserveGuestNetworkServicePort
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnReserveGuestNetworkServicePort
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 8ea3b58488b231ea2c5d6616784bf47fab9f9463855a7e5c579668cbe7d33e35
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnReserveGuestNetworkServicePort.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnReserveGuestNetworkServicePort']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnReserveGuestNetworkServicePort.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnReserveGuestNetworkServicePort).

# HcnReserveGuestNetworkServicePort

## Description

Reserves a single port.

## Syntax

```cpp
HRESULT
WINAPI
HcnReserveGuestNetworkServicePort(
    _In_ HCN_GUESTNETWORKSERVICE GuestNetworkService,
    _In_ HCN_PORT_PROTOCOL Protocol,
    _In_ HCN_PORT_ACCESS Access,
    _In_ USHORT Port,
    _Out_ HANDLE* PortReservationHandle
    );
```

## Parameters

`GuestNetworkService`

The [[HCN_GUESTNETWORKSERVICE|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_guestnetworkservice]] for the reservation.

`Protocol`

The [[HCN_PORT_PROTOCOL|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_port_protocol]] for the reservation.

`Access`

The [[HCN_PORT_ACCESS|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_port_access]] for the reservation.

`Port`

The port for the reservation.

`PortReservationHandle`

Receives a handle. It is the responsibility of the caller to release the handle using [[HcnReleaseGuestNetworkServicePortReservationHandle|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnreleaseguestnetworkserviceportreservationhandle]] once it is no longer in use.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |