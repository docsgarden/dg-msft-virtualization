---
id: 7tRvdcpsRZh3gGTHFBS7CpW
title: >-
    HvCallSendSyntheticClusterIpi
desc: >-
    HvCallSendSyntheticClusterIpi hypercall
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallSendSyntheticClusterIpi
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: a9ebc5864be9302568b90c61d65921c56bfe0012e6e1dc6b9f15115b2d7f7519
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallSendSyntheticClusterIpi.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallSendSyntheticClusterIpi.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallSendSyntheticClusterIpi).

# HvCallSendSyntheticClusterIpi

This hypercall sends a virtual fixed interrupt to the specified virtual processor set. It does not support NMIs.

## Interface

 ```c
HV_STATUS
HvCallSendSyntheticClusterIpi(
    _In_ UINT32 Vector,
    _In_ HV_INPUT_VTL TargetVtl,
    _In_ UINT64 ProcessorMask
    );
 ```

## Call Code
`0x000b` (Simple)

## Input Parameters

| Name                    | Offset     | Size     | Information Provided                      |
|-------------------------|------------|----------|-------------------------------------------|
| `Vector`                | 0          | 4        | Specified the vector asserted. Must be between >= 0x10 and <= 0xFF.  |
| `TargetVtl`             | 4          | 1        | Target VTL                                |
| Padding                 | 5          | 3        |                                           |
| `ProcessorMask`         | 8          | 1        | Specifies a mask representing VPs to target|