---
id: AkAjLrebNM4qVWFxekKWu3k
title: >-
    HDV_DEVICE_TYPE Structure
desc: >-
    HDV_DEVICE_TYPE Structure
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciDeviceInterface
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 3b0715efc06c4e030e7ee1506c6870e98aa111650ce7e645726eab0f5ece643a
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciDeviceInterface.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HDV_DEVICE_TYPE Structure']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciDeviceInterface.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciDeviceInterface).

# HDV_DEVICE_TYPE Structure

Device emulation callbacks for PCI devices, passed by the emulated device to HdvCreateDeviceInstance.

## Syntax

```C++
typedef struct HDV_PCI_DEVICE_INTERFACE
{
    HDV_PCI_INTERFACE_VERSION Version;
    HDV_PCI_DEVICE_INITIALIZE Initialize;
    HDV_PCI_DEVICE_TEARDOWN Teardown;
    HDV_PCI_DEVICE_SET_CONFIGURATION SetConfiguration;
    HDV_PCI_DEVICE_GET_DETAILS GetDetails;
    HDV_PCI_DEVICE_START Start;
    HDV_PCI_DEVICE_STOP Stop;
    HDV_PCI_READ_CONFIG_SPACE ReadConfigSpace;
    HDV_PCI_WRITE_CONFIG_SPACE WriteConfigSpace;
    HDV_PCI_READ_INTERCEPTED_MEMORY ReadInterceptedMemory;
    HDV_PCI_WRITE_INTERCEPTED_MEMORY WriteInterceptedMemory;
} HDV_PCI_DEVICE_INTERFACE, *PHDV_PCI_DEVICE_INTERFACE;
```

## Members

`Version`

Version.

`Initialize`

Version.

`Teardown`

Version.

`SetConfiguration`

Version.

`GetDetails`

Version.

`Start`

Version.

`Stop`

Version.

`ReadConfigSpace`

Version.

`WriteConfigSpace`

Version.

`ReadInterceptedMemory`

Version.

`WriteInterceptedMemory`

Version.

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |