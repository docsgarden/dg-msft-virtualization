---
id: AiTf8BeWXYxbnMjjD9LSwEu
title: >-
    HDV_PCI_WRITE_INTERCEPTED_MEMORY
desc: >-
    HDV_PCI_WRITE_INTERCEPTED_MEMORY
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciWriteInterceptedMemory
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 7cd692b2ee7667fc03cd53ab7f9db6e5404a3cc3ead8236ab895c0f1aa4d5a31
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciWriteInterceptedMemory.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HDV_PCI_WRITE_INTERCEPTED_MEMORY']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciWriteInterceptedMemory.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciWriteInterceptedMemory).

# HDV_PCI_WRITE_INTERCEPTED_MEMORY

## Description

Function called to execute an intercepted MMIO write for the emulated device.

## Syntax

```C++
typedef HRESULT (CALLBACK *HDV_PCI_WRITE_INTERCEPTED_MEMORY)(
    _In_opt_           void*                DeviceContext,
    _In_               HDV_PCI_BAR_SELECTOR BarIndex,
    _In_               UINT64               Offset,
    _In_               UINT64               Length,
    _In_reads_(Length) const BYTE*          Value
    );
```

## Parameters

|Parameter|Description|
|---|---|---|---|---|---|---|---|
|`DeviceContext` |Context pointer that was supplied to HdvCreateDeviceInstance.|
|`BarIndex` |Index to the BAR the write operation pertains to.|
|`Offset` |Offset in bytes from the base of the BAR to write.|
|`Length` |Length in bytes to write (1 / 2 / 4 / 8 bytes).|
|`Value` |Value to write.|
|    |    |

## Return Values

|Return Value     |Description|
|---|---|
|`S_OK` | Returned if function succeeds.|
|`HRESULT` | An error code is returned if the function fails.
|     |     |

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |