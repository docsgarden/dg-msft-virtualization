---
id: 6pinhEKafL3cfDLR97CAXuP
title: >-
    HcnModifyLoadBalancer
desc: >-
    HcnModifyLoadBalancer
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnModifyLoadBalancer
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: c374b5335a48d670ae5bd12f0980115239bbb321fcf6bef5b9af5a5a56e8f0da
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnModifyLoadBalancer.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnModifyLoadBalancer']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnModifyLoadBalancer.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnModifyLoadBalancer).

# HcnModifyLoadBalancer

## Description

Modifies a load balancer.

## Syntax

```cpp
HRESULT
WINAPI
HcnModifyLoadBalancer(
    _In_ HCN_LOADBALANCER LoadBalancer,
    _In_ PCWSTR Settings,
    _Outptr_opt_ PWSTR* ErrorRecord
    );
```

## Parameters

`LoadBalancer`

The [[HCN\_LOADBALANCER|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_loadbalancer]] to modify.

`Settings`

JSON document specifying the settings of the [[load balancer|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.HNS_Schema#HostComputeLoadBalancer]].

`ErrorRecord`

Receives a JSON document with extended errorCode information. The caller must release the buffer using CoTaskMemFree.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |