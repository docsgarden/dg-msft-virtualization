---
id: 96AdxnJkKjEE2d4LbNb38iF
title: >-
    HcsCreateOperation
desc: >-
    HcsCreateOperation
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsCreateOperation
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 60a7a94c618ffe9c9fa450b37d9accc2d6f1b67f6547e056f4a441cbc0cf73ce
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsCreateOperation.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsCreateOperation']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsCreateOperation.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsCreateOperation).

# HcsCreateOperation

## Description

Creates a new operation.

## Syntax

```cpp
HCS_OPERATION WINAPI
HcsCreateOperation(
    _In_opt_ HCS_OPERATION_COMPLETION callback
    _In_opt_ void*                    context
    );
```

## Parameters

`callback`

Optional pointer to an [[`HCS_OPERATION_COMPLETION`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_operation_completion]] callback to be invoked when the operation completes.

`context`

Optional pointer to a context that is passed to the callback.

## Return Values

Returns the `HCS_OPERATION` handle to the newly created operation on success, `NULL` if resources required for the operation couldn't be allocated. It is the responsibility of the caller to release the operation using [[`HcsCloseOperation`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscloseoperation]] once it is no longer used.

## Remarks

Refer to the async model sample code for details on how to use HCS operations.

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |