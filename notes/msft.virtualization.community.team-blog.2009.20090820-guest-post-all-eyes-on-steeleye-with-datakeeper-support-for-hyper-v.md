---
id: AZV8LYqCVqnj59AdM3qUiGB
title: >-
    Guest Post&#58; All Eyes on SteelEye with DataKeeper Support for Hyper-V
desc: >-
    SteelEye delivers DataKeeper Cluster Edition, a real-time data replication solution for Hyper-V.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090820-guest-post-all-eyes-on-steeleye-with-datakeeper-support-for-hyper-v
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: a6840941b6781e7f9b3ada786370bc87c3344b6603a77cc958cab99cdd474d03
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090820-guest-post-all-eyes-on-steeleye-with-datakeeper-support-for-hyper-v.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2009-08-20 00:10:00"
    ms.date: "08/20/2009"
    categories: "disaster-recovery"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090820-guest-post-all-eyes-on-steeleye-with-datakeeper-support-for-hyper-v.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090820-guest-post-all-eyes-on-steeleye-with-datakeeper-support-for-hyper-v).

# Guest Post: All Eyes on SteelEye with DataKeeper Support for Hyper-V
Hi all,

My name is Dave Bermingham and I’m the director of Windows product management for SteelEye Technology, a provider of business continuity and disaster recovery solutions. I’d like to thank Microsoft’s Virtualization team for the opportunity to guest blog on SteelEye’s continued support for Hyper-V as well as exciting new features coming in support of Hyper-V on Windows Server 2008 R2. 

I can’t believe it is almost a year ago since I demonstrated [SteelEye DataKeeper Cluster Edition](http://www.steeleye.com/products/windows/datakeeper.php) at the Microsoft Virtualization Launch event in Bellevue, WA.  In the past year we have seen the interest in our Hyper-V disaster recovery products increase dramatically and with the recent release of Windows Server 2008 R2, I believe we have just seen the tip of the iceberg!

SteelEye’s mission is to “Replicate Any Data, Protect Any Application.” We pride ourselves on our commitment to provide the Windows marketplace with flexible, scalable and cost-effective solutions that are enterprise-grade yet customizable for any size business.  Most recently SteelEye identified an opportunity to extend Microsoft’s Windows Server 2008 Failover Clustering (WSFC) by delivering SteelEye DataKeeper Cluster Edition, a real-time data replication solution that integrates seamlessly with WSFC to enable multi-site clusters while eliminating the single point of failure traditionally associated with shared storage resources.  A comprehensive and cost-effective disaster recovery solution, DataKeeper Cluster Edition helps supports improved productivity and availability of your Hyper-V VMs. 

SteelEye soon will be announcing support for Windows Server 2008 R2 and the highly anticipated “Live Migration”.  As our Windows team continues to develop and build additional capabilities for enhanced disaster recovery support of Hyper-V, we welcome your comments and suggestions. To learn more about the latest developments for Hyper-V support, take a look at our [FAQs](https://docs.us.sios.com/WindowsSPS/7.6/DKLE/DKCloudTechDoc/Content/DataKeeper/FAQs.htm) or visit the [SteelEye DataKeeper Cluster Edition](http://www.steeleye.com/products/windows/datakeeper.php) page on our website.

Thanks,

Dave