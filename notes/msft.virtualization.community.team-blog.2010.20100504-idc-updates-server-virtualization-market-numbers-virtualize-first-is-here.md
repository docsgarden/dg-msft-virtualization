---
id: 5SD7J8LhLxAhnGGhcxDUnWA
title: >-
    IDC updates server virtualization market numbers&#58; “Virtualize first” is here
desc: >-
    IDC published their worldwide quarterly server virtualization tracker report, a news release, where VMware, Microsoft and Citrix were the only vendors called out.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100504-idc-updates-server-virtualization-market-numbers-virtualize-first-is-here
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 370840eab44f1171a8e0054f9542a01ec575b107949b42bd2e3a6c8287ecd0bb
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100504-idc-updates-server-virtualization-market-numbers-virtualize-first-is-here.md
    author: "mabriggs"
    ms.author: "mabrigg"
    date: "2010-05-04 22:08:00"
    ms.date: "05/04/2010"
    categories: "citrix"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100504-idc-updates-server-virtualization-market-numbers-virtualize-first-is-here.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100504-idc-updates-server-virtualization-market-numbers-virtualize-first-is-here).

# "IDC updates server virtualization market numbers&#58; “Virtualize first” is here"

The other day the folks at IDC published their [worldwide quarterly server virtualization tracker report](https://www.idc.com/getdoc.jsp?containerId=IDC_P15379). You can see the news release [here](https://www.idc.com/getdoc.jsp?containerId=prUS22316610). Some of the main takeaways are:

·       18% of all new servers shipped in Q4’09 were virtualized, up 3% year over year

·       Sales of virtualized servers declined 14% for 2009

·       Virtualization licenses declined 7% for 2009, but were up 13% in Q4 compared to Q4 of 2008.

·       HP was the #1 server OEM for annual new server shipments virtualized (38%), followed by Dell (28%) and IBM (15%).

·       IDC said virtualization continues to remain a top priority.

IDC didn’t publish market share for virtualization software licenses, but rather published growth stats. VMware, Microsoft and Citrix were the only vendors called out in the news release. See [IDC’s news release](https://www.idc.com/getdoc.jsp?containerId=prUS22316610) for details.

Microsoft’s take. I received some interesting points from colleagues. I thought you’d be interested, too.

·       Almost 1/5 of new servers are now virtualized, and we expect the largest growth to come from servers sold to mid-market customers, and those customers finally getting around to consolidating Linux servers.

·       Customers are cost-conscious when choosing a server virtualization software.

·       According to IDC, Microsoft’s share of new x86 virtualization licenses, which includes Hyper-V and Virtual Server, is now 25%. This represents an increase of 3 points year over year, the growth obviously attributed to Hyper-V and not Virtual Server ;-).

·       During our Q3 earnings call [April 22], we reported that System Center server revenue grew by more than 20%. This figure represents enterprise customers adopting System Center management tools for configuring virtual machines, monitoring and backup of virtual and non-virtual applications.

·       On the same Q3 earnings call, we reported that the high-end editions of Windows Server have grown by more than 20%. This figure represents customers who are using Hyper-V, multiple instances of the OS to run multiple applications, and other enterprise features such failover clustering. 

·       During the last several months customers switching away from VMware, or adding Hyper-V and System Center alongside their VMware tools, became more common. These customers include Premiere Global Services (US), Group Health (US), Miele Appliances (Germany), Union Pacific (US), Telecom Italia, SuperGroup (South Africa), Mercuri Urval (Switzerland), Fpweb.net (US), Landratsamt Bayreuth (Germany), Swedish Red Cross, Apps4Rent (US), Kolektor Group (Slovenia). You can find case studies for these customers [here](https://www.microsoft.com/casestudies/).

 

Most of the above enterprise customers turned to Microsoft because we were able to extend their existing investments in Windows and System Center, and we have key features they need (e.g., live migration, manage ESX Server and Hyper-V from single console, built-in clustering). And we’re painting a picture for the future. Customers are learning more about our PaaS, Saas and IaaS capabilities to help them transition to the cloud. [Check out Bob’s take](https://www.microsoft.com/presspass/presskits/infrastructure/videoGallery.aspx?contentID=xinfra_MMS2010_day1KeynoteClip4&WT.z_convert=Share "Bob Muglia keynote highlight") on managing the gap between on-premises datacenter and online services. It’s about delivering cloud computing on your terms, with one common platform, one application model and one set of management tools.

 

 

Patrick