---
id: 6k55Hfb4CSTTZBMUpoUFtYw
title: >-
    HCS_OPERATION_COMPLETION
desc: >-
    About HCS_OPERATION_COMPLETION
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HCS_OPERATION_COMPLETION
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 4c4872332ee14ffa95f980dbbfce2edeb938def63b1afd6dface8b0451deee71
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HCS_OPERATION_COMPLETION.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HCS_OPERATION_TYPE']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HCS_OPERATION_COMPLETION.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HCS_OPERATION_COMPLETION).

# About HCS_OPERATION_COMPLETION

## Description

Function type for the completion callback of an operation.

## Syntax

```cpp
typedef void (CALLBACK *HCS_OPERATION_COMPLETION)(
    _In_ HCS_OPERATION operation,
    _In_opt_ void* context
    );
```

## Parameters

`operation`

Handle to an operation on a compute system.

`context`

Handle to the pointer of callback context.

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeDefs.h |