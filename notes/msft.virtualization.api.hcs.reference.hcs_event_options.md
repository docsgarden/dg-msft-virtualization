---
id: 44XSMdNCdsnptsJLb48s9Lk
title: >-
    HCS_EVENT_OPTIONS
desc: >-
    HCS_EVENT_OPTIONS
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HCS_EVENT_OPTIONS
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 013fa1a965ca1368ac6fd365dca3da8fbf6760c0422e3a9e403ae3d6c0394f17
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HCS_EVENT_OPTIONS.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HCS_EVENT_OPTIONS']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HCS_EVENT_OPTIONS.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HCS_EVENT_OPTIONS).

# HCS_EVENT_OPTIONS

## Description

Defines the options for an event callback registration, used in [[HcsSetComputeSystemCallback|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssetcomputesystemcallback]] and [[HcsSetProcessCallback|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssetprocesscallback]].

## Syntax

```cpp
typedef enum HCS_EVENT_OPTIONS
{
    HcsEventOptionNone = 0x00000000,
    HcsEventOptionEnableOperationCallbacks = 0x00000001
} HCS_EVENT_OPTIONS;
```

## Constants

|Name|Description|
|---|---|
|`HcsEventOptionNone`|No special event options.|
|`HcsEventOptionEnableOperationCallbacks`|Enables `HcsEventOperationCallback` [[event type|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_event_type]] in the callback supplied in [[HcsSetComputeSystemCallback|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssetcomputesystemcallback]] and [[HcsSetProcessCallback|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssetprocesscallback]]. This allows for a compute system handle or compute process handle to get notified when the operation used to track the function call completes. When event operation completion callbacks are enabled, hcs operations with callbacks are not allowed when calling HCS functions that require an operation handle. Such function calls will fail with `HCS_E_OPERATION_SYSTEM_CALLBACK_ALREADY_SET`.|


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeDefs.h |