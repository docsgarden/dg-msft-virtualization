---
id: AyhsvTVZNNCFPcxbAH7g4H3
title: >-
    The Virtualization platform vs. the Operating System
desc: >-
    The Virtualization platform vs. the Operating System
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090901-the-virtualization-platform-vs-the-operating-system
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 8d12ef3e8f0467bca43bb9570517ac0cab76838b0bf871c572aff6e70b987056
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090901-the-virtualization-platform-vs-the-operating-system.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "09/01/2009"
    date: "2009-09-01 09:01:00"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090901-the-virtualization-platform-vs-the-operating-system.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090901-the-virtualization-platform-vs-the-operating-system).

# The Virtualization platform vs. the Operating System

Day 1 at VMworld was mostly about Labs and content which builds up to the main event starting with the keynote on Tuesday morning. Looking forward to today.

In the meanwhile I have been hearing about a concept being put forth that the virtualization platform is it and the OS is but relegated to the consigns of history. That’s an interesting argument, one that in my opinion is built around the myth that somehow the interface exposed by the virtualization platform is what applications are written to. The fact of the matter is that the “Virtual Machine” as exposed by the virtualization platform mimics the underlying hardware with all its diversity. An entity is still required to manage the hardware resources and provide the software abstractions to interact with incredibly diverse hardware. That is the operating system and that’s what applications are developed for.

Back in time such as of the [Manchester Mark 1](http://en.wikipedia.org/wiki/Manchester_Mark_1) application programs were self-contained which were loaded into the memory of the machine and then executed from there. Overtime developers realized the value of re-use and rather than build in everything into the application program, re-use and common abstractions became prevalent. As the diversity of hardware and peripherals increased in complexity, the operating system is what provided the abstractions which provided a common mechanism for all applications to interact with and utilize the machine effectively. That is what gave rise to the operating system. What befuddles me is the argument that somehow the emergence of the virtualization platform minimizes the tasks as performed by the operating system. There are some who will argue that the emergence of application frameworks such as .net and Java take on this role, but in the end below them they still require the abstractions offered by an OSJ. What do people think?

An interesting debate but mostly put out to confuse our customers who are fundamentally interested in running an efficient IT infrastructure that provides them the flexibility and ability to meet the business goals and be a strategic asset to their organizations. Customers are running a multitude of applications such as SQL, Sharepoint, Exchange, CRM, home grown line of business applications etc. Our partners and us are committed and focused to make sure that it happens. 

By the way talking of partners, it was great to our partner [Leostream](http://www.leostream.com) announce their connection broker for Windows Server 2008 R2 and System Center Virtual machine Manager 2008 R2. In talking to our partner here at VMworld, it’s been wonderful to see the incredible innovation on their part based on the Microsoft platform. Thanks, this is what the Microsoft platform the best for our customers with its breadth and partner solutions. 

Vijay Tewari

Principal Program Manager, Windows Server Virtualization