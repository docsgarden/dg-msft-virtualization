---
id: 7uo253Wfj2i68xGm4LgDLB9
title: >-
    HcsSubmitWerReport
desc: >-
    HcsSubmitWerReport
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsSubmitWerReport
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 8ab324fafdc5e7472c8b6cfd582f09f8e5bcbcfe06a396b7dd5ddeed2ee2e45f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsSubmitWerReport.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsSubmitWerReport']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsSubmitWerReport.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsSubmitWerReport).

# HcsSubmitWerReport

## Description

This function submits a WER report for a bugcheck of a VM, see [[sample code|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.servicesample#submitreport]].

## Syntax

```cpp
HRESULT WINAPI
HcsSubmitWerReport(
    _In_ PCWSTR settings
    );
```

## Parameters

`settings`

JSON document of [[CrashReport|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#CrashReport]] with the bugcheck information.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |