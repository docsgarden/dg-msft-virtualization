---
id: 4r42Ey3dRz4QEsUMfRzqePU
title: >-
    Upcoming Preview of 'Disaster Recovery to Azure' Functionality in Hyper-V Recovery Manager
desc: >-
    This article discusses the upcoming preview of 'Disaster Recovery to Azure' Functionality in Hyper-V Recovery Manager.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2014/20140515-upcoming-preview-of-disaster-recovery-to-azure-functionality-in-hyper-v-recovery-manager
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 4053384eb861067b19936237e9bf931404046e71f77d137fba7f5395556c75fc
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140515-upcoming-preview-of-disaster-recovery-to-azure-functionality-in-hyper-v-recovery-manager.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "05/15/2014"
    date: "2014-05-15 06:02:35"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140515-upcoming-preview-of-disaster-recovery-to-azure-functionality-in-hyper-v-recovery-manager.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2014/20140515-upcoming-preview-of-disaster-recovery-to-azure-functionality-in-hyper-v-recovery-manager).

# New functionality in Hyper-V Recovery Manager

In the coming weeks, we will Preview functionality within Hyper-V Recovery Manager to enable Microsoft Azure as a Disaster Recovery point for virtualized workloads. The new functionality will add support for secure and seamless management of failover and failback operations using Azure IaaS Virtual Machines, thereby enabling our customers to save precious CAPEX and ongoing OPEX incurred in managing a secondary site for Disaster Recovery. Our enhanced DRaaS offering further delivers on our promise of democratizing Disaster Recovery and of making it available to _everyone_ , _everywhere_. Hyper-V Recovery Manager provides enterprise-scale Disaster Recovery using a sing-click failover in the event of a disaster to an alternate enterprise data center or to an IaaS VM in Microsoft Azure. Application and Site Level Disaster Recovery is delivered via automation of overall DR workflow, smart networking, and frequent testing using DR Drills. 

 

We announced the Preview during TechEd 2014. For more details about the upcoming Preview and existing Hyper-V Recovery Manager functionality, check out the [**DCIM-B322**](https://channel9.msdn.com/Events/TechEd/NorthAmerica/2014/DCIM-B322#fbid=?hashlink=fbid) session recording.