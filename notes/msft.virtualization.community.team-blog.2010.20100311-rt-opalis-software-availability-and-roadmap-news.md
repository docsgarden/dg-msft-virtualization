---
id: 5AQBVQnL9WWgTwvqFqe9sE6
title: >-
    RT&#58; Opalis Software Availability and Roadmap News
desc: >-
    Opalis Software Availability and Roadmap News
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100311-rt-opalis-software-availability-and-roadmap-news
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 3428fa4bdbc4e72be3ce4e00381094736b3221d54f98136c393987eb36ca9403
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100311-rt-opalis-software-availability-and-roadmap-news.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "03/11/2010"
    date: "2010-03-11 17:45:00"
    categories: "community"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100311-rt-opalis-software-availability-and-roadmap-news.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100311-rt-opalis-software-availability-and-roadmap-news).

# Opalis Software Availability and Roadmap News

Louise Johns [blogged](https://blogs.technet.com/systemcenter/archive/2010/03/03/opalis-software-availability-and-roadmap-news.aspx "System Center blog") about software availability and roadmap news for the Opalis software [we acquired](https://blogs.technet.com/virtualization/archive/2009/12/11/microsoft-acquires-opalis-software.aspx "my blog post from December") in December. Since all of you care about systems management as much as virtualization, I wanted to make sure you saw Louise's post. Here's some excerpts:

> We have started the process of sending notifications of the grant of Opalis software to all customers who are licensed for SMSE/SMSD. The key licensing contact in your organization will be receiving an email on how you can download the software and start to automate your IT processes. Any SMSE or SMSD customer with active Software Assurance on or after December 10 2009 will receive the Opalis grant. As [announced](https://blogs.technet.com/systemcenter/archive/2010/01/08/server-management-suites-new-capabilities-and-price-change.aspx) previously, the prices of SMSE/SMSD will go up on July 1, 2010 so purchase or renew now to take advantage of current prices. 

> Today, Opalis supports integration to 3rd party tools such as BMC, CA, HP, EMC, IBM and VMware. In Q2 we will expand on interoperability capabilities with new Integration Packs for UNIX, Linux Redhat and Suse. In Q3, Integration Packs for Service Manager 2010, Configuration Manager 07 R2, VMM 2008 R2 and DPM 2010 will be made available. **** Planning for Opalis v-next, 2011 releases are underway and will be communicated in the upcoming months.

>  We’ll provide more information about the sessions at MMS in the coming weeks.

Patrick