---
id: fCXsfZxGdNZ5SoDHbYySKnd
title: >-
    Editing VMConnect session settings
desc: >-
    Blog post that discusses the differences between the options given to users regarding settings for display and local resources.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2017/20170202-editing-vmconnect-session-settings
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 8f31cfaf1e45954959c4aa3189e9ad4d82611a4484aaa6ae73d6645c4b55efad
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2017/20170202-editing-vmconnect-session-settings.md
    author: "scooley"
    ms.author: "scooley"
    date: "2017-02-02 21:00:39"
    ms.date: "03/20/2019"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2017/20170202-editing-vmconnect-session-settings.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2017/20170202-editing-vmconnect-session-settings).

# Editing VMConnect session settings

When you connect to a VM with Virtual Machine Connection in enhanced session mode, you're prompted to choose some settings for display and local resources. <!--![VMConnect Session Settings](https://msdnshared.blob.core.windows.net/media/2017/02/Capture.png)](https://msdnshared.blob.core.windows.net/media/2017/02/Capture.png) -->The main thing that changes between sessions is usually display configuration. But since you can now [resize after connecting](https://blogs.technet.microsoft.com/virtualization/2017/01/27/introducing-vmconnect-dynamic-resize/) starting in the latest Insider build, you might not want to see this page each time you connect. You can select "Save my settings for future connections to this virtual machine" and you won't see this page for future sessions. <!--![VMConnect save session settings](https://msdnshared.blob.core.windows.net/media/2017/02/Capture77.png)](https://msdnshared.blob.core.windows.net/media/2017/02/Capture77.png)--> However, you might want to occasionally configure local resources like audio and devices, so there are 2 easy ways to get back to these settings:

  1. In Hyper-V Manager, you will see an option to " **Edit Session Settings...** " for any VM for which you have saved settings. <!--[ ](https://msdnshared.blob.core.windows.net/media/2017/02/Capture22.png)![VMConnect edit session settings](https://msdnshared.blob.core.windows.net/media/2017/02/Capture22.png) ](https://msdnshared.blob.core.windows.net/media/2017/02/Capture22.png)-->
  2. Open VMConnect from command line or Powershell, and specify the **/edit** flag to open the session settings.

<!--![capture33](https://msdnshared.blob.core.windows.net/media/2017/02/Capture33.png) ](https://msdnshared.blob.core.windows.net/media/2017/02/Capture33.png)--> Cheers, Andy