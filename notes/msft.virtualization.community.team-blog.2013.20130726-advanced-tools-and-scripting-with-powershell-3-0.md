---
id: 7osAdvkfui3Vtg8p7q9VdXh
title: >-
    Advanced Tools and Scripting with PowerShell 3.0
desc: >-
    Learn Advanced Tools and Scripting with PowerShell 3.0 at our free training event for Microsoft Virtual Academy on August 1st.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2013/20130726-advanced-tools-and-scripting-with-powershell-3-0
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 0d92cc1e86cfb1d6568e130a02dff602b708309bb9a4104f0741232d92fd44dc
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20130726-advanced-tools-and-scripting-with-powershell-3-0.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2013-07-26 08:00:00"
    ms.date: "07/26/2013"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20130726-advanced-tools-and-scripting-with-powershell-3-0.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2013/20130726-advanced-tools-and-scripting-with-powershell-3-0).

# Advanced Tools and Scripting with PowerShell 3.0

Join our next Microsoft Virtual Academy (MVA) Jump Start free online training event on August 1, featuring Microsoft’s [Jeffrey Snover](https://twitter.com/jsnover), Distinguished Engineer and Lead Architect for the Windows Server division and inventor of PowerShell, together with [Jason Helmick](https://twitter.com/theJasonHelmick), Senior Technologist at Concentrated Technology. Find out how to turn your real time management and automation scripts into useful reusable tools and cmdlets. You’ll learn the best patterns and practices for building and maintaining tools and you’ll pick up some special tips and tricks along the way. Register online today! <https://aka.ms/AdvPwShl>