---
id: 5Ce7jACZhA56vcZeLVTR7oL
title: >-
    HvCallSendSyntheticClusterIpiEx
desc: >-
    HvCallSendSyntheticClusterIpiEx hypercall
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallSendSyntheticClusterIpiEx
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 5c2b0606c0356412854fac6fa8f697309e45523443c9d82625775c63d842cf97
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallSendSyntheticClusterIpiEx.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallSendSyntheticClusterIpiEx.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallSendSyntheticClusterIpiEx).

# HvCallSendSyntheticClusterIpiEx

This hypercall sends a virtual fixed interrupt to the specified virtual processor set. It does not support NMIs. This version differs from [[HvCallSendSyntheticClusterIpi|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallsendsyntheticclusteripi]] in that a variable sized VP set can be specified.

The following checks should be used to infer the availability of this hypercall:

- ExProcessorMasks must be indicated via CPUID leaf 0x40000004.

## Interface

 ```c
HV_STATUS
HvCallSendSyntheticClusterIpiEx(
    _In_ UINT32 Vector,
    _In_ HV_INPUT_VTL TargetVtl,
    _In_ HV_VP_SET ProcessorSet
    );
 ```

## Call Code
`0x0015` (Simple)

## Input Parameters

| Name                    | Offset     | Size     | Information Provided                      |
|-------------------------|------------|----------|-------------------------------------------|
| `Vector`                | 0          | 4        | Specified the vector asserted. Must be between >= 0x10 and <= 0xFF.  |
| `TargetVtl`             | 4          | 1        | Target VTL                                |
| Padding                 | 5          | 3        |                                           |
| `ProcessorSet`          | 8          | Variable | Specifies a processor set representing VPs to target|

## See also

[[HV_VP_SET|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.datatypes.HV_VP_SET]]