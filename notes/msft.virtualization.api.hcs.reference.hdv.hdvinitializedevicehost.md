---
id: 7LhvRhkvvS9PvHGvf2NqeeN
title: >-
    HdvInitializeDeviceHost function
desc: >-
    HdvInitializeDeviceHost function
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvInitializeDeviceHost
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 1cbedf9a3c5eb670bb7d92051f61f887fbbf89da5cd0b47ae90bfe77137be5aa
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvInitializeDeviceHost.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HdvInitializeDeviceHost function']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvInitializeDeviceHost.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvInitializeDeviceHost).

# HdvInitializeDeviceHost function

Initializes a device emulator host in the caller’s process and associates it with the specified compute system.


## Syntax

```C++
HRESULT WINAPI
HdvInitializeDeviceHost(
    _In_  HCS_SYSTEM ComputeSystem,
    _Out_ HDV_HOST*  DeviceHost
    );
```

## Parameters

`ComputeSystem`

Handle to the compute system of the VM in which the device is used. The VM’s configuration must indicate that an external device host for the VM will be present.

`DeviceHost`

Receives the handle to the device emulator host for the VM.

## Return Value

If the function succeeds, the return value is `S_OK`.

If the function fails, the return value is an  `HRESULT` error code.

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |