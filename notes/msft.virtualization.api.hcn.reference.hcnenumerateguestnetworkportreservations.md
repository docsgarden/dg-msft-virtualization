---
id: 7zwNLRC4XWQcnSE7peuwkXf
title: >-
    HcnEnumerateGuestNetworkPortReservations
desc: >-
    HcnEnumerateGuestNetworkPortReservations
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnEnumerateGuestNetworkPortReservations
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: d63d61b2989954305b92b1c4baa8858557efc1d9fdd2fa5a6c664e370af76b86
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnEnumerateGuestNetworkPortReservations.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnEnumerateGuestNetworkPortReservations']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnEnumerateGuestNetworkPortReservations.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnEnumerateGuestNetworkPortReservations).

# HcnEnumerateGuestNetworkPortReservations

## Description

Enumerates the guest network port reservations.

## Syntax

```cpp
HRESULT
WINAPI
HcnEnumerateGuestNetworkPortReservations(
    _Out_ ULONG* ReturnCount,
    _Out_writes_bytes_all_(*ReturnCount) HCN_PORT_RANGE_ENTRY** PortEntries
    );
```

## Parameters

`ReturnCount`

Recieves the count of reserved port entries.

`PortEntries`

Recieves the list of [[port entries|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_port_range_entry]].

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |