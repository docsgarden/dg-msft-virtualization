---
id: 6UmYahmBrSEUvfwx4bNErBH
title: >-
    HvCallNotifyLongSpinWait
desc: >-
    HvCallNotifyLongSpinWait hypercall
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallNotifyLongSpinWait
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 97c8192d3c1389d39147754f47cfea74cb55450ee1a80fedde0f963131afac94
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallNotifyLongSpinWait.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallNotifyLongSpinWait.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallNotifyLongSpinWait).

# HvCallNotifyLongSpinWait

The HvCallNotifyLongSpinWait hypercall is used by a guest OS to notify the hypervisor that the calling virtual processor is attempting to acquire a resource that is potentially held by another virtual processor within the same partition. This scheduling hint improves the scalability of partitions with more than one virtual processor.

## Interface

 ```c
HV_STATUS
HvNotifyLongSpinWait(
    _In_ UINT64 SpinCount
    );
 ```

## Call Code

`0x0008` (Simple)

## Input Parameters

| Name                    | Offset     | Size     | Information Provided                      |
|-------------------------|------------|----------|-------------------------------------------|
| `SpinCount`             | 0          | 4        | Specifies the accumulated count the guest was spinning. |
| RsvdZ                   | 4          | 4        |                                           |

## Return Values

There is no error status for this hypercall, only HV_STATUS_SUCCESS will be returned as this is an advisory hypercall.