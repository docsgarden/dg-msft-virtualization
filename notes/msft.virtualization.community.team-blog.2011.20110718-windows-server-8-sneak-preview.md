---
id: 95Tkd7Jejsa9vmUWpydTWC4
title: >-
    “Windows Server 8” sneak preview
desc: >-
    Windows Server 8 sneak preview
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110718-windows-server-8-sneak-preview
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: c9f09f8d109b54299c0ed08c57974a2681f36d9cb2f41fa5a127c16dec2d2358
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110718-windows-server-8-sneak-preview.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "07/18/2011"
    date: "2011-07-18 11:12:56"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110718-windows-server-8-sneak-preview.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110718-windows-server-8-sneak-preview).

# Windows Server 8 sneak preview

If you’re in IT you are likely pulled between an almost infinite need for more computing power to deliver business solutions and the ever increasing demands for greater agility, higher efficiency and lower costs.  Fortunately, you can now deliver on these seemingly contradictory demands by leveraging the benefits of cloud computing with our[ public](http://bit.ly/r1IKqP) and [private cloud](http://bit.ly/feubuL) solutions.  And they are only going to get better. Today we are excited to give you a [sneak peek](http://bit.ly/nsJ0TT) at the next step in private cloud computing by showing you just two of **hundreds** of new capabilities coming in the next version of Windows Server, internally code-named  “Windows Server 8.” At [36:50 of this online video](http://digitalwpc.com/Videos/AllVideos/Permalink/3cb3788c-5c47-4b9e-987c-0dec4194058b/#fbid=xeCtyhEp9Qw) we demonstrate how Windows Server 8 virtual machines will help you build private clouds of greater scale by supporting (at least…) 16 virtual processors fully loaded with business critical workloads like SQL Server.  Then we show you how you can deliver improved fault tolerance and flexibility, without the added tax or complexity of additional hardware, tools and software licenses, by using the new built-in Hyper-V Replica feature.  All it takes is a few clicks, a network connection and Windows Server 8. 

But this is just the beginning!  We’re looking forward to sharing more about Windows Server 8 at Microsoft’s [BUILD](http://bit.ly/pPIhjX) conference, September 13-16, in Anaheim.