---
id: 8kE2jXyqsFJxcBJHsSoeHQP
title: >-
    HDV_PCI_INTERFACE_VERSION Enumeration
desc: >-
    HDV_PCI_INTERFACE_VERSION Enumeration
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciInterfaceVersion
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 92e72dc5556836753d69a357a7630f0358f291449e1ccb5f3a8cd078ddf006ad
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciInterfaceVersion.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HDV_PCI_INTERFACE_VERSION Enumeration']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciInterfaceVersion.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciInterfaceVersion).

# HDV_PCI_INTERFACE_VERSION Enumeration

Discriminator for the PCI device version.

## Syntax

```C++
typedef enum
{
    HdvPciDeviceInterfaceVersionInvalid = 0,
    HdvPciDeviceInterfaceVersion1 = 1

} HDV_PCI_INTERFACE_VERSION;
```

## Constants

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| HdvPciDeviceInterfaceVersionInvalid | null |
| HdvPciDeviceInterfaceVersion1 | null |
|    |    |

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |