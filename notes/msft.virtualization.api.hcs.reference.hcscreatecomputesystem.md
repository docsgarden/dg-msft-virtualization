---
id: 7Vg8MNVCnGfHupeMfEmcDxK
title: >-
    HcsCreateComputeSystem
desc: >-
    HcsCreateComputeSystem
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/reference/HcsCreateComputeSystem
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 59b98844a2d4ae7799f6916a14fa44f1666973c62479640e3e96c8ad25f8b191
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/reference/HcsCreateComputeSystem.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsCreateComputeSystem']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/reference/HcsCreateComputeSystem.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/reference/HcsCreateComputeSystem).

# HcsCreateComputeSystem

## Description

Creates a new compute system, see [[sample code|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.tutorial]] for simple example.

## Syntax

```cpp
HRESULT WINAPI
HcsCreateComputeSystem(
    _In_ PCWSTR id,
    _In_ PCWSTR configuration,
    _In_ HCS_OPERATION operation,
    _In_opt_ const SECURITY_DESCRIPTOR* securityDescriptor,
    _Out_ HCS_SYSTEM* computeSystem
    );
```

## Parameters

`id`

Unique Id identifying the compute system.

`configuration`

JSON document specifying the settings of the [[compute system|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ComputeSystem]]. The compute system document is expected to have a `Container`, `VirtualMachine` or `HostedSystem` property set since they are mutually exclusive.

`operation`

The handle to the operation that tracks the create operation.

`securityDescriptor`

Reserved for future use, must be `NULL`.

`computeSystem`

Receives a handle to the newly created compute system. It is the responsibility of the caller to release the handle using [[HcsCloseComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsclosecomputesystem]] once it is no longer in use.


## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

If the return value is `S_OK`, it means the operation started successfully. Callers are expected to get the operation's result using [[`HcsWaitForOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]] or [[`HcsGetOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]].


## Operation Results

The return value of [[`HcsWaitForOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]] or [[`HcsGetOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]] based on current operation listed as below.

| Operation Result Value | Description |
| -- | -- |
| `S_OK` | The compute system was created successfully |
| `HCS_E_OPERATION_PENDING` | The compute system has not been fully created yet |
| Other Windows `HRESULT` value | If something went wrong when creating the compute system, the return value here will give hints on what could have gone wrong |

If the operation's result is not `S_OK`, then it's possible the result document might contain the error message.


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |