---
id: A9RJWeqKQkskK5h457bManR
title: >-
    HcsResumeComputeSystem
desc: >-
    HcsResumeComputeSystem
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsResumeComputeSystem
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 077547444e792a8b3c607a61c51ae04935a02348d8fcb0b21d912fdaa6e82088
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsResumeComputeSystem.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsResumeComputeSystem']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsResumeComputeSystem.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsResumeComputeSystem).

# HcsResumeComputeSystem

## Description

Resumes the execution of a compute system, see [[sample code|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.computesystemsample#pauseresumecs]].

## Syntax

```cpp
HRESULT WINAPI
HcsResumeComputeSystem(
    _In_ HCS_SYSTEM computeSystem,
    _In_ HCS_OPERATION operation,
    _In_opt_ PCWSTR options
    );
```

## Parameters

`computeSystem`

The handle to the compute system to resume.

`operation`

The handle to the operation that tracks the resume operation.

`options`

Reserved for future use. Must be `NULL`.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

If the return value is `S_OK`, it means the operation started successfully. Callers are expected to get the operation's result using [[`HcsWaitForOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]] or [[`HcsGetOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]].


## Operation Results

The return value of [[`HcsWaitForOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]] or [[`HcsGetOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]] based on current operation listed as below.

| Operation Result Value | Description |
| -- | -- |
| `S_OK` | The compute system was resumed successfully |

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |