---
id: zKuksQjmsRAwSZReV5T72iK
title: >-
    HcsCreateProcess
desc: >-
    HcsCreateProcess
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsCreateProcess
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 9ea846e156a2143a36d7c436b747ed7c51d6a8d5582489d35fe40593aab22e8f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsCreateProcess.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsCreateProcess']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsCreateProcess.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsCreateProcess).

# HcsCreateProcess

## Description

Starts a process in a compute system.

## Syntax

```cpp
HRESULT WINAPI
HcsCreateProcess(
    _In_ HCS_SYSTEM computeSystem,
    _In_ PCWSTR processParameters,
    _In_ HCS_OPERATION operation,
    _In_opt_ const SECURITY_DESCRIPTOR* securityDescriptor,
    _Out_ HCS_PROCESS* process
    );
```

## Parameters

`computeSystem`

The handle to the compute system in which to start the process.

`processParameters`

JSON document of [[ProcessParameters|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ProcessParameters]] specifying the command line and environment for the process.

`operation`

Handle to the operation that tracks the process creation operation.

`securityDescriptor`

Reserved for future use, must be `NULL`.

`process`

Receives the `HCS_PROCESS` handle to the newly created process.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

If the return value is `S_OK`, it means the operation started successfully. Callers are expected to get the operation's result using [[`HcsWaitForOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresultandprocessinfo]] or [[`HcsGetOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresultandprocessinfo]].


## Operation Results

The return value of [[`HcsWaitForOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresultandprocessinfo]] or [[`HcsGetOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresultandprocessinfo]] based on current operation listed as below.

| Operation Result Value | Description |
| -- | -- |
| `S_OK` | The process was created successfully |


## Remarks

It is recommended for callers to use [[`HcsWaitForOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresultandprocessinfo]] or [[`HcsGetOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresultandprocessinfo]] function calls to ensure you can get a reference to the process information. This is important when the process has created standard Input/Output/Error handles. You can still get this through a call to [[`HcsGetProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetprocessinfo]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |