---
id: AXaFDDePYLe2H3GHNmSUmdX
title: >-
    HcnQueryNetworkProperties
desc: >-
    HcnQueryNetworkProperties
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnQueryNetworkProperties
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 86e870c5a131c98df4c3400717456f483219bcd824014ab906f3c052b8cee642
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnQueryNetworkProperties.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnQueryNetworkProperties']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnQueryNetworkProperties.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnQueryNetworkProperties).

# HcnQueryNetworkProperties

## Description

Queries the properties of a network.

## Syntax

```cpp
HRESULT
WINAPI
HcnQueryNetworkProperties(
    _In_ HCN_NETWORK Network,
    _In_ PCWSTR Query,
    _Outptr_ PWSTR* Properties,
    _Outptr_opt_ PWSTR* ErrorRecord
    );
```

## Parameters

`Network`

Handle to an network [[`HCN_NETWORK`|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_network]]

`Query`

Optional JSON document of [[HostComputeQuery|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.HNS_Schema#HostComputeQuery]].

`Properties`

The properties in the form of a JSON document of [[HostComputeNetwork|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.HNS_Schema#HostComputeNetwork]].

`ErrorRecord`

Receives a JSON document with extended errorCode information. The caller must release the buffer using CoTaskMemFree.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |