---
id: 9aEPG3Dx8UbS52WikXEdUuF
title: >-
    Hyper-V Replica Kerberos Error
desc: >-
    Hyper-V Replica Kerberos Error
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2013/20130213-hyper-v-replica-kerberos-error
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: fc56ca63b71fe390c8b28a0cec593dd6156c6c75b78f7197f5d302a5ce2024d3
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20130213-hyper-v-replica-kerberos-error.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "02/13/2013"
    date: "2013-02-13 22:55:00"
    categories: "hvr"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20130213-hyper-v-replica-kerberos-error.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2013/20130213-hyper-v-replica-kerberos-error).

# Kerberos Error in Hyper-V Replica

David is one of our Premier Field Engineers and he approached us with an interesting problem where the customer was encountering a Kerberos error when trying to create a replication relationship. On first glance, the setup looked very straight forward and all our standard debugging steps did not reveal anything suspicious. The error which was being encountered was "Hyper-V failed to authenticate using Kerberos authentication" and "The connection with the server was terminated abnormally (0x00002EFE)". 

David debugged the issue further and captured his experience and solution in this blog post -[ http://blogs.technet.com/b/davguents_blog/archive/2013/02/07/the-case-of-the-unexplained-windows-server-2012-replica-kerberos-errors-0x8009030c-0x00002efe.aspx.](https://blogs.technet.com/b/davguents_blog/archive/2013/02/07/the-case-of-the-unexplained-windows-server-2012-replica-kerberos-errors-0x8009030c-0x00002efe.aspx " http://blogs.technet.com/b/davguents_blog/archive/2013/02/07/the-case-of-the-unexplained-windows-server-2012-replica-kerberos-errors-0x8009030c-0x00002efe.aspx") This is a good read if you are planning to use Kerberos based authentication.