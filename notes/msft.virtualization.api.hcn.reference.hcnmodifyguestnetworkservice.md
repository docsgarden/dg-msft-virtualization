---
id: 7Atg8wgocDrufJ5yqPKVqfF
title: >-
    HcnModifyGuestNetworkService
desc: >-
    HcnModifyGuestNetworkService
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnModifyGuestNetworkService
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: ade2f0981df02adec634939988d918057ef0a4fdfe6d9852e002e7a3827e2737
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnModifyGuestNetworkService.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnModifyGuestNetworkService']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnModifyGuestNetworkService.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnModifyGuestNetworkService).

# HcnModifyGuestNetworkService

## Description

Modifies a guest network service.

## Syntax

```cpp
HRESULT
WINAPI
HcnModifyGuestNetworkService(
    _In_ HCN_GUESTNETWORKSERVICE GuestNetworkService,
    _In_ PCWSTR Settings,
    _Outptr_opt_ PWSTR* ErrorRecord
    );
```

## Parameters

`GuestNetworkService`

The [[HCN\_GUESTNETWORKSERVICE|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_guestnetworkservice]] to modify.

`Settings`

JSON document specifying the settings of the [[guest network service|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.HNS_Schema#GuestNetworkService]].

`GuestNetworkService`

Receives a handle to the newly created guest network service. It is the responsibility of the caller to release the handle using [[HcnCloseGuestNetworkService|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcncloseguestnetworkservice]] once it is no longer in use.

`ErrorRecord`

Receives a JSON document with extended errorCode information. The caller must release the buffer using CoTaskMemFree.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |