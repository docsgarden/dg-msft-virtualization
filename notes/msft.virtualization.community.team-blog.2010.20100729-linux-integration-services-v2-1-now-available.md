---
id: 6bFDZZUhLUTjHSJLHf2syXu
title: >-
    Linux Integration Services v2.1 Now Available
desc: >-
    We are really excited to announce the availability of the Hyper-V Linux Integration Services for Linux Version 2.1.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100729-linux-integration-services-v2-1-now-available
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 0bce160d761d6a260d29037a342b93951f574a6bff510ab69a49e477f1e8f9ac
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100729-linux-integration-services-v2-1-now-available.md
    author: "scooley"
    ms.author: "scooley"
    date: "2010-07-29 13:30:00"
    ms.date: "07/29/2010"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100729-linux-integration-services-v2-1-now-available.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100729-linux-integration-services-v2-1-now-available).

# Linux Integration Services v2.1 Now Available

We are really excited to announce the availability of the Hyper-V Linux Integration Services for Linux Version 2.1. This release marks yet another milestone in providing a comprehensive virtualization platform to our customers. Customers who have a heterogeneous operating system environment desire their virtualization platform to provide support for all operating systems that they have in their datacenters. We have supported Linux as a guest operating system on our virtualization platform from the days of Virtual Server and continue to enhance our support in that regard. 

The following features are included in the 2.1 release:

**Driver support for synthetic devices** : Linux Integration Services supports the synthetic network controller and the synthetic storage controller that were developed specifically for Hyper-V.

**Fastpath Boot Support for Hyper-V** : Boot devices take advantage of the block Virtualization Service Client (VSC) to provide enhanced performance.

**Timesync:** The clock inside the virtual machine will remain synchronized with the clock on the host.

**Integrated Shutdown:** Virtual machines running Linux can be gracefully shut down from either Hyper-V Manager or System Center Virtual Machine Manager.

**Symmetric Multi-Processing (SMP) Support:** Supported Linux distributions can use up to 4 virtual processors (VP) per virtual machine. 

**Heartbeat:** Allows the host to detect whether the guest is running and responsive. **

**

**Pluggable Time Source:** A pluggable clock source module is included to provide a more accurate time source to the guest. **

**

                                                                

This version of the integration services for Hyper-V supports Novell SUSE Linux Enterprise Server 10 SP3, SUSE Linux Enterprise Server 11, and Red Hat Enterprise Linux 5.2 / 5.3 / 5.4 / 5.5.

Customers can obtain the Linux IC’s via the Microsoft Download Center at this link: [https://www.microsoft.com/downloads/details.aspx?displaylang=en&FamilyID=eee39325-898b-4522-9b4c-f4b5b9b64551](https://www.microsoft.com/downloads/details.aspx?displaylang=en&FamilyID=eee39325-898b-4522-9b4c-f4b5b9b64551)