---
id: 9oTNZnjwg5MHBfrfEMwHREL
title: >-
    System Center Ops Manager management pack for Hyper-V
desc: >-
    A beta of the Hyper-V management pack for System Center Operations Manager (2007, 2007 R2) is available.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090313-system-center-ops-manager-management-pack-for-hyper-v
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 564c4eef72734ac8e02711ce3fd34eb856491d52654a12b041dc0fff3bfe2d84
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090313-system-center-ops-manager-management-pack-for-hyper-v.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-03-13 14:43:00"
    ms.date: "03/13/2009"
    categories: "esx"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090313-system-center-ops-manager-management-pack-for-hyper-v.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090313-system-center-ops-manager-management-pack-for-hyper-v).

# System Center Ops Manager management pack for Hyper-V

A quick note that a beta of the Hyper-V management pack for System Center Operations Manager (2007, 2007 R2) is available. In combination with SCVMM (for advanced monitoring of VMs across your Hyper-V, ESX and Virtual Server environments), this SCOM management pack enables health/perf monitoring of the Hyper-V host. The management pack includes health diagram view of virtual machines, virtual components roll-up per host, critical Hyper-V Service monitoring, disk space threshold monitoring.

This beta management pack supports the following OS only (for now):

§  Windows Server 2008 Standard Edition (Full Installation with Hyper-V Role enabled)

§  Windows Server 2008 Enterprise Edition (Full Installation with Hyper-V Role enabled)

§  Windows Server 2008 Datacenter Edition (Full Installation with Hyper-V Role enabled)

 

To request an invitation to access this beta, you can send a message to the following address: [MPCC@microsoft.com](https://blogs.technet.commailto:MPCC@microsoft.com?subject=Request:%20Hyper-V%20Management%20Pack%20Registration%20Code)

Patrick