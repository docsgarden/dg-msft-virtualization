---
id: 6Y3Ab4MC4sG6Lb6wm6LMg9T
title: >-
    A closer look at VM Quick Create
desc: >-
    A blog post about the virtual machine Quick Create feature that makes the virtual machine setup process easier for users.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2017/20170120-a-closer-look-at-vm-quick-create
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 3f97731f53370b37672ee95f30abb7d6f84863f06aa1aa820d8b70ba7b53b991
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2017/20170120-a-closer-look-at-vm-quick-create.md
    author: "scooley"
    ms.author: "scooley"
    date: "2017-01-20 22:19:32"
    ms.date: "03/20/2019"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2017/20170120-a-closer-look-at-vm-quick-create.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2017/20170120-a-closer-look-at-vm-quick-create).

# A closer look at VM Quick Create

Author: Andy Atkinson

In the last Insiders build, we introduced Quick Create to quickly create virtual machines with less configuration (see [blog](https://blogs.technet.microsoft.com/virtualization/2017/01/10/cool-new-things-for-hyper-v-on-desktop/)).

<!--![image](https://msdnshared.blob.core.windows.net/media/2017/01/image_thumb433.png)](https://msdnshared.blob.core.windows.net/media/2017/01/image511.png)-->

We’re trying a few things to make it easier to set up a virtual machine, such as combining installation options to a single field for all supported file types, and adding a control to enable Windows Secure Boot more easily.

<!--![image](https://msdnshared.blob.core.windows.net/media/2017/01/image_thumb434.png)](https://msdnshared.blob.core.windows.net/media/2017/01/image512.png)-->

Quick Create can also help set up your network. If there’s no available switch, you’ll see a button to set up an “automatic network”, which will automatically configure an external switch for the virtual machine and connect it to the network.

To simplify the number of settings, we had to pick some good default settings for the virtual machine, which are currently:

  * Generation: 2
  * StartupRAM: 1024 MB
  * DynamicRAM: Enabled
  * Virtual Processors: 1



After the virtual machine is created, you will see the confirmation page with quick access to edit settings or to connect.

<!--![image](https://msdnshared.blob.core.windows.net/media/2017/01/image_thumb435.png)](https://msdnshared.blob.core.windows.net/media/2017/01/image513.png)-->

Are there other controls you want in Quick Create? Are we picking good defaults? 

This is still a work in progress, so let us know what you think!

\- Andy