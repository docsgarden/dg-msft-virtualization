---
id: 6TdbS9QrdSkC4autXs9BRrP
title: >-
    Big news&#58;  Microsoft and HP team up to move IT forward
desc: >-
    Microsoft and HP team up to move IT forward
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100113-big-news-microsoft-and-hp-team-up-to-move-it-forward
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: d5296e65c5a7c1a71c91a06b7ef814224f899a9f83f5157b5ca8ec19c0408b78
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100113-big-news-microsoft-and-hp-team-up-to-move-it-forward.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "01/13/2010"
    date: "2010-01-13 08:11:00"
    categories: "application-virtualization"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100113-big-news-microsoft-and-hp-team-up-to-move-it-forward.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100113-big-news-microsoft-and-hp-team-up-to-move-it-forward).

# Big news, Microsoft and HP team up to move IT forward

This morning Microsoft and HP announced a three-year, $250 million agreement to jointly deliver a new infrastructure-to-application model, advance cloud computing and generally reduce the costs and complexities of IT.   

 

The press release, executive videos and other information about the agreement are available [here](https://www.hp.com/us-en/hp-news/press-release.html?id=168065#.YmwJyO3MK70).

 

This is a big commitment on the part of both companies, representing the industry’s most comprehensive technology stack integration to date.  There are three main components to the agreement: 

 

·         A shared engineering roadmap to ensure strong integration between hardware and software - both on premises and in the cloud.

·         Joint solutions to optimize and simplify the deployment, management and health of the datacenter.

·         Investment in joint sales, service and channel partner programs 

 

Virtualization is (of course) at the center of the partnership.  New solutions based on HP Converged Infrastructure with Microsoft Hyper-V and applications will enable customers to speed up implementation time, reduce unplanned downtime, and lower network/cabling infrastructure costs.  And they will help customers deliver service-based infrastructure and the foundation for cloud computing.

 

Together we’ll provide heterogeneous data center management solutions for virtualized environments through deep integration of [HP’s Insight Software](https://www.hp.com/hk-en/hp-news/press-release.html?id=103725#.YmwKbe3MK71) and [Business Technology Optimization software](https://www.hp.com/hpinfo/newsroom/press/2008/080617xa.html) with the System Center suite. You will be able to automate the provisioning, monitoring and ongoing maintenance of virtualized IT services in a mixed datacenter.  We’ll simplify initial deployment, hardware monitoring and power management while providing performance optimization of HP ProLiant servers and HP BladeSystem infrastructure. 

 

We’ll also deliver HP Virtualization Smart Bundles with Hyper-V for small- to midsize businesses that simplify the purchase, setup and maintenance of a virtualized infrastructure. These will be based on HP server, storage and networking solutions, coupled with Microsoft Windows Server and HP Insight Software. You will also be able to add System Center Essentials and HP Operations Center solutions to unify the management of hardware, OS and applications.

 

Over the course of the next three years the two companies will work together on joint technology to: 

 

·         Ensure storage is an integrated part of the virtual infrastructure, via in-depth management of HP storage within System Center environments.

 

·         Enable faster virtualization deployments by repurposing direct-attached server storage into shared storage resources with the HP Virtual SAN Appliance for Hyper-V.

 

·         Help customers implement private cloud by combining HP’s virtualization infrastructure expertise with the [Microsoft Dynamic Datacenter Toolkit](https://www.microsoft.com/hosting/dynamicdatacenter/Home.html). 

 

·         Help mid-market customers to take control of their server, storage, networking and client devices - as well as OS and applications - from a single console.   We’ll integrate core HP ProLiant management with HP Operations Center and Microsoft System Center Essentials.

 

·         Facilitate dynamic management of virtual and physical networks by delivering support of HP Flex Fabric within Microsoft virtualization and System Center.

 

A big day for both companies…and for our customers and partners!  Let us know what you think!