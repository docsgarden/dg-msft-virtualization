---
id: 5P8mPE8KPxz4JfNZzRoXM2z
title: >-
    GA of Hyper-V Recovery Manager (HRM)
desc: >-
    This article shares the general availability of HRM and provides relevant resources.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2014/20140116-ga-of-hyper-v-recovery-manager-hrm
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 4cb3d68a19050de37cc8b7b747e5bcd7c51f1e1ab879f9a8e1d36569c8fb20f9
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140116-ga-of-hyper-v-recovery-manager-hrm.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "01/16/2014"
    categories: "hrm"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140116-ga-of-hyper-v-recovery-manager-hrm.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2014/20140116-ga-of-hyper-v-recovery-manager-hrm).

# Hyper-V (HRM) Recovery Manager Availability

We are excited to announce the General Availability of Hyper-V Recovery Manager or HRM for short. HRM is an Azure hosted service which orchestrates the protection and recovery of virtual machines in your datacenter. Hyper-V Replica replicates your virtual machines from your primary datacenter to your secondary datacenter. To re-emphasize, your VMs are **not replicated to Windows Azure**. HRM is a service hosted in Azure which acts as the “control plane”.

The high level solution is as follows:

![image](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/metablogapi/image_thumb_08F9A01C.png)](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/metablogapi/image_63015F14.png)

A step by step guide on setting up HRM is available @ <http://www.windowsazure.com/en-us/documentation/articles/hyper-v-recovery-manager-configure-vault/>

If you have questions on HRM, visit the TechNet forum @ <https://social.msdn.microsoft.com/Forums/windowsazure/en-US/home?forum=hypervrecovmgr>

Pricing, SLA details for the service is available @ <http://www.windowsazure.com/en-us/pricing/details/recovery-manager/>

Brad Anderson’s blog on HRM - <https://blogs.technet.com/b/in_the_cloud/archive/2014/01/16/announcing-the-ga-of-windows-azure-hyper-v-recovery-manager.aspx> – provides more details on the solution.