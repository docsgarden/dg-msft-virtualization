---
id: AZtcreDmUEy3U9yqowvfNvo
title: >-
    HdvDeliverGuestInterrupt
desc: >-
    HdvDeliverGuestInterrupt
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvDeliverGuestInterrupt
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 3fc2e6c319a906f6edd1dd1558d51171a5a86828c591d61bbbb598f6188b0101
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvDeliverGuestInterrupt.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HdvDeliverGuestInterrupt']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvDeliverGuestInterrupt.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvDeliverGuestInterrupt).

# HdvDeliverGuestInterrupt

## Description

Delivers a message signalled interrupt (MSI) to the guest partition.

## Syntax

```C++
HRESULT WINAPI
HdvDeliverGuestInterrupt(
    _In_ HDV_DEVICE Requestor,
    _In_ UINT64     MsiAddress,
    _In_ UINT32     MsiData
    );
```

## Parameters

|Parameter|Description|
|---|---|---|---|---|---|---|---|
|`Requestor` |Handle to the device requesting the interrupt.|
|`MsiAddress` |The guest address to which the interrupt message is written.|
|`MsiData`|The data to write at MsiAddress.|
|    |    |

## Return Values

|Return Value     |Description|
|---|---|
|`S_OK` | Returned if function succeeds.|
|`HRESULT` | An error code is returned if the function fails.
|     |     |

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |