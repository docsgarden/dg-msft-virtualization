---
id: 7aggemhc3LqFvFhRaP3YrYB
title: >-
    The ReadGuestPhysicalAddress function
desc: >-
    Reads from the saved state file the given guest physical address range and then it is written into the supplied buffer.
canonicalUrl: https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/ReadGuestPhysicalAddress
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 4cc8aead72a2a19342f6ee7acb5202b6c3f76b3d22cf46b0b15367a7ae37f1b4
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/ReadGuestPhysicalAddress.md
    ms.date: "04/19/2022"
    author: "mattbriggs"
    ms.author: "mabrigg"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/ReadGuestPhysicalAddress.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/ReadGuestPhysicalAddress).

# ReadGuestPhysicalAddress function

Reads from the saved state file the given guest physical address range and then it is written into the supplied buffer. If BytesRead returns something lower than BufferSize, then the end of memory has been reached.

## Syntax

```C
HRESULT
WINAPI
ReadGuestPhysicalAddress(
    _In_        VM_SAVED_STATE_DUMP_HANDLE  VmSavedStateDumpHandle,
    _In_        GUEST_PHYSICAL_ADDRESS      PhysicalAddress,
    _Out_writes_bytes_(BufferSize) LPVOID   Buffer,
    _In_        UINT32                      BufferSize,
    _Out_opt_   UINT32*                     BytesRead
    );
```

## Parameters

`VmSavedStateDumpHandle`

Supplies a handle to a dump provider instance.

`PhysicalAddress`

Supplies the physical address to read.

`Buffer`

Returns the read memory range on the given address.

`BufferSize`

Supplies the requested byte count to read.

`BytesRead`

Optionally returns the bytes actually read

## Return Value

If the operation completes successfully, the return value is `S_OK`.

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |