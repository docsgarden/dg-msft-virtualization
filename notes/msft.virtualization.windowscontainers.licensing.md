---
id: 8iZXRbkHpgsrAPMcPQ2VWQD
title: >-
    Licensing
desc: >-
    Licensing information for Virtualization on Windows
canonicalUrl: https://docs.microsoft.com/virtualization/windowscontainers/licensing
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 5daaab1aa73ce9a11cc82d27ee790fe26ac8a3aa1339a20d1b8103fb201f3a0f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/windowscontainers/licensing.md
    author: "Heidilohr"
    ms.author: "helohr"
    ms.date: "12/19/2016"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/windowscontainers/licensing.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/windowscontainers/licensing).

# Licensing