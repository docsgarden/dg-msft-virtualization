---
id: 8CwG9TfPdmv5p8xGi8ADPK2
title: >-
    WHV_EMULATOR_MEMORY_ACCESS_INFO method
desc: >-
    Learn about the WHV_EMULATOR_MEMORY_ACCESS_INFO method.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorMemoryAccessInfo
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 76b67ffc02568d53a9201fa3a8ef857549a3f528b4448f497202ad167e17a660
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorMemoryAccessInfo.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/19/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorMemoryAccessInfo.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorMemoryAccessInfo).

# WHV_EMULATOR_MEMORY_ACCESS_INFO


## Syntax

```c
typedef struct _WHV_EMULATOR_MEMORY_ACCESS_INFO {
    UINT64 GpaAddress;
    UINT8 Direction; // 0 for read, 1 for write
    UINT8 AccessSize; // 1 thru 8
    UINT8 Data[8]; // Raw byte contents to be read from/written to memory
} WHV_EMULATOR_MEMORY_ACCESS_INFO;
```
## Remarks
Information about the requested memory access by the emulator.

`GpaAddress` is the guest physical address attempting to be accessed.

`Direction` is 0 for a memory read access, 1 for a write access.

`AccessSize` is how big this memory access is in bytes, with a valid size of 1 to 8.

`Data` is a byte array with the same layout as memory, to either be filled out by the virtualization stack for a read, or containing the data to write to memory for a write.