---
id: 3fP5vFpbBwZUb9us9dhF4MP
title: >-
    HcnCloseNamespace
desc: >-
    HcnCloseNamespace
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnCloseNamespace
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 2d54cd87d6c0c5ac981a7860b3378f1d961de7cf7ab7f4842da06af1cf933174
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnCloseNamespace.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnCloseNamespace']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnCloseNamespace.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnCloseNamespace).

# HcnCloseNamespace

## Description

Close a handle to a namespace.

## Syntax

```cpp
HRESULT
WINAPI
HcnCloseNamespace(
    _In_ HCN_NAMESPACE Namespace
    );

```

## Parameters

`Namespace`

Handle to a namespace [[`HCN_NAMESPACE`|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_namespace]]

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |