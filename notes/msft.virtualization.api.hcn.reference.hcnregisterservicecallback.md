---
id: 4SaiAkMeof5L8WBPySvQyZX
title: >-
    HcnRegisterServiceCallback
desc: >-
    HcnRegisterServiceCallback
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnRegisterServiceCallback
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: baa4d5d91ed1599429d71ef8a8074c019529f3597ce945612860f92bbe78c117
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnRegisterServiceCallback.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnRegisterServiceCallback']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnRegisterServiceCallback.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnRegisterServiceCallback).

# HcnRegisterServiceCallback

## Description

Registers a callback function to receive service notifications.

## Syntax

```cpp
HRESULT
WINAPI
HcnRegisterServiceCallback(
    _In_ HCN_NOTIFICATION_CALLBACK Callback,
    _In_ void* Context,
    _Outptr_ HCN_CALLBACK* CallbackHandle
    );
```

## Parameters

`Callback`

The [[HCN_NOTIFICATION_CALLBACK|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_notification_callback]] for the callback.

`Context`

Context that is provided on the callbacks.

`CallbackHandle`

Receives a [[HCN_CALLBACK|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_callback]]. It is the responsibility of the caller to release the handle using [[HcnUnregisterServiceCallback|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnunregisterservicecallback]] once it is no longer in use.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |