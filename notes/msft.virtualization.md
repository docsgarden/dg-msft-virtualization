---
id: ggbz1p0evfjb3nmeungnf8l
title: Microsoft Virtualization Docs Vault
desc: ''
updated: 1659059126103
created: 1657855896379
---

[![Converted for Dendron](https://img.shields.io/badge/Converted%20for-Dendron-brightgreen)](https://dendron.so)
[![Published with GitLab Pages](https://img.shields.io/badge/Published%20with-GitLab%20Pages-blue)](https://docsgarden.gitlab.io/dg-msft-virtualization/)
[![CC-BY 4.0 License](https://img.shields.io/badge/License-Creative%20Commons%20Attribution%204.0%20International-orange)](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE)

Dendron docs vault of Microsoft Virtualization converted from [official Microsoft source docs](https://github.com/MicrosoftDocs/Virtualization-Documentation).

- [View the published Dendron vault on GitLab Pages](https://docsgarden.gitlab.io/dg-msft-virtualization/)

## Dendron Vault Visualization

> Generated with `dendron visualize` via [Dendron CLI](https://wiki.dendron.so/notes/RjBkTbGuKCXJNuE4dyV6G/).

![Packed circles visualization](./assets/diagram-dg-msft-virtualization.svg)
