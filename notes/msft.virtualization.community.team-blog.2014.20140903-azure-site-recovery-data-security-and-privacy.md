---
id: 46qTu3wR2frCM2KinsTyS9Q
title: >-
    Azure Site Recovery - Data Security and Privacy
desc: >-
    Learn about how Azure Site Recovery maintains customer data security and privacy.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2014/20140903-azure-site-recovery-data-security-and-privacy
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 790a1f559f151c93a240f17040dd355970523314a068940338677f15ed45897d
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140903-azure-site-recovery-data-security-and-privacy.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2014-09-03 01:56:45"
    ms.date: "09/03/2014"
    categories: "azure-site-recovery"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140903-azure-site-recovery-data-security-and-privacy.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2014/20140903-azure-site-recovery-data-security-and-privacy).

# Azure Site Recovery - Data Security and Privacy

Microsoft is committed to ensuring the Privacy and Security of our customer’s data whenever it crosses on-premises boundaries and into Microsoft Azure. Azure Site Recovery, a cloud-based Disaster Recovery Service that enables protection and orchestrated recovery of your virtualized workloads across on-premises private clouds or directly into Azure, has been designed ground up to align with [Microsoft’s privacy and security commitment](https://aka.ms/asrsecurityblog_bradblog). 

_Specifically our promise is to ensure that:_

  * We encrypt customer data while in transit and at rest 
  * We use best-in-class industry cryptography to protect all channels, including Perfect Forward Secrecy and 2048-bit key lengths



To read more about how the Azure Site Recovery architecture delivers on these key goals, check out our new blog post, [Azure Site Recovery: Our Commitment to Keeping Your Data Secure](https://aka.ms/virtualization_azure_blog_security_blog) on the Microsoft Azure blog.