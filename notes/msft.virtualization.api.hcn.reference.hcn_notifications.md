---
id: AFvPuNfJSYea2ERsjdVaqHa
title: >-
    HCN_NOTIFICATIONS
desc: >-
    HCN_NOTIFICATIONS
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_NOTIFICATIONS
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: e0140be230c87716f311bfd87c64466e6691152dd167a3f13091192b867f0927
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_NOTIFICATIONS.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HCN_NOTIFICATIONS']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_NOTIFICATIONS.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_NOTIFICATIONS).

# HCN\_NOTIFICATIONS

## Description

Function type for compute system event callbacks.

## Syntax

```cpp
typedef enum HCN_NOTIFICATIONS
{
       HcnNotificationInvalid                                  = 0x00000000,

       /// Notifications for HCN_SERVICE handles
       HcnNotificationNetworkPreCreate                         = 0x00000001,
       HcnNotificationNetworkCreate                            = 0x00000002,
       HcnNotificationNetworkPreDelete                         = 0x00000003,
       HcnNotificationNetworkDelete                            = 0x00000004,

       /// Namespace Notifications
       HcnNotificationNamespaceCreate                          = 0x00000005,
       HcnNotificationNamespaceDelete                          = 0x00000006,

       /// Notifications for HCN_SERVICE handles
       HcnNotificationGuestNetworkServiceCreate                = 0x00000007,
       HcnNotificationGuestNetworkServiceDelete                = 0x00000008,

       /// Notifications for HCN_NETWORK handles
       HcnNotificationNetworkEndpointAttached                  = 0x00000009,
       HcnNotificationNetworkEndpointDetached                  = 0x00000010,

       /// Notifications for HCN_GUESTNETWORKSERVICE handles
       HcnNotificationGuestNetworkServiceStateChanged          = 0x00000011,
       HcnNotificationGuestNetworkServiceInterfaceStateChanged = 0x00000012,

       /// Common notifications
       HcnNotificationServiceDisconnect                        = 0x01000000,

       /// The upper 4 bits are reserved for flags
       HcnNotificationFlagsReserved                            = 0xF0000000
} HCN_NOTIFICATIONS;
```


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |