---
id: 6UMmtcXWDK5Qs7UADm9GmpN
title: >-
    Check out the New Microsoft Server and Cloud Platform Website!
desc: >-
    Check out the New Microsoft Server and Cloud Platform Website!
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110825-check-out-the-new-microsoft-server-and-cloud-platform-website
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 8a8db83d372ee8a5e5e620c907571c5274cff30488fe9e818a5bdc82468ac590
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110825-check-out-the-new-microsoft-server-and-cloud-platform-website.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "08/25/2011"
    date: "2011-08-25 13:11:42"
    categories: "cloud-computing"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110825-check-out-the-new-microsoft-server-and-cloud-platform-website.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110825-check-out-the-new-microsoft-server-and-cloud-platform-website).

# Check out the New Microsoft Server and Cloud Platform Website!

We’re pleased to announce the launch of the new [Microsoft Server and Cloud Platform website](http://bit.ly/omgt2U)   
([http://www.microsoft.com/server-cloud](http://bit.ly/omgt2U)).  

The new site is a comprehensive source for information relating to our private cloud  
and virtualization solutions, as well as other solutions based on Windows  
Server, System Center, Forefront and related products.  It is designed to  
help connect you to relevant information across Microsoft web destinations,  
including TechNet, MSDN, Pinpoint and more - making it a great place to  
understand what we offer, why you should consider it and how to get started.  
  
Enjoy the new site and stay tuned to the blog for the latest Server & Cloud  
Platform updates!