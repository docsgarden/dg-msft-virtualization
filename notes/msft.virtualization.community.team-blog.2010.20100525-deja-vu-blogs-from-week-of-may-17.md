---
id: 4kPafinEusq4AC4EqGejFJM
title: >-
    Deja vu&#58; blogs from week of May 17
desc: >-
    Due to the planned MSDN/TechNet blog upgrade, we have to re-post blogs posted during the week of May 17.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100525-deja-vu-blogs-from-week-of-may-17
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: df107b6888e1645ccb503d2cd65a9d7ce2e9f8267ba951e9dd576bcafb7934c4
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100525-deja-vu-blogs-from-week-of-may-17.md
    author: "scooley"
    ms.author: "scooley"
    date: "2010-05-25 19:29:44"
    ms.date: "05/25/2010"
    categories: "community"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100525-deja-vu-blogs-from-week-of-may-17.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100525-deja-vu-blogs-from-week-of-may-17).

# Deja vu: blogs from week of May 17

Hi readers, Due to the planned MSDN/TechNet blog upgrade, we have to re-post blogs posted during the week of May 17.  We wanted you to know you'll be seeing 2-3 posts again. Thanks for your patience with us. 

Patrick