---
id: Ab6Re78gB7qbAtFDTvX4PBG
title: >-
    Troubleshoot Hyper-V on Windows 10
desc: >-
    Troubleshoot Hyper-V on Windows 10
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/user-guide/troubleshooting
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: cd0966d26fa09530261222cac45b14efadefebea1f725d796fd73db8f7d3c191
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/user-guide/troubleshooting.md
    keywords: "windows 10, hyper-v"
    author: "scooley"
    ms.author: "scooley"
    ms.date: "05/02/2016"
    ms.topic: "article"
    ms.prod: "windows-10-hyperv"
    ms.assetid: "f0ec8eb4-ffc4-4bf1-9a19-7a8c3975b359"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/user-guide/troubleshooting.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/user-guide/troubleshooting).

# Troubleshoot Hyper-V on Windows 10

## I updated to Windows 10 and now I can't connect to my downlevel (Windows 8.1 or Server 2012 R2) host
In Windows 10, Hyper-V manager moved to WinRM for remote management.  What that means is now Remote Management has to be enabled on the remote host in order to use Hyper-V manager to manage it.

For more information see [Managing Remote Hyper-V Hosts](https://docs.microsoft.com/en-us/windows-server/virtualization/hyper-v/manage/Remotely-manage-Hyper-V-hosts)

## I changed the checkpoint type, but it is still taking the wrong type of checkpoint
If you are taking the checkpoint from VMConnect and you change the checkpoint type in Hyper-V manager the checkpoint taken be whatever checkpoint type was specified when VMConnect was opened.

Close VMConnect and reopen it to make it take the correct type of checkpoint.

## When I try to create a virtual hard disk on a flash drive, an error message is displayed
Hyper-V does not support FAT/FAT32 formatted disk drives since these file systems do not provide access control lists (ACLs) and do not support files larger than 4GB. ExFAT formatted disks only provide limited ACL functionality and are therefore also not supported for security reasons.
The error message displayed in PowerShell is "The system failed to create '\[path to VHD\]': The requested operation could not be completed due to a file system limitation (0x80070299)."

Use a NTFS formatted drive instead. 

## I get this message when I try to install: "Hyper-V cannot be installed: The processor does not support second level address translation (SLAT)."
Hyper-V requires SLAT in order to run virtual machines. If you computer does not support SLAT, then it cannot be a host for virtual mahchines.

If you are only trying to install the management tools, unselect **Hyper-V Platform** in **Programs and Features** > **Turn Windows features on or off**.