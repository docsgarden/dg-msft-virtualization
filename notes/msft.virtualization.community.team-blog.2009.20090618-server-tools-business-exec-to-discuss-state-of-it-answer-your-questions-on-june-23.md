---
id: CjZsJvPXiY9hmpBVpMCb8gJ
title: >-
    Server & Tools Business Exec to discuss state of IT, answer your questions on June 23
desc: >-
    On Tuesday June 23 2009 Bob Kelly will talk about the state of IT within the context of results from a new Harris Interactive study.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090618-server-tools-business-exec-to-discuss-state-of-it-answer-your-questions-on-june-23
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 84daff5b063669b02a43f887190612f80a237ba0bcbf5d5ac4011b319a25340e
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090618-server-tools-business-exec-to-discuss-state-of-it-answer-your-questions-on-june-23.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-06-18 00:27:00"
    ms.date: "06/18/2009"
    categories: "cloud-computing"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090618-server-tools-business-exec-to-discuss-state-of-it-answer-your-questions-on-june-23.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090618-server-tools-business-exec-to-discuss-state-of-it-answer-your-questions-on-june-23).

# Server & Tools Business Exec to discuss state of IT, answer your questions on June 23

Do you have questions about Microsoft’s efforts to deliver new server infra (Windows Server, virt, System Center, Forefront security) to customers and partners? 

What is Microsoft doing about enterprise security?

Are you interested in hearing how other IT pros are reacting to economic conditions and where they’re investing? 

 

On Tuesday, June 23rd from 10:30am – 11:00am (PDT), you're invited to join a teleconference with [Bob Kelly](https://www.microsoft.com/presspass/exec/kelly/), corporate VP of Infrastructure Server Marketing. Bob will talk about the state of IT within the context of results from a new Harris Interactive study. The study, of 1,200 IT professionals from the U.S., United Kingdom, Japan and Germany, was commissioned by Microsoft’s Server & Tools Business. 

 

There will be time for your questions following the brief presentation. Submit questions over the phone or you can submit them at any time leading up to, or during, the teleconference by tweeting with the Twitter hashtag,  **#qs4ms**. If you are interested in attending, please [ REGISTER NOW](http://www.weccg.com/hosting/emails/MSInfrastructurePressQA.ics). Once you open the invite box, you can save and close to your calendar.

 

Patrick