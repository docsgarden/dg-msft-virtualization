---
id: 6bhgDT4Eg6cWFuC6kNctChn
title: >-
    HcsSetOperationCallback
desc: >-
    HcsSetOperationCallback
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsSetOperationCallback
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: d5be047cd44426764506e6143afb494c2a2a6fbde9e41bdfd86d6343efcab00b
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsSetOperationCallback.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsSetOperationCallback']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsSetOperationCallback.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsSetOperationCallback).

# HcsSetOperationCallback

## Description

Sets a callback that is invoked on completion of an operation.

## Syntax

```cpp
HRESULT WINAPI
HcsSetOperationCallback(
    _In_ HCS_OPERATION operation,
    _In_opt_ const void* context,
    _In_ HCS_OPERATION_COMPLETION callback
    );
```

## Parameters

`operation`

The handle to an active operation.

`context`

Optional pointer to a context that is passed to the callback.

`callback`

The target [[`HCS_OPERATION_COMPLETION`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_operation_completion]] callback that is invoked on completion of an operation.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |