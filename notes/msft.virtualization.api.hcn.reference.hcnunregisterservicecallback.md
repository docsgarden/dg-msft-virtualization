---
id: 6kALKunLV3oPcb6N6Lw96CX
title: >-
    HcnUnregisterServiceCallback
desc: >-
    HcnUnregisterServiceCallback
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnUnregisterServiceCallback
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 59b3ad8d7a5711b2ba2fd52314caad217917a1f85bd70a92f322a31242eb545e
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnUnregisterServiceCallback.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnUnregisterServiceCallback']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnUnregisterServiceCallback.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnUnregisterServiceCallback).

# HcnUnregisterServiceCallback

## Description

Unregisters a guest network service callback.

## Syntax

```cpp
HRESULT
WINAPI
HcnUnregisterServiceCallback(
    _In_ HCN_CALLBACK CallbackHandle
    );
```

## Parameters

`CallbackHandle`

The [[HCN_CALLBACK|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_callback]] for the callback.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |