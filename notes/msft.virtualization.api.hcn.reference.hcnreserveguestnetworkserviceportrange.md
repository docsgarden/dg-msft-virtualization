---
id: 3dkzaS9ux3kbXfzAHthAQaV
title: >-
    HcnReserveGuestNetworkServicePortRange
desc: >-
    HcnReserveGuestNetworkServicePortRange
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnReserveGuestNetworkServicePortRange
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 20474729269714d3d94e5bd82986d915592ac80001419484f93a8bf8ca063d5c
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnReserveGuestNetworkServicePortRange.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnReserveGuestNetworkServicePortRange']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnReserveGuestNetworkServicePortRange.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnReserveGuestNetworkServicePortRange).

# HcnReserveGuestNetworkServicePortRange

## Description

Reserves a range of ports.

## Syntax

```cpp
HRESULT
WINAPI
HcnReserveGuestNetworkServicePortRange(
    _In_ HCN_GUESTNETWORKSERVICE GuestNetworkService,
    _In_ USHORT PortCount,
    _Out_ HCN_PORT_RANGE_RESERVATION* PortRangeReservation,
    _Out_ HANDLE* PortReservationHandle
    );
```

## Parameters

`GuestNetworkService`

The [[HCN\_GUESTNETWORKSERVICE|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_guestnetworkservice]] for the reservation.

`PortCount`

The number of ports to reserve.

`PortRangeReservation`

The list of [[HCN_PORT_RANGE_RESERVATION|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_port_range_reservation]] for the reservation.

`PortReservationHandle`

Receives a handle. It is the responsibility of the caller to release the handle using [[HcnReleaseGuestNetworkServicePortReservationHandle|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnreleaseguestnetworkserviceportreservationhandle]] once it is no longer in use.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |