---
id: 7vuMQM7j3uRZJhYTbesfwQn
title: >-
    HV_VP_ASSIST_PAGE
desc: >-
    HV_VP_ASSIST_PAGE
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_vp_assist_page
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 00b04f58e5abef6af0006ba064aefb9423d244f4dbd121a1d2a7e1f913bac86e
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_vp_assist_page.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_vp_assist_page.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_vp_assist_page).

# HV_VP_ASSIST_PAGE

 This structure defines the format of the [[Virtual Processor Assist Page|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.vp-properties#virtual-processor-assist-page]].

## Syntax

```c
typedef union
{
    struct
    {
        //
        // APIC assist for optimized EOI processing.
        //
        HV_VIRTUAL_APIC_ASSIST ApicAssist;
        UINT32 ReservedZ0;

        //
        // VP-VTL control information
        //
        HV_VP_VTL_CONTROL VtlControl;

        HV_NESTED_ENLIGHTENMENTS_CONTROL NestedEnlightenmentsControl;
        BOOLEAN EnlightenVmEntry;
        UINT8 ReservedZ1[[7];
        HV_GPA CurrentNestedVmcs;

        BOOLEAN SyntheticTimeUnhaltedTimerExpired;
        UINT8 ReservedZ2[7];

        //
        // VirtualizationFaultInformation must be 16 byte aligned.
        //
        HV_VIRTUALIZATION_FAULT_INFORMATION VirtualizationFaultInformation;
    };

    UINT8 ReservedZBytePadding[HV_PAGE_SIZE];

} HV_VP_ASSIST_PAGE, *PHV_VP_ASSIST_PAGE;
 ```

## See also

[HV_NESTED_ENLIGHTENMENTS_CONTROL|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.datatypes.hv_nested_enlightenments_control]]

[[HV_VIRTUALIZATION_FAULT_INFORMATION|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.datatypes.hv_virtualization_fault_information]]

[[HV_VP_VTL_CONTROL|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.datatypes.hv_vp_vtl_control]]