---
id: 7yUskLQC2mES3ncgXyL4XGj
title: >-
    Customers Reap Benefits from Comprehensive Cloud Approach
desc: >-
    Customers Reap Benefits from Comprehensive Cloud Approach
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110829-customers-reap-benefits-from-comprehensive-cloud-approach
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 744ff8bc5589582a0638f224fa705127a1199900803523245e0bec747e284b10
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110829-customers-reap-benefits-from-comprehensive-cloud-approach.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "08/29/2011"
    date: "2011-08-29 05:05:56"
    categories: "private-cloud"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110829-customers-reap-benefits-from-comprehensive-cloud-approach.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110829-customers-reap-benefits-from-comprehensive-cloud-approach).

# Customers Reap Benefits from Comprehensive Cloud Approach

Take a look at  Brad Anderson’s, Corporate Vice President at Microsoft, perspective on Microsoft’s  
cloud computing strategy, our private cloud solutions and the economics of those solutions versus VMware.  
[Click here](http://bit.ly/oS2cNf) to read more.