---
id: 5WBmx5pCXwpCMUBNgB8cyad
title: >-
    Hyper-V at Ignite
desc: >-
    Find articles about announcement and new information on Hyper-V revealed at Ignite 2015.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2015/20150512-hyper-v-at-ignite
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 8c587e1d4c962facd5d8ffdb1643f4f106b36fe9a410ac6d6583037639b76eaa
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2015/20150512-hyper-v-at-ignite.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2015-05-12 18:11:00"
    ms.date: "05/12/2015"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2015/20150512-hyper-v-at-ignite.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2015/20150512-hyper-v-at-ignite).

# Hyper-V at Ignite

Ignite 2015 was full of new information about Hyper-V.  Check out some of the many talks about (or related to) Hyper-V.  I'm sure I inevitably missed a few talks/interviews.  If there are any great ones I'm missing, leave a link in the comments.

 _* See also:[Information about Windows containers from Build and Ignite](https://docs.microsoft.com/en-us/b/virtualization/archive/2015/05/11/windows-server-containers-and-hyper-v-containers-debut-at-ignite-and-build.aspx "Information about Windows containers from Build and Ignite")  
_

 **The Future Microsoft Datacenter/Fabric**

[Mathew John and Jeff Woolsey -- Platform Vision & Strategy (2 of 7): Server Virtualization Overview](https://channel9.msdn.com/Events/Ignite/2015/BRK2466 "Mathew John and Jeff Woolsey -- Platform Vision Strategy \(2 of 7\): Server Virtualization Overview")

[Philip Moss -- The Power of Windows Server Software Defined Datacenter in Action](https://channel9.msdn.com/Events/Ignite/2015/BRK2469 "Philip Moss -- The Power of Windows Server Software Defined Datacenter in Action")

[Andrew Mason and Jeffrey Snover -- Nano Server:  The Future of Windows Server Starts Now](https://channel9.msdn.com/Events/Ignite/2015/BRK2461 "Andrew Mason and Jeffrey Snover -- Nano Server: The Future of Windows Server Starts Now")

[Dan Harman and Jeffrey Snover -- Remotely Managing Nano Server](https://channel9.msdn.com/Events/Ignite/2015/BRK3455 "Dan Harman and Jeffrey Snover -- Remotely Managing Nano Server")

 ** **

 ** **

 **Hyper-V Nuts and Bolts**

[Ben Armstrong and Sarah Cooley -- What's New in Windows Server Hyper-V](https://channel9.msdn.com/Events/Ignite/2015/BRK3461 "Ben Armstrong and Sarah Cooley -- What's New in Windows Server Hyper-V")

[Senthil Rajaram and Jose Barreto -- Hyper-V Storage Performance with Storage Quality of Service](https://channel9.msdn.com/Events/Ignite/2015/BRK3504 "Senthil Rajaram and Jose Barreto -- Hyper-V Storage Performance with Storage Quality of Service")

[Allen Marshall, Amitabh Tamhane, and Dean Wells -- Harden the Fabric: Protecting Tenant Secrets in Hyper-V](https://channel9.msdn.com/Events/Ignite/2015/BRK3457 "Allen Marshall, Amitabh Tamhane, and Dean Wells -- Harden the Fabric: Protecting Tenant Secrets in Hyper-V")

[Ben Armstrong and Rob Hindman -- Upgrading your Private Cloud to Server 2012R2 and Beyond](https://channel9.msdn.com/Events/Ignite/2015/BRK3484 "Ben Armstrong and Rob Hindman -- Upgrading your Private Cloud to Server 2012R2 and Beyond")

[Aidan Finn -- The Hidden Treasures of Windows Server 2012 R2 Hyper-V](https://channel9.msdn.com/Events/Ignite/2015/BRK3506 "Aidan Finn -- The Hidden Treasures of Windows Server 2012R2 Hyper-V")

[Matt McSpirit -- Migrating to Microsoft: VMWare to Hyper-V and Microsoft Azure](https://channel9.msdn.com/Events/Ignite/2015/BRK3493 "Matt McSpirit -- Migrating to Microsoft: VMWare to Hyper-V and Microsoft Azure")

[Jeff Woolsey -- Preparing your Fabric and Applications for Server 2003 End of Support](https://channel9.msdn.com/Events/Ignite/2015/BRK2474 "Jeff Woolsey -- Preparing your Fabric and Applications for Server 2003 End of Support")

Cheers,  
Sarah