---
id: xpbwYXQqFMbWoqeaXQ63MjS
title: >-
    JSON Schema Reference
desc: >-
    JSON Schema Reference
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/SchemaReference
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 2df360249a86535417169231b1766094f2a081d36a4dd022198ba61c54b4cd72
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/SchemaReference.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['JSON Schema Reference']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/SchemaReference.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/SchemaReference).

# JSON Schema Reference

## Agenda
- [[Enums|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#enums]]
- [[Structs|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#structs]]
- [[JSON type table|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]
- [[Version Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]
---
<a name = "enums"></a>
## Enums
Note: all variants listed should be used as string
<a name = "ApplySecureBootTemplateType"></a>
## ApplySecureBootTemplateType
Referenced by: [[Uefi|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#uefi]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Skip"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Apply"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "AttachmentType"></a>
## AttachmentType
Referenced by: [[Attachment|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#attachment]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"VirtualDisk"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Iso"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"PassThru"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "CacheMode"></a>
## CacheMode
Referenced by: [[Layer|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#layer]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Unspecified"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Use the default caching scheme (typically Enabled).|
|`"Disabled"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Disable caching entirely.|
|`"Enabled"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Enable caching in the system memory partition.|
|`"Private"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Enable caching in the private memory partition.|
|`"PrivateAllowSharing"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Enable caching in the private memory partition, but allow access by other partitions.|

---

<a name = "CachingMode"></a>
## CachingMode
Referenced by: [[Attachment|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#attachment]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Uncached"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Use uncached IO to read and write VHD files (default).|
|`"Cached"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Use cached IO for all files.|
|`"ReadOnlyCached"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Use cached IO for all read-only files in the VHD chain, and uncached IO for writable files.|

---

<a name = "ContainerCredentialGuardModifyOperation"></a>
## ContainerCredentialGuardModifyOperation
Referenced by: [[ContainerCredentialGuardOperationRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containercredentialguardoperationrequest]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"AddInstance"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Determines that a Container Credential Guard request operation is trying to add a new Container Credential Guard Instance.|
|`"RemoveInstance"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Determines that a Container Credential Guard request operation is trying to remove an existing running Container Credential Guard Instance.|

---

<a name = "ContainerCredentialGuardTransport"></a>
## ContainerCredentialGuardTransport
Referenced by: [[ContainerCredentialGuardAddInstanceRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containercredentialguardaddinstancerequest]]; [[ContainerCredentialGuardState|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containercredentialguardstate]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"LRPC"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Specifies that the Container Credential Guard transport is configured using Local Remote Procedure calls.|
|`"HvSocket"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Specifies that the Container Credential Guard transport is configured using Remote Procedure calls over HvSocket.|

---

<a name = "CrashType"></a>
## CrashType
Referenced by: [[CrashOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#crashoptions]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"CrashGuest"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Crash the guest through an architectured defined mechanism|

---

<a name = "DeviceType"></a>
## DeviceType
Referenced by: [[Device|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#device]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"ClassGuid"`<br>|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"DeviceInstance"`<br>|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"GpuMirror"`<br>|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Make all GPUs on the host visible to the container.|

---

<a name = "EnlightenmentModifications"></a>
## EnlightenmentModifications
Enum for enlightenment modifications

|Variants|NewInVersion|Description|
|---|---|---|
|`"Unreported"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FavorAutoEoi"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"RsvdZ1"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "EventDataType"></a>
## EventDataType
Referenced by: [[EventData|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#eventdata]]

Data types for event data elements, based on EVT_VARIANT_TYPE

|Variants|NewInVersion|Description|
|---|---|---|
|`"Empty"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"String"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"AnsiString"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"SByte"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Byte"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Int16"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"UInt16"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Int32"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"UInt32"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Int64"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"UInt64"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Single"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Double"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Boolean"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Binary"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Guid"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ExitInitiator"></a>
## ExitInitiator
Referenced by: [[SystemExit|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#systemexit]]; [[WorkerExit|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#workerexit]]

Initiator of an exit (guest, management client, etc.)

|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"GuestOS"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Initiated by the guest OS (e.g. guest OS shutdown)|
|`"Client"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Initiated by the management client|
|`"Internal"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Initiated internally (e.g. due to an error) by the virtual machine or HCS.|
|`"Unknown"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Initiator is unknown, e.g. a process was terminated or crashed.|

---

<a name = "FilesystemIsolationMode"></a>
## FilesystemIsolationMode
Referenced by: [[FilesystemNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#filesystemnamespace]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"hard"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"soft"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "FilesystemNestingMode"></a>
## FilesystemNestingMode
Referenced by: [[FilesystemNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#filesystemnamespace]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"inner"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"outer"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "FlexibleIoDeviceHostingModel"></a>
## FlexibleIoDeviceHostingModel
Referenced by: [[FlexibleIoDevice|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#flexibleiodevice]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Internal"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"External"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "GetPropertyType"></a>
## GetPropertyType
Referenced by: [[Service_PropertyQuery|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#service_propertyquery]]

Service property type queried by HcsGetServiceProperties

|Variants|NewInVersion|Description|
|---|---|---|
|`"Basic"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Supported schema versions|
|`"CpuGroup"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Cpu group information|
|`"ProcessorTopology"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Logical processors details|
|`"ContainerCredentialGuard"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Container credential guard Information|
|`"QoSCapabilities"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Query of service capabilities|

---

<a name = "GpuAssignmentMode"></a>
## GpuAssignmentMode
Referenced by: [[GpuConfiguration|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#gpuconfiguration]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Disabled"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Do not assign GPU to the guest.|
|`"Default"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Assign the single default GPU to guest, which currently is POST GPU.|
|`"List"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Assign the GPU(s)/partition(s) specified in AssignmentRequest to guest. If AssignmentRequest is empty, do not assign GPU to the guest.|
|`"Mirror"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Assign all current and future GPUs to guest.|

---

<a name = "IntegrationComponentOperatingStateReason"></a>
## IntegrationComponentOperatingStateReason
Referenced by: [[IntegrationComponentStatus|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#integrationcomponentstatus]]

Possible reason for integration component's state

|Variants|NewInVersion|Description|
|---|---|---|
|`"Unknown"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"AppsInCriticalState"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"CommunicationTimedOut"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FailedCommunication"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"HealthyApps"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ProtocolMismatch"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "IntegrationComponentOperationalState"></a>
## IntegrationComponentOperationalState
Referenced by: [[IntegrationComponentStatus|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#integrationcomponentstatus]]

Operational status for integration component

|Variants|NewInVersion|Description|
|---|---|---|
|`"Unknown"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Degraded"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Dormant"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Error"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"LostCommunication"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"NonRecoverableError"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"NoContact"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Ok"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "InterfaceClassType"></a>
## InterfaceClassType
Referenced by: [[InterfaceClass|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#interfaceclass]]

Enum used to specify how the interface class should be treated when applying a device extension/creating container symlinks. This enum is ordering-sensitive; if two interface classes with the same identifier are merged, the type of the resulting interface class is given by the larger enum value (e.g., DeviceInstance + ClassGuid = ClassGuid).

|Variants|NewInVersion|Description|
|---|---|---|
|`"Inherit"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Used only in the namespace portion of a device extension (i.e., an interface class that is added to the container definition when a given interface class is specified). This placeholder value will be replaced with the same type as the interface class that caused the device extension to be merged in.|
|`"DeviceInstance"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Represents a single device instance.|
|`"ClassGuid"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Represents all device interfaces of this class GUID on the system.|

---

<a name = "InterruptModerationMode"></a>
## InterruptModerationMode
Referenced by: [[IovSettings|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#iovsettings]]

The valid interrupt moderation modes for I/O virtualization (IOV) offloading.

|Variants|NewInVersion|Description|
|---|---|---|
|`"Default"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Adaptive"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Off"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Low"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Medium"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"High"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "JobTerminationPolicy"></a>
## JobTerminationPolicy
Referenced by: [[JobNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#jobnamespace]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"PermanentQuiescent"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"KillOnHandleClose"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "KvpSource"></a>
## KvpSource
Referenced by: [[KvpQuery|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#kvpquery]]

The source sets the location of the key-value pairs stored depending on the type of the key-value pairs

|Variants|NewInVersion|Description|
|---|---|---|
|`"KvpSetByHost"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|This source specifies to use the location of the key-value pairs set by host|
|`"KvpSetByGuest"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|This source specifies to use the location of the key-value pairs set by guest|
|`"GuestOSInfo"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|This source specifies to use the location of the populated information about guest's oeprating system|

---

<a name = "MappedPipePathType"></a>
## MappedPipePathType
Referenced by: [[MappedPipe|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#mappedpipe]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"AbsolutePath"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The path associated to this path type is an absolute path. The path is passed as-is to Windows APIs.|
|`"VirtualSmbPipeName"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The path associated to this path type is a virtual SMB pipe name. The path is translated to a file system path, relative to the virtual SMB object namespace path, before passed to Windows APIs.|

---

<a name = "MemoryBackingPageSize"></a>
## MemoryBackingPageSize
Referenced by: [[VirtualMachine_Memory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine_memory]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Small"`<br>|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Small (4KB) page size unit|
|`"Large"`<br>|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Large (2MB) page size unit|

---

<a name = "ModifyOperation"></a>
## ModifyOperation
Referenced by: [[ProcessModifyRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processmodifyrequest]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"ConsoleSize"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Update the console size|
|`"CloseHandle"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Close one or all of the std handles|

---

<a name = "ModifyPropertyType"></a>
## ModifyPropertyType
Referenced by: [[ModificationRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#modificationrequest]]

Service property type modified by HcsModifyServiceSettings

|Variants|NewInVersion|Description|
|---|---|---|
|`"CpuGroup"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Cpu group information|
|`"ContainerCredentialGuard"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Container credential guard Information|

---

<a name = "ModifyRequestType"></a>
## ModifyRequestType
Referenced by: [[GuestModifySettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#guestmodifysettingrequest]]; [[ModifySettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#modifysettingrequest]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Add"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Remove"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Update"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ModifyResourceType"></a>
## ModifyResourceType
Referenced by: [[GuestModifySettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#guestmodifysettingrequest]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Memory"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MappedDirectory"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MappedPipe"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MappedVirtualDisk"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"CombinedLayers"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"NetworkNamespace"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"CimMount"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ModifyServiceOperation"></a>
## ModifyServiceOperation
Referenced by: [[HostProcessorModificationRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#hostprocessormodificationrequest]]

Enumeration of different supported service processor modification requests

|Variants|NewInVersion|Description|
|---|---|---|
|`"CreateGroup"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"DeleteGroup"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"SetProperty"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "NonArchitecturalCoreSharing"></a>
## NonArchitecturalCoreSharing
Referenced by: [[ProcessorCapabilitiesInfo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processorcapabilitiesinfo]]; [[VmProcessorRequirements|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#vmprocessorrequirements]]

Enum for non architecture core sharing

|Variants|NewInVersion|Description|
|---|---|---|
|`"Unreported"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Off"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"On"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "NotificationType"></a>
## NotificationType
Referenced by: [[Properties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#properties]]; [[SystemExitStatus|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#systemexitstatus]]

Exit type of a compute system.

|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"GracefulExit"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The compute system exited cracefully, either by a system initiated shutdown or HcsShutdownComputeSystem.|
|`"ForcedExit"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The compute system was forcefully terminated with HcsTerminateComputeSystem.|
|`"UnexpectedExit"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The compute system exited unexpectedly.|
|`"Unknown"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The compute system exit type could not be determined.|

---

<a name = "ObjectDirectoryShadow"></a>
## ObjectDirectoryShadow
Referenced by: [[ObjectDirectory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#objectdirectory]]; [[ObjectNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#objectnamespace]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"false"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"true"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ifexists"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ObjectSymlinkScope"></a>
## ObjectSymlinkScope
Referenced by: [[ObjectSymlink|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#objectsymlink]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Local"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Global"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"GlobalDropSilo"`<br>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "OperationFailureDetail"></a>
## OperationFailureDetail
Referenced by: [[OperationFailure|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#operationfailure]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Invalid"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"CreateInternalError"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ConstructStateError"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"RuntimeOsTypeMismatch"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Construct"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsCreateComputeSystem|
|`"Start"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsStartComputeSystem|
|`"Pause"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsPauseComputeSystem|
|`"Resume"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsResumeComputeSystem|
|`"Shutdown"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsShutdownComputeSystem|
|`"Terminate"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsTerminateComputeSystem|
|`"Save"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsSaveComputeSystem|
|`"GetProperties"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsGetComputeSystemProperties|
|`"Modify"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsModifyComputeSystem|
|`"Crash"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsCrashComputeSystem|
|`"GuestCrash"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|A guest OS crash occurred during an HCS API call.|
|`"LifecycleNotify"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ExecuteProcess"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsCreateProcess|
|`"GetProcessInfo"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsGetProcessInfo|
|`"WaitForProcess"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"SignalProcess"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsSignalProcess|
|`"ModifyProcess"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsModifyProcess|
|`"PrepareForHosting"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"RegisterHostedSystem"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"UnregisterHostedSystem"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"PrepareForClone"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"GetCloneTemplate"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "OperationType"></a>
## OperationType
Referenced by: [[OperationInfo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#operationinfo]]

|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Construct"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsCreateComputeSystem|
|`"Start"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsStartComputeSystem|
|`"Pause"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsPauseComputeSystem|
|`"Resume"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsResumeComputeSystem|
|`"Shutdown"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsShutdownComputeSystem|
|`"Terminate"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsTerminateComputeSystem|
|`"Save"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsSaveComputeSystem|
|`"GetProperties"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsGetComputeSystemProperties|
|`"Modify"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsModifyComputeSystem|
|`"Crash"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsCrashComputeSystem|
|`"ExecuteProcess"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsCreateProcess|
|`"GetProcessInfo"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsGetProcessInfo|
|`"SignalProcess"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsSignalProcess|
|`"ModifyProcess"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsModifyProcess|
|`"CancelOperation"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HcsCancelOperation|

---

<a name = "OsLayerType"></a>
## OsLayerType
Referenced by: [[OsLayerOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#oslayeroptions]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Container"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Vm"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "OsType"></a>
## OsType
Referenced by: [[ComputeSystemProperties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#computesystemproperties]]; [[Properties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#properties]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Unknown"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Windows"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Linux"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "PathType"></a>
## PathType
Referenced by: [[Layer|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#layer]]; [[MappedDirectory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#mappeddirectory]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"AbsolutePath"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The path associated to this path type is an absolute path. The path is passed as-is to windows APIs.|
|`"VirtualSmbShareName"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The path associated to this path type is a virtual SMB share name. The path is translated to a file system path, relative to the virtual SMB object namespace path, before passed to windows APIs.|

---

<a name = "PauseReason"></a>
## PauseReason
Referenced by: [[PauseNotification|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#pausenotification]]

Pause reason that is indicated to components running in the Virtual Machine.

|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Save"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Template"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "PauseSuspensionLevel"></a>
## PauseSuspensionLevel
Referenced by: [[PauseOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#pauseoptions]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Suspend"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MemoryLow"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MemoryMedium"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MemoryHigh"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ProcessorFeature"></a>
## ProcessorFeature
Referenced by: [[ProcessorCapabilitiesInfo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processorcapabilitiesinfo]]; [[ProcessorFeatureSet|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processorfeatureset]]; [[VmProcessorRequirements|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#vmprocessorrequirements]]

Enum for processor features

|Variants|NewInVersion|Description|
|---|---|---|
|`"Sse3"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"LahfSahf"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Ssse3"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Sse4_1"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Sse4_2"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Sse4A"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Xop"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Popcnt"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Cmpxchg16B"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Altmovcr8"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Lzcnt"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Misalignsse"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MmxExt"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Amd3Dnow"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ExtAmd3Dnow"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Page1Gb"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"AmdAes"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Pclmulqdq"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Pcid"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Fma4"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"F16C"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Rdrand"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Rdwrfsgs"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Smep"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"EnhancedFastString"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Bmi1"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Bmi2"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Movbe"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Npiep1"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"DepX87FpuSave"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Rdseed"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Adx"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"IntelPrefetch"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Smap"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Hle"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Rtm"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Rdtscp"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Clflushopt"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Clwb"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Sha"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"X87PointersSaved"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Invpcid"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Ibrs"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Stibp"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Ibpb"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"UnrestrictedGuest"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Mdd"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FastShortRepMov"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"L1DCacheFlush"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"RdclNo"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"IbrsAll"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"SkipL1Df"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"SsbNo"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"RsbaNo"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"VirtSpecCtrl"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Rdpid"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Umip"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MbsNo"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MbClear"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"TaaNo"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"TsxCtrl"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"AcountMcount"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"TscInvariant"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Clzero"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Rdpru"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"La57"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Mbec"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"NestedVirt"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Psfd"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"CetSs"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"CetIbt"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"VmxExceptionInject"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"UmwaitTpause"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Movdiri"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Movdir64b"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Cldemote"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Serialize"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"TscDeadlineTmr"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"TscAdjust"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FZLRepMovsb"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FSRepStosb"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FSRepCmpsb"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"SentinelAmd"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Asid16"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"TGran16"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"TGran64"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Haf"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Hdbs"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Pan"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"AtS1E1"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Uao"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"El0Aarch32"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Fp"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FpHp"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"AdvSimd"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"AdvSimdHp"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"GicV3V4"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"PmuV3"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"PmuV3ArmV81"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"PmuV3ArmV84"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ArmAes"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"PolyMul"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Sha1"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Sha256"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Sha512"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Crc32"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Atomic"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Rdm"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Sha3"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Sm3"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Sm4"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Dp"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Fhm"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"DcCvap"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"DcCvadp"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ApaBase"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ApaEp"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ApaEp2"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ApaEp2Fp"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ApaEp2Fpc"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Jscvt"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Fcma"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"RcpcV83"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"RcpcV84"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Gpa"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"L1ipPipt"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"DzPermitted"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"SentinelArm"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ProcessorFeatureSetMode"></a>
## ProcessorFeatureSetMode
Referenced by: [[ProcessorFeatureSet|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processorfeatureset]]

Enum for setting Strict/Flexible configuration mode

|Variants|NewInVersion|Description|
|---|---|---|
|`"Strict"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Default mode that throws error when feature values not supported by the host are configured|
|`"Permissive"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|mode that filters and leaves out the feature values not supported by the host|

---

<a name = "ProcessSignal"></a>
## ProcessSignal
Referenced by: [[SignalProcessOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#signalprocessoptions]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"CtrlC"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"CtrlBreak"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"CtrlClose"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"CtrlLogOff"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"CtrlShutdown"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "RegistryHive"></a>
## RegistryHive
Referenced by: [[RegistryKey|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrykey]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"System"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Software"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Security"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Sam"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "RegistryValueType"></a>
## RegistryValueType
Referenced by: [[RegistryValue|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registryvalue]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"String"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ExpandedString"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MultiString"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Binary"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"DWord"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"QWord"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"CustomType"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "SaveType"></a>
## SaveType
Referenced by: [[SaveOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#saveoptions]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"ToFile"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The system's memory and device states are saved to the runtime state file.|
|`"AsTemplate"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The system's device state is saved to the runtime state file. The system is then placed in a state such that other systems can be cloned from it.|

---

<a name = "SerialConsole"></a>
## SerialConsole
Referenced by: [[Uefi|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#uefi]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Default"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Disabled"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ComPort1"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ComPort2"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ShutdownMechanism"></a>
## ShutdownMechanism
Referenced by: [[ShutdownOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#shutdownoptions]]

Different mechanisms to perform a shutdown operation

|Variants|NewInVersion|Description|
|---|---|---|
|`"GuestConnection"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"IntegrationService"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ShutdownType"></a>
## ShutdownType
Referenced by: [[ShutdownOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#shutdownoptions]]

Different operations that are related or classified as a type of shutdown

|Variants|NewInVersion|Description|
|---|---|---|
|`"Shutdown"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Hibernate"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Reboot"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "State"></a>
## State
Referenced by: [[ComputeSystemProperties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#computesystemproperties]]; [[Properties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#properties]]

Compute system state which is exposed to external clients

|Variants|NewInVersion|Description|
|---|---|---|
|`"Created"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Running"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Paused"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Stopped"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"SavedAsTemplate"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Unknown"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "StateOverride"></a>
## StateOverride
Referenced by: [[KvpExchange|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#kvpexchange]]; [[NetworkAdapter|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#networkadapter]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Default"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Use the default mode specified by the system|
|`"Disabled"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Enabled"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "StdHandle"></a>
## StdHandle
Referenced by: [[CloseHandle|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#closehandle]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"StdIn"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"StdOut"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"StdErr"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"All"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "SubnodeType"></a>
## SubnodeType
Referenced by: [[Subnode|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#subnode]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Any"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Socket"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Cluster"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"L3"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "System_PropertyType"></a>
## System_PropertyType
Referenced by: [[System_PropertyQuery|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#system_propertyquery]]

Compute system property types. The properties will be returned as a Schema.Responses.System.Properties instance.

|Variants|NewInVersion|Description|
|---|---|---|
|`"Memory"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Statistics"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ProcessList"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"TerminateOnLastHandleClosed"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"SharedMemoryRegion"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"GuestConnection"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "SystemExitDetail"></a>
## SystemExitDetail
Referenced by: [[SystemExit|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#systemexit]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Invalid"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"HcsApiFatalError"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An non-recoverable error occurred during an HCS API call.|
|`"ServiceStop"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Forced exit due to stopping the vmcompute service.|
|`"Shutdown"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|If the initiator is ExitInitiator::Client, the exit is due to HcsShutdownComputeSystem If the initiator is ExitInitiator::GuestOS, the exit was initiated by the guest OS.|
|`"Terminate"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Forced exit due to HcsTerminateComputeSystem|
|`"UnexpectedExit"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The system exited unexpectedly, other attribution records may provide more information.|

---

<a name = "SystemType"></a>
## SystemType
Referenced by: [[ComputeSystemProperties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#computesystemproperties]]; [[Properties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#properties]]; [[SystemQuery|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#systemquery]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Container"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"VirtualMachine"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "UefiBootDevice"></a>
## UefiBootDevice
Referenced by: [[UefiBootEntry|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#uefibootentry]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"ScsiDrive"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"VmbFs"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Network"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"File"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "VirtualDeviceFailureDetail"></a>
## VirtualDeviceFailureDetail
Referenced by: [[VirtualDeviceFailure|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualdevicefailure]]

Provides detail on the context in which a virtual device failed. These values are informational only. Clients should not take a dependency on them

|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Create"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Initialize"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"StartReservingResources"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FinishReservingResources"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FreeReservedResources"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"SaveReservedResources"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"PowerOnCold"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"PowerOnRestore"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"PowerOff"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Save"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Resume"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Pause"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"EnableOptimizations"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"StartDisableOptimizations"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FinishDisableOptimizations"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Reset"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"PostReset"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Teardown"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"SaveCompatibilityInfo"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MetricRestore"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MetricEnumerate"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MetricEnumerateForSave"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MetricReset"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MetricEnable"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MetricDisable"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "VirtualPMemBackingType"></a>
## VirtualPMemBackingType
Referenced by: [[VirtualPMemController|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualpmemcontroller]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Virtual"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Physical"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "VirtualPMemImageFormat"></a>
## VirtualPMemImageFormat
Referenced by: [[VirtualPMemDevice|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualpmemdevice]]; [[VirtualPMemMapping|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualpmemmapping]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Vhdx"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Vhd1"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "WindowsCrashPhase"></a>
## WindowsCrashPhase
Referenced by: [[WindowsCrashReport|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#windowscrashreport]]

Indicated the progress of a Windows memory dump in a WindowsCrashReport.

|Variants|NewInVersion|Description|
|---|---|---|
|`"Inactive"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|A memory dump was not active.|
|`"CrashValues"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Crash values have been reported through CrashParameters.|
|`"Starting"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|A memory dump is in the process of starting.|
|`"Started"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|A memory dump has been initialized.|
|`"Writing"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Memory dump data is being written.|
|`"Complete"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Memory dump was successfully written.|

---

<a name = "WorkerExitDetail"></a>
## WorkerExitDetail
Referenced by: [[WorkerExit|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#workerexit]]

Detailed reasons for a VM stop. These values are informational only. Clients should not take a dependency on them

|Variants|NewInVersion|Description|
|---|---|---|
|`"Invalid"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"PowerOff"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"PowerOffCritical"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Reset"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"GuestCrash"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"GuestFirmwareCrash"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"TripleFault"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"DeviceFatalApicRequest"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"DeviceFatalMsrRequest"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"DeviceFatalException"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"DeviceFatalError"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"DeviceMachineCheck"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"EmulatorError"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"VidTerminate"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ProcessUnexpectedExit"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"InitializationFailure"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"InitializationStartTimeout"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ColdStartFailure"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"ResetStartFailure"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FastRestoreStartFailure"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"RestoreStartFailure"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FastSavePreservePartition"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FastSavePreservePartitionHandleTransfer"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FastSave"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"CloneTemplate"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Save"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Migrate"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MigrateFailure"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"CannotReferenceVm"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"MgotUnregister"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FastSavePreservePartitionHandleTransferStorage"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|deprecated; use FastSavePreservePartitionWithHandleTransfer|
|`"FastSavePreservePartitionHandleTransferNetworking"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|deprecated; see above|
|`"FastSavePreservePartitionHandleTransferStorageAndNetworking"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|deprecated; see above|
|`"FastSavePreservePartitionHsr"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FastSavePreservePartitionWithHandleTransfer"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"FastSaveWithHandleTransfer"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "WorkerExitType"></a>
## WorkerExitType
Referenced by: [[WorkerExit|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#workerexit]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"InitializationFailed"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|VM Failed to initialize.|
|`"Stopped"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|VM shutdown after complete stop|
|`"Saved"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|VM shutdown after complete save|
|`"StoppedOnReset"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|VM reset and the VM was configured to stop on reset|
|`"UnexpectedStop"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|VM worker process exited unexpectedly|
|`"ResetFailed"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|VM exit after failing to reset|
|`"UnrecoverableError"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|VM stopped because of an unrecoverable error (e.g., storage failure)|

---

<a name = "XsaveProcessorFeature"></a>
## XsaveProcessorFeature
Referenced by: [[ProcessorCapabilitiesInfo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processorcapabilitiesinfo]]; [[ProcessorFeatureSet|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processorfeatureset]]; [[VmProcessorRequirements|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#vmprocessorrequirements]]

Enum for xsave processor features

|Variants|NewInVersion|Description|
|---|---|---|
|`"Xsave"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Xsave_Opt"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Avx"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Avx2"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Fma"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Mpx"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Avx512"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Avx512Dq"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Avx512Cd"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Avx512Bw"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Avx512Vl"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Xsave_Comp"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Xsave_Supervisor"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Xcr1"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Avx512_Bitalg"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"AVX512_Ifma"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Avx512_Vbmi"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Avx512_Vbmi2"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Avx512_Vnni"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Gfni"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Vaes"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Avx512_Vpopcntdq"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Vpclmulqdq"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Avx512_Bf16"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Avx512_Vp2Intersect"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Avx512Fp16"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"Xfd"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"AmxTile"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"AmxBf16"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"AmxInt8"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"AvxVnni"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|`"SentinelAmdXsave"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "structs"></a>
## Structs
<a name = "Attachment"></a>
## Attachment
Referenced by: [[Scsi|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#scsi]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Type**<br>|[[AttachmentType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#attachmenttype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Path**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**CachingMode**<br>|[[CachingMode|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#cachingmode]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ReadOnly**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**SupportCompressedVolumes**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**AlwaysAllowSparseFiles**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**SupportEncryptedFiles**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "AttributionRecord"></a>
## AttributionRecord
Referenced by: [[ResultError|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#resulterror]]; [[SystemExitStatus|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#systemexitstatus]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**WorkerExit**<br>|[[WorkerExit|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#workerexit]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**GuestCrash**<br>|[[GuestCrash|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#guestcrash]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**TripleFault**<br>|[[TripleFault|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#triplefault]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**InjectNonMaskableInterrupt**<br>|[[InjectNonMaskableInterrupt|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#injectnonmaskableinterrupt]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**OperationFailure**<br>|[[OperationFailure|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#operationfailure]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**SystemExit**<br>|[[SystemExit|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#systemexit]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**VirtualDeviceFailure**<br>|[[VirtualDeviceFailure|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualdevicefailure]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "BasicInformation"></a>
## BasicInformation
Basic information

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**SupportedSchemaVersions**<br>|<[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#version]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The supported schema versions will be returned as an array. Array element A.X implies all versions with major version A and minor version from 0 to X are also supported.|

---

<a name = "Battery"></a>
## Battery
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]




**Note:** This is an empty struct with no fields, and to be used in the JSON document must be specified as an empty object: `"{}"`.

---

<a name = "Chipset"></a>
## Chipset
Referenced by: [[VirtualMachine|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Uefi**<br>|[[Uefi|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#uefi]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**IsNumLockDisabled**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**BaseBoardSerialNumber**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ChassisSerialNumber**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ChassisAssetTag**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**EnableHibernation**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**UseUtc**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**LinuxKernelDirect**<br>|[[LinuxKernelDirect|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#linuxkerneldirect]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "CimMount"></a>
## CimMount


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ImagePath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**FileSystemName**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**VolumeGuid**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**MountFlags**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|MountFlags are the flags that are used to alter the behaviour of a mounted cim. These values are defined by the CIMFS API. The value passed for this field will be forwarded to the the CimMountImage call.|

---

<a name = "CloseHandle"></a>
## CloseHandle
Referenced by: [[ProcessModifyRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processmodifyrequest]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Handle**<br>|[[StdHandle|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#stdhandle]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "CombinedLayers"></a>
## CombinedLayers
Object used by a modify request to add or remove a combined layers structure in the guest. For Windows, the GCS applies a filter in ContainerRootPath using the specified layers as the parent content. Ignores property ScratchPath since the container path is already the scratch path. For linux, the GCS unions the specified layers and ScratchPath together, placing the resulting union filesystem at ContainerRootPath.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Layers**<br>|<[[Layer|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#layer]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Layer hierarchy to be combined.|
|**ScratchPath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ContainerRootPath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ComPort"></a>
## ComPort
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]

ComPort specifies the named pipe that will be used for the port, with empty string indicating a disconnected port.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**NamedPipe**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**OptimizeForDebugger**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ComputeSystem"></a>
## ComputeSystem
Describes the configuration of a compute system to create with all of the necessary resources it requires for a successful boot.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|A string identifying the owning client for this system.|
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|A version structure for this schema. Properties nested within this object may identify their own schema versions.|
|**HostingSystemId**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The identifier of the compute system that will host the system described by HostedSystem. The hosting system must already have been created.|
|**HostedSystem**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The JSON describing the compute system that will be launched inside of the system identified by HostingSystemId. This property is mutually exclusive with the Container and VirtualMachine properties.|
|**Container**<br>|[[Container|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The set of properties defining a container. This property is mutualy exclusive with the HostedSystem and VirtualMachine properties.|
|**VirtualMachine**<br>|[[VirtualMachine|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The set of properties defining a virtual machine. This property is mutually exclusive with the HostedSystem and Container properties.|
|**ShouldTerminateOnLastHandleClosed**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|If true, this system will be forcibly terminated when the last HCS_SYSTEM handle corresponding to it is closed.|

---

<a name = "ComputeSystemProperties"></a>
## ComputeSystemProperties
An object consisting of basic system properties for a compute system

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Id**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**SystemType**<br>|[[SystemType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#systemtype]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**State**<br>|[[State|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#state]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**RuntimeId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**RuntimeOsType**<br>|[[OsType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#ostype]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**HostingSystemId**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**IsDummy**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ObRoot**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**RuntimeTemplateId**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ConsoleImage"></a>
## ConsoleImage


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Image**<br>|[[ByteArray|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ConsoleImageQuery"></a>
## ConsoleImageQuery
A query to request a screenshot of the console of the virtual machine

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Width**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Requested width of the resulting image|
|**Height**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Requested height of the resulting image|
|**Flags**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Flags that determine certain characteristics of the output of the returned image 0x00000001ul: The image data is returned in the OctetString format defined by the DMTF. The first four bytes of the data contains the number of octets (bytes), including the four-byte length, in the big-endian format|

---

<a name = "ConsoleSize"></a>
## ConsoleSize
Referenced by: [[ProcessModifyRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processmodifyrequest]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Height**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Width**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "Container"></a>
## Container
Referenced by: [[ComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#computesystem]]; [[HostedSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#hostedsystem]]

Configuration of a Windows Server Container, used during its creation to set up and/or use resources.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**GuestOs**<br>|[[GuestOs|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#guestos]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Properties specific to the guest operating system that runs on the container.|
|**Storage**<br>|[[Storage|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#storage]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Storage configuration of a container.|
|**MappedDirectories**<br>|<[[MappedDirectory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#mappeddirectory]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Optional list of directories in the container host that will be mapped to the container guest on creation.|
|**MappedPipes**<br>|<[[MappedPipe|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#mappedpipe]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Optional list of named pipes in the container host that will be mapped to the container guest on creation.|
|**Memory**<br>|[[Container_Memory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container_memory]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Optional memory configuration of a container.|
|**Processor**<br>|[[Container_Processor|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container_processor]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Optional processor configuration of a container.|
|**Networking**<br>|[[Networking|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#networking]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Network configuration of a container.|
|**HvSocket**<br>|[[Container_HvSocket|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container_hvsocket]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HvSocket configuration of a container. Used to configure ACLs to control what host process can connect\bind to the container. Hosted container ACLs are inherited from the hosting system.|
|**ContainerCredentialGuard**<br>|[[ContainerCredentialGuardState|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containercredentialguardstate]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Optional configuration and description of credentials forwarded to the container guest from the container host.|
|**RegistryChanges**<br>|[[RegistryChanges|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrychanges]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|List of Windows registry key/value changes applied on the container on creation.|
|**AssignedDevices**<br>|<[[Device|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#device]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Optional list of direct device assignment configurations.|
|**AdditionalDeviceNamespace**<br>|[[ContainersDef_Device|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containersdef_device]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Optional list of container device extensions to use during device assignment scenarios.|

---

<a name = "Container_HvSocket"></a>
## Container_HvSocket
Referenced by: [[Container|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container]]

Describes the HvSocket configuration and options for a container.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Config**<br>|[[HvSocketSystemConfig|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#hvsocketsystemconfig]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Optional detailed HvSocket configuration.|
|**EnablePowerShellDirect**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|When true, enables Powershell Direct service in the guest to allow it to use the HvSocket transport.|

---

<a name = "Container_Memory"></a>
## Container_Memory
Referenced by: [[Container|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container]]

Describes memory configuration for a container.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**SizeInMB**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Specifies the memory size in megabytes.|

---

<a name = "Container_Processor"></a>
## Container_Processor
Referenced by: [[Container|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container]]

Specifies CPU limits for a container. Count, Maximum and Weight are all mutually exclusive.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Count**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Optional property that represents the fraction of the configured processor count in a container in relation to the processors available in the host. The fraction ultimately determines the portion of processor cycles that the threads in a container can use during each scheduling interval, as the number of cycles per 10,000 cycles.|
|**Weight**<br>|[[int64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Optional property that limits the share of processor time given to the container relative to other workloads on the processor. The processor weight is a value between 0 and 10000.|
|**Maximum**<br>|[[int64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Optional property that determines the portion of processor cycles that the threads in a container can use during each scheduling interval, as the number of cycles per 10,000 cycles. Set processor maximum to a percentage times 100.|

---

<a name = "ContainerCredentialGuardAddInstanceRequest"></a>
## ContainerCredentialGuardAddInstanceRequest
Object describing a request to add a Container Credential Guard Instance.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Id**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Globally unique identifier to use for the Container Credential Guard Instance.|
|**CredentialSpec**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|JSON document as a string that describes the Container Credential Guard Instance's credential specification to use.|
|**Transport**<br>|[[ContainerCredentialGuardTransport|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containercredentialguardtransport]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Specifies the transport the Container Credential Guard Instance will use at runtime.|

---

<a name = "ContainerCredentialGuardHvSocketServiceConfig"></a>
## ContainerCredentialGuardHvSocketServiceConfig
Referenced by: [[ContainerCredentialGuardInstance|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containercredentialguardinstance]]

Specifies the HvSocket configurations required for a Container Credential Guard instance that is meant to be used with HvSocket transport.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ServiceId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Identifier of the service that needs to be configured over HvSocket.|
|**ServiceConfig**<br>|[[HvSocketServiceConfig|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#hvsocketserviceconfig]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Necessary HvSocket settings that allow a Container Credential Guard instance to configure a transport using Remote Procedure Calls over HvSocket.|

---

<a name = "ContainerCredentialGuardInstance"></a>
## ContainerCredentialGuardInstance
Referenced by: [[ContainerCredentialGuardSystemInfo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containercredentialguardsysteminfo]]

Describes the configuration of a running Container Credential Guard Instance.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Id**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Identifier of a Container Credential Guard Instance, globally unique.|
|**CredentialGuard**<br>|[[ContainerCredentialGuardState|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containercredentialguardstate]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Object that describes the state of a running Container Credential Guard Instance.|
|**HvSocketConfig**<br>|[[ContainerCredentialGuardHvSocketServiceConfig|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containercredentialguardhvsocketserviceconfig]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Optional HvSocket configuration to allow a Container Credential Guard Instance to communicate over an HvSocket transport.|

---

<a name = "ContainerCredentialGuardOperationRequest"></a>
## ContainerCredentialGuardOperationRequest
Object describing a Container Credential Guard system request.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Operation**<br>|[[ContainerCredentialGuardModifyOperation|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containercredentialguardmodifyoperation]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Determines the type of Container Credential Guard request.|
|**OperationDetails**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Object describing the input properties used by the specified operation type.|

---

<a name = "ContainerCredentialGuardRemoveInstanceRequest"></a>
## ContainerCredentialGuardRemoveInstanceRequest
Object describing a request to remove a running Container Credential Guard Instance.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Id**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Globally unique identifier of a running Container Credential Guard Instance.|

---

<a name = "ContainerCredentialGuardState"></a>
## ContainerCredentialGuardState
Referenced by: [[Container|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container]]; [[ContainerCredentialGuardInstance|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containercredentialguardinstance]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Cookie**<br>|[[string_binary|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Authentication cookie for calls to a Container Credential Guard instance.|
|**RpcEndpoint**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Name of the RPC endpoint of the Container Credential Guard instance.|
|**Transport**<br>|[[ContainerCredentialGuardTransport|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containercredentialguardtransport]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Transport used for the configured Container Credential Guard instance.|
|**CredentialSpec**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Credential spec used for the configured Container Credential Guard instance.|

---

<a name = "ContainerCredentialGuardSystemInfo"></a>
## ContainerCredentialGuardSystemInfo
Object listing the system's running Container Credential Guard Instances.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Instances**<br>|<[[ContainerCredentialGuardInstance|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containercredentialguardinstance]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Array of running Container Credential Guard Instances.|

---

<a name = "ContainersDef_BatchedBinding"></a>
## ContainersDef_BatchedBinding
Referenced by: [[FilesystemNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#filesystemnamespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**filepath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**bindingroots**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ContainersDef_Container"></a>
## ContainersDef_Container


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**namespace**<br>|[[Namespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#namespace]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ContainersDef_Device"></a>
## ContainersDef_Device
Referenced by: [[Container|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**device_extension**<br>|<[[DeviceExtension|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#deviceextension]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "CpuGroup"></a>
## CpuGroup
Referenced by: [[VirtualMachine_Processor|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine_processor]]

CPU groups allow Hyper-V administrators to better manage and allocate the host's CPU resources across guest virtual machines

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Id**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "CpuGroupAffinity"></a>
## CpuGroupAffinity
Referenced by: [[CpuGroupConfig|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#cpugroupconfig]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**LogicalProcessorCount**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**LogicalProcessors**<br>|<[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "CpuGroupConfig"></a>
## CpuGroupConfig
Referenced by: [[CpuGroupConfigurations|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#cpugroupconfigurations]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**GroupId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Affinity**<br>|[[CpuGroupAffinity|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#cpugroupaffinity]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**GroupProperties**<br>|<[[CpuGroupProperty|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#cpugroupproperty]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**HypervisorGroupId**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Hypervisor CPU group IDs exposed to clients|

---

<a name = "CpuGroupConfigurations"></a>
## CpuGroupConfigurations
Structure used to return cpu groups for a Service property query

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**CpuGroups**<br>|<[[CpuGroupConfig|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#cpugroupconfig]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "CpuGroupProperty"></a>
## CpuGroupProperty
Referenced by: [[CpuGroupConfig|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#cpugroupconfig]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**PropertyCode**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**PropertyValue**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "CrashOptions"></a>
## CrashOptions


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Type**<br>|[[CrashType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#crashtype]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "CrashReport"></a>
## CrashReport
Crash information reported through HcsEventSystemCrashInitiated and HcsEventSystemCrashReport notifications. This object is also used as the input to HcsSubmitWerReport.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**SystemId**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Compute system id the CrashReport is for.|
|**ActivityId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Trace correlation activity Id.|
|**WindowsCrashInfo**<br>|[[WindowsCrashReport|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#windowscrashreport]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Additional Windows specific crash report information. This information is only present in HcsEventSystemCrashReport and only if the GuestCrashReporting device has been configured in the Devices as well as the Windows guest OS.|
|**CrashParameters**<br>|<[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Crash parameters as reported by the guest OS. For Windows these correspond to the bug check code followed by 4 bug check code specific values. The CrashParameters are available in both HcsEventSystemCrashInitiated and HcsEventSystemCrashReport events.|
|**CrashLog**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional string provided by the guest OS. Currently only used by Linux guest OSes with Hyper-V Linux Integration Services configured.|
|**Status**<br>|[[int32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Provides overall status on crash reporting, S_OK indicates success, other HRESULT values on error.|
|**PreOSId**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Opaque guest OS reported ID.|
|**CrashStackUnavailable**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|If true, the guest OS reported that a crash dump stack/handler was unavailable or could not be invoked.|

---

<a name = "CreateGroupOperation"></a>
## CreateGroupOperation
Create group operation settings

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**GroupId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**LogicalProcessorCount**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**LogicalProcessors**<br>|<[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "DeleteGroupOperation"></a>
## DeleteGroupOperation
Delete group operation settings

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**GroupId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "Device"></a>
## Device
Referenced by: [[Container|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Type**<br>|[[DeviceType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devicetype]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The type of device to assign to the container.|
|**InterfaceClassGuid**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The interface class guid of the device interfaces to assign to the container. Only used when Type is ClassGuid.|
|**LocationPath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The location path of the device to assign to the container. Only used when Type is DeviceInstance.|

---

<a name = "DeviceCategory"></a>
## DeviceCategory
Referenced by: [[DeviceExtension|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#deviceextension]]; [[DeviceNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devicenamespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**interface_class**<br>|<[[InterfaceClass|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#interfaceclass]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "DeviceExtension"></a>
## DeviceExtension
Referenced by: [[ContainersDef_Device|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containersdef_device]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**device_category**<br>|[[DeviceCategory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devicecategory]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**namespace**<br>|[[Namespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#namespace]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "DeviceInstance"></a>
## DeviceInstance
Referenced by: [[DeviceNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devicenamespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**id**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**location_path**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**port_name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**interface_class**<br>|<[[InterfaceClass|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#interfaceclass]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "DeviceNamespace"></a>
## DeviceNamespace
Referenced by: [[Namespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#namespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**requires_driverstore**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**device_category**<br>|<[[DeviceCategory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devicecategory]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**device_instance**<br>|<[[DeviceInstance|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#deviceinstance]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "Devices"></a>
## Devices
Referenced by: [[VirtualMachine|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ComPorts**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], [[ComPort|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#comport]]>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object that maps COM Port objects for any ports configured on the virtual machine. The key in the map is the integer, starting from zero, that will identify the COM port into the guest.|
|**VirtioSerial**<br>|[[VirtioSerial|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtioserial]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Scsi**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], [[Scsi|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#scsi]]>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object that maps SCSI controllers, identified by friendly name. The provided name is hashed to create the controller's channel instance identifier. If the name is already a GUID, that GUID will be used as the channel instance identifier as-is.|
|**VirtualPMem**<br>|[[VirtualPMemController|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualpmemcontroller]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object defining the settings for virtual persistent memory.|
|**NetworkAdapters**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], [[NetworkAdapter|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#networkadapter]]>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object that maps network adapters, identified by friendly name. The provided name is hashed to create the adapter's instance identifier. If the name is already a GUID, that GUID will be used as the identifier as-is.|
|**VideoMonitor**<br>|[[VideoMonitor|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#videomonitor]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object describing the video monitor device.|
|**Keyboard**<br>|[[Keyboard|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#keyboard]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object describing the keyboard device.|
|**Mouse**<br>|[[Mouse|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#mouse]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object describing the mouse device.|
|**HvSocket**<br>|[[VirtualMachine_HvSocket|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine_hvsocket]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object describing socket services exposed to the virtual machine.|
|**EnhancedModeVideo**<br>|[[EnhancedModeVideo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#enhancedmodevideo]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object describing the configuration of enhanced-mode video, including connection configuration.|
|**GuestCrashReporting**<br>|[[GuestCrashReporting|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#guestcrashreporting]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object defining settings for how guest crashes should be captured for later analysis.|
|**VirtualSmb**<br>|[[VirtualSmb|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualsmb]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object describing any virtual SMB shares to be exposed to the guest OS.|
|**Plan9**<br>|[[Plan9|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#plan9]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object describing any Plan9 shares to be exposed to the guest OS.|
|**Battery**<br>|[[Battery|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#battery]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional battery device that will forward host battery state to the guest OS.|
|**FlexibleIov**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], [[FlexibleIoDevice|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#flexibleiodevice]]>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object that maps flexible IoV devices, identified by friendly name. The provided name is hashed to create the device's instance identifier. If the name is already a GUID, that GUID will be used as the identifier as-is.|
|**SharedMemory**<br>|[[SharedMemoryConfiguration|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#sharedmemoryconfiguration]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object describing any shared memory settings for the virtual machine.|
|**VirtualPci**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], [[VirtualPciDevice|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualpcidevice]]>|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "EnhancedModeVideo"></a>
## EnhancedModeVideo
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ConnectionOptions**<br>|[[RdpConnectionOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#rdpconnectionoptions]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ErrorEvent"></a>
## ErrorEvent
Referenced by: [[ResultError|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#resulterror]]

Error descriptor that provides the info of an error object

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Message**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Fully formated error message|
|**StackTrace**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Stack trace in string form|
|**Provider**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Event definition|
|**EventId**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Event Id|
|**Flags**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Flags|
|**Source**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Source|
|**Data**<br>|<[[EventData|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#eventdata]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Event data|

---

<a name = "EventData"></a>
## EventData
Referenced by: [[ErrorEvent|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#errorevent]]

Event data element

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Type**<br>|[[EventDataType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#eventdatatype]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Event data type|
|**Value**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Event value|

---

<a name = "ExportLayerOptions"></a>
## ExportLayerOptions


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**IsWritableLayer**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "FilesystemLayer"></a>
## FilesystemLayer
Referenced by: [[FilesystemNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#filesystemnamespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**identifier**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**path**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "FilesystemNamespace"></a>
## FilesystemNamespace
Referenced by: [[Namespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#namespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**path**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**isolation**<br>|[[FilesystemIsolationMode|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#filesystemisolationmode]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**nesting**<br>|[[FilesystemNestingMode|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#filesystemnestingmode]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**layer**<br>|<[[FilesystemLayer|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#filesystemlayer]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**bindings**<br>|<[[ContainersDef_BatchedBinding|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containersdef_batchedbinding]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "FlexibleIoDevice"></a>
## FlexibleIoDevice
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**EmulatorId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**HostingModel**<br>|[[FlexibleIoDeviceHostingModel|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#flexibleiodevicehostingmodel]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Configuration**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "GpuConfiguration"></a>
## GpuConfiguration


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**AssignmentMode**<br>|[[GpuAssignmentMode|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#gpuassignmentmode]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The mode used to assign GPUs to the guest.|
|**AssignmentRequest**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], [[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|This only applies to List mode, and is ignored in other modes. In GPU-P, string is GPU device interface, and unit16 is partition id. HCS simply assigns the partition with the input id. In GPU-PV, string is GPU device interface, and unit16 is 0xffff. HCS needs to find an available partition to assign.|
|**AllowVendorExtension**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Whether we allow vendor extension.|

---

<a name = "GuestConnection"></a>
## GuestConnection
Referenced by: [[VirtualMachine|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**UseVsock**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|When true, use Vsock rather than Hyper-V sockets to communicate with the guest service.|
|**UseConnectedSuspend**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|When true, don't disconnect the guest connection when pausing the virtual machine.|
|**UseHostTimeZone**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|when true, set the guest's time zone to that of the host.|

---

<a name = "GuestConnectionInfo"></a>
## GuestConnectionInfo
Referenced by: [[Properties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#properties]]

Information about the guest.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**SupportedSchemaVersions**<br>|<[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#version]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Each schema version x.y stands for the range of versions a.b where a==x and b<=y. This list comes from the SupportedSchemaVersions field in GcsCapabilities.|
|**ProtocolVersion**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**GuestDefinedCapabilities**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "GuestCrash"></a>
## GuestCrash
Referenced by: [[AttributionRecord|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#attributionrecord]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**CrashParameters**<br>|<[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Crash parameters as reported by the guest.|

---

<a name = "GuestCrashReporting"></a>
## GuestCrashReporting
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**WindowsCrashSettings**<br>|[[WindowsCrashReporting|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#windowscrashreporting]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "GuestModifySettingRequest"></a>
## GuestModifySettingRequest


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ResourceType**<br>|[[ModifyResourceType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#modifyresourcetype]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**RequestType**<br>|[[ModifyRequestType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#modifyrequesttype]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Settings**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "GuestOs"></a>
## GuestOs
Referenced by: [[Container|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container]]

Properties of the guest operating system that boots on a Windows Server Container.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**HostName**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HostName assigned to a container guest operating system.|

---

<a name = "GuestState"></a>
## GuestState
Referenced by: [[VirtualMachine|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**GuestStateFilePath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The path to an existing file uses for persistent guest state storage. An empty string indicates the system should initialize new transient, in-memory guest state.|
|**RuntimeStateFilePath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The path to an existing file for persistent runtime state storage. An empty string indicates the system should initialize new transient, in-memory runtime state.|
|**ForceTransientState**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|If true, the guest state and runtime state files will be used as templates to populate transient, in-memory state instead of using the files as persistent backing store.|

---

<a name = "Heartbeat"></a>
## Heartbeat
Referenced by: [[Services|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#services]]




**Note:** This is an empty struct with no fields, and to be used in the JSON document must be specified as an empty object: `"{}"`.

---

<a name = "HostedSystem"></a>
## HostedSystem
Describes the configuration of a container compute system hosted by another compute system. This can have its own schema version since the hosted system could support different versions compared to the host machine.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|A version structure for this schema. Properties nested within this object may identify their own schema versions.|
|**Container**<br>|[[Container|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The set of properties defining a container.|

---

<a name = "HostFiles"></a>
## HostFiles
Referenced by: [[Namespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#namespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**base_image_path**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**sandbox_path**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**file**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "HostProcessorModificationRequest"></a>
## HostProcessorModificationRequest
Structure used to request a service processor modification

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Operation**<br>|[[ModifyServiceOperation|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#modifyserviceoperation]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**OperationDetails**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "HvSocketAddress"></a>
## HvSocketAddress
This class defines address settings applied to a VM by the GCS every time a VM starts or restores.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**LocalAddress**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ParentAddress**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "HvSocketServiceConfig"></a>
## HvSocketServiceConfig
Referenced by: [[ContainerCredentialGuardHvSocketServiceConfig|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containercredentialguardhvsocketserviceconfig]]; [[HvSocketSystemConfig|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#hvsocketsystemconfig]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**BindSecurityDescriptor**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|SDDL string that HvSocket will check before allowing a host process to bind to this specific service. If not specified, defaults to the system DefaultBindSecurityDescriptor, defined in HvSocketSystemWpConfig in V1.|
|**ConnectSecurityDescriptor**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|SDDL string that HvSocket will check before allowing a host process to connect to this specific service. If not specified, defaults to the system DefaultConnectSecurityDescriptor, defined in HvSocketSystemWpConfig in V1.|
|**AllowWildcardBinds**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|If true, HvSocket will process wildcard binds for this service/system combination. Wildcard binds are secured in the registry at SOFTWARE/Microsoft/Windows NT/CurrentVersion/Virtualization/HvSocket/WildcardDescriptors|

---

<a name = "HvSocketSystemConfig"></a>
## HvSocketSystemConfig
Referenced by: [[Container_HvSocket|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container_hvsocket]]; [[VirtualMachine_HvSocket|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine_hvsocket]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**DefaultBindSecurityDescriptor**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|SDDL string that HvSocket will check before allowing a host process to bind to an unlisted service for this specific container/VM (not wildcard binds).|
|**DefaultConnectSecurityDescriptor**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|SDDL string that HvSocket will check before allowing a host process to connect to an unlisted service in the VM/container.|
|**ServiceTable**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<Guid, [[HvSocketServiceConfig|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#hvsocketserviceconfig]]>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "IdleProcessorsRequest"></a>
## IdleProcessorsRequest


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**IdleProcessorCount**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "InjectNonMaskableInterrupt"></a>
## InjectNonMaskableInterrupt
Referenced by: [[AttributionRecord|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#attributionrecord]]

A non-maskable interrupt (NMI) was inject by the host management client or other tool.


**Note:** This is an empty struct with no fields, and to be used in the JSON document must be specified as an empty object: `"{}"`.

---

<a name = "IntegrationComponentStatus"></a>
## IntegrationComponentStatus


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**IsEnabled**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|If IC is enabled on this compute system|
|**State**<br>|[[IntegrationComponentOperationalState|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#integrationcomponentoperationalstate]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The current state of the IC inside the VM|
|**Reason**<br>|[[IntegrationComponentOperatingStateReason|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#integrationcomponentoperatingstatereason]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Explanation for the State|

---

<a name = "InterfaceClass"></a>
## InterfaceClass
Referenced by: [[DeviceCategory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devicecategory]]; [[DeviceInstance|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#deviceinstance]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**type**<br>|[[InterfaceClassType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#interfaceclasstype]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**identifier**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**recurse**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "IovSettings"></a>
## IovSettings
Referenced by: [[NetworkAdapter|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#networkadapter]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**OffloadWeight**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The weight assigned to this port for I/O virtualization (IOV) offloading. Setting this to 0 disables IOV offloading.|
|**QueuePairsRequested**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The number of queue pairs requested for this port for I/O virtualization (IOV) offloading.|
|**InterruptModeration**<br>|[[InterruptModerationMode|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#interruptmoderationmode]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The interrupt moderation mode for I/O virtualization (IOV) offloading.|

---

<a name = "JobCpu"></a>
## JobCpu
Referenced by: [[JobNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#jobnamespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**rate**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**weight**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "JobMemory"></a>
## JobMemory
Referenced by: [[JobNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#jobnamespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**limit**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "JobNamespace"></a>
## JobNamespace
Referenced by: [[Namespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#namespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**cpu**<br>|[[JobCpu|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#jobcpu]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**memory**<br>|[[JobMemory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#jobmemory]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**systemroot**<br>|[[JobSystemRoot|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#jobsystemroot]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**terminationpolicy**<br>|[[JobTerminationPolicy|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#jobterminationpolicy]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**threadimpersonation**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "JobSystemRoot"></a>
## JobSystemRoot
Referenced by: [[JobNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#jobnamespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**path**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "Keyboard"></a>
## Keyboard
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]




**Note:** This is an empty struct with no fields, and to be used in the JSON document must be specified as an empty object: `"{}"`.

---

<a name = "KvpExchange"></a>
## KvpExchange
Referenced by: [[Services|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#services]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**EnableHostOSInfoKvpItems**<br>|[[StateOverride|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#stateoverride]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**EntriesToBeAdded**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], [[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**EntriesToBeRemoved**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "KvpQuery"></a>
## KvpQuery
Query information to request from the key-value exchange integration component

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Source**<br>|[[KvpSource|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#kvpsource]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The location of the key-value pairs depending on the type of the key-value pairs|
|**Keys**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Keys in the key-value pair to be used for the query depending on the operation|

---

<a name = "Layer"></a>
## Layer
Referenced by: [[CombinedLayers|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#combinedlayers]]; [[LayerData|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#layerdata]]; [[Storage|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#storage]]

Describe the parent hierarchy for a container's storage

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Id**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Identifier for a layer.|
|**Path**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Root path of the layer.|
|**PathType**<br>|[[PathType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#pathtype]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Defines how to interpret the layer's path.|
|**Cache**<br>|[[CacheMode|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#cachemode]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Unspecified defaults to Enabled|

---

<a name = "LayerData"></a>
## LayerData


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#version]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Layers**<br>|<[[Layer|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#layer]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "LinuxKernelDirect"></a>
## LinuxKernelDirect
Referenced by: [[Chipset|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#chipset]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**KernelFilePath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**InitRdPath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**KernelCmdLine**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "LogicalProcessor"></a>
## LogicalProcessor
Referenced by: [[ProcessorTopology|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processortopology]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**LpIndex**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**NodeNumber**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**PackageId**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**CoreId**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**RootVpIndex**<br>|[[int32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Subnodes**<br>|<[[Subnode|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#subnode]]> array|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "MappedDirectory"></a>
## MappedDirectory
Referenced by: [[Container|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container]]

Object that describes a directory in the host that is requested to be mapped into a compute system's guest.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**HostPath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Path in the host that is going to be mapped into the compute system.|
|**HostPathType**<br>|[[PathType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#pathtype]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Defines how to interpret the host path.|
|**ContainerPath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Path relative to the compute system's guest. This is the resulting path from mapping the host path.|
|**ReadOnly**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|When set to true, the mapped directory in the compute system's guest will be read-only.|
|**SupportCloudFiles**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|When set to true, the mapped directory in the compute system's guest will support cloud files.|

---

<a name = "MappedPipe"></a>
## MappedPipe
Referenced by: [[Container|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container]]

Object that describes a named pipe that is requested to be mapped into a compute system's guest.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ContainerPipeName**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The resulting named pipe that will be accessible in the compute system's guest.|
|**HostPath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The named pipe path in the host that will be mapped into a compute system's guest.|
|**HostPathType**<br>|[[MappedPipePathType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#mappedpipepathtype]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Defines how to interpret the host path.|

---

<a name = "MappedVirtualDisk"></a>
## MappedVirtualDisk


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ContainerPath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Lun**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "MemoryInformationForVm"></a>
## MemoryInformationForVm
Referenced by: [[Properties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#properties]]

The response of memory information for virtual machine when query memory property of compute system

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**VirtualNodeCount**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**VirtualMachineMemory**<br>|[[VmMemory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#vmmemory]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**VirtualNodes**<br>|<[[VirtualNodeInfo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualnodeinfo]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "MemoryStats"></a>
## MemoryStats
Referenced by: [[Statistics|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#statistics]]

Memory runtime statistics

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**MemoryUsageCommitBytes**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**MemoryUsageCommitPeakBytes**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**MemoryUsagePrivateWorkingSetBytes**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ModificationRequest"></a>
## ModificationRequest
Structure used for service level modification request. Right now, we support modification of a single property type in a call.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**PropertyType**<br>|[[ModifyPropertyType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#modifypropertytype]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Specifies the property to be modified|
|**Settings**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Settings to the modification request|

---

<a name = "ModifySettingRequest"></a>
## ModifySettingRequest


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ResourcePath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**RequestType**<br>|[[ModifyRequestType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#modifyrequesttype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Settings**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**GuestRequest**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "MountManagerMountPoint"></a>
## MountManagerMountPoint
Referenced by: [[MountManagerNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#mountmanagernamespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**path**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "MountManagerNamespace"></a>
## MountManagerNamespace
Referenced by: [[Namespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#namespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**mount_point**<br>|<[[MountManagerMountPoint|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#mountmanagermountpoint]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "Mouse"></a>
## Mouse
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]




**Note:** This is an empty struct with no fields, and to be used in the JSON document must be specified as an empty object: `"{}"`.

---

<a name = "NamedPipeNamespace"></a>
## NamedPipeNamespace
Referenced by: [[Namespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#namespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**symlink**<br>|<[[NamedPipeSymlink|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#namedpipesymlink]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "NamedPipeSymlink"></a>
## NamedPipeSymlink
Referenced by: [[NamedPipeNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#namedpipenamespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**path**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "Namespace"></a>
## Namespace
Referenced by: [[ContainersDef_Container|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#containersdef_container]]; [[DeviceExtension|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#deviceextension]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**job**<br>|[[JobNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#jobnamespace]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**filesystem**<br>|[[FilesystemNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#filesystemnamespace]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**mountmgr**<br>|[[MountManagerNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#mountmanagernamespace]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**namedpipe**<br>|[[NamedPipeNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#namedpipenamespace]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ob**<br>|[[ObjectNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#objectnamespace]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**registry**<br>|[[RegistryNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrynamespace]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**network**<br>|[[NetworkNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#networknamespace]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**device**<br>|[[DeviceNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devicenamespace]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**hostfiles**<br>|[[HostFiles|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#hostfiles]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "NetworkAdapter"></a>
## NetworkAdapter
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**EndpointId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**MacAddress**<br>|[[MacAddress|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**InstanceId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**DisableInterruptBatching**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Disable interrupt batching (MNF) for network to decrease latency and increase throughput, at per-interrupt processing cost.|
|**IovSettings**<br>|[[IovSettings|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#iovsettings]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The I/O virtualization (IOV) offloading configuration.|
|**ConnectionState**<br>|[[StateOverride|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#stateoverride]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|When updating a network adapter, indicates whether the adapter should be connected, disconnected, or updated in place.|

---

<a name = "Networking"></a>
## Networking
Referenced by: [[Container|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**AllowUnqualifiedDnsQuery**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**DnsSearchList**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**NetworkSharedContainerName**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Namespace**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Guid in windows; string in linux|
|**NetworkAdapters**<br>|<[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "NetworkNamespace"></a>
## NetworkNamespace
Referenced by: [[Namespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#namespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**compartment**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ObjectDirectory"></a>
## ObjectDirectory
Referenced by: [[ObjectDirectory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#objectdirectory]]; [[ObjectNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#objectnamespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**clonesd**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**shadow**<br>|[[ObjectDirectoryShadow|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#objectdirectoryshadow]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**symlink**<br>|<[[ObjectSymlink|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#objectsymlink]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**objdir**<br>|<[[ObjectDirectory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#objectdirectory]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ObjectNamespace"></a>
## ObjectNamespace
Referenced by: [[Namespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#namespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**shadow**<br>|[[ObjectDirectoryShadow|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#objectdirectoryshadow]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**symlink**<br>|<[[ObjectSymlink|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#objectsymlink]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**objdir**<br>|<[[ObjectDirectory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#objectdirectory]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ObjectSymlink"></a>
## ObjectSymlink
Referenced by: [[ObjectDirectory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#objectdirectory]]; [[ObjectNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#objectnamespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**path**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**scope**<br>|[[ObjectSymlinkScope|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#objectsymlinkscope]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**pathtoclone**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**access_mask**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "OperationFailure"></a>
## OperationFailure
Referenced by: [[AttributionRecord|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#attributionrecord]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Detail**<br>|[[OperationFailureDetail|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#operationfailuredetail]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "OperationFailureInfo"></a>
## OperationFailureInfo
Referenced by: [[OperationInfo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#operationinfo]]

/ Contains information describing a failure in an operation on a compute system.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Result**<br>|[[int32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "OperationInfo"></a>
## OperationInfo
/ Information about an operation on a compute system. / An operation is typically an HCS API call but can also be something like / a compute system exit or crash.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Operation**<br>|[[OperationType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#operationtype]]|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|/ Type of the operation|
|**StartTimestamp**<br>|[[DateTime|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|/ Start time of the opertation|
|**EndTimestamp**<br>|[[DateTime|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|/ End time of the opertation|
|**FailureInfo**<br>|[[OperationFailureInfo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#operationfailureinfo]]|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|/ Contains extra information if the operation failed. / If this field is not present, the operation succeeded.|

---

<a name = "OperationNotificationInfo"></a>
## OperationNotificationInfo
Payload for an operation notification


**Note:** This is an empty struct with no fields, and to be used in the JSON document must be specified as an empty object: `"{}"`.

---

<a name = "OperationProgress"></a>
## OperationProgress
Information about the progress of an operation


**Note:** This is an empty struct with no fields, and to be used in the JSON document must be specified as an empty object: `"{}"`.

---

<a name = "OsLayerOptions"></a>
## OsLayerOptions


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Type**<br>|[[OsLayerType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#oslayertype]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**DisableCiCacheOptimization**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**SkipUpdateBcdForBoot**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "PauseNotification"></a>
## PauseNotification
Referenced by: [[PauseOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#pauseoptions]]

Notification data that is indicated to components running in the Virtual Machine.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Reason**<br>|[[PauseReason|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#pausereason]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "PauseOptions"></a>
## PauseOptions
Options for HcsPauseComputeSystem

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**SuspensionLevel**<br>|[[PauseSuspensionLevel|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#pausesuspensionlevel]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**HostedNotification**<br>|[[PauseNotification|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#pausenotification]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "Plan9"></a>
## Plan9
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Shares**<br>|<[[Plan9Share|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#plan9share]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "Plan9Share"></a>
## Plan9Share
Referenced by: [[Plan9|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#plan9]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**AccessName**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The name by which the guest operation system can access this share, via the aname parameter in the Plan9 protocol.|
|**Path**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Port**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**AllowedFiles**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ProcessDetails"></a>
## ProcessDetails
Referenced by: [[Properties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#properties]]

Information about a process running in a container

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ProcessId**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ImageName**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**CreateTimestamp**<br>|[[DateTime|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**UserTime100ns**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**KernelTime100ns**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**MemoryCommitBytes**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**MemoryWorkingSetPrivateBytes**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**MemoryWorkingSetSharedBytes**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ProcessModifyRequest"></a>
## ProcessModifyRequest
Passed to HcsRpc_ModifyProcess

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Operation**<br>|[[ModifyOperation|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#modifyoperation]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ConsoleSize**<br>|[[ConsoleSize|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#consolesize]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**CloseHandle**<br>|[[CloseHandle|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#closehandle]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ProcessorCapabilitiesInfo"></a>
## ProcessorCapabilitiesInfo
Host specific processor feature capabilities

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ProcessorFeatures**<br>|<[[ProcessorFeature|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processorfeature]]> array|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Processor features|
|**XsaveProcessorFeatures**<br>|<[[XsaveProcessorFeature|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#xsaveprocessorfeature]]> array|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Xsave processor features|
|**CacheLineFlushSize**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Processor cache line flush size|
|**ImplementedPhysicalAddressBits**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Processor physical address bits|
|**NonArchitecturalCoreSharing**<br>|[[NonArchitecturalCoreSharing|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#nonarchitecturalcoresharing]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Processor non architectural core sharing|

---

<a name = "ProcessorFeatureSet"></a>
## ProcessorFeatureSet
Referenced by: [[VirtualMachine_Processor|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine_processor]]

VM specific processor feature requirements

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ProcessorFeatures**<br>|<[[ProcessorFeature|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processorfeature]]> array|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Processor features|
|**XsaveProcessorFeatures**<br>|<[[XsaveProcessorFeature|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#xsaveprocessorfeature]]> array|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Xsave processor features|
|**ProcessorFeatureSetMode**<br>|[[ProcessorFeatureSetMode|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processorfeaturesetmode]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Processor feature set mode|

---

<a name = "ProcessorLimits"></a>
## ProcessorLimits
Used when modifying processor scheduling limits of a virtual machine.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Limit**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Maximum amount of host CPU resources that the virtual machine can use.|
|**Weight**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Value describing the relative priority of this virtual machine compared to other virtual machines.|
|**Reservation**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Minimum amount of host CPU resources that the virtual machine is guaranteed.|
|**MaximumFrequencyMHz**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Provides the target maximum CPU frequency, in MHz, for a virtual machine.|

---

<a name = "ProcessorStats"></a>
## ProcessorStats
Referenced by: [[Statistics|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#statistics]]

CPU runtime statistics

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**TotalRuntime100ns**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**RuntimeUser100ns**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**RuntimeKernel100ns**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ProcessorTopology"></a>
## ProcessorTopology
Structure used to return processor topology for a Service property query

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**LogicalProcessorCount**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**LogicalProcessors**<br>|<[[LogicalProcessor|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#logicalprocessor]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ProcessParameters"></a>
## ProcessParameters


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ApplicationName**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**CommandLine**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**CommandArgs**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|optional alternative to CommandLine, currently only supported by Linux GCS|
|**User**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**WorkingDirectory**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Environment**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], [[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**RestrictedToken**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|if set, will run as low-privilege process|
|**EmulateConsole**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|if set, ignore StdErrPipe|
|**CreateStdInPipe**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**CreateStdOutPipe**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**CreateStdErrPipe**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ConsoleSize**<br>|<[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], 2> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|height then width|
|**UseExistingLogin**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|if set, find an existing session for the user and create the process in it|
|**UseLegacyConsole**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|if set, use the legacy console instead of conhost|

---

<a name = "ProcessStatus"></a>
## ProcessStatus
Provided in the EventData parameter of an HcsEventProcessExited HCS_EVENT.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ProcessId**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The process id (PID) of the process.|
|**Exited**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|True if the process has exited, false if it has not exited yet.|
|**ExitCode**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Exit code of the process. The ExitCode is valid only if LastWaitResult is S_OK and Exited is true.|
|**LastWaitResult**<br>|[[int32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Status of waiting for process exit. S_OK indicates success. Other HRESULT values on error.|

---

<a name = "Properties"></a>
## Properties


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Id**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**SystemType**<br>|[[SystemType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#systemtype]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**RuntimeOsType**<br>|[[OsType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#ostype]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**RuntimeId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**RuntimeTemplateId**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**State**<br>|[[State|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#state]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Stopped**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ExitType**<br>|[[NotificationType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#notificationtype]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Memory**<br>|[[MemoryInformationForVm|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#memoryinformationforvm]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Statistics**<br>|[[Statistics|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#statistics]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ProcessList**<br>|<[[ProcessDetails|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processdetails]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**TerminateOnLastHandleClosed**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**HostingSystemId**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**SharedMemoryRegionInfo**<br>|<[[SharedMemoryRegionInfo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#sharedmemoryregioninfo]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**GuestConnectionInfo**<br>|[[GuestConnectionInfo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#guestconnectioninfo]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**PropertyResponses**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], [[PropertyResponse|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#propertyresponse]]>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|This is a new response object, introduced in version 2.5, which maps the requested property names to their associated response objects.|

---

<a name = "PropertyResponse"></a>
## PropertyResponse
Referenced by: [[Properties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#properties]]; [[ServiceProperties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#serviceproperties]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Error**<br>|[[ResultError|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#resulterror]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Response**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "QoSCapabilities"></a>
## QoSCapabilities
Quality of Service (QoS) capabilities

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ProcessorQoSSupported**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Whether or not processor QoS is supported|

---

<a name = "RdpConnectionOptions"></a>
## RdpConnectionOptions
Referenced by: [[EnhancedModeVideo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#enhancedmodevideo]]; [[VideoMonitor|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#videomonitor]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**AccessSids**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**NamedPipe**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "RegistryChanges"></a>
## RegistryChanges
Referenced by: [[Container|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container]]; [[VirtualMachine|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**AddValues**<br>|<[[RegistryValue|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registryvalue]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**DeleteKeys**<br>|<[[RegistryKey|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrykey]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "RegistryDeleteKey"></a>
## RegistryDeleteKey
Referenced by: [[RegistryHiveStack|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registryhivestack]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "RegistryFlushState"></a>
## RegistryFlushState
Represents the flush state of the registry hive for a Windows container's job object.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Enabled**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Determines whether the flush state of the registry hive is enabled or not. When not enabled, flushes are ignored and changes to the registry are not preserved.|

---

<a name = "RegistryHiveStack"></a>
## RegistryHiveStack
Referenced by: [[RegistryNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrynamespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**hive**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**layer**<br>|<[[RegistryLayer|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrylayer]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**mkkey**<br>|<[[RegistryMakeKey|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrymakekey]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**delkey**<br>|<[[RegistryDeleteKey|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrydeletekey]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "RegistryKey"></a>
## RegistryKey
Referenced by: [[RegistryChanges|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrychanges]]; [[RegistryValue|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registryvalue]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Hive**<br>|[[RegistryHive|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registryhive]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Volatile**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "RegistryLayer"></a>
## RegistryLayer
Referenced by: [[RegistryHiveStack|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registryhivestack]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**hosthive**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**filepath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**identifier**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**readonly**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**immutable**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**inherittrustclass**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**trustedhive**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**writethrough**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**fileaccesstoken**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The FileAccessToken field should only be used in-memory and not serialized/deserialized, since it refers to a token handle.|

---

<a name = "RegistryMakeKey"></a>
## RegistryMakeKey
Referenced by: [[RegistryHiveStack|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registryhivestack]]; [[RegistryMakeKey|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrymakekey]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**clonesd**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**mkkey**<br>|<[[RegistryMakeKey|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrymakekey]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**mkvalue**<br>|<[[RegistryMakeValue|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrymakevalue]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**volatile**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "RegistryMakeValue"></a>
## RegistryMakeValue
Referenced by: [[RegistryMakeKey|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrymakekey]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**data_multistring**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**data_dword**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**data_string**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**data_binary**<br>|[[NullableByteArray|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "RegistryNamespace"></a>
## RegistryNamespace
Referenced by: [[Namespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#namespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**symlink**<br>|<[[RegistrySymlink|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrysymlink]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**redirectionnode**<br>|<[[RegistryRedirectionNode|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registryredirectionnode]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**hivestack**<br>|<[[RegistryHiveStack|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registryhivestack]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "RegistryRedirectionNode"></a>
## RegistryRedirectionNode
Referenced by: [[RegistryNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrynamespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**containerpath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**hostpath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**hivestack**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**access_mask**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**trustedhive**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**exitnode**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "RegistrySymlink"></a>
## RegistrySymlink
Referenced by: [[RegistryNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrynamespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**key**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**target**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "RegistryValue"></a>
## RegistryValue
Referenced by: [[RegistryChanges|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrychanges]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Key**<br>|[[RegistryKey|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrykey]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Type**<br>|[[RegistryValueType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registryvaluetype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**StringValue**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|One and only one value type must be set.|
|**BinaryValue**<br>|[[ByteArray|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**DWordValue**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**QWordValue**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**CustomType**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Only used if RegistryValueType is CustomType The data is in BinaryValue|

---

<a name = "RestoreState"></a>
## RestoreState
Referenced by: [[VirtualMachine|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**SaveStateFilePath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The path to the save state file to restore the system from.|
|**TemplateSystemId**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The ID of the template system to clone this new system off of. An empty string indicates the system should not be cloned from a template.|

---

<a name = "ResultError"></a>
## ResultError
Referenced by: [[PropertyResponse|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#propertyresponse]]

Extended error information returned by the HCS

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Error**<br>|[[int32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|HRESULT error code|
|**ErrorMessage**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Error message|
|**ErrorEvents**<br>|<[[ErrorEvent|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#errorevent]]> array|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Error event details|
|**Attribution**<br>|<[[AttributionRecord|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#attributionrecord]]> array|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Attribution record|

---

<a name = "SaveOptions"></a>
## SaveOptions


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**SaveType**<br>|[[SaveType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#savetype]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The type of save operation to be performed.|
|**SaveStateFilePath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The path to the file that will container the saved state.|

---

<a name = "Scsi"></a>
## Scsi
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]

Object describing a SCSI controller.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Attachments**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], [[Attachment|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#attachment]]>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Map of attachments, where the key is the integer LUN number on the controller.|
|**DisableInterruptBatching**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Disable interrupt batching (MNF) for storage to decrease latency and increase throughput, at per-interrupt processing cost.|

---

<a name = "Service_PropertyQuery"></a>
## Service_PropertyQuery
Structure used to perform property query

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**PropertyTypes**<br>|<[[GetPropertyType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#getpropertytype]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Specifies the property array to query|
|**PropertyQueries**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], [[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "ServiceProperties"></a>
## ServiceProperties
Properties of the host

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Properties**<br>|<[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The service properties will be returned as an array corresponding to the requested property types.|
|**PropertyResponses**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], [[PropertyResponse|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#propertyresponse]]>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|This is a response object, introduced in version 2.5, which takes the name of the property and its associated query object if needed|

---

<a name = "Services"></a>
## Services
Referenced by: [[VirtualMachine|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Heartbeat**<br>|[[Heartbeat|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#heartbeat]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Heartbeat integration component that is used to detect if a VM is operational|
|**Shutdown**<br>|[[Shutdown|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#shutdown]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Shutdown integration component that is used for any shutdown-related actions|
|**Timesync**<br>|[[Timesync|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#timesync]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Timesync integration component that syncs time guest's time based on host's time|
|**KvpExchange**<br>|[[KvpExchange|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#kvpexchange]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Key-value exchange integration component to exchange key-value pairs between host and guests|

---

<a name = "SetPropertyOperation"></a>
## SetPropertyOperation
Set properties operation settings

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**GroupId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**PropertyCode**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**PropertyValue**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "SharedMemoryConfiguration"></a>
## SharedMemoryConfiguration
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Regions**<br>|<[[SharedMemoryRegion|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#sharedmemoryregion]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "SharedMemoryRegion"></a>
## SharedMemoryRegion
Referenced by: [[SharedMemoryConfiguration|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#sharedmemoryconfiguration]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**SectionName**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**StartOffset**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Length**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**AllowGuestWrite**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**HiddenFromGuest**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "SharedMemoryRegionInfo"></a>
## SharedMemoryRegionInfo
Referenced by: [[Properties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#properties]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**SectionName**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**GuestPhysicalAddress**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "Shutdown"></a>
## Shutdown
Referenced by: [[Services|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#services]]




**Note:** This is an empty struct with no fields, and to be used in the JSON document must be specified as an empty object: `"{}"`.

---

<a name = "ShutdownOptions"></a>
## ShutdownOptions
Options for HcsShutdownComputeSystem

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Mechanism**<br>|[[ShutdownMechanism|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#shutdownmechanism]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|What kind of mechanism used to perform the shutdown operation|
|**Type**<br>|[[ShutdownType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#shutdowntype]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|What is the type of the shutdown operation|
|**Force**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|If this shutdown is forceful or not|
|**Reason**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Reason for the shutdown|

---

<a name = "SignalProcessOptions"></a>
## SignalProcessOptions


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Signal**<br>|[[ProcessSignal|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processsignal]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "Statistics"></a>
## Statistics
Referenced by: [[Properties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#properties]]

Runtime statistics for a container

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Timestamp**<br>|[[DateTime|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ContainerStartTime**<br>|[[DateTime|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Uptime100ns**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Processor**<br>|[[ProcessorStats|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processorstats]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Memory**<br>|[[MemoryStats|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#memorystats]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Storage**<br>|[[StorageStats|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#storagestats]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "Storage"></a>
## Storage
Referenced by: [[Container|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#container]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Layers**<br>|<[[Layer|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#layer]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|List of layers that describe the parent hierarchy for a container's storage. These layers combined together, presented as a disposable and/or committable working storage, are used by the container to record all changes done to the parent layers.|
|**Path**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Path that points to the scratch space of a container, where parent layers are combined together to present a new disposable and/or committable layer with the changes done during its runtime.|
|**QoS**<br>|[[StorageQoS|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#storageqos]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Optional quality of service configurations for a container's storage.|

---

<a name = "StorageQoS"></a>
## StorageQoS
Referenced by: [[Storage|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#storage]]; [[VirtualMachine|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine]]

Describes storage quality of service settings, relative to a storage volume.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**IopsMaximum**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Defines the maximum allowed Input/Output operations per second in a volume.|
|**BandwidthMaximum**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Defines the maximum bandwidth (bytes per second) allowed in a volume.|

---

<a name = "StorageStats"></a>
## StorageStats
Referenced by: [[Statistics|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#statistics]]

Storage runtime statistics

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ReadCountNormalized**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ReadSizeBytes**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**WriteCountNormalized**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**WriteSizeBytes**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "Subnode"></a>
## Subnode
Referenced by: [[LogicalProcessor|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#logicalprocessor]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Type**<br>|[[SubnodeType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#subnodetype]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Id**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "System_PropertyQuery"></a>
## System_PropertyQuery
By default the basic properties will be returned. This query provides a way to request specific properties.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**PropertyTypes**<br>|<[[System_PropertyType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#system_propertytype]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Queries**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], [[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|This is a new property request object, introduced in version 2.5, which takes the names of the properties and their associated query objects if needed.|

---

<a name = "SystemExit"></a>
## SystemExit
Referenced by: [[AttributionRecord|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#attributionrecord]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Detail**<br>|[[SystemExitDetail|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#systemexitdetail]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Initiator**<br>|[[ExitInitiator|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#exitinitiator]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "SystemExitStatus"></a>
## SystemExitStatus
Document provided in the EventData parameter of an HcsEventSystemExited HCS_EVENT.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Status**<br>|[[int32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Exit status (HRESULT) for the system.|
|**ExitType**<br>|[[NotificationType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#notificationtype]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Exit type for the system.|
|**Attribution**<br>|<[[AttributionRecord|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#attributionrecord]]> array|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "SystemProcessorModificationRequest"></a>
## SystemProcessorModificationRequest
Structure used to request a system processor modification

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**GroupId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "SystemQuery"></a>
## SystemQuery


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Ids**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Names**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Types**<br>|<[[SystemType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#systemtype]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Owners**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "SystemTime"></a>
## SystemTime
Referenced by: [[TimeZoneInformation|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#timezoneinformation]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Year**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Month**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**DayOfWeek**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Day**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Hour**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Minute**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Second**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Milliseconds**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "Timesync"></a>
## Timesync
Referenced by: [[Services|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#services]]




**Note:** This is an empty struct with no fields, and to be used in the JSON document must be specified as an empty object: `"{}"`.

---

<a name = "TimeZoneInformation"></a>
## TimeZoneInformation


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Bias**<br>|[[int32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**StandardName**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**StandardDate**<br>|[[SystemTime|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#systemtime]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**StandardBias**<br>|[[int32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**DaylightName**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**DaylightDate**<br>|[[SystemTime|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#systemtime]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**DaylightBias**<br>|[[int32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "Topology"></a>
## Topology
Referenced by: [[VirtualMachine|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Memory**<br>|[[VirtualMachine_Memory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine_memory]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Processor**<br>|[[VirtualMachine_Processor|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine_processor]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "TripleFault"></a>
## TripleFault
Referenced by: [[AttributionRecord|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#attributionrecord]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ErrorType**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "Uefi"></a>
## Uefi
Referenced by: [[Chipset|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#chipset]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**EnableDebugger**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**SecureBootTemplateId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ApplySecureBootTemplate**<br>|[[ApplySecureBootTemplateType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#applysecureboottemplatetype]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**BootThis**<br>|[[UefiBootEntry|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#uefibootentry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Console**<br>|[[SerialConsole|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#serialconsole]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**StopOnBootFailure**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "UefiBootEntry"></a>
## UefiBootEntry
Referenced by: [[Uefi|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#uefi]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**DeviceType**<br>|[[UefiBootDevice|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#uefibootdevice]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**DevicePath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**DiskNumber**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**OptionalData**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**VmbFsRootPath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "Version"></a>
## Version
Referenced by: [[BasicInformation|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#basicinformation]]; [[ComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#computesystem]]; [[GuestConnectionInfo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#guestconnectioninfo]]; [[HostedSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#hostedsystem]]; [[LayerData|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#layerdata]]; [[VirtualMachine|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualmachine]]

Object that describes a version with a Major.Minor format.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Major**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The major version value. Individual major versions are not compatible with one another.|
|**Minor**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The minor version value. A lower minor version is considered a compatible subset of a higher minor version.|

---

<a name = "VideoMonitor"></a>
## VideoMonitor
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**HorizontalResolution**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**VerticalResolution**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ConnectionOptions**<br>|[[RdpConnectionOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#rdpconnectionoptions]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "VirtioSerial"></a>
## VirtioSerial
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Ports**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], [[VirtioSerialPort|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtioserialport]]>|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "VirtioSerialPort"></a>
## VirtioSerialPort
Referenced by: [[VirtioSerial|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtioserial]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**NamedPipe**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Pipe name to connect to this port from the host.|
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Friendly name provided to the guest.|

---

<a name = "VirtualDeviceFailure"></a>
## VirtualDeviceFailure
Referenced by: [[AttributionRecord|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#attributionrecord]]

Provides information on failures originated by a virtual device.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Detail**<br>|[[VirtualDeviceFailureDetail|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualdevicefailuredetail]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Friendly name of the virtual device.|
|**DeviceId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Id of the virtual device.|
|**InstanceId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Instance Id of the virtual device.|

---

<a name = "VirtualMachine"></a>
## VirtualMachine
Referenced by: [[ComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#computesystem]]

Configuration of a virtual machine, used during its creation to set up and/or use resources.

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Version**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#version]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The virtual machine's version that defines which virtual device's features and which virtual machine features the virtual machine supports. If a version isn't specified, the latest version will be used.|
|**StopOnReset**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|When set to true, the virtual machine will treat a reset as a stop, releasing resources and cleaning up state.|
|**Chipset**<br>|[[Chipset|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#chipset]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An object describing the chipset settings, including boot settings.|
|**ComputeTopology**<br>|[[Topology|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#topology]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An object describing the processor and memory configuration of a virtual machine.|
|**Devices**<br>|[[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Nested objects describing the set of devices attached to the virtual machine.|
|**Services**<br>|[[Services|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#services]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An object that configures the different guest services the virtual machine will support. Most of these are Windows specific.|
|**GuestState**<br>|[[GuestState|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#gueststate]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object describing files used to back guest state. When omitted, guest state is transient and kept purely in memory.|
|**RestoreState**<br>|[[RestoreState|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#restorestate]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object describing the state the virtual machine should restore from as part of start.|
|**RegistryChanges**<br>|[[RegistryChanges|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#registrychanges]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|A set of changes applied to a Windows guest's registry at boot time.|
|**StorageQoS**<br>|[[StorageQoS|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#storageqos]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional set of quality of service restrictions on the virtual machine's storage.|
|**GuestConnection**<br>|[[GuestConnection|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#guestconnection]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object describing settings for a conection to the guest OS. If GuestConnection is not specified, the virtual machine will be considered started once the chipset is fully powered on. If specified, the virtual machine start will wait until a guest connection is established.|

---

<a name = "VirtualMachine_HvSocket"></a>
## VirtualMachine_HvSocket
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]

HvSocket configuration for a VM

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**HvSocketConfig**<br>|[[HvSocketSystemConfig|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#hvsocketsystemconfig]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "VirtualMachine_Memory"></a>
## VirtualMachine_Memory
Referenced by: [[Topology|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#topology]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**SizeInMB**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**AllowOvercommit**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|If enabled, then the VM's memory is backed by the Windows pagefile rather than physically backed, statically allocated memory.|
|**BackingPageSize**<br>|[[MemoryBackingPageSize|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#memorybackingpagesize]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|The preferred page size unit (chunk size) used when allocating backing pages for the VM.|
|**FaultClusterSizeShift**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Fault clustering size for primary RAM. Backported to windows 10 version 2004|
|**DirectMapFaultClusterSizeShift**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Fault clustering size for direct mapped memory. Backported to windows 10 version 2004|
|**PinBackingPages**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|If enabled, then each backing page is physically pinned on first access.|
|**ForbidSmallBackingPages**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|If enabled, then backing page chunks smaller than the backing page size are never used unless the system is under extreme memory pressure. If the backing page size is Small, then it is forced to Large when this option is enabled.|
|**EnableHotHint**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|If enabled, then the memory hot hint feature is exposed to the VM, allowing it to prefetch pages into its working set. (if supported by the guest operating system).|
|**EnableColdHint**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|If enabled, then the memory cold hint feature is exposed to the VM, allowing it to trim zeroed pages from its working set (if supported by the guest operating system).|
|**EnableColdDiscardHint**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|If enabled, then the memory cold discard hint feature is exposed to the VM, allowing it to trim non-zeroed pages from the working set (if supported by the guest operating system).|
|**EnableDeferredCommit**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|If enabled, then commit is not charged for each backing page until first access.|
|**LowMmioGapInMB**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Low MMIO region allocated below 4GB|
|**HighMmioBaseInMB**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|High MMIO region allocated above 4GB (base and size)|
|**HighMmioGapInMB**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "VirtualMachine_Processor"></a>
## VirtualMachine_Processor
Referenced by: [[Topology|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#topology]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Count**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Limit**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Weight**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Reservation**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**MaximumFrequencyMHz**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Provides the target maximum CPU frequency, in MHz, for a virtual machine.|
|**ExposeVirtualizationExtensions**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**EnablePerfmonPmu**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**EnablePerfmonArchPmu**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**EnablePerfmonPebs**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**EnablePerfmonLbr**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**EnablePerfmonIpt**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**EnablePageShattering**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Useful to enable if you want to protect against the Intel Processor Machine Check Error vulnerability (CVE-2018-12207). For instance, if you have some virtual machines that you trust that won't cause denial of service on the virtualization hosts and some that you don't trust. Additionally, disabling this may improve guest performance for some workloads. This feature does nothing on non-Intel machines or on Intel machines that are not vulnerable to CVE-2018-12207.|
|**DisableSpeculationControls**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Hides the presence of speculation controls commonly used by guest operating systems as part of side channel vulnerability mitigations. Additionally, these mitigations are often detrimental to guest operating system performance|
|**ProcessorFeatureSet**<br>|[[ProcessorFeatureSet|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processorfeatureset]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Processor features object with processor features bit fields|
|**CpuGroup**<br>|[[CpuGroup|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#cpugroup]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|An optional object that configures the CPU Group to which a Virtual Machine is going to bind to.|

---

<a name = "VirtualNodeInfo"></a>
## VirtualNodeInfo
Referenced by: [[MemoryInformationForVm|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#memoryinformationforvm]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**VirtualNodeIndex**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**PhysicalNodeNumber**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**VirtualProcessorCount**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**MemoryUsageInPages**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "VirtualPciDevice"></a>
## VirtualPciDevice
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Functions**<br>|<[[VirtualPciFunction|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualpcifunction]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "VirtualPciFunction"></a>
## VirtualPciFunction
Referenced by: [[VirtualPciDevice|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualpcidevice]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**DeviceInstancePath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**VirtualFunction**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**AllowDirectTranslatedP2P**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "VirtualPMemController"></a>
## VirtualPMemController
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Devices**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], [[VirtualPMemDevice|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualpmemdevice]]>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**MaximumCount**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|This field indicates how many empty devices to add to the controller. If non-zero, additional VirtualPMemDevice objects with no HostPath and no Mappings will be added to the Devices map to get up to the MaximumCount. These devices will be configured with either the MaximumSizeBytes field if non-zero, or with the default maximum, 512 Mb.|
|**MaximumSizeBytes**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Backing**<br>|[[VirtualPMemBackingType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualpmembackingtype]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "VirtualPMemDevice"></a>
## VirtualPMemDevice
Referenced by: [[VirtualPMemController|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualpmemcontroller]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**HostPath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ReadOnly**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ImageFormat**<br>|[[VirtualPMemImageFormat|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualpmemimageformat]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**SizeBytes**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Mappings**<br>|[[Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]<[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]], [[VirtualPMemMapping|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualpmemmapping]]>|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "VirtualPMemMapping"></a>
## VirtualPMemMapping
Referenced by: [[VirtualPMemDevice|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualpmemdevice]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**HostPath**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ImageFormat**<br>|[[VirtualPMemImageFormat|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualpmemimageformat]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "VirtualSmb"></a>
## VirtualSmb
Referenced by: [[Devices|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#devices]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Shares**<br>|<[[VirtualSmbShare|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualsmbshare]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**DirectFileMappingInMB**<br>|[[int64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "VirtualSmbShare"></a>
## VirtualSmbShare
Referenced by: [[VirtualSmb|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualsmb]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Path**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**AllowedFiles**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Options**<br>|[[VirtualSmbShareOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualsmbshareoptions]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "VirtualSmbShareOptions"></a>
## VirtualSmbShareOptions
Referenced by: [[VirtualSmbShare|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#virtualsmbshare]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ReadOnly**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ShareRead**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|convert exclusive access to shared read access|
|**CacheIo**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|all opens will use cached I/O|
|**NoOplocks**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|disable oplock support|
|**TakeBackupPrivilege**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Acquire the backup privilege when attempting to open|
|**UseShareRootIdentity**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Use the identity of the share root when opening|
|**NoDirectmap**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|disable Direct Mapping|
|**NoLocks**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|disable Byterange locks|
|**NoDirnotify**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|disable Directory CHange Notifications|
|**VmSharedMemory**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|share is use for VM shared memory|
|**RestrictFileAccess**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|allow access only to the files specified in AllowedFiles|
|**ForceLevelIIOplocks**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|disable all oplocks except Level II|
|**ReparseBaseLayer**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Allow the host to reparse this base layer|
|**PseudoOplocks**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Enable pseudo-oplocks|
|**NonCacheIo**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|All opens will use non-cached IO|
|**PseudoDirnotify**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Enable pseudo directory change notifications|
|**SingleFileMapping**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Block directory enumeration, renames, and deletes.|
|**SupportCloudFiles**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Support Cloud Files functionality|
|**FilterEncryptionAttributes**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Filter EFS attributes from the guest|

---

<a name = "VmMemory"></a>
## VmMemory
Referenced by: [[MemoryInformationForVm|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#memoryinformationforvm]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**AvailableMemory**<br>|[[int32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**AvailableMemoryBuffer**<br>|[[int32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**ReservedMemory**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**AssignedMemory**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**SlpActive**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**BalancingEnabled**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**DmOperationInProgress**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "VmProcessorRequirements"></a>
## VmProcessorRequirements
VM specific processor feature requirements

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ProcessorFeatures**<br>|<[[ProcessorFeature|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#processorfeature]]> array|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Processor features|
|**XsaveProcessorFeatures**<br>|<[[XsaveProcessorFeature|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#xsaveprocessorfeature]]> array|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Xsave processor features|
|**CacheLineFlushSize**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Processor cache line flush size|
|**ImplementedPhysicalAddressBits**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Processor physical address bits|
|**NonArchitecturalCoreSharing**<br>|[[NonArchitecturalCoreSharing|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#nonarchitecturalcoresharing]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Processor non architectural core sharing|

---

<a name = "WindowsCrashReport"></a>
## WindowsCrashReport
Referenced by: [[CrashReport|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#crashreport]]

Windows specific crash information

|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**DumpFile**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Path to a Windows memory dump file. This will contain the same path as the configured in the GuestCrashReporting device. This field is not valid unless the FinalPhase is Complete.|
|**OsMajorVersion**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Major version as reported by the guest OS.|
|**OsMinorVersion**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Minor version as reported by the guest OS.|
|**OsBuildNumber**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Build number as reported by the guest OS.|
|**OsServicePackMajorVersion**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Service pack major version as reported by the guest OS.|
|**OsServicePackMinorVersion**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Service pack minor version as reported by the guest OS.|
|**OsSuiteMask**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Suite mask as reported by the guest OS.|
|**OsProductType**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Product type as reported by the guest OS.|
|**Status**<br>|[[int32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Status of the crash dump. S_OK indicates success, other HRESULT values on error.|
|**FinalPhase**<br>|[[WindowsCrashPhase|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#windowscrashphase]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Indicates progress of a Windows memory dump when the crash report was sent.|

---

<a name = "WindowsCrashReporting"></a>
## WindowsCrashReporting
Referenced by: [[GuestCrashReporting|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#guestcrashreporting]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**DumpFileName**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**MaxDumpSize**<br>|[[int64|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "WorkerExit"></a>
## WorkerExit
Referenced by: [[AttributionRecord|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#attributionrecord]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ExitCode**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]|Exit code of the virtual machine worker process.|
|**Type**<br>|[[WorkerExitType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#workerexittype]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Detail**<br>|[[WorkerExitDetail|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#workerexitdetail]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||
|**Initiator**<br>|[[ExitInitiator|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#exitinitiator]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.schemareference#schema-version-map]]||

---

<a name = "JSON-type"></a>
## JSON type

The table shows the mapping from type name for field of classes to JSON type, its format and pattern. See details in [Swagger IO spec](https://swagger.io/specification/#data-types)

|Name|JSON Type|Format|Pattern|
|---|---|---|---|
|Any|object|||
|bool|boolean|||
|ByteArray|string|byte||
|DateTime|string|date-time||
|Guid|string||^[0-9A-Fa-f]{8}-([0-9A-Fa-f]{4}-){3}[0-9A-Fa-f]{12}$|
|int32|integer|int32||
|int64|integer|int64||
|MacAddress|string|mac-address||
|Map|object|||
|NullableByteArray|string|binary||
|string|string|||
|string_binary|string|binary||
|uint16|integer|uint16||
|uint32|integer|uint32||
|uint64|integer|uint64||
|uint8|integer|uint8||

---
<a name = "Schema-Version-Map"></a>
## Schema Version Map

|Schema Version|Release Version|
|---|---|
|2.0|Windows 10 SDK, version 1809 (10.0.17763.0)|
|2.1|Windows 10 SDK, version 1809 (10.0.17763.0)|
|2.2|Windows 10 SDK, version 1903 (10.0.18362.1)|
|2.3|Windows 10 SDK, version 2004 (10.0.19041.0)|
|2.4|Windows Server 2022 (OS build 20348.169)|
|2.5|Windows Server 2022 (OS build 20348.169)|
|2.6|Windows 11 SDK, version 21H2 (10.0.22000.194)|

---