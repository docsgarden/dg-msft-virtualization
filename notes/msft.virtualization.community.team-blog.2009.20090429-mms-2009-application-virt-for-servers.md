---
id: 5UWHXKXSmaV6Qk3NRFCH4sz
title: >-
    MMS 2009&#58; Application Virt for Servers
desc: >-
    At the Microsoft Management Summit, we did a demo of application virtualization for servers.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090429-mms-2009-application-virt-for-servers
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 140ad4a1a45834d0d616014cac4660aec4b5ce9c8d4fe364b903b9cb2c9730e9
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090429-mms-2009-application-virt-for-servers.md
    author: "scooley"
    ms.author: "scooley"
    ms.date: "04/29/2009"
    date: "2009-04-29 18:50:00"
    categories: "application-virtualization"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090429-mms-2009-application-virt-for-servers.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090429-mms-2009-application-virt-for-servers).

# MMS 2009 - Application Virt for Servers

I'm a bit behind is sharing the following demo video. On Tuesday at Microsoft Management Summit, we did a demo of application virtualization for servers. Both [Mary Jo](http://blogs.zdnet.com/microsoft/?p=2506 "Mary Jo Foley blog post") and [Alessandro](http://www.virtualization.info/2009/04/microsoft-confirms-app-v-for-servers-in.html "Alessandro's blog post") predicted this demo a couple weeks ago - so not a real surprise. The demo was described as a technology preview, meaning there's no plan of record for beta testing date. So what did Bill Morein demo?

As you'll see, Bill's demo was setup by talk about administering the compute fabric and managing deployment of a tightly bound OS can create challenges. By applying application virtualization to this challenge, we can separate apps from the server. This should mean a more simplified, and accelerated, approach to deploying those virtualized apps and patching. Bill pointed out that this leads to reduce image libraries, and accelerated migration. 

Check out the demo - it's less than 6 minutes.

 

Patrick