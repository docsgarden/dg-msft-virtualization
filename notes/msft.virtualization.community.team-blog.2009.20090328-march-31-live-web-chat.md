---
id: 7CcgrS7B2s7uEKEzqAZFUzn
title: >-
    March 31 live web chat
desc: >-
    On Tuesday, March 31, Microsoft's Edwin Yuen will be hosting a live web chat 11am-3pm EST.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090328-march-31-live-web-chat
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 8ea34f2c91b07ea9febc2191c8e48f2fe0738b65d32b2d4702f18fa55236c290
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090328-march-31-live-web-chat.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-03-28 12:24:00"
    ms.date: "3/28/2009"
    categories: "application-virtualization"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090328-march-31-live-web-chat.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090328-march-31-live-web-chat).

# March 31 live web chat

On Tuesday, March 31, Microsoft's Edwin Yuen will be hosting a live web chat 11am-3pm EST. Edwin is a sr. technical product manager. Edwin came to Microsoft with the acquisition of Softricity (and the SoftGrid application virtualization technology). He now also covers Hyper-V and System Center VMM. Sign up [here](http://itknowledgeexchange.techtarget.com/microsoft-virtualization-chat/ "TechTaret sign up").  <!--- ![Leigh Seifert virtual knowledge exchange](http://www.leighseifert.com/Virtual_KnowledgeExchang-06.jpg) --->

Patrick