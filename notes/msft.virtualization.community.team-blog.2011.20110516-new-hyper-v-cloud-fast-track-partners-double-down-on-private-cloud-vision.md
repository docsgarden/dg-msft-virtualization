---
id: 9cMnzFF3fMwiaTJsyqUL6ah
title: >-
    New Hyper-V Cloud Fast Track Partners Double Down on Private Cloud Vision
desc: >-
    New Hyper-V Cloud Fast Track Partners Double Down on Private Cloud Vision
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110516-new-hyper-v-cloud-fast-track-partners-double-down-on-private-cloud-vision
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 12a12841e899b2f04fb6aef1b9f0e31f4e153f4c806b6a6ed3d810afe3f3eeb6
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110516-new-hyper-v-cloud-fast-track-partners-double-down-on-private-cloud-vision.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "05/16/2011"
    date: "2011-05-16 06:18:22"
    categories: "system-center"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110516-new-hyper-v-cloud-fast-track-partners-double-down-on-private-cloud-vision.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110516-new-hyper-v-cloud-fast-track-partners-double-down-on-private-cloud-vision).

# New Hyper-V Cloud Fast Track Partners Double Down on Private Cloud Vision

Tech Ed North America 2011 <http://bit.ly/mdfwpA> kicked into high gear today with news that Cisco and NetApp are participating in Microsoft’s Hyper-V Cloud Fast Track program.   Together, NetApp and Cisco have developed a pre-validated private cloud solution for customers who want to deploy with less risk, more flexibility and choice, and more quickly.  Their solution is interesting because it offers the flexibility of the cloud with higher levels of control needed to meet security and regulatory guidelines.  It uses Windows Server 2008 R2 Hyper-V and Microsoft Systems Center to ease management, enable automation and self-service provisioning of the entire private cloud infrastructure, including automated self-service provisioning via native Opalis Integration Packs.  It is scalable vertically or horizontally, paving the way for greater efficiency, agility and cost savings.  Check out the insider insights on this TechNet Edge video from NetApp and Cisco engineers in a TechNet Edge video here: <http://bit.ly/jWAplo>.   NetApp and Cisco join six of our largest OEM partners who are betting on Microsoft’s private cloud strategy, and our commitment to meet customers where they are and help them plan for the future.  Together with our Hyper-V Cloud Fast Track partners, our customers will have the ability to leverage the best of what they have and then move to other options when the time is right, on their terms. Customers get the benefit of proven private cloud solutions now available from eight vendors who represent more than 80% share of the world’s server market.   Customers are on a path toward virtualization and cloud, driving increasing interest in Hyper-V.  There is growing recognition that [**management**](https://blogs.technet.com/b/virtualization/archive/2011/04/13/windows-server-hyper-v-and-system-center-raise-the-stakes-in-the-virtualization-race.aspx) is key.  Our solutions include enterprise-class virtualization and comprehensive management technologies to deliver private cloud services, and powerful application insights to ensure maximum performance and availability across private and public cloud assets.  The Windows Hyper-V Cloud Fast Track program ensures customers have a broad choice of predefined, validated configurations for private cloud deployments comprising compute, storage, networking resources, virtualization and management software. 

Congratulations to NetApp and Cisco on their decision to Fast Track with us to the cloud!