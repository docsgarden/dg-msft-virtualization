---
id: VKaXebnSPvYhaqqGYT98Rsb
title: >-
    Windows Hypervisor Platform Instruction Emulator API Definitions and Support DLLs
desc: >-
    Learn about the Windows Hypervisor Platform Instruction Emulator API Definitions and Support DLLs.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/hypervisor-instruction-emulator
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: eb0db87dae744384b39e62d123709eb09e2f4627777d54d3822e0f0feebffc12
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/hypervisor-instruction-emulator.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/19/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/hypervisor-instruction-emulator.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/hypervisor-instruction-emulator).

# Windows Hypervisor Platform Instruction Emulator API Definitions and Support DLLs
>**This API is available starting in the Windows April 2018 Update.**

Instruction emulation expects several higher-level abstractions at the interface between the accelerators and the device emulation that are not provided directly by the platform APIs. This functionality is provided by a separate DLL that builds these abstractions on top of the platform APIs.
 
## Instruction Emulation
Device emulation expects the platform to provide the details of an I/O access by a virtual processor. For an MMIO and string I/O port access this requires decoding and completing the instruction that issued the I/O access. 


|Structure   |Description|
|---|---|---|---|---|---|---|---|
|[[MMIO Access|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.mmioaccessie]]|Instruction emulation expects this data for MMIO access|
|[[I/O Port Access|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.ioportaccessie]]|Instruction emulation expects this data for port access|
|   |   |
 
 
## Virtual Processor Register State
Instruction emulation uses a fixed set of registers that are queried and set together if necessary. For X64, this set of registers include:
 
* RAX...R15, RIP, RFLAGS 
* CS, SS, DS, ES, FS, GS, LDT, TR, GDT, IDT 
* CR0, CR2, CR3, CR4 
* FPU: 
    * XMM0...15, FPMMX0...7 
    * FCW, FSW, FTW, FOP, FIP, FDP, MXCSR 
* MSRs: 
    * SYSENTER_CS, SYSENTER_ESP, SYSENTER_EIP, TSC 
    * EFER, STAR, LSTAR, CSTAR, FMASK, KERNELGSBASE 

This set of registers is provided using the [`WHvGetVirtualProcessorRegisters`](https://docs.microsoft.com/en-us/virtualization/api/hypervisor-platform/funcs/WHvGetVirtualProcessorRegisters) and [`WHvSetVirtualProcessorRegisters`](https://docs.microsoft.com/en-us/virtualization/api/hypervisor-platform/funcs/WHvSetVirtualProcessorRegisters) platform functions, keeping it in sync with the register state used by the instruction emulation.  

## Windows Hypervisor Platform Instruction Emulator API Reference

### Emulator Structures
|Structure   |Description|
|---|---|---|---|---|---|---|---|
|[[`WHV_EMULATOR_CALLBACKS`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.whvemulatorcallbacks]]|Used in [[`WHvEmulatorCreateEmulator`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.whvemulatorcreateemulator]] to specify callback methods needed by the emulator.|
|[[`WHV_EMULATOR_STATUS`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.whvemulatorstatus]]|Describes extended return status information from a given emulation call.|
|[[`WHV_EMULATOR_MEMORY_ACCESS_INFO`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.whvemulatormemoryaccessinfo]]|Information about the requested memory access by the emulator |
|[[`WHV_EMULATOR_IO_ACCESS_INFO`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.whvemulatorioaccessinfo]]|Information about the requested Io Port access by the emulator|
|   |   |


## API Methods
|Methods   |Description|
|---|---|---|---|---|---|---|---|
|[[WHvEmulatorCreateEmulator|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.whvemulatorcreateemulator]]|Create an instance of the instruction emulator with the specified callback methods|
|[[WHvEmulatorDestoryEmulator|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.whvemulatordestoryemulator]]|Destroy an instance of the instruction emulator created by [[`WHvEmulatorCreateEmulator`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.whvemulatorcreateemulator]]|
|[[WHvEmulatorTryIoEmulation and WHvEmulatorTryMmioEmulation|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.whvemulatortryemulation]]|Attempt to emulate a given type of instruction with the given instruction context returned by the WinHv APIs from a `RunVirtualProcessor` call. |
|   |   |




## Callback Functions
|Functions   |Description|
|---|---|---|---|---|---|---|---|
|[[`WHV_EMULATOR_IO_PORT_CALLBACK`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.whvemulatorioportcallback]]|Callback notifying the virtualization stack that the current instruction has modified the IO Port specified in the IoAccess structure|
|[[`WHV_EMULATOR_MEMORY_CALLBACK`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.whvemulatormemorycallback]]|Callback notifying the virtualization stack that the current instruction is attempting to accessing memory as specified in the MemoryAccess structure.|
|[[`WHV_EMULATOR_GET_VIRTUAL_PROCESSOR_REGISTERS_CALLBACK`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.whvemulatorgetvirtualprocessorregisterscallback]]|Callback requesting VP register state, similar to the WinHv API|
|[[`WHV_EMULATOR_SET_VIRTUAL_PROCESSOR_REGISTERS_CALLBACK`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.whvemulatorsetvirtualprocessorregisterscallback]]|Callback setting VP register state, similar to the WinHv API|
|[[`WHV_EMULATOR_TRANSLATE_GVA_PAGE_CALLBACK`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-instruction-emulator.funcs.whvemulatortranslategvapagecallback]]|Callback requesting the virtualization stack to translate the Guest Virtual Address GvaPage that points to the start of a 4K page, with the specified TranslateFlags|
|   |   |