---
id: 7t5TUSX5BeThUW49CevFQfn
title: >-
    WHV_EMULATOR_MEMORY_CALLBACK method
desc: >-
    Learn about the WHV_EMULATOR_MEMORY_CALLBACK method.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorMemoryCallback
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: b362904143f4fac1a86b2eace7c35712250f308c779de403662cf9a02a73aba8
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorMemoryCallback.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/19/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorMemoryCallback.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorMemoryCallback).

# WHV_EMULATOR_MEMORY_CALLBACK


## Syntax

```c
typedef HRESULT (CALLBACK *WHV_EMULATOR_MEMORY_CALLBACK)(
    _In_ VOID* Context,
    _Inout_ WHV_EMULATOR_MEMORY_ACCESS_INFO* MemoryAccess
    );
```

## Remarks
Callback notifying the virtualization stack that the current instruction is attempting to accessing memory as specified in the `MemoryAccess` structure.

**NOTE:** As mentioned above, since in x86/AMD64 it is legal to do unaligned memory accesses, a memory access spanning a page boundary ie:

`movq [0xffe], rax`

This would cause two different memory callbacks to be invoked, one for two bytes, and one for 6 bytes.