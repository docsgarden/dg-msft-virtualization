---
id: 6ZNUALH769ZRHac9i6vLA7F
title: >-
    Host Compute System API Reference
desc: >-
    Host Compute System API Reference
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/APIOverview
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 87bf123d06d04fe302f88f25fb961218fca0fa01ae46688da0c6393ddf514702
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/APIOverview.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['Host Compute System API Reference']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/APIOverview.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/APIOverview).

# Host Compute System API Reference

The following section contains the definitions of the Host Compute System APIs. The DLL exports a set of C-style Windows API functions, using JSON schema as configuration.


## Data types

|Type|Description|
|---|---|
|[[HCS ERROR CODE|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]]|Common HCS Error Code|
|[[HCS_EVENT_TYPE|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_event_type]]|Events indicated to callbacks registered by [[HcsSetComputeSystemCallback|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssetcomputesystemcallback]] or [[HcsSetProcessCallback|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssetprocesscallback]]|
|[[HCS_EVENT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_event]]|Information about an event that ocurred on a compute system or process.|
|[[HCS_EVENT_OPTIONS|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_event_options]]|Options for an event callback registration|
|[[HCS_OPERATION_TYPE|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_operation_type]]|Operation type assigned to a valid hcs operation|
|[[HCS_PROCESS_INFORMATION|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_process_information]]|Information about a process created by [[HcsCreateProcess|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscreateprocess]]|

## Handle types

|Type|Description|
|---|---|
|HCS_SYSTEM|Handle to a compute system|
|HCS_PROCESS|Handle to a process running in a compute system|
|HCS_OPERATION|Handle to an operation on a compute system|
|HCS_CALLBACK|Handle to a callback registered on a compute system or process|

## Callback function types

|Type|Description|
|---|---|
|[[HCS_OPERATION_COMPLETION|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_operation_completion]]|The completion callback of an hcs operation|
|[[HCS_EVENT_CALLBACK|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_event_callback]]|The compute system events callback|

## Operations

|Function|Description|
|---|---|
|[[HcsCreateOperation|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscreateoperation]]|Create a new operation|
|[[HcsCloseOperation|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscloseoperation]]|Close an operation|
|[[HcsGetOperationContext|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationcontext]]|Get the context pointer of an operation|
|[[HcsSetOperationContext|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssetoperationcontext]]|Set the context pointer on an operation|
|[[HcsGetComputeSystemFromOperation|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetcomputesystemfromoperation]]|Get the compute system handle associated with operation|
|[[HcsGetProcessFromOperation|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetprocessfromoperation]]|Return the handle to the process associated with an operation|
|[[HcsGetOperationType|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationtype]]|Get the type of the operation|
|[[HcsGetOperationId|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationid]]|Get the id of the operation|
|[[HcsGetOperationResult|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]]|Get the result of the operation|
|[[HcsGetOperationResultAndProcessInfo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresultandprocessinfo]]|Return the result of an operation|
|[[HcsWaitForOperationResult|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]]| Wait for the completion of the create operation|
|[[HcsWaitForOperationResultAndProcessInfo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresultandprocessinfo]]|Wait for the completion of the create operation and returns both the result and process info|
|[[HcsSetOperationCallback|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssetoperationcallback]]|Set the callback that is invoked on completion of an operation|
|[[HcsCancelOperation|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscanceloperation]]|Cancel the operation|

## Compute System Operations

|Function|Description|
|---|---|
|[[HcsCreateComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscreatecomputesystem]]|Create a new compute system|
|[[HcsOpenComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsopencomputesystem]]|Open a handle to an existing compute system|
|[[HcsCloseComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsclosecomputesystem]]|Close a handle to a compute system|
|[[HcsStartComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsstartcomputesystem]]|Start a compute system|
|[[HcsShutDownComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsshutdowncomputesystem]]|Cleanly Shut down a compute system|
|[[HcsTerminateComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsterminatecomputesystem]]|Forcefully terminate a compute system|
|[[HcsCrashComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscrashcomputesystem]]|Forcefully terminate a compute system|
|[[HcsPauseComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcspausecomputesystem]]|Pause the execution of a compute system|
|[[HcsResumeComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsresumecomputesystem]]|Resume the execution of a compute system|
|[[HcsSaveComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssavecomputesystem]]|Save the state of a compute system|
|[[HcsGetComputeSystemProperties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetcomputesystemproperties]]|Query properties of a compute system|
|[[HcsModifyComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsmodifycomputesystem]]|Modify setting of a compute system|
|[[HcsSetComputeSystemCallback|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssetcomputesystemcallback]]|Register a callback function to receive notifications for the compute system|
|[[HcsEnumerateComputeSystems|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsenumeratecomputesystems]]|Enumerate existing compute systems|
|[[HcsWaitForComputeSystemExit|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforcomputesystemexit]]|Wait compute system to exit|


## Process Execution

The following functions enable applications to execute a process in a compute system. For containers, these functions are the main way for an application to start and interact with the workload running in the container.

|Function|Description|
|---|---|
|[[HcsCreateProcess|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscreateprocess]]|Start a process in a compute system |
|[[HcsOpenProcess|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsopenprocess]]|Open an existing process in a compute system |
|[[HcsCloseProcess|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscloseprocess]]|Close the handle to a  process in a compute system |
|[[HcsTerminateProcess|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsterminateprocess]]|Terminate a process in a compute system |
|[[HcsSignalProcess|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssignalprocess]]|Send a signal to a process in a compute system |
|[[HcsGetProcessInfo|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetprocessinfo]]|Return the initial startup info of a process in a compute system |
|[[HcsGetProcessProperties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetprocessproperties]]|Return properties a process in a compute system |
|[[HcsModifyProcess|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsmodifyprocess]]|Modify the parameters in a process of a compute system |
|[[HcsSetProcessCallback|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssetprocesscallback]]|Register a callback function to receive notifications for a process in a compute system |
|[[HcsWaitForProcessExit|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforprocessexit]]|Wait a process in a compute system to exit|

## Host Service Operations

The following functions provide functionality for host compute service.

|Function|Description|
|---|---|
|[[HcsGetServiceProperties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetserviceproperties]]|Return the properties of the Host Compute Service|
|[[HcsModifyServiceSettings|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsmodifyservicesettings]]|Modify the settings of the Host Compute Service|
|[[HcsSubmitWerReport|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssubmitwerreport]]|Submit a WER report|

## Utility Functions for Virtual Machines

The following set of functions allow applications to set up the environment to run virtual machines.

|Function|Description|
|---|---|
|[[HcsCreateEmptyGuestStateFile|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscreateemptygueststatefile]]|Create an empty guest-state file (.vmgs) for VMs|
|[[HcsCreateEmptyRuntimeStateFile|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscreateemptyruntimestatefile]]|Create an empty runtime-state file (.vmrs) for a VM|
|[[HcsGrantVmAccess|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgrantvmaccess]]|Add an entry to a file's ACL that grants access for a VM|
|[[HcsRevokeVmAccess|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsrevokevmaccess]]|Remove an entry to a file's ACL that granted access for a VM|
|[[HcsGrantVmGroupAccess|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgrantvmgroupaccess]]|Grant VM group access (R/O) to the specified file.|
|[[HcsRevokeVmGroupAccess|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsrevokevmgroupaccess]]|Remove VM group access for the specified file.|
|[[HcsGetProcessorCompatibilityFromSavedState|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetprocessorcompatibilityfromsavedstate]]|Get the processor compatibility from runtime-state file (.vmrs)|

## Storage Utility Functions for Containers

The following functions allow applications to create and manage the file system and storage environment that is required to run containers.

|Function|Description|
|---|---|
|[[HcsImportLayer|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsimportlayer]]|Import a container layer and configures it for use on the host|
|[[HcsExportLayer|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsexportlayer]]|Export a container layer that can be copied to another host or uploaded to a container registry|
|[[HcsExportLegacyWritableLayer|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsexportlegacywritablelayer]]|Export a legacy container writable layer|
|[[HcsDestroyLayer|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsdestroylayer]]|Delete a container layer from the host|
|[[HcsSetupBaseOSLayer|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssetupbaseoslayer]]|Set up a layer that contains a base OS for a container|
|[[HcsSetupBaseOSVolume|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssetupbaseosvolume]]|Set up the base OS layer based on the mounted volume|
|[[HcsInitializeWritableLayer|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsinitializewritablelayer]]|Initialize the writable layer for a container (i.e. the layer that captures the filesystem)|
|[[HcsInitializeLegacyWritableLayer|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsinitializelegacywritablelayer]]|Initialize the writable layer for a container using the legacy hive folder format|
|[[HcsAttachLayerStorageFilter|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsattachlayerstoragefilter]]|Set up the layer storage filter on a writable container layers|
|[[HcsDetachLayerStorageFilter|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsdetachlayerstoragefilter]]|Detach the layer storage filter from a writable container layer|
|[[HcsFormatWritableLayerVhd|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsformatwritablelayervhd]]|Format a virtual disk for the use as a writable container layer|
|[[HcsGetLayerVhdMountPath|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetlayervhdmountpath]]| Return the volume path for a virtual disk of a writable container layer|