---
id: A8P22Whn3ZRfmumfx66XTBJ
title: >-
    Simplify your customers’ cloud migration planning with MAP 6.0
desc: >-
    Simplify your customers’ cloud migration planning with MAP 6.0
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110715-simplify-your-customers-cloud-migration-planning-with-map-6-0
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 4bd387231f0aee9a28b2c6b289443790782cd1e34b66b4f9874c7bc2bf474afc
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110715-simplify-your-customers-cloud-migration-planning-with-map-6-0.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "07/15/2011"
    date: "2011-07-15 12:28:02"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110715-simplify-your-customers-cloud-migration-planning-with-map-6-0.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110715-simplify-your-customers-cloud-migration-planning-with-map-6-0).

# Simplify your customers’ cloud migration planning with MAP 6.0

The next version of the Microsoft Assessment and Planning (MAP) Toolkit—version 6.0— is now available for [free download](http://bit.ly/o7nXDX).Planning a customer journey to the cloud just got easier. The Microsoft Assessment and Planning (MAP) Toolkit 6.0 includes assessment capabilities to evaluate workloads for both public and private cloud platforms. With MAP 6.0, you now have the ability to identify customers’ workloads and estimate the infrastructure size and resources needed for both Windows Azure and Hyper-V Fast Track. Also new to MAP 6.0 is an Office 365 client assessment, enhanced VMware inventory, and Oracle Schema discovery and reporting. Expanded assessment and discovery capabilities from MAP help you simplify planning for your next migration project. Plan what's next with MAP.   **New Features and Benefits from MAP 6.0 help you:**

  *  Accelerate private cloud planning with Hyper-V Cloud Fast Track Onboarding.
  * Analyze customer’s portfolio of applications for a migration to the Windows Azure Platform.
  * Identify migration opportunities with enhanced heterogeneous server environment inventory.
  * Assess your client environment for Office 365 readiness.
  * Determine readiness for migration to Windows Internet Explorer 9.
  * Identify web browser compatibility issues prior to Windows 7 deployment.
  * Discover Oracle database schemas for migration to SQL Server.

MAP works with the Microsoft Deployment Toolkit and Security Compliance Manager to help you plan, securely deploy, and manage new Microsoft technologies—easier, faster, and at less cost. [Learn more.](http://bit.ly/nIms2t) **Next steps:**

  * [Download MAP 6.0](http://bit.ly/o7nXDX)
  * [Learn more](http://bit.ly/nIms2t) about the MAP Toolkit
  * Send your questions or comments to the [MAP Team](https://blogs.technet.commailto:MAPfdbk@microsoft.com).



Get the latest tips from Microsoft Solution Accelerators—in 140 characters or less! Follow us on Twitter: [@MSSolutionAccel](http://www.twitter.com/mssolutionaccel). And here’s how to stay up-to-date on all the latest Microsoft Server and Cloud Platform news during and after WPC:

  * Read our new Server and Cloud Platform blog: <https://blogs.technet.com/b/server-cloud/>
  * “Like” us on our new Facebook page: <http://www.facebook.com/Server.Cloud>
  * Follow us on Twitter: [@MSServerCloud](https://twitter.com/#!/MSServerCloud)



Thank you. Microsoft Server & Cloud Platform Team