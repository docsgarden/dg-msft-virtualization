---
id: 6p2SFQ6XYieq7fzUoSLvg7R
title: >-
    Promotion of Brad Anderson to Corporate Vice President, Management and Services Division
desc: >-
    Brad Anderson one of the key drivers behind Microsoft’s management offerings, including the System Center suite and products such as VMM, has been promoted to corporate vice president.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090728-promotion-of-brad-anderson-to-corporate-vice-president-management-and-services-division
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: ec033a265c6ffb50a0c8d463de30a0b3842b7e1b1a71bf084fbde583a656b6bb
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090728-promotion-of-brad-anderson-to-corporate-vice-president-management-and-services-division.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2009-07-28 16:16:00"
    ms.date: "07/28/2009"
    categories: "management"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090728-promotion-of-brad-anderson-to-corporate-vice-president-management-and-services-division.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090728-promotion-of-brad-anderson-to-corporate-vice-president-management-and-services-division).

# Promotion of Brad Anderson to Corporate Vice President
It comes as no surprise to the regular readers of this blog, that a key tenant of Microsoft’s virtualization strategy is that customers should be able to manage their physical and virtual IT resources from the same set of easy to use tools.  It seems that customers agree, as demonstrated through the growth of the Microsoft Management and Services Division (30% growth year over year) offerings during some challenging economic times.

 

[Today we’ve announced that Brad Anderson](https://blogs.technet.com/systemcenter/archive/2009/07/28/brad-anderson-promoted-to-corporate-vice-president-of-management-and-services-division.aspx "Brad Anderson promotion to CVP"), one of the key drivers behind Microsoft’s management offerings, including the System Center suite and products such as VMM, has been promoted to corporate vice president.  We wanted to share the news with those of you that have worked with or met him over the years through events such as the annual Microsoft Management Summit conference.  Congratulations Brad!