---
id: 6grKuJTnPnQWrx64AZ8Uxh9
title: >-
    Delete a virtual processor in a partition
desc: >-
    Learn about the WHvDeleteVirtualProcessor function that deletes a virtual processor in a partition.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvDeleteVirtualProcessor
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 5f947cf2d018bf6b404cbb8ff1a5083ee11427658fb23a1f43b02613c62cf1f9
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvDeleteVirtualProcessor.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/20/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvDeleteVirtualProcessor.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvDeleteVirtualProcessor).

# WHvDeleteVirtualProcessor

## Syntax

```C
HRESULT
WINAPI
WHvDeleteVirtualProcessor(
    _In_ WHV_PARTITION_HANDLE Partition,
    _In_ UINT32 VpIndex
    );
```

### Parameters

`Partition`

Handle to the partition object

`VpIndex`

 Specifies the index of the virtual processor that is deleted
  

## Remarks

The `WHvDeleteVirtualProcessor` function deletes a virtual processor in a partition.