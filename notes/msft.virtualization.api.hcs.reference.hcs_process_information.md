---
id: 5f3UWZNjPQJHnSPxDh8Ptuc
title: >-
    HCS_PROCESS_INFORMATION
desc: >-
    HCS_PROCESS_INFORMATION
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HCS_PROCESS_INFORMATION
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 03636a806b391d7da541a0c86189b420dcc35ea8cb48b7de4b735f44ece7321f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HCS_PROCESS_INFORMATION.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HCS_PROCESS_INFORMATION']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HCS_PROCESS_INFORMATION.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HCS_PROCESS_INFORMATION).

# HCS_PROCESS_INFORMATION

## Description

The struct contains information about a process created by [[HcsCreateProcess|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscreateprocess]]. This structure can be obtained with [[`HcsGetOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresultandprocessinfo]] and [[`HcsWaitForOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresultandprocessinfo]].

## Syntax

```cpp
typedef struct
{
    DWORD ProcessId;
    DWORD Reserved;
    HANDLE StdInput;
    HANDLE StdOutput;
    HANDLE StdError;
} HCS_PROCESS_INFORMATION;
```

## Members

`ProcessId`

Identifier of the created process.

`Reserved`

`StdInput`

If created, standard input handle of the process.

`StdOutput`

If created, standard output handle of the process.

`StdError`

If created, standard error handle of the process.

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeDefs.h |