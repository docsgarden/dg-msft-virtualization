---
id: 8xtW7gogEyLevHSh2LZvspW
title: >-
    Happy holidays! windows server 2008 RC1 with hyper-v beta now available
desc: >-
    post id 3843
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2007/20071213-happy-holidays-windows-server-2008-rc1-with-hyper-v-beta-now-available
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: cff1522e2bf9b90ff72fd435173c97b602fa3ae4e4b7eb8b7979242c83e819fd
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2007/20071213-happy-holidays-windows-server-2008-rc1-with-hyper-v-beta-now-available.md
    keywords: "virtualization, blog"
    author: "scooley"
    ms.author: "scooley"
    ms.date: "12/13/2007"
    ms.topic: "article"
    ms.prod: "virtualization"
    ms.assetid: "None"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2007/20071213-happy-holidays-windows-server-2008-rc1-with-hyper-v-beta-now-available.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2007/20071213-happy-holidays-windows-server-2008-rc1-with-hyper-v-beta-now-available).

# Happy holidays! Windows Server 2008 RC1 with hyper-v beta now available

Greetings!  Jeff Woolsey here from the virtualization team.

We have some big news today! We just released the Windows Hyper-V BETA! The Hyper-V beta is a _major_ step forward from the Preview release we provided just a couple of months ago.

This beta is publicly available and you can download it [here](https://www.microsoft.com/downloads/details.aspx?FamilyId=8F22F69E-D1AF-49F0-8236-2B742B354919&displaylang=en).

Some of the new features added since the Windows Hyper-V CTP include:

1. Quick Migration and high availability providing solutions for planned and unplanned downtime
2. Support for running Hyper-V with Server Core in the parent partition
3. Import/Export of virtual machines
4. Hyper-V now integrated with Windows Server Manager
5. Integration components are now included in Windows Server 2008 meaning that when you install Windows Server 2008 in a Hyper-V virtual machine, Windows will _automatically_ install the ICs
6. VHD Tools support (compaction, expansion and inspection)
7. Emulated video card has changed from an S3 Trio video card to a more generic VESA compatible device. (This change resolves numerous video issues with non-Windows operating systems like Linux)
8. Support for up to four virtual SCSI controllers per virtual machine
9. Numerous fixes for compatibility, performance and scalability
10. ...and lots more that I'll cover in upcoming blogs

In short, you want to start evaluating Windows Server 2008 Hyper-V today!

The last thing I want point out are a couple of Hyper-V videos that demonstrate just some of Hyper-V's built-in SMP, networking and storage capabilities. Cheers!

Windows Hyper-V SMP Virtual Machines
![Windows Hyper-V SMP Virtual Machines](https://docs.microsoft.com/virtualization/community/team-blog/2007/media/video3c43c74a9a4f.jpg)](http://video.msn.com/video.aspx?vid=26086837-dd73-444b-9466-65a1ed759544&ifs=true&fr=msnvideo&mkt=en-US&brand=&from=writer)

Windows Hyper-V Virtual Storage
![Windows Hyper-V Virtual Storage](https://docs.microsoft.com/virtualization/community/team-blog/2007/media/video3672b1e85070.jpg)](http://video.msn.com/video.aspx?vid=e1eb9aeb-9cb4-413e-982f-283667232590&ifs=true&fr=msnvideo&mkt=en-US&brand=&from=writer)

Windows Hyper-V Networking
![Windows Hyper-V Networking](https://docs.microsoft.com/virtualization/community/team-blog/2007/media/video5ebd0be1f03f.jpg)](http://video.msn.com/video.aspx?vid=5f2b04d7-c501-4c28-8046-dece495cb5c9&ifs=true&fr=msnvideo&mkt=en-US&brand=&from=writer)

[Original post](https://blogs.technet.microsoft.com/virtualization/2007/12/13/happy-holidays-windows-server-2008-rc1-with-hyper-v-beta-now-available/)