---
id: 7nnimEvTekAosoFWbKzRqzn
title: >-
    Storage and Continuous Availability Enhancements in Windows Server 8
desc: >-
    Storage and Continuous Availability Enhancements in Windows Server 8
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110920-storage-and-continuous-availability-enhancements-in-windows-server-8
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 8aebdd66d49e00af684ab9cedea6dd1264852aaf87251f78ddff8e1193cbeb3d
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110920-storage-and-continuous-availability-enhancements-in-windows-server-8.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "09/20/2011"
    date: "2011-09-20 10:20:00"
    categories: "availability"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110920-storage-and-continuous-availability-enhancements-in-windows-server-8.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110920-storage-and-continuous-availability-enhancements-in-windows-server-8).

# Storage and Continuous Availability Enhancements in Windows Server 8

Check out Thomas Pfenning's blog post, General Manager Server and Tools, where he discusses the storage and availability enhancements in the next release of Windows Server codenamed "Windows Server 8". 

Read Thomas' post on the [Microsoft Server and Cloud Platform blog](http://bit.ly/oIcKJP "Microsoft Server and Cloud Platform blog").