---
id: AduAncX3zHNg7tdkGhMRX7L
title: >-
    The Virtualization Essentials from Citrix
desc: >-
    Bonjour from Cannes - this is just a quick post about Citrix making public the Essentials for Hyper-V news.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090220-the-virtualization-essentials-from-citrix
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 1b3b5d6838af6fff22c619827c0d9fbc23ad2626ff919b323b40ace832efedd0
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090220-the-virtualization-essentials-from-citrix.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-02-20 00:10:00"
    ms.date: "02/20/2009"
    categories: "application-virtualization"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090220-the-virtualization-essentials-from-citrix.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090220-the-virtualization-essentials-from-citrix).

# The Virtualization Essentials from Citrix

Bonjour from <?xml:namespace prefix = st1 ns = "urn:schemas-microsoft-com:office:smarttags" />Cannes. This is just a quick post about Citrix making public the [Essentials for Hyper-V new](http://www.citrix.com/English/NE/news/news.asp?newsID=1687128 "Citrix news release")[s](http://www.citrix.com/English/NE/news/news.asp?newsID=1687128 "Citrix news release"). It's been talked about already by [Alessandro ](http://www.virtualization.info/2009/02/citrix-to-release-xenserver-for-free.html "Virtualization.info")and [others](http://practical-tech.com/infrastructure/citrix-to-offer-free-xenserver-virtualization/ "Stephen Vaughn-Nichols"). There's lots of opinions of what this means for the virtualization software vendor landscape. But that's less important really than what it means for IT pros and developers. They're now getting much broader access to the benefits of a hypervisor platform, one that's [interoperable with other hypervisors](http://blog.scottlowe.org/2009/02/19/citrix-open-sources-their-vhd-implementation/ "Scott Lowe blog"), and one that will have a common management interface. It's a reminder that virtualization is just another means to an end (obviously a great one, otherwise I wouldn't blog here). The value has become management, automation, processes, and the like. That's enough from me. [Here's a video](https://www.microsoft.com/emea/presscentre/pressreleases/CitrixPR_230209.mspx "Simon and Mike Video") with Simon Crosby, CTO of Citrix, and Mike Neil, GM of virtualization, talking about Citrix Essentials for Hyper-V. Enjoy,

Patrick

[ **update** : Two items. First, the real-world clock on our blog is out of sorts. I hit publish on this blog on Feb. 23, not Feb. 20. Just in case someone out there was wondering. I'll try to fix this issue. Second, Barry at Citrix [blogged about Essentials for Hyper-V](http://community.citrix.com/blogs/citrite/barryf/ "BarryF blog at Citrix"), too, and included demo. Check it out].