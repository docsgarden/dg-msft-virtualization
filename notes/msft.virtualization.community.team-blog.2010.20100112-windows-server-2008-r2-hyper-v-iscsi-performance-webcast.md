---
id: YkFiAPCbJQ6eMCXQb4E992s
title: >-
    Windows Server 2008 R2 Hyper-V iSCSI Performance Webcast
desc: >-
    Windows Server 2008 R2 Hyper-V iSCSI Performance Webcast
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100112-windows-server-2008-r2-hyper-v-iscsi-performance-webcast
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: e3ac07cff509238313e58ca4563c5f38aa76c6e2e310b816f6607ff2ba481ff4
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100112-windows-server-2008-r2-hyper-v-iscsi-performance-webcast.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "01/12/2010"
    date: "2010-01-12 19:20:00"
    categories: "dell"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100112-windows-server-2008-r2-hyper-v-iscsi-performance-webcast.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100112-windows-server-2008-r2-hyper-v-iscsi-performance-webcast).

# Windows Server 2008 R2 Hyper-V iSCSI Performance Webcast

My friend and teammate Jim Schwartz on our Virtualization Solutions Marketing Team has asked me to post this entry about a very exciting [webcast](https://msevents.microsoft.com/CUI/WebCastEventDetails.aspx?EventID=1032432956&EventCategory=4&culture=en-US&CountryCode=US) coming up, but he can describe it much better than me...

 

Hi, I’m Jim Schwartz, a director of virtualization solutions marketing with Microsoft in Redmond.

 

Over the past year we’ve seen more customers moving from basic deployment scenarios like dev/test and consolidation into more advanced deployments that take full advantage of VM mobility for high availability and site recovery.  Deployment of critical business workloads shows how virtualization features like Live Migration are driving IT agility.  Shared storage offers significant benefits for storage consolidation including centralized backup, ease of management, and scalability.  Combining these benefits with Microsoft server virtualization delivers a powerful solution for IT customers to maximize hardware utilization in their datacenters.

 

In evaluation of SAN alternatives, IT Pros are increasingly finding that iSCSI meets their requirements offering a cost effective alternative to Fibre Channel.  According to the Q3 09 IDC Storage Tracker, shipments of iSCSI targets are growing at 50% a year. 

 

So advanced virtualization deployments on iSCSI shared storage provide a cost effective option to realize the benefits of virtualization.  Here are the practical implications:

 

Windows Server 2008 R2 includes Hyper-V which means virtualization is built in as a role in the operating system.  Hyper-V takes advantage of the mature protocol and driver architecture in Windows Server.  The Microsoft iSCSI Software Initiator in Windows Server is a foundation for unified fabrics that use existing Ethernet infrastructure and management tools.  Because the Hyper-V driver model and verification is based on Windows Server, any partner who develops and certifies iSCSI solutions for Windows Server also has products that work seamlessly with Hyper-V.

 

How is that working out?  There is a strong ecosystem of partners who have delivered iSCSI targets that are certified with Windows Server including HP, Dell, EMC, NetApp, IBM and Hitachi Data Systems.  This translates into a full range of solutions for Hyper-V delivering scale, performance and broad interoperability. 

 

In addition to iSCSI storage target providers, advances in CPU and network acceleration from our key ecosystem partners help deliver maximum CPU efficiency.  For example, the Intel Xeon 5500 processor is optimized for multi-core implementation to scale iSCSi performance.  The Intel Ethernet adapter line offers advanced iSCSI and transport acceleration while using the Windows trusted iSCSI software initiator and TCP stack.  Intel is also delivering industry leading 10G server adapters that support and enhance iSCSI translating into greater ease of use for Hyper-V customers. 

 

If you’re interested in hearing more about Windows Server R2 Hyper-V, the iSCSI initiator and Intel’s iSCSI enhancements, join us for a webcast on 1/14—the speakers will provide a deep dive into the technologies and cover practical considerations.  Microsoft and Intel will announce physical and virtual throughput and IOP performance numbers developed as a result of collaborative engineering efforts during the Windows Server 2008 R2 release cycle...you may be surprised at the results!  Register for the webcast [here](https://msevents.microsoft.com/CUI/WebCastEventDetails.aspx?EventID=1032432956&EventCategory=4&culture=en-US&CountryCode=US). 

 

Thank you for reading

 

Kenon Owens

Product Marketing Manager

Integrated Virtualization