---
id: 8AVqNDwVDC8G2EzfVHXEWTm
title: >-
    Device Virtualization
desc: >-
    Device Virtualization
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/DeviceVirtualization
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: a451440089a19db7c7f67a1e4382eb5f4363ff528ff6fa37fb1c41ea5fae37c1
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/DeviceVirtualization.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['Device Virtualization']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/DeviceVirtualization.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/DeviceVirtualization).

# Device Virtualization

## Overview
The Host Compute APIs allow applications to extend the Hyper-V platform with virtualization support for generic PCI devices. By leveraging these APIs, applications can create virtual machines and offer them virtual devices that are not natively supported by the Hyper-V platform, such as cameras or sensors. The host-side code that virtualizes the device is supplied by the application, and the application is in full control of the device behavior as observed from the guest.

The following section contains the definitions of the APIs for device virtualization in the HCS. The DLL exports a set of C-style Windows API functions, the functions return HRESULT error codes indicating the result of the function call.


## Compute System Opterations
|Function   |Description|
|---|---|---|---|---|---|---|---|
|[[HdvInitializeDeviceHost|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hdv.hdvpcideviceinitialize]]|Create a compute system|
|[[HdvTeardownDeviceHost|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hdv.hdvteardowndevicehost]]| Query a compute system's properties|
|[[HdvCreateDeviceInstance|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hdv.hdvcreatedeviceinstance]]|Modify a compute system|
|[[HdvReadGuestMemory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hdv.hdvreadguestmemory]]|Open a compute system|
|[[HdvWriteGuestMemory|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hdv.hdvwriteguestmemory]]|Pause a compute system|
|[[HdvCreateGuestMemoryAperture|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hdv.hdvcreateguestmemoryaperture]]|Resume a compute system|
|[[HdvDeliverGuestInterrupt|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hdv.hdvdeliverguestinterrupt]]|Save a compute system|