---
id: 4AEkyVdgtyLXXT4mjCe3xDP
title: >-
    Virtualization for Server Workloads&#58; Avanade Case Study, NetApp Whitepaper
desc: >-
    Virtualization for Server Workloads, Avanade Case Study, NetApp Whitepaper
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100308-virtualization-for-server-workloads-avanade-case-study-netapp-whitepaper
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 7f9e6ed27df3bbc920e0f41ccccc017b50cc2e650c514dbfb7742a0ccf3ffed8
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100308-virtualization-for-server-workloads-avanade-case-study-netapp-whitepaper.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "03/08/2010"
    date: "2010-03-08 15:18:00"
    categories: "high-availability"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100308-virtualization-for-server-workloads-avanade-case-study-netapp-whitepaper.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100308-virtualization-for-server-workloads-avanade-case-study-netapp-whitepaper).

# Virtualization for Server Workloads, Avanade Case Study, NetApp Whitepaper

Our customers often deploy Microsoft server workloads, such as SQL Server, SharePoint and Exchange, on Microsoft Hyper-V and System Center, and want to know how other customers have deployed these workloads and if there are deployment guidelines from specific solution partners that can help optimize for scale and business continuity.

A case in point is our customer [Avanade](https://www.microsoft.com/casestudies/Case_Study_Detail.aspx?CaseStudyID=4000006429). Avanade wanted to reduce their datacenter costs and turned to Microsoft Virtualization, even for their most demanding workloads. Avanade designed a series of tests to evaluate virtualization of SQL Server in production environments. In one test, Avanade was impressed to see Hyper-V support 6,000 Microsoft Dynamics CRM users on the SQL Server 2005 cluster. “We felt extremely comfortable standing behind our corporate strategy to virtualize our biggest workloads—our upgrade to Microsoft SQL Server 2008, Exchange Server 2007—all those applications people were saying just couldn’t be done,” says Andy Schneider, Infrastructure Architect at Avanade. Avanade was able to virtualize its databases and consolidate them on fewer servers ** ** connected to a 50TB storage solution from NetApp. Overall, they reduced servers by 85% and improved performance by 50%.  

Speaking of NetApp, they recently released a whitepaper ([click here](https://www.netapp.com/pdf.html?item=/media/19782-Architectural-Plans.pdf&v=202010152134) for pdf) that discusses their testing with consolidation of multiple workloads: Exchange Server 2007 (2000 heavy users; 250 MB Mailbox), SharePoint 2007 (3000 users; 500MB user quota) and SQL Server 2008 (3000 users using OLTP workloads such as CRM). They used Windows Server 2008 R2 Hyper-V with its Clustered Shared Volume (CSV) technology. They used LiveMigration along with NetApp MetroCluster software for high availability and cross-site disaster recovery. In these their tests, they were able to achieve 100% up-time!  For more details, check out [their blog](http://blogs.netapp.com/msenviro/2010/03/ha_for_hv.html).

Last but not least, I would like to encourage you to join Microsoft and its solution partners at the Windows ITPro[ Business Critical Virtualization](https://www.vconferenceonline.com/) online conference on March 31st. This in-depth virtual event will provide all you need to know around the best way to virtualize Microsoft applications like SQL Server, SharePoint and Exchange on a highly resilient virtual infrastructure. You can get more information and register for the event [here](https://www.vconferenceonline.com/).

Vipul Shah  
Sr. Product Manager  
Microsoft Virtualization Marketing