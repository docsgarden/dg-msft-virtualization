---
id: AuSaqtf9nGAKhRkgsrbi4rr
title: >-
    HcsOpenProcess
desc: >-
    HcsOpenProcess
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsOpenProcess
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 06fcc6fcdfc28702e5fe2cf18a4f6fbf104d8d31e22b3c95e1d9dbe4560d9d93
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsOpenProcess.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsOpenProcess']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsOpenProcess.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsOpenProcess).

# HcsOpenProcess

## Description

Opens an existing process in a compute system.

## Syntax

```cpp
HRESULT WINAPI
HcsOpenProcess(
    _In_ HCS_SYSTEM computeSystem,
    _In_ DWORD processId,
    _In_ DWORD requestedAccess,
    _Out_ HCS_PROCESS* process
    );

```

## Parameters

`computeSystem`

The handle to the compute system in which to start the process.

`processId`

Specifies the Id of the process to open.

`requestedAccess`

Specifies the required access to the compute system.

`process`

Receives the handle to the process.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |