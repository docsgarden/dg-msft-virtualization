---
id: 9imuQRQmQFiCYVFFAXPpvdx
title: >-
    HDV_PCI_BAR_SELECTOR Enumeration
desc: >-
    HDV_PCI_BAR_SELECTOR Enumeration
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciBarSelector
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 51524969f678749f8dd548e9e8d1905cd4b53312e3122f4059dbc6ccd8f3cf4c
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciBarSelector.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HDV_PCI_BAR_SELECTOR Enumeration']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciBarSelector.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciBarSelector).

# HDV_PCI_BAR_SELECTOR Enumeration

Discriminator for the BAR selection.

## Syntax

```C++
typedef enum
{
    HDV_PCI_BAR0 = 0,
    HDV_PCI_BAR1,
    HDV_PCI_BAR2,
    HDV_PCI_BAR3,
    HDV_PCI_BAR4,
    HDV_PCI_BAR5

} HDV_PCI_BAR_SELECTOR;
```

## Constants

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| HDV_PCI_BAR0 | null |
| HDV_PCI_BAR1 | null |
| HDV_PCI_BAR2 | null |
| HDV_PCI_BAR3 | null |
| HDV_PCI_BAR4 | null |
| HDV_PCI_BAR5 | null |
|    |    |

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |