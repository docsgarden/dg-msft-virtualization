---
id: oTZf5uSTGJ3WKq56aUe6g2Y
title: >-
    The GetGuestRawSavedMemorySize function
desc: >-
    Returns the size in bytes of the saved memory for a given VM saved state file.
canonicalUrl: https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/GetGuestRawSavedMemorySize
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 343b48863e7599e17715f0f0f5006b358b7e524eff6f6f5ae72892d300ceb0da
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/GetGuestRawSavedMemorySize.md
    ms.date: "04/18/2022"
    author: "mattbriggs"
    ms.author: "mabrigg"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/GetGuestRawSavedMemorySize.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/GetGuestRawSavedMemorySize).

# GetGuestRawSavedMemorySize function

Returns the size in bytes of the saved memory for a given VM saved state file.


## Syntax

```C
HRESULT
WINAPI
GetGuestRawSavedMemorySize(
    _In_    VM_SAVED_STATE_DUMP_HANDLE      VmSavedStateDumpHandle,
    _Out_   UINT64*                         GuestRawSavedMemorySize
    );
```

## Parameters

`VmSavedStateDumpHandle`

Supplies a handle to a dump provider instance.

`GuestRawSavedMemorySize`

Returns the size of the saved memory of a given guest in bytes.

## Return Value

If the operation completes successfully, the return value is `S_OK`.

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |