---
id: Aoo2RLTDD2NRiW228Ec63co
title: >-
    Resource Controls on Windows 10
desc: >-
    Outline of server-only features that may appear, but are not usable, in Hyper-V Manager on Windows 10.
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/reference/server-only-features
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 1eaaaa45112ffb5d321b011f21117f73e083817811c060112023efa75e073643
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/reference/server-only-features.md
    keywords: "windows 10, windows server, hyper-v"
    author: "scooley"
    ms.author: "scooley"
    ms.date: "06/28/2017"
    ms.topic: "article"
    ms.prod: "windows-10-hyperv"
    ms.assetid: "d75b96a9-f430-492e-95d4-b05135ec9b9e"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/reference/server-only-features.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/reference/server-only-features).

# Resource Controls Missing on Windows 10?