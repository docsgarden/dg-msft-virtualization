---
id: 5tYSQRd6bDraQsQRDtEVVXQ
title: >-
    Upcoming Webcasts on Best Practices for Virtualizing MS Server Applications
desc: >-
    Upcoming Webcasts on Best Practices for Virtualizing MS Server Applications
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20091027-upcoming-webcasts-on-best-practices-for-virtualizing-ms-server-applications
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 46e95c745fdf32872a3a8d563945e6c7c095091d0c753ae312ad5364f413425a
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091027-upcoming-webcasts-on-best-practices-for-virtualizing-ms-server-applications.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "10/27/2009"
    date: "2009-10-27 15:25:00"
    categories: "high-availability"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091027-upcoming-webcasts-on-best-practices-for-virtualizing-ms-server-applications.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20091027-upcoming-webcasts-on-best-practices-for-virtualizing-ms-server-applications).

# Upcoming Webcasts on Best Practices for Virtualizing MS Server Applications

We’ve got some great webcasts coming up in the next few weeks to discuss recommendations for virtualizing MS server applications and the benefits of choosing hyper-V + System Center as your virtualization solution.  Each technical webcast will focus on a specific server application- Exchange, SQL and SharePoint.   Please see below for detailed information.

 [TechNet Webcast: Microsoft Virtualization Best Practices for SQL Server (Level 300)](https://msevents.microsoft.com/CUI/WebCastEventDetails.aspx?EventID=1032428763&EventCategory=4&culture=en-US&CountryCode=US "Reg link"). 

Thursday, Oct. 29, 2009 at 10am Pacific time

> 
> 
> Virtualizing business critical applications will deliver significant customer benefits including cost savings, enhanced business continuity and an agile and efficient management solution.  This session will focus on virtualizing SQL Server using Microsoft solutions, the benefits over key competitors such as VMware, and guidance for virtualizing SQL server for Production and Test/Dev scenarios focusing on consolidation, scale, load balancing, dynamic provisioning and high availability. We will go into technical details with best practices. Customer evidence and results from lab deployment tests will also be discussed.



[TechNet Webcast: Microsoft Virtualization Best Practices for Exchange Server (Level 300)](https://msevents.microsoft.com/CUI/WebCastEventDetails.aspx?EventID=1032428203&EventCategory=4&culture=en-US&CountryCode=US "Reg link")  Wednesday, Nov. 4 at 10am Pacific time

 

Virtualizing business critical applications will deliver significant customer benefits including cost savings, enhanced business continuity and an agile and efficient management solution.  This session will focus on virtualizing Exchange using Microsoft solutions, the benefits over key competitors such as VMware, and guidance for virtualizing Exchange for various Production scenarios. We will go into technical details with best practices. Customer evidence and results from lab deployment tests will also be discussed.  
  
---