---
id: ATEYfXBQk83V6WEo8bNpcyu
title: >-
    Update&#58; More than recession&#58; IDC's server virtualization tracker report
desc: >-
    Update; More than recession; IDC's server virtualization tracker report
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20091026-update-more-than-recession-idc-s-server-virtualization-tracker-report
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: c71a1ab3b7d7a8900d18d343577bbf0390aaf70fa3ccc0a6ad715de433451de8
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091026-update-more-than-recession-idc-s-server-virtualization-tracker-report.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "10/26/2009"
    date: "2009-10-26 21:24:00"
    categories: "esx"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091026-update-more-than-recession-idc-s-server-virtualization-tracker-report.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20091026-update-more-than-recession-idc-s-server-virtualization-tracker-report).

# Update; More than recession; IDC's server virtualization tracker report

**Update** : The folks at IDC asked that I remove this blog post until further notice. That said, do check out Roger Johnson's [post](https://i.dell.com/sites/content/business/solutions/operating-systems/en/Documents/crutchfield-case-study.pdf "Roger Johnson's blog post"). He discusses Dell PowerEdge R910 and his Hyper-V deployment at Crutchfield. I met Roger at VMworld 2009. He's a VCP, having run an ESX Server-based datacenter for some time before [Crutchfield](https://www.microsoft.com/casestudies/Case_Study_Detail.aspx?casestudyid=4000005133 "Microsoft case study on Crutchfield") moved away from VMware and onto Hyper-V/SCVMM earlier this year.

Patrick