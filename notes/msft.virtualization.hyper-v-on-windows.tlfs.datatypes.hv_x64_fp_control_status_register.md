---
id: 89p737z8zNVcaBZ47uUzqzy
title: >-
    HV_X64_FP_CONTROL_STATUS_REGISTER
desc: >-
    HV_X64_FP_CONTROL_STATUS_REGISTER
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_x64_fp_control_status_register
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 68b2306adf192a55ca1b7eb635936309bd7e5a9c1afe4a45f610afaaf35eafb3
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_x64_fp_control_status_register.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_x64_fp_control_status_register.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_x64_fp_control_status_register).

# HV_X64_FP_CONTROL_STATUS_REGISTER

## Syntax

```c
typedef struct
{
    UINT16 FpControl;
    UINT16 FpStatus;
    UINT8 FpTag;
    UINT8 Reserved:8;
    UINT16 LastFpOp;
    union {
        UINT64 LastFpRip;
        struct {
            UINT32 LastFpEip;
            UINT16 LastFpCs;
        };
     };
 } HV_X64_FP_CONTROL_STATUS_REGISTER;
 ```