---
id: AveB552yVpw98YwjxD9fN39
title: >-
    Announcing GA of Disaster Recovery to Azure - Purpose-Built for Branch Offices and SMB
desc: >-
    An announcement about GA of Disaster Recovery to Azure to Branch Office and SMB customers.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2014/20141211-announcing-ga-of-disaster-recovery-to-azure-purpose-built-for-branch-offices-and-smb
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: e93c2ea3847b99bb1cd8b5a8bba4521709d98b7b455046095988e17e7d68e911
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20141211-announcing-ga-of-disaster-recovery-to-azure-purpose-built-for-branch-offices-and-smb.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2014-12-11 11:05:00"
    ms.date: "12/11/2014"
    categories: "asr"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20141211-announcing-ga-of-disaster-recovery-to-azure-purpose-built-for-branch-offices-and-smb.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2014/20141211-announcing-ga-of-disaster-recovery-to-azure-purpose-built-for-branch-offices-and-smb).

# Announcing GA of Disaster Recovery to Azure - Purpose-Built for Branch Offices and SMB

Today, we are excited to announce the GA for **_Branch Office and SMB Disaster Recovery to Azure_**.   [Azure Site Recovery](https://aka.ms/smb_asr_landingpage) delivers a simpler, reliable & cost effective Disaster Recovery solution to Branch Office and SMB customers.  ASR with new _Hyper-V Virtual Machine Protection_ from **_Windows Server 2012 R2 to Microsoft Azure_** can now be used at customer owned sites  and SCVMM is optional.

 

You can visit the [Getting Started with Azure Site Recovery ](https://aka.ms/smb_asr_gettingstarted)for additional information.