---
id: 5N9A2uDvNNCYF5DAHHcGAbV
title: >-
    The GuestPhysicalAddressToRawSavedMemoryOffset function
desc: >-
    Translates the given guest physical address to a raw saved memory offset.
canonicalUrl: https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/GuestPhysicalAddressToRawSavedMemoryOffset
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 51a9dcf1f4270845de003def96c06520adcedcc6f9fed443cd6e0decc5c5e9a8
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/GuestPhysicalAddressToRawSavedMemoryOffset.md
    ms.date: "04/19/2022"
    author: "mattbriggs"
    ms.author: "mabrigg"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/GuestPhysicalAddressToRawSavedMemoryOffset.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/GuestPhysicalAddressToRawSavedMemoryOffset).

# GuestPhysicalAddressToRawSavedMemoryOffset function

Translates the given guest physical address to a raw saved memory offset. This is specially useful if callers need to read a memory range directly from all of the guest's saved memory starting in the saved memory address equivalent to the supplied guest physical address. Translation from raw saved memory offset to physical address is not supported.

## Syntax

```C
HRESULT
WINAPI
GuestPhysicalAddressToRawSavedMemoryOffset(
    _In_    VM_SAVED_STATE_DUMP_HANDLE      VmSavedStateDumpHandle,
    _In_    GUEST_PHYSICAL_ADDRESS          PhysicalAddress,
    _Out_   UINT64*                         RawSavedMemoryOffset
    );
```

## Parameters

`VmSavedStateDumpHandle`

Supplies a handle to a dump provider instance.

`PhysicalAddress`

Supplies the guest physical address to translate.

`RawSavedMemoryOffset`

Returns the raw saved memory offset for a given physical address.

## Return Value

If the operation completes successfully, the return value is `S_OK`.

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |