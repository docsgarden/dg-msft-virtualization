---
id: AHWdnMUKbqDRnT9ButZjajE
title: >-
    Visual Studio Lab Management 2010 beta 2
desc: >-
    Visual Studio Lab Management 2010 beta 2
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20091105-visual-studio-lab-management-2010-beta-2
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 221416c66c5da0be736ec93622a12265133d9d483b746f42139e47b8739520aa
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091105-visual-studio-lab-management-2010-beta-2.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "11/05/2009"
    date: "2009-11-05 22:20:00"
    categories: "net"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091105-visual-studio-lab-management-2010-beta-2.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20091105-visual-studio-lab-management-2010-beta-2).

# Visual Studio Lab Management 2010 beta 2

Hello, this is Bryon Surace again.  I’m a senior program manager on the Windows virtualization team at Microsoft.

 

[Recently](https://blogs.msdn.com/lab_management/archive/2009/10/20/announcing-visual-studio-team-lab-management-2010-beta2.aspx "blog post"), the Visual Studio 2010 and .NET Framework 4 beta 2 became available for public download.  Part of this release is the **_Beta 2_ version of Visual Studio Team System 2010 Lab Management** ; an integrated solution to give you all the benefits of virtualization for application lifecycle management. Congratulations to the Visual Studio Lab Management team for reaching this milestone!

 **Beta 2 Improvements:

**

§  Simplified Virtual Environment creation

§  Lab Environment Viewer with full-screen capability

§  Manual and automated testing in virtual environment

§  Build-deploy-test workflow for virtual environments 

§  Enhanced workflow process parameters UI and report

§  Network isolation with support for domain controller VMs

§  Setup & Config – verification and auto provisioning of accounts

§  Virtual Machine templates

§  “In-Use” support for shared environments

 **The main features of Lab Management are:

**

  * **Easily   manage more complex test configurations**

    * Lab environment as a first class entity for managing multi-machine test environments

    * Setup multiple configurations for testing quickly 

    * Consistent, reliable access to test environments 

    * Create multiple copies of test environments for parallel testing 

  * **New   “clean” environment in minutes**

    * Environment snapshot: lightweight and efficient 

    * Facilitates exploratory testing 

  * **Scheduled build/test cycle on steroids**

    * Snapshot to clean environment 

    * Build triggered app deployment 

    * Testing in more realistic environment 

  * **Rich bugs with environment snapshot

**
    * reduce the number of no-repro bugs



[Visual Studio Download Page](https://msdn.microsoft.com/vstudio/dd582936.aspx) – you can download all the required components from there. This [video](https://channel9.msdn.com/shows/10-4/10-4-Episode-33-Downloading-and-Installing-Visual-Studio-2010-Beta-2/) can guide you through the download and installation process of TFS. And check out the [Lab Management blog](https://blogs.msdn.com/lab_management/).

 

Thank you,

Bryon Surace