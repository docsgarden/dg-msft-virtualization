---
id: 59VPRus6Vg7Rjv6V38hfGP9
title: >-
    Available to Windows 10 Insiders Today - Access to published container ports via “localhost”/127.0.0.1
desc: >-
    This is a cross post that redirects you the Microsoft Networking Blog.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2017/20171106-available-to-windows-10-insiders-today-access-to-published-container-ports-via-localhost-127-0-0-1
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: e60893ae1d3f3561f2b8906cff510bb52c6fe9c7128cb35a9dd0b03ca5595d0a
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2017/20171106-available-to-windows-10-insiders-today-access-to-published-container-ports-via-localhost-127-0-0-1.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2017-11-06 17:47:40"
    ms.date: "11/06/2017"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2017/20171106-available-to-windows-10-insiders-today-access-to-published-container-ports-via-localhost-127-0-0-1.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2017/20171106-available-to-windows-10-insiders-today-access-to-published-container-ports-via-localhost-127-0-0-1).

# Available to Windows 10 Insiders Today: Access to published container ports via “localhost”/127.0.0.1

_This is a cross-post_ ; click the link below to be rerouted to our main post on the Networking Blog :) https://blogs.technet.microsoft.com/networking/2017/11/06/available-to-windows-10-insiders-today-access-to-published-container-ports-via-localhost127-0-0-1/