---
id: 3wNPFz8AzWZUFWFnnr6AASk
title: >-
    Answering Exchange Virtualization Questions and Addressing Misleading VMware Guidance
desc: >-
    We love that our customers are excited to deploy Exchange Server within virtualized environments.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20101110-answering-exchange-virtualization-questions-and-addressing-misleading-vmware-guidance
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: f53890dcbdb4f2eab2352df1db36721124ddf62cecc6b4baf0063ea2ba69db5a
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20101110-answering-exchange-virtualization-questions-and-addressing-misleading-vmware-guidance.md
    author: "scooley"
    ms.author: "scooley"
    date: "2010-11-10 00:07:32"
    ms.date: "11/10/2010"
    categories: "disaster-recovery"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20101110-answering-exchange-virtualization-questions-and-addressing-misleading-vmware-guidance.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20101110-answering-exchange-virtualization-questions-and-addressing-misleading-vmware-guidance).

# Answering Exchange Virtualization Questions and Addressing Misleading VMware Guidance

I recommend anyone who is considering virtualizing MS Exchange read [Jim Lucey's blog post](http://msexchangeteam.com/archive/2010/11/09/456851.aspx) and the comments below his post. VMware's HA guidance on virtualizing Exchange could be mis-interpreted, resulting in increased storage costs and placing data at risk. Here's an excerpt: 

We love that our customers are excited to deploy Exchange Server within virtualized environments. While VMware leveraged Exchange performance and sizing tools to provide guidance, their recommendations casually tiptoe around Microsoft system requirements, unnecessarily increasing storage and maintenance costs and putting customers at risk. Exchange Server 2010 provides choice and flexibility in deployment options. We are committed to virtualization technology deeply, and will continuously review as the various technologies evolve. We hope to do even more in the future with our roadmap. As we work to test and update guidance pertaining to Exchange running under virtualized environments, our current [system requirements](https://technet.microsoft.com/library/aa996719.aspx "Go to 'Exchange 2010 System Requirements' on TechNet") are in place to give customers the most reliable email infrastructure possible. Patrick