---
id: 3wMQDg5RTpV9u2r4eAKZwNq
title: >-
    HV_SVM_ENLIGHTENED_VMCB_FIELDS
desc: >-
    HV_SVM_ENLIGHTENED_VMCB_FIELDS
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_svm_enlightened_vmcb_fields
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: f354515987a2991623fefa7d7421737fec4e50ec0e2f8d51dde64941b6172d78
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_svm_enlightened_vmcb_fields.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_svm_enlightened_vmcb_fields.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_svm_enlightened_vmcb_fields).

# HV_SVM_ENLIGHTENED_VMCB_FIELDS

Enlightened fields in the VMCB (on AMD platforms). The enlightened fields are in the control section, offset 0x3E0-3FF, of the VMCB.

## Syntax

```c
typedef struct
{
    struct
    {
        // Direct virtual flush.
        UINT32 NestedFlushVirtualHypercall : 1;

        // Enlightened MSR bitmap.
        UINT32 MsrBitmap : 1;

        // Enlightened TLB: ASID flushes do not affect TLB entries derived from the NPT.
        // Hypercalls must be used to invalidate NPT TLB entries.
        UINT32 EnlightenedNptTlb : 1;

        UINT32 Reserved : 29;
    } EnlightenmentsControl;

    UINT32 VpId;
    UINT64 VmId;
    UINT64 PartitionAssistPage;
    UINT64 Reserved;

} HV_SVM_ENLIGHTENED_VMCB_FIELDS;
 ```