---
id: AV3Jbe4y7qHevjmin9w8ttd
title: >-
    Windows Server 8&#58; An Introduction
desc: >-
    Windows Server; an introduction
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110911-windows-server-8-an-introduction
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: dcd3d987012bc3e66aa4fbb5dce8560afb4d51d57960bef2299c4ce7f7ff0124
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110911-windows-server-8-an-introduction.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "09/11/2011"
    date: "2011-09-11 10:37:00"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110911-windows-server-8-an-introduction.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110911-windows-server-8-an-introduction).

# Windows Server; an introduction

Take a look at Bill Laing's blog, Corporate Vice President for Microsoft's Server and Cloud business, to get an overview of the next release of Windows Server codenamed Windows Server 8.

Read Bill's post on the [Microsoft Server and Cloud Platform blog](http://bit.ly/oa3JwY "Microsoft Server and Cloud Platform blog").