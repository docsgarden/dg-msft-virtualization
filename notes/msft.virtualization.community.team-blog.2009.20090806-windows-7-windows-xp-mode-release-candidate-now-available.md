---
id: 4Cyn9TxS4RYGUWURk936Bbn
title: >-
    Windows 7&#58; Windows XP Mode Release Candidate Now Available
desc: >-
    Contains highlights of the WIndows XP Mode Release Candidate for Windows 7.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090806-windows-7-windows-xp-mode-release-candidate-now-available
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: b4f5b44c6f3d9d35732d520898244b50c29bf55a3eee4c20e73a431e2a6dcc43
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090806-windows-7-windows-xp-mode-release-candidate-now-available.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2009-08-06 11:59:34"
    ms.date: "08/06/2009"
    categories: "hp"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090806-windows-7-windows-xp-mode-release-candidate-now-available.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090806-windows-7-windows-xp-mode-release-candidate-now-available).

# Windows XP Mode Release Candidate
Virtualization Nation, Just a quick blog in case you missed the big news for Windows 7. Windows XP Mode has hit Release Candidate. Here are some highlights of the new Release Candidate: **New Features in Windows XP Mode RC** Based on feedback from the Windows XP Mode beta, we've made several improvements to the usability of Windows XP Mode for small and medium-sized business users: 

  * You can now attach USB devices to Windows XP Mode applications directly from the Windows 7 task-bar. This means your USB devices, such as printers and flash drives, are available to applications running in Windows XP Mode, without the need to go into full screen mode. 
  * You can now access Windows XP Mode applications with a "jump-list". Right click on the Windows XP Mode applications from the Windows 7 task bar to select and open most recently used files. 
  * You now have the flexibility of customizing where Windows XP Mode differencing disk files are stored. 
  * You can now disable drive sharing between Windows XP Mode and Windows 7 if you do not need that feature. 
  * The initial setup now includes a new user tutorial about how to use Windows XP Mode. 

Personally, all the apps I use work just fine with Windows 7 (zero app compatibility issues), but I've found one killer reason for Windows XP Mode. At home, we have an HP Photosmart printer that prints great photos. It's really a great printer, but unfortunately, HP never updated the drivers for this printer beyond XP. I haven't had the heart to recycle it just yet and now I don't need to. I setup Windows XP Mode on my system at home, hooked up this USB device to my Windows XP Mode virtual machine, setup printer sharing within XP, and voila we can use this printer again. Very cool. :-) For more on Windows XP Mode and download location, [check out this blog post from the Windows Client Team](http://windowsteamblog.com/blogs/windows7/archive/2009/08/04/windows-xp-mode-rc-now-available.aspx). Cheers, _Jeff Woolsey_ _Principal Group Program Manager_

_Windows Server, Hyper-V_