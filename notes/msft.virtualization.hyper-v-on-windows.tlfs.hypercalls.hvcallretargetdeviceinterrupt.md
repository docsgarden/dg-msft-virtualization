---
id: 4wZUQBitQqZRJDbfj5Doa2e
title: >-
    HvCallRetargetDeviceInterrupt
desc: >-
    HvCallRetargetDeviceInterrupt hypercall
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallRetargetDeviceInterrupt
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: ef4b02d70a97cb567784eec2291c66abd40f3a03d1acfd41d2ff7732787512a7
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallRetargetDeviceInterrupt.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallRetargetDeviceInterrupt.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallRetargetDeviceInterrupt).

# HvCallRetargetDeviceInterrupt

This hypercall retargets a device interrupt, which may be useful for rebalancing IRQs within a guest.

## Interface

 ```c
HV_STATUS
HvRetargetDeviceInterrupt(
    _In_ HV_PARTITION_ID PartitionId,
    _In_ UINT64 DeviceId,
    _In_ HV_INTERRUPT_ENTRY InterruptEntry,
    _In_ UINT64 Reserved,
    _In_ HV_DEVICE_INTERRUPT_TARGET InterruptTarget
    );
 ```

## Call Code
`0x007e` (Simple)

## Input Parameters

| Name                    | Offset     | Size     | Information Provided                      |
|-------------------------|------------|----------|-------------------------------------------|
| `PartitionId`           | 0          | 8        | Partition Id (can be HV_PARTITION_SELF)   |
| `DeviceId`              | 8          | 6        | Supplies the unique (within a guest) logical device ID that is assigned by the host.   |
| RsvdZ                   | 32         | 8        | Reserved                                  |
| `InterruptEntry`        | 16         | 16       | Supplies the MSI address and data that identifies the interrupt. |
| `InterruptTarget`       | 40         | 16       | Specifies a mask representing VPs to target|

## See also

[[HV_INTERRUPT_ENTRY|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.datatypes.HV_INTERRUPT_ENTRY]]

[[HV_DEVICE_INTERRUPT_TARGET|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.datatypes.HV_DEVICE_INTERRUPT_TARGET]]