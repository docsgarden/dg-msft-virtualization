---
id: 34Vas7ThvzY6c35CUSsNYM8
title: >-
    HdvCreateDeviceInstance
desc: >-
    HdvCreateDeviceInstance
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvCreateDeviceInstance
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 268325dcfd5e71026ea16140b41c7c881bdefa6d7262ccbe6cafe02aa1384417
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvCreateDeviceInstance.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HdvCreateDeviceInstance']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvCreateDeviceInstance.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvCreateDeviceInstance).

# HdvCreateDeviceInstance

## Description

Creates a device instance in the current host and associates it with a device emulation interface and context.

## Syntax

```C++
HRESULT WINAPI
HdvCreateDeviceInstance(
    _In_     HDV_HOST        DeviceHost,
    _In_     HDV_DEVICE_TYPE DeviceType,
    _In_     const GUID*     DeviceClassId,
    _In_     const GUID*     DeviceInstanceId,
    _In_     const VOID*     DeviceInterface,
    _In_opt_ void*           DeviceContext,
    _Out_    HDV_DEVICE*     DeviceHandle
    );
```

## Parameters

|Parameter|Description|
|---|---|---|---|---|---|---|---|
|`DeviceHost` | Handle to the device host in which to create the new device.|
|`DeviceType` | Specifies the [[HDV_DEVICE_TYPE|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hdv.hdvdevicetype]] type of the device instance to create.|
|`DeviceClassId` | Supplies the client-defined class ID of the device instance to create.|
|`DeviceInterface` | Supplies the client-defined instance ID of the device instance to create.|
|`DeviceInterface` | Supplies a function table representing the interface exposed by the device instance. The actual type of this parameter is implied by the DeviceType parameter.|
|`DeviceContext` | An optional opaque context pointer that will be supplied to the device instance callbacks.|
|`DeviceHandle` | Receives a handle to the created device instance.|
|     |     |

## Return Values

|Return Value     |Description|
|---|---|
|`S_OK` | Returned if function succeeds.|
|`HRESULT` | An error code is returned if the function fails.
|     |     |

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |