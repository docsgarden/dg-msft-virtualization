---
id: 5ZQmtgerFZXADXcCjonNzB4
title: >-
    System Center Virtual Machine Manager 2008 R2 beta available
desc: >-
    The beta of System Center Virtual Machine Manager 2008 R2 is now available on the Microsoft Connect site for download.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090316-system-center-virtual-machine-manager-2008-r2-beta-available
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 2b56437ed93e3b15c8fecd0f98cb6d9a14dcf547e043c72f0c21c0e4ce01f411
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090316-system-center-virtual-machine-manager-2008-r2-beta-available.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-03-16 10:44:00"
    ms.date: "03/16/2009"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090316-system-center-virtual-machine-manager-2008-r2-beta-available.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090316-system-center-virtual-machine-manager-2008-r2-beta-available).

# System Center Virtual Machine Manager 2008 R2 beta available

Exciting news for Windows Server Hyper-V, System Center, and VMWare customers.  The beta of System Center Virtual Machine Manager 2008 R2 is now available on the [Microsoft Connect site for download](https://connect.microsoft.com/ "Microsoft Connect Dashboard"). If you don't already have access to VMM on the Connect site you can get to it [here by simply filling out a quick survey](https://connect.microsoft.com/directory/ "Microsoft Connection Directory"), which will help us better understand your virtualization needs.  The new beta continues to extend the data center management capabilities of the System Center suite of offerings to build on the all up physical and virtual IT asset management features you already know, including the integration of Operations Manager and Virtual Machine Manager through the PRO functionality. You can find current, full details on the VMM 2008 R2 beta in a post by [Rakesh Malhotra at this location](https://blogs.technet.com/rakeshm/ "Rakesh Malhotra's Blog"). In summary, VMM 2008 R2 beta now supports new Windows Server 2008 R2 beta features such as Live Migration.  Some additional features include: 
* Windows Server 2008 R2 Beta Hyper-V host management 
* Enhanced migration including Live (Live Migration) and SAN migration in and out of clusters 
* Multiple VMs per LUN using Clustered share volumes 
* Hot add of VM storage
A reminder also that the [Microsoft Management Summit is April 27 - May 1 in Las Vegas](http://mms-2009.com/ "Microsoft Management Summit 2009").  This is an excellent time to learn more about all of the management offerings from Microsoft including the System Center suite and VMM 2008 R2.  Hope to see you there. 

Please go download the beta of Virtual Machine Manager 2008 R2 today from the [Microsoft Connect site](https://connect.microsoft.com/ "Microsoft Connect Dashboard") and let us know what you think.