---
id: 8MYGnNp6bKqd6qq4vUYKJmZ
title: >-
    VMworld 2009 an oppurtunity to meet our customers
desc: >-
    VMworld 2009 an oppurtunity to meet our customers
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090826-vmworld-2009-an-oppurtunity-to-meet-our-customers
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 9b09ca211705ed71646ea28fb6872de332850ced6e45f1b91316abc49eabf2ae
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090826-vmworld-2009-an-oppurtunity-to-meet-our-customers.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "08/26/2009"
    date: "2009-08-26 12:09:00"
    categories: "cloud-computing"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090826-vmworld-2009-an-oppurtunity-to-meet-our-customers.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090826-vmworld-2009-an-oppurtunity-to-meet-our-customers).

# VMworld 2009 an oppurtunity to meet our customers

Hi, I am Vijay Tewari a program manager with the Virtualization team here at Microsoft. Having spent the last 15 months or so deeply involved in the development of Hyper-V I am really looking forward to meeting customers and partners at [VMworld 2009](https://vmworld2009.com/).<?xml:namespace prefix = o ns = "urn:schemas-microsoft-com:office:office" />  Much is being made about the fact that we are [not showing](https://blogs.technet.com/virtualization/archive/2009/08/25/What-you-won_2700_t-see-at-VMworld-2009-_2D00_-a-demo-of-SCVMM-2008-R2.aspx) Hyper-V and System Center Virtual Machine Manager at this event due to the show policies. I will stay away from that debate although coming on the heels of our RTM I would have loved to show the product. However, a lot of us will be there at the event and are really looking forward to interacting with customers. At the end of the day the product is only useful if it meets our customers’ demands and there is nothing like having a face to face interaction with our customers to hear firsthand what they like and hate about the product. 

Below is my tentative schedule for VMworld. As you can see a lot of wait lists, but I am hoping that I can squeeze in. Would like your feedback if there are sessions you would like to see covered. 

We will be tweeting (follow vtango on [Twitter](https://twitter.com/)) and blogging all through the event, so follow along.  Looking forward to seeing you!! Vijay Vijay Tewari, Principal Program Manager, Virtualization

**** 

Personal Schedule for Vijay Tewari  
  
---  
  
 

  
Monday   
  
---  
  
   
  
7:30 AM-9:30 AM  
(Waiting List) 

| 

LAB01 

| 

VMware vSphere™ 4 - New Features, Best of, Advanced Topics   
  
10:00 AM-11:00 AM  
(Waiting List) 

| 

V13100 

| 

The VMware Competitive Advantage - A Comparison of Server Virtualization Offerings   
  
11:30 AM-12:30 PM  
(Waiting List) 

| 

V13229 

| 

Introduction to VMware vSphere   
  
1:30 PM-2:30 PM  
(Waiting List)