---
id: 45TqPpqvQraokoyR55bASCa
title: >-
    HvCallModifyVtlProtectionMask
desc: >-
    HvCallModifyVtlProtectionMask hypercall
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallModifyVtlProtectionMask
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 956f68170a47aab0d93d3f5410cc8f731c03304aa3cb0d1fa059dbd5ac17bd3d
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallModifyVtlProtectionMask.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallModifyVtlProtectionMask.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallModifyVtlProtectionMask).

# HvCallModifyVtlProtectionMask

The HvCallModifyVtlProtectionMask hypercall modifies the VTL protections applied to an existing set of GPA pages.

## Interface

 ```c
HV_STATUS
HvModifyVtlProtectionMask(
    _In_ HV_PARTITION_ID TargetPartitionId,
    _In_ HV_MAP_GPA_FLAGS MapFlags,
    _In_ HV_INPUT_VTL TargetVtl,
    _In_reads(PageCount) HV_GPA_PAGE_NUMBER GpaPageList
    );
 ```

A VTL can only place protections on a lower VTL.

Any attempt to apply VTL protections on non-RAM ranges will fail with HV_STATUS_INVALID_PARAMETER.

## Call Code

`0x000C` (Rep)

## Input Parameters

| Name                    | Offset     | Size     | Information Provided                      |
|-------------------------|------------|----------|-------------------------------------------|
| `TargetPartitionId`     | 0          | 8        | Supplies the partition ID of the partition this request is for. |
| `MapFlags`              | 8          | 4        | Specifies the new mapping flags to apply. |
| `TargetVtl`             | 12         | 1        | Specified the target VTL.                 |
| RsvdZ                   | 13         | 3        |                                           |

## Input List Element

| Name                    | Offset     | Size     | Information Provided                      |
|-------------------------|------------|----------|-------------------------------------------|
| `GpaPageList`           | 0          | 8        | Supplies the pages to be protected.       |