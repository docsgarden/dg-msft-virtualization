---
id: 8AuRWRZAwBxZ52TJAJXBEi3
title: >-
    HcsCreateEmptyRuntimetStateFile
desc: >-
    HcsCreateEmptyRuntimetStateFile
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsCreateEmptyRuntimeStateFile
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 013b18618ae0b8043662aafb4edd3caecab5ef50285c0a7b622f08183eb127cc
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsCreateEmptyRuntimeStateFile.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsCreateEmptyRuntimetStateFile']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsCreateEmptyRuntimeStateFile.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsCreateEmptyRuntimeStateFile).

# HcsCreateEmptyRuntimetStateFile

## Description

This function creates an empty runtime-state file (.vmrs) for a VM. This file is used to save a running VM. It is specified in the options document of the [[`HcsSaveComputeSystem`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssavecomputesystem]] function. See [[sample code|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.utilityfunctionsample#createfilesgrantaccess]].

## Syntax

```cpp
HRESULT WINAPI
HcsCreateEmptyRuntimeStateFile(
    _In_ PCWSTR runtimeStateFilePath
    );
```

## Parameters

`runtimeStateFilePath`

Full path to the runtime-state file to create.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |