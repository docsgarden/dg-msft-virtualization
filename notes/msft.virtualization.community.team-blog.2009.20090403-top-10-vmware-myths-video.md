---
id: 56gudR9PqXpC2CPfVEHkHvY
title: >-
    Top 10 VMWare myths video
desc: >-
    Edwin and David recently sat down in front of a video camera to talk about the top 10 myths from VMWare.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090403-top-10-vmware-myths-video
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 20f550974c326afbb4c4aac4e391a0077a92c48e34a86814e989be099ce54013
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090403-top-10-vmware-myths-video.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-04-03 00:58:00"
    ms.date: "04/03/2009"
    categories: "application-virtualization"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090403-top-10-vmware-myths-video.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090403-top-10-vmware-myths-video).

# Top 10 VMWare myths video

Edwin and David recently sat down in front of a video camera to talk about the top 10 myths from VMWare. Here's a quick outline of the topics discussed during the 11 minute video: 

  * Live migration
  * clustered shared volumes
  * Hyper-V scalability
  * Hyper-V performance
  * Hyper-V footprint
  * Hardware support
  * Memory overcommit
  * End-to-end management
  * Value
  * Why pay VMWare's virtualization tax?



See the video below, or the other 22 videos [here](https://www.microsoft.com/video/en/us/related?video=c754f940-b5a1-43ee-99f1-d9e083dbc81a "Microsoft virtualization videos").

 

  
[Microsoft Mythbusters: Top 10 VMware Myths](https://www.microsoft.com/video/en/us/details/f8c3314f-c82d-4f8d-8b19-6a59733670f8?vp_evt=eref&vp_video=Microsoft+Mythbusters%3a+Top+10+VMware+Myths)

  Patrick