---
id: 5MSAYzSUP9Tp6rzqUuLBC6S
title: >-
    Container Images are now out for Windows Server version 1709
desc: >-
    Learn about some notable features of Windows Server version 1709 including container images and updated tagging schemes.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2017/20171018-container-images-are-now-out-for-windows-server-version-1709
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 29d9a311730d5fe98a62c1595edfa080058407803b4ed4e0f53360b8e33b5451
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2017/20171018-container-images-are-now-out-for-windows-server-version-1709.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2017-10-18 19:30:43"
    ms.date: "10/18/2017"
    categories: "baseos"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2017/20171018-container-images-are-now-out-for-windows-server-version-1709.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2017/20171018-container-images-are-now-out-for-windows-server-version-1709).

# Container Images are now out for Windows Server version 1709

With the release of Windows Server version 1709 also come Windows Server Core and Nano Server base OS container images. It is important to note that while older versions of the base OS container images will work on a newer host (with Hyper-V isolation), the opposite is not true. Container images based on Windows Server version 1709 will **not** work on a host using Windows Server 2016. [Read more about the different versions of Windows Server](https://docs.microsoft.com/en-us/windows-server/get-started/semi-annual-channel-overview "Container base image versioning"). We’ve also made some changes to our tagging scheme so you can more easily specify which version of the container images you want to use. From now on, the “latest” tag will follow the releases of the current LTSC product, Windows Server 2016. If you want to keep up with the latest patches for Windows Server 2016, you can use: “mcr.microsoft.com/windows/nanoserver” or “mcr.microsoft.com/windows/servercore” in your dockerfiles to get the most up-to-date version of the Windows Server 2016 base OS images. You can also continue using specific versions of the Windows Server 2016 base OS container images by using the tags specifying the build, like so: “mcr.microsoft.com/windows/nanoserver:10.0.17134.1305” or “windows/servercore:10.0.17134.1305. If you would like to use base OS container images based on Windows Server version 1709, you will have to specify that with the tag. In order to get the most up-to-date base OS container images of Windows Server version 1709, you can use the tags: “mcr.microsoft.com/windows/nanoserver” or “mcr.microsoft.com/windows/servercore” And if you would like a specific version of these base OS container images, you can specify the KB number that you need on the tag, like this: “mcr.microsoft.com/windows/nanoserver:1709_KB4043961” or “mcr.microsoft.com/windows/servercore:1709_KB4043961”. We hope that this tagging scheme will ensure that you always choose the image that you want and need for your environment. Please let us know in the comments if you have any feedback for us. **Note:** We currently do not intend to use the build numbers to specify Windows Server version 1709 container images. We will only be using the KB schema specified above for the tagging of these images. Let us know if you have feedback about this as well Regards, Ender