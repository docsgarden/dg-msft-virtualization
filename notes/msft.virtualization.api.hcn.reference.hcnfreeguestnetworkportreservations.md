---
id: 9U9fcnA3k868aZg3S7NhBcy
title: >-
    HcnFreeGuestNetworkPortReservations
desc: >-
    HcnFreeGuestNetworkPortReservations
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnFreeGuestNetworkPortReservations
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 70a2e5503f40bd0d1b31a1f4c4b5642928bd7154b4045a6cddae71b32ff16476
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnFreeGuestNetworkPortReservations.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnFreeGuestNetworkPortReservations']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnFreeGuestNetworkPortReservations.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnFreeGuestNetworkPortReservations).

# HcnFreeGuestNetworkPortReservations

## Description

Free port reservations.

## Syntax

```cpp
VOID
WINAPI
HcnFreeGuestNetworkPortReservations(
    _Inout_opt_ HCN_PORT_RANGE_ENTRY* PortEntries
    );
```

## Parameters

`PortEntries`

The list of [[`HCN_PORT_RANGE_ENTRY`|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_port_range_entry]] instances to free.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |