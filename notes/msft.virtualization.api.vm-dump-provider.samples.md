---
id: 5y8hT3o9nCo3PjNgCvKRezf
title: >-
    VM Saved State Dump Provider Samples
desc: >-
    Two samples written in C to demonstrate usage of the dump provider APIs.
canonicalUrl: https://docs.microsoft.com/virtualization/api/vm-dump-provider/samples
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: f642578bdf5f92ddc115114be28c268dd46c876d5da7393c63b65a0ba3d4de0f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/samples.md
    ms.date: "04/19/2022"
    author: "mattbriggs"
    ms.author: "mabrigg"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/samples.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/vm-dump-provider/samples).

# VM Saved State Dump Provider Samples

We've provided two samples written in C to demonstrate usage of the dump provider APIs.

## 1. Dump Sample

This sample demonstrates how to open a VM saved state file and query it for a variety of things (virtual processor count, architecture, etc.)

You can find the source code for this sample on our [Github repo here](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/live/virtualization/api/vm-dump-provider/samples/dumpsample.cpp).

## 2. Memory-to-file Sample

This sample demonstrates how to extract a memory dump from a VM's saved state file and output it to a new binary file.

You can find the source code for this sample on our [Github repo here](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/live/virtualization/api/vm-dump-provider/samples/rawmemtofile.cpp).