---
id: Akp4mQAQbnShybLZuy5JiGQ
title: >-
    Upgrading to Windows Server 2012 R2 with Hyper-V Replica
desc: >-
    This article provides basic steps to upgrade to Windows Server 2012 R2 with Hyper-V Replica.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2013/20131202-upgrading-to-windows-server-2012-r2-with-hyper-v-replica
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 7aadebb9bdab200e5bf597ce2fecd2817c03721d2befe442f9c82fd52b5df9cf
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20131202-upgrading-to-windows-server-2012-r2-with-hyper-v-replica.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "12/02/2013"
    categories: "hvr"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20131202-upgrading-to-windows-server-2012-r2-with-hyper-v-replica.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2013/20131202-upgrading-to-windows-server-2012-r2-with-hyper-v-replica).

# Upgrading to Windows Server 2012 R2

The TechNet article [**https://technet.microsoft.com/library/dn486799.aspx**](https://technet.microsoft.com/library/dn486799.aspx "https://technet.microsoft.com/library/dn486799.aspx") provides detailed guidance on migrating Hyper-V VMs from a Windows Server 2012 deployment to a Windows Server 2012 R2 deployment.

[ **https://technet.microsoft.com/library/dn486792.aspx**](https://technet.microsoft.com/library/dn486792.aspx "https://technet.microsoft.com/library/dn486792.aspx") calls out the various VM migration techniques which are available as part of upgrading your deployment. The section titled “Hyper-V Replica” calls out further guidance for deployments which have replicating virtual machines.

At a very high level, if you have a Windows Server 2012 setup containing replicating VMs, we recommend that you use the cross version live migration feature to migrate your replica VMs first. This is followed by fix-ups in the primary replicating VM (eg: changing replica server name). Once replication is back on track, you can migrate your primary VMs from a Windows Server 2012 server to a Windows Server 2012 R2 server without any VM downtime. The authorization table in the replica server may require to be updated once the primary VM migration is complete.

The above approach does not require you to re-IR your VMs, ensures zero downtime for your production VMs and gives you the flexibility to stagger the upgrade process on your replica and primary servers.