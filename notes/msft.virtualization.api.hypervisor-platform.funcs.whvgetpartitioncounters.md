---
id: 9RfCrNkjgyQhxTxPooN7aYa
title: >-
    WHvGetPartitionCounters
desc: >-
    Description on WHvGetPartitionCounters and understanding its parameters, syntax, and return value.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvGetPartitionCounters
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: fdf0f40521a64a45cda79d5ce8e2f4f50ef3dd3a45edb1d1a26170ccdfe21c55
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvGetPartitionCounters.md
    author: "jstarks"
    ms.author: "jostarks"
    ms.date: "06/06/2019"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvGetPartitionCounters.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvGetPartitionCounters).

# WHvGetPartitionCounters

## Syntax

```
typedef enum WHV_PARTITION_COUNTER_SET
{
    WHvPartitionCounterSetMemory,
} WHV_PARTITION_COUNTER_SET;

typedef struct WHV_PARTITION_MEMORY_COUNTERS
{
    UINT64 Mapped4KPageCount;
    UINT64 Mapped2MPageCount;
    UINT64 Mapped1GPageCount;
} WHV_PARTITION_MEMORY_COUNTERS;

HRESULT
WINAPI
WHvGetPartitionCounters(
    _In_ WHV_PARTITION_HANDLE Partition,
    _In_ WHV_PARTITION_COUNTER_SET CounterSet,
    _Out_writes_bytes_to_(BufferSizeInBytes, *BytesWritten) VOID* Buffer,
    _In_ UINT32 BufferSizeInBytes,
    _Out_opt_ UINT32* BytesWritten
    );
```

### Parameters

`Partition`

Specifies the partition to query.

`CounterSet`

Specifies the counter set to query.

`Buffer`

Specifies the buffer to write the counters into.

`BufferSizeInBytes`

Specifies `Buffer`'s size in bytes.

`BytesWritten`

If non-NULL, specifies a pointer that will be updated with the size of the counter set in bytes.

### Return Value

If the operation completed successfully, the return value is `S_OK`.

If an unrecognized value was passed for `CounterSet`, the return value is `WHV_E_UNKNOWN_PROPERTY`.