---
id: 8BNTYHz7XwqJyGispucnUuz
title: >-
    HcsEnumerateComputeSystems
desc: >-
    HcsEnumerateComputeSystems
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsEnumerateComputeSystems
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 90f3dc17d483dd41cf6508316f5103b301c493347b964c5e065b45954469eb8c
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsEnumerateComputeSystems.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsEnumerateComputeSystems']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsEnumerateComputeSystems.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsEnumerateComputeSystems).

# HcsEnumerateComputeSystems

## Description

Enumerates existing compute systems, see [[sample code|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.computesystemsample#enumcs]].

## Syntax

```cpp
HRESULT WINAPI
HcsEnumerateComputeSystems(
    _In_opt_ PCWSTR        query,
    _In_     HCS_OPERATION operation
    );
```

## Parameters

`query`

Optional JSON document of [[SystemQuery|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#SystemQuery]] specifying a query for specific compute systems.

`operation`

The handle to the operation that tracks the enumerate operation.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

If the return value is `S_OK`, it means the operation started successfully. Callers are expected to get the operation's result using [[`HcsWaitForOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]] or [[`HcsGetOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]]


## Operation Results

The return value of [[`HcsWaitForOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]] or [[`HcsGetOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]] based on current operation listed as below.

| Operation Result Value | Description |
| -- | -- |
| `S_OK` | The operation was finished successfully, the result document returned by the hcs operation is a JSON document representing an array of compute system [[Properties|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#Properties]] |


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |