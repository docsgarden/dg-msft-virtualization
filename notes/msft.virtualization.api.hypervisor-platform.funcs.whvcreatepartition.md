---
id: 32fxBFJYEEwpsMVnf8v4n3p
title: >-
    Create a new partition object
desc: >-
    Learn about the WHvCreatePartition function that creates a new partition object.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvCreatePartition
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: f672ff560831dfdd710fbf7eeb63594b3dfad73f520b15f62963c58f7f52f1ff
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvCreatePartition.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/20/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvCreatePartition.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvCreatePartition).

# WHvCreatePartition

## Syntax

```C
typedef VOID* WHV_PARTITION_HANDLE;

HRESULT
WINAPI
WHvCreatePartition(
    _Out_ WHV_PARTITION_HANDLE* Partition
    );
```

### Parameters

`Partition`

Receives the partition handle to the newly created partition object. All operations on the partition are performed through this handle.

To delete a partition created by `WHvCreatePartition`, use the [[`WHvDeletePartition`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-platform.funcs.whvdeletepartition]] function.
  

## Remarks

The `WHvCreatePartition` function creates a new partition object.

`WHvCreatePartition` only creates the partition object and does not yet create the actual partition in the hypervisor. After creation of the partition object, the partition object should be configured using [[`WHvSetPartitionProperty`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-platform.funcs.whvsetpartitionproperty]]. After the partition object is configured, the [[`WHvSetupPartition`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-platform.funcs.whvsetuppartition]] function should be called to create the hypervisor partition.