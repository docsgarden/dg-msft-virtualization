---
id: 3ScJFEMVB5eX4WLR2hHSJKN
title: >-
    HcnQueryEndpointProperties
desc: >-
    HcnQueryEndpointProperties
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnQueryEndpointProperties
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 6786cf79a420e4e8304bac4083fe7fdfdf495cc61561683c11af9823a7002369
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnQueryEndpointProperties.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnQueryEndpointProperties']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnQueryEndpointProperties.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnQueryEndpointProperties).

# HcnQueryEndpointProperties

## Description

Queries the properties of an endpoint.

## Syntax

```cpp
HRESULT
WINAPI
HcnQueryEndpointProperties(
    _In_ HCN_ENDPOINT Endpoint,
    _In_ PCWSTR Query,
    _Outptr_ PWSTR* Properties,
    _Outptr_opt_ PWSTR* ErrorRecord
    );
```

## Parameters

`Endpoint`

Handle to an endpoint [[`HCN_ENDPOINT`|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_endpoint]]

`Query`

Optional JSON document of [[HostComputeQuery|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.HNS_Schema#HostComputeQuery]].

`Properties`

The properties in the form of a JSON document of [[HostComputeEndpoint|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.HNS_Schema#HostComputeEndpoint]].

`ErrorRecord`

Receives a JSON document with extended errorCode information. The caller must release the buffer using CoTaskMemFree.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |