---
id: 79MnVdWZj7ppWQN6XkCcFrK
title: >-
    HcnCloseGuestNetworkService
desc: >-
    HcnCloseGuestNetworkService
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnCloseGuestNetworkService
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 592d4697a1a61cb073f4f86557e1076197858d8546c471ac2bb58adf32704776
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnCloseGuestNetworkService.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnCloseGuestNetworkService']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnCloseGuestNetworkService.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnCloseGuestNetworkService).

# HcnCloseGuestNetworkService

## Description

Close a handle to a guest network service handle.

## Syntax

```cpp
HRESULT
WINAPI
HcnCloseGuestNetworkService(
    _In_ HCN_GUESTNETWORKSERVICE GuestNetworkService
    );
```

## Parameters

`GuestNetworkService`

Handle to a guest network service [[`HCN_GUESTNETWORKSERVICE`|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_guestnetworkservice]]

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |