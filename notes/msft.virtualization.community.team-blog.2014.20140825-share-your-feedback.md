---
id: R9cp8nzZF3iDgaxsCFcBacz
title: >-
    Share your feedback
desc: >-
    Learn how to share your opinion to help influence the future of secure virtual infrastructure.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2014/20140825-share-your-feedback
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 65d6be8fa9e63e4211f24e874ab7a1586eebb1daf38b457a9c3f7ff5499b526f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140825-share-your-feedback.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2014-08-25 22:08:40"
    ms.date: "08/25/2014"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140825-share-your-feedback.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2014/20140825-share-your-feedback).

# Share your feedback

Are you a System Administrator or Security Analyst? Would you like to influence the future of securing your virtualized infrastructure? If this sounds interesting to you, Microsoft Windows Server and cloud management Program Managers would like to hear from you. 

**Please complete this**[ **short survey**](https://illumeweb.smdisp.net/collector/Survey.ashx?Name=SecurityWinSvrAug_2014) that will help identify your specific areas of interest and expertise to make sure our discussions fit your interests.