---
id: 9LiAqE9C7FFv2sabLRecSAY
title: >-
    Virtualization Community Resources
desc: >-
    Virtualization Community Resources
canonicalUrl: https://docs.microsoft.com/virtualization/community/index
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 1cdb33485631998fcf055b74b9645a137fea35fec459bc10cc61ff4f7e4a6a3b
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/index.md
    keywords: "windows 10, hyper-v, container, docker"
    author: "scooley"
    ms.author: "scooley"
    ms.date: "09/28/2018"
    ms.topic: "article"
    ms.prod: "virtualization"
    ms.assetid: "9ee0be0c-fc79-4099-b6db-52873589f09e"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/index.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/index).

# Virtualization Community

Welcome to the Windows virtualization community!

## Read the team blog

Follow the latest stories, developments, and projects from the Hyper-V team, follow our team blog.

## Contribute to open source virtualization tools

* [Docker for Windows](https://github.com/Microsoft/docker) on GitHub
* [This documentation site and samples](https://github.com/Microsoft/Virtualization-Documentation) on GitHub

## Talk to other virtualization users

* [Windows Containers forum](https://social.msdn.microsoft.com/Forums/en-US/home?forum=windowscontainers)
* [TechNet forum](https://social.technet.microsoft.com/Forums/windowsserver/en-US/home "TechNet Forums")
* [Azure forum](https://azure.microsoft.com/support/forums/)

## Featured content

* [//build 2016 Container Announcements: Hyper-V Containers and Windows 10 and PowerShell For Docker!](https://blogs.technet.microsoft.com/virtualization/2016/04/01/build-2016-container-announcements-hyper-v-containers-and-windows-10-and-powershell-for-docker/)
* [Announcing the release of Hyper-V Containers in Windows Server 2016 Technical Preview 4](https://blogs.technet.com/b/virtualization/archive/2015/11/19/announcing-the-release-of-hyper-v-containers-in-windows-server-2016-technical-preview-4.aspx)
* [Early look at containers in Windows Server, Hyper-V and Azure – with Mark Russinovich](https://youtu.be/YoA_MMlGPRc)
* [Announcing Windows Server 2016 Containers Preview](https://weblogs.asp.net/scottgu/announcing-windows-server-2016-containers-preview)
* [Docker -Introducing the Technical Preview of Docker Enging for Windows Server 2016](https://blog.docker.com/2015/08/tp-docker-engine-windows-server-2016/)
* [Containers 101 Video](https://channel9.msdn.com/Blogs/containers/Containers-101-with-Microsoft-and-Docker)
* [Containers: Docker, Windows and Trends](https://azure.microsoft.com/blog/2015/08/17/containers-docker-windows-and-trends/)

### Blogs 
* [Channel 9 - The Containers Channel](https://channel9.msdn.com/Blogs/containers)
* [Server & Cloud blog](https://blogs.technet.com/b/server-cloud/)
* [Azure blog](https://azure.microsoft.com/blog/)

### Presentations
* [Build 2015 - Day One Keynote Presentation](https://channel9.msdn.com/Events/Build/2015/KEY01)
* [Build 2015 - Windows Containers: What, Why and How](https://channel9.msdn.com/events/Build/2015/2-704)
* [Build 2015 - Thinking in Containers: Building a Scalable, Next-Gen Application with Docker on Azure](https://channel9.msdn.com/events/Build/2015/2-683)
* [Build 2015 - Deploying Complex Open Source Workloads on Azure](https://channel9.msdn.com/Events/Build/2015/2-732)

### Articles 
* [Virtual Machines and Containers in Azure](https://azure.microsoft.com/documentation/articles/virtual-machines-vms-containers/)
* [New Windows Server containers and Azure support for Docker](https://azure.microsoft.com/blog/2014/10/15/new-windows-server-containers-and-azure-support-for-docker/)
* [Microsoft Announces New Container Technologies for the Next Generation Cloud](https://blogs.technet.com/b/server-cloud/archive/2015/04/08/microsoft-announces-new-container-technologies-for-the-next-generation-cloud.aspx)