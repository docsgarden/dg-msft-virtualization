---
id: 8hRDcPC2Mg9TjqYEw2JqooF
title: >-
    WHvSetVirtualProcessorRegisters
desc: >-
    Description for WHvSetVirtualProcessorRegisters and understaning how to work with its parameters and syntax.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvSetVirtualProcessorRegisters
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: c2472ef14e01349faf287ae5cbef3d53ec3af5f5bcbd702b1d45cec94fded115
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvSetVirtualProcessorRegisters.md
    author: "Juarezhm"
    ms.author: "hajuarez"
    ms.date: "06/22/2018"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvSetVirtualProcessorRegisters.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvSetVirtualProcessorRegisters).

# WHvSetVirtualProcessorRegisters

## Syntax

```C
HRESULT
WINAPI
WHvSetVirtualProcessorRegisters(
    _In_ WHV_PARTITION_HANDLE Partition,
    _In_ UINT32 VpIndex,
    _In_reads_(RegisterCount) const WHV_REGISTER_NAME* RegisterNames,
    _In_ UINT32 RegisterCount,
    _In_reads_(RegisterCount) const WHV_REGISTER_VALUE* RegisterValues
    );
```

### Parameters

`Partition`

Handle to the partition object

`VpIndex`

Specifies the index of the virtual processor whose registers are set

`RegisterNames`

Array specifying the names of the registers that are set

`RegisterCount`

Specifies the number of elements in the `RegisterNames` array

`RegisterValues`

Array specifying the values of the registers that are set