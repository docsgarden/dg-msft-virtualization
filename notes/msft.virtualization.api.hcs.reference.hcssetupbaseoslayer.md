---
id: Aqp2Q3dWhVHqEtsmpGvcZ9M
title: >-
    HcsSetupBaseOSLayer
desc: >-
    HcsSetupBaseOSLayer
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsSetupBaseOSLayer
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: bc93a27cc249d48d70ff6f1d5a2d324d97e2036afc440aa84a91508d1b90767d
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsSetupBaseOSLayer.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsSetupBaseOSLayer']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsSetupBaseOSLayer.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsSetupBaseOSLayer).

# HcsSetupBaseOSLayer

## Description

This function sets up a base OS layer for the use on a host. The base OS layer is the first layer in the set of layers used by a container or virtual machine.

## Syntax

```cpp
HRESULT WINAPI
HcsSetupBaseOSLayer(
    _In_ PCWSTR layerPath,
    _In_ HANDLE vhdHandle,
    _In_ PCWSTR options
    );
```

## Parameters

`layerPath`

Path to the root of the base OS layer.

`vhdHandle`

The handle to a VHD.

`options`

Optional JSON document  of [[OsLayerOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#OsLayerOptions]] describing options for setting up the layer.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeStorage.h |
| **Library** | ComputeStorage.lib |
| **Dll** | ComputeStorage.dll |