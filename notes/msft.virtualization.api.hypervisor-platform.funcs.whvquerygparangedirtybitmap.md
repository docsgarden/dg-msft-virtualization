---
id: 9cmqqJg4BajspCmLzVTQV2Z
title: >-
    WHvQueryGpaRangeDirtyBitmap
desc: >-
    Description for understanding parameters, return value and remarks when working with WHvQueryGpaRangeDirtyBitmap
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvQueryGpaRangeDirtyBitmap
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 16d4fae1270efab2d7478df08bd91663cf184b08d8366f24a35dec985b3b7910
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvQueryGpaRangeDirtyBitmap.md
    author: "jstarks"
    ms.author: "jostarks"
    ms.date: "06/06/2019"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvQueryGpaRangeDirtyBitmap.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvQueryGpaRangeDirtyBitmap).

# WHvQueryGpaRangeDirtyBitmap

## Syntax

```
HRESULT
WINAPI
WHvQueryGpaRangeDirtyBitmap(
    _In_ WHV_PARTITION_HANDLE Partition,
    _In_ WHV_GUEST_PHYSICAL_ADDRESS GuestAddress,
    _In_ UINT64 RangeSizeInBytes,
    _Out_writes_bytes_opt_(BitmapSizeInBytes) UINT64* Bitmap,
    _In_ UINT32 BitmapSizeInBytes
    );
```

### Parameters

`Partition`

Specifies the partition to query.

`GuestAddress`

Specifies the guest physical address, in bytes, of the beginning of the range to query. This address must be page-aligned.

`RangeSizeInBytes`

Specifes the size of the range to query, in bytes.

`Bitmap`

If non-NULL, specifies a pointer to the bitmap to write to. The bitmap must be 8-byte aligned.

`BitmapSizeInBytes`

Specifies the size of the bitmap, in bytes. The bitmap must be large enough to include one bit per each page, rounded up to an 8-byte value.

If zero, then `Bitmap` can be NULL. This indicates that the range's dirty state should be cleared without querying the current state.

## Return Value

If the function succeeds, the return value is `S_OK`.

If the specified address range has not been mapped or has not been registered for dirty range tracking, the return value is `WHV_E_GPA_RANGE_NOT_FOUND`.

## Remarks

To use this function for a given address range, that address range must be mapped with [[WHvMapGpaRange|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-platform.funcs.whvmapgparange]], specifying the `WHvMapGpaRangeFlagTrackDirtyPages` flag.