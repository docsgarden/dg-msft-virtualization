---
id: mWt4tZ9nTbVVCaD5sc83jP3
title: >-
    WHvGetVirtualProcessorXsaveState
desc: >-
    Description on working with WHvGetVirtualProcessorXsaveState, including parameters, syntax, and return value.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvGetVirtualProcessorXSaveState
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 6bb2dbbae51aecd56301b7f85bf44fce1fc897f1b417baae489bc7f4332f13ff
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvGetVirtualProcessorXSaveState.md
    author: "jstarks"
    ms.author: "jostarks"
    ms.date: "06/06/2019"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvGetVirtualProcessorXSaveState.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvGetVirtualProcessorXSaveState).

# WHvGetVirtualProcessorXsaveState

## Syntax

```
HRESULT
WINAPI
WHvGetVirtualProcessorXsaveState(
    _In_ WHV_PARTITION_HANDLE Partition,
    _In_ UINT32 VpIndex,
    _Out_writes_bytes_to_(BufferSizeInBytes, *BytesWritten) VOID* Buffer,
    _In_ UINT32 BufferSizeInBytes,
    _Out_ UINT32* BytesWritten
    );
```

### Parameters

`Partition`

Specifies the partition of the virtual processor.

`VpIndex`

Specifies the index of the virtual processor whose XSAVE state should be queried.

`Buffer`

Specifies the buffer to receive the virtual processor's XSAVE state.

`BufferSizeInBytes`

Specifies the size of the buffer, in bytes.

`BytesWritten`

Receives the number of bytes written to the buffer.

## Return Value

If the function succeeds, the return value is `S_OK`.

If the buffer is not large enough, the return value is `WHV_E_INSUFFICIENT_BUFFER`. In this case, `BytesWritten` receives the required buffer size.