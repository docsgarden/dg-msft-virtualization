---
id: 7SFuqTmWNMFHCgzeGtcpJFH
title: >-
    HvCallFlushGuestPhysicalAddressSpace
desc: >-
    HvCallFlushGuestPhysicalAddressSpace hypercall
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallFlushGuestPhysicalAddressSpace
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: d6fab85930f904c92f483535fe503b87aa553ef064240267f5496a966b278aaf
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallFlushGuestPhysicalAddressSpace.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallFlushGuestPhysicalAddressSpace.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallFlushGuestPhysicalAddressSpace).

# HvCallFlushGuestPhysicalAddressSpace

The HvCallFlushGuestPhysicalAddressSpace hypercall invalidates cached L2 GPA to GPA mappings within a second level address space.

## Interface

 ```c
HV_STATUS
HvCallFlushGuestPhysicalAddressSpace(
    _In_ HV_SPA AddressSpace,
    _In_ UINT64 Flags
    );
 ```

This hypercall can only be used with nested virtualization is active. The virtual TLB invalidation operation acts on all processors.

On Intel platforms, the HvCallFlushGuestPhysicalAddressSpace hypercall is like the execution of an INVEPT instruction with type “single-context” on all processors.

This call guarantees that by the time control returns to the caller, the observable effects of all flushes have occurred.
If the TLB is currently “locked”, the caller’s virtual processor is suspended.

## Call Code

`0x00AF` (Simple)

## Input Parameters

| Name                    | Offset     | Size     | Information Provided                      |
|-------------------------|------------|----------|-------------------------------------------|
| `AddressSpace`          | 0          | 8        | Specifies an address space ID (EPT PML4 table pointer). |
| `Flags`                 | 8          | 8        | RsvdZ                                     |