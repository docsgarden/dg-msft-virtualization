---
id: 7MvVACRy3cVsj8mt89JBJKt
title: >-
    HCN_NETWORK
desc: >-
    HCN_NETWORK
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_NETWORK
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 72422bdc261333b1cce3fcf58c018754196c2aee5cae683ccf0c917df5befc58
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_NETWORK.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HCN_NETWORK']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_NETWORK.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_NETWORK).

# HCN_NETWORK

## Description

Context handle referencing a network in HNS.


## Syntax

```cpp
typedef void* HCN_NETWORK;
```

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |