---
id: 96DXnDjGt6zmqkxHFKafk58
title: >-
    Host Service Samples
desc: >-
    Host Service Samples
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/ServiceSample
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: eafc924ba7f8e5574274ec97d4663191e3241aa0506052648b9ef6bd04cd535a
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/ServiceSample.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['Host Service Samples']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/ServiceSample.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/ServiceSample).

# Host Service Samples

<a name = "GetServiceProperties"></a>
## Get the properties of host service

Here takes the memory property in the service as the example.

```cpp
    static constexpr wchar_t c_ServicePropertyQuery[[] = LR"(
    {
        "PropertyTypes": [
            "Memory"
        ]
    })";

    wil::unique_hlocal_string resultDoc;
    THROW_IF_FAILED(HcsGetServiceProperties(c_ServicePropertyQuery, &resultDoc));
    std::wcout << L"Service property is " << resultDoc.get() << std::endl;
```

<a name = "ModifyServiceSettings"></a>
## Modify the service settings

Here takes the CPUGroup property in the service as the example.

```cpp
    static constexpr wchar_t c_ServiceSettings[] = LR"(
    {
        "PropertyType": "CpuGroup",
        "Settings": {
            "Operation": "CreateGroup",
            "OperationDetails": {
                "GroupId": "GUID",
                "LogicalProcessorCount": 2,
                "LogicalProcessors": [0, 1]
            }
        }
    })";

    wil::unique_hlocal_string resultDoc;
    HRESULT hr = HcsModifyServiceSettings(c_ServiceSettings, &resultDoc);
    if (FAILED(hr))
    {
        std::wcout << resultDoc.get() << std::endl;
    }
    THROW_IF_FAILED(hr);
```

<a name = "SubmitReport"></a>
## Submit Crash Report

```cpp
    // Assume you have a valid unique_hcs_system object
    // to a newly created compute system.
    // We set compute system callbacks to wait specifically
    // for a crash system report.
    THROW_IF_FAILED(HcsSetComputeSystemCallback(
        system.get(), // system handle
        nullptr, // context
        [|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_event* event, void*]]
        {
            if (Event->Type == HcsEventSystemCrashReport)
            {
                THROW_IF_FAILED(HcsSubmitWerReport(Event->EventData));
            }
        }));
```