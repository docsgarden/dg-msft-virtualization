---
id: 8oY2WKdyM8RBKttzRLZXe2T
title: >-
    HcsImportLayer
desc: >-
    HcsImportLayer
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsImportLayer
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 00b93e9ee067cb14e0d329428dd3529a20da92180e3ee1068968afb0b4c1bd07
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsImportLayer.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsImportLayer']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsImportLayer.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsImportLayer).

# HcsImportLayer

## Description

This function imports a container layer and sets it up for use on boot. This function is used by an application to setup a container layer that was copied in a transport format to the host (e.g was downloaded from a container registry such as DockerHub). 

## Syntax

```cpp
HRESULT WINAPI
HcsImportLayer(
    _In_ PCWSTR layerPath,
    _In_ PCWSTR sourceFolderPath,
    _In_ PCWSTR layerData
    );
```

## Parameters

`layerPath`

Destination path for the container layer.

`sourceFolderPath`

Source path that contains the downloaded layer files.

`layerData`

JSON document of [[layerData|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#LayerData]] providing the locations of the antecedent layers that are used by the imported layer.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeStorage.h |
| **Library** | ComputeStorage.lib |
| **Dll** | ComputeStorage.dll |