---
id: 42h8cSSMCWATVvgofaoZ96S
title: >-
    Integration components available for virtual machines not connected to Windows Update
desc: >-
    Learn about the available integration components for virtual machines not connected to a Windows update using Hyper-V.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2015/20150724-integration-components-available-for-virtual-machines-not-connected-to-windows-update
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: e414329fcad4d31d4a3273b475f50d875c488806c8edf0d32ae05430fe43d256
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2015/20150724-integration-components-available-for-virtual-machines-not-connected-to-windows-update.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2015-07-24 10:42:00"
    ms.date: "07/24/2015"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2015/20150724-integration-components-available-for-virtual-machines-not-connected-to-windows-update.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2015/20150724-integration-components-available-for-virtual-machines-not-connected-to-windows-update).

# Integration components available for virtual machines not connected to Windows Update

In Technical Preview, Hyper-V began delivering integration components through Windows Update ([see blog for more information](https://docs.microsoft.com/en-us/b/virtualization/archive/2014/11/11/hyper-v-integration-components-are-available-through-windows-update.aspx "see blog for more information")). Pushing updates through Windows Update was a good first step -- it is the easiest way to keep integration components up to date. There are situations, however, where the virtual machine isn't connected to Windows Update and sometimes it is more convenient to patch an offline (turned off) virtual machine. Now, in addition to receiving integration component updates automatically through Windows Update, you can also update integration components on virtual machines that aren't running or aren't connected to Windows Update using the cab files available in [KB3071740](https://support.microsoft.com/en-us/kb/3071740). (Last time I looked, the download links weren't working. The downloads are [here](https://www.microsoft.com/en-us/search/Results.aspx?q=kb3071740&form=DLC)). ****** **Note:** Everything in this blog post applies to Server 2016 Technical Preview or Windows 10 and associated preview builds or later. The instructions here should work for virtual machines on Server 2012R2/Windows 8.1 but that is not tested or supported! ** 

## Updating integration components on an virtual machine that is not turned on

Here's a script that updates integration components. It assumes you are updating a virtual machine from the Hyper-V host. If the virtual machine is running, it will need to be stopped (the script below does this for you) and you'll need to locate its VHD (virtual hard drive). ** For step by step instructions with explanations read [this post ](https://docs.microsoft.com/en-us/b/virtualization/archive/2013/04/19/how-to-install-integration-services-when-the-virtual-machine-is-not-running.aspx)but use the cabs from the KB article. Run the following in PowerShell as administrator. You will need the path to the cab file that matches the operating system running in your virtual machine and the path to your VHD. 

## Update integration components inside the virtual machine without using Windows Update

These instructions assume you are running on the virtual machine you want to update. First, find the cab file that matches the operating system running in your virtual machine and download it. Run the following in PowerShell as administrator. Remember to set the right path to the downloaded cab file.  $integrationServicesCabPath="C:\Users\sarah\Downloads\windows6.2-hypervintegrationservices-x86.cab"  #Install the patch Add-WindowsPackage -Online -PackagePath $integrationServicesCabPath Now your virtual machines can all have the latest integration components! Cheers, Sarah