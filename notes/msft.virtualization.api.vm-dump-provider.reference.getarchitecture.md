---
id: 7XVn2nqAeC5aZzZmqnVkp2n
title: >-
    The GetArchitecture function
desc: >-
    Learn about the GetArchitecture function.
canonicalUrl: https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/GetArchitecture
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: b11b38d8cc9d92e0943afb418dc8551e77b2bdd9cb9b33180dba38c05cea98db
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/GetArchitecture.md
    ms.date: "04/18/2022"
    author: "mattbriggs"
    ms.author: "mabrigg"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/GetArchitecture.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/GetArchitecture).

# GetArchitecture function

Queries for the current Architecture/ISA the virtual processor was running at the time the saved state file was generated.

## Syntax

```C
HRESULT
WINAPI
GetArchitecture(
    _In_    VM_SAVED_STATE_DUMP_HANDLE      VmSavedStateDumpHandle,
    _In_    UINT32                          VpId,
    _Out_   VIRTUAL_PROCESSOR_ARCH*         Architecture
    );
```
## Parameters

`VmSavedStateDumpHandle`

Supplies a handle to a dump provider instance.

`VpId`

Supplies the VP to query.

`Architecture`

Returns the architecture of the supplied vp.

## Return Value

If the operation completes successfully, the return value is `S_OK`.

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |