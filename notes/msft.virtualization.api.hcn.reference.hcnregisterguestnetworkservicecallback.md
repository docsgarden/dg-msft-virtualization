---
id: 64En7mmagpvGnhETjuKDRfE
title: >-
    HcnRegisterGuestNetworkServiceCallback
desc: >-
    HcnRegisterGuestNetworkServiceCallback
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnRegisterGuestNetworkServiceCallback
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 45b0e654d09bac976bff6a24b8d0062c5c1abe64ed3f64342b888a7a086340e5
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnRegisterGuestNetworkServiceCallback.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnRegisterGuestNetworkServiceCallback']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnRegisterGuestNetworkServiceCallback.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnRegisterGuestNetworkServiceCallback).

# HcnRegisterGuestNetworkServiceCallback

## Description

Registers a callback function to receive GuestNetworkService notifications.

## Syntax

```cpp
HRESULT
WINAPI
HcnRegisterGuestNetworkServiceCallback(
    _In_ HCN_GUESTNETWORKSERVICE GuestNetworkService,
    _In_ HCN_NOTIFICATION_CALLBACK Callback,
    _In_ void* Context,
    _Outptr_ HCN_CALLBACK* CallbackHandle
    );
```

## Parameters

`GuestNetworkService`

The [[HCN_GUESTNETWORKSERVICE|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_guestnetworkservice]] for the callback.

`Callback`

The [[HCN_NOTIFICATION_CALLBACK|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_notification_callback]] for the callback.

`Context`

Context that is provided on the callbacks.

`CallbackHandle`

Receives a [[HCN_CALLBACK|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_callback]]. It is the responsibility of the caller to release the handle using [[HcnUnregisterGuestNetworkServiceCallback|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnunregisterguestnetworkservicecallback]] once it is no longer in use.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |