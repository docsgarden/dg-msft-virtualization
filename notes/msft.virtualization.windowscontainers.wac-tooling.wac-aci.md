---
id: B2wacVa59e7D4wxK72yV9G3
title: >-
    Manage Azure Container Instances on Windows Admin Center
desc: >-
    Azure Container Instances on Windows Admin Center
canonicalUrl: https://docs.microsoft.com/virtualization/windowscontainers/wac-tooling/wac-aci
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: f02ebc562f0015f50c32efd691934f365673d69cf8ad377fca760693a001dbeb
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/windowscontainers/wac-tooling/wac-aci.md
    keywords: "docker, containers, Windows Admin Center"
    author: "vrapolinario"
    ms.author: "viniap"
    ms.date: "12/24/2020"
    ms.topic: "tutorial"
    ms.assetid: "bb9bfbe0-5bdc-4984-912f-9c93ea67105f"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/windowscontainers/wac-tooling/wac-aci.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/windowscontainers/wac-tooling/wac-aci).

# Manage Azure Container Instances using Windows Admin Center

This topic describes how to manage Azure Container Instances (ACI) using Windows Admin Center. Azure Container Instances is a solution for any scenario that can operate in isolated containers, without orchestration.

> **Note:**
>An Azure subscription is required to run the steps in this tutorial. For more information on how to connect your Windows Admin Center instance to Azure, see [Configuring Azure integration](https://docs.microsoft.com/en-us/windows-server/manage/windows-admin-center/azure/azure-integration).

Windows Admin Center allows you to perform basic management of Azure Container Instances, which includes listing existing container instances, starting and stopping an instance, removing instances, and opening the Azure Portal for advanced management.

![Azure container instance]([Azure container instance|dendron://dg-msft-https://docs.microsoft.com/virtualization/msft.https://docs.microsoft.com/virtualization/windowscontainers/wac-tooling/media/wac-aci.png]])

With the container instance, you can perform the following actions:

- **Start**: To start an existing instance that is currently stopped.
- **End**: To stop a running instance.
- **Delete**: To delete an instance. This is irreversible and removes any configuration that's been made to the instance.
- **Manage in Azure**: This opens the **Azure Portal** pane to manage the selected container instance on a new browser tab.

## Next steps

> [[!div class="nextstepaction"]
> [Manage Azure Container Registry on Windows Admin Center|dendron://dg-msft-virtualization/msft.virtualization.windowscontainers.wac-tooling.wac-acr]]