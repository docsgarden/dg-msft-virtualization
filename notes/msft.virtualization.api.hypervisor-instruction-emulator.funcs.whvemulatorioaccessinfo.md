---
id: 3yp7cWWdqpnVRToUSaPTWgk
title: >-
    WHV_EMULATOR_IO_ACCESS_INFO method
desc: >-
    Learn about the WHV_EMULATOR_IO_ACCESS_INFO method.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorIOAccessInfo
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 1e19786424a3e4b55675e1ccff31c6782429ca77fe0864d36c38d57cac746fd3
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorIOAccessInfo.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/19/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorIOAccessInfo.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorIOAccessInfo).

# WHV_EMULATOR_IO_ACCESS_INFO


## Syntax

```c
typedef struct _WHV_EMULATOR_IO_ACCESS_INFO {
    UINT8 Direction; // 0 for in, 1 for out
    UINT16 Port;
    UINT16 AccessSize; // only 1, 2, 4
    UINT32 Data[4];
} WHV_EMULATOR_IO_ACCESS_INFO;
```
## Remarks
Information about the requested Io Port access by the emulator.

`Direction` is the same as for memory, a 0 for a read and 1 for a write.

`Port` is the Io Port the emulator is attempting to access.

`AccessSize` is the same as described above for memory, but limited to values of 1, 2 or 4.

`Data` is the data read/written to the Io Port, casted to the appropriate size value (UINT8, UINT16, UINT32).