---
id: 5iznVJ5gzhKcga9jRqLLcd7
title: >-
    HcnUnregisterGuestNetworkServiceCallback
desc: >-
    HcnUnregisterGuestNetworkServiceCallback
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnUnregisterGuestNetworkServiceCallback
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 67ebdbd2aa8df99627d3ab5f3be390ca9e2b78fe19216577898923a5f5b40302
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnUnregisterGuestNetworkServiceCallback.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnUnregisterGuestNetworkServiceCallback']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnUnregisterGuestNetworkServiceCallback.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnUnregisterGuestNetworkServiceCallback).

# HcnUnregisterGuestNetworkServiceCallback

## Description

Unregisters a guest network service callback.

## Syntax

```cpp
HRESULT
WINAPI
HcnUnregisterGuestNetworkServiceCallback(
    _In_ HCN_CALLBACK CallbackHandle
    );
```

## Parameters

`CallbackHandle`

The [[HCN_CALLBACK|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_callback]] for the callback.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |