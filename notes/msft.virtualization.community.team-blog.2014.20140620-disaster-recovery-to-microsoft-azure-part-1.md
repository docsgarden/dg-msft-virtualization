---
id: 5iEn2Ecs6FpsMHzaTknTJtD
title: >-
    Disaster Recovery to Microsoft Azure – Part 1
desc: >-
    This article discusses protecting, replicating, and failover VMs directly to Microsoft Azure, Part 1.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2014/20140620-disaster-recovery-to-microsoft-azure-part-1
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 9301fdf53dd2bdb9495435faa9f8e20ebb2d7a6c602ace73a66532c7998c9fb4
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140620-disaster-recovery-to-microsoft-azure-part-1.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "06/20/2014"
    date: "2014-06-20 04:34:43"
    categories: "asr"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140620-disaster-recovery-to-microsoft-azure-part-1.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2014/20140620-disaster-recovery-to-microsoft-azure-part-1).

# Disaster Recovery to Microsoft Azure - PArt 1

Drum roll please! 

We are super excited to announce the availability of the preview bits of [**Azure Site Recovery**](https://azure.microsoft.com/services/site-recovery/) **(ASR)** which enables you to replicate Hyper-V VMs to Microsoft Azure for business continuity and disaster recovery purposes. 

You can now protect, replicate, and failover VMs directly to Microsoft Azure – our guarantee remains that whether you enable Disaster Recovery across On-Premise Enterprise Private Clouds or directly to Azure, your virtualized workloads will be recovered **_accurately_ , _consistently_ , _with_ _minimal downtime and with minimal data loss._**

ASR supports Automated Protection and Replication of VMs, customizable Recovery Plans that enable One-Click Recovery, No-Impact Recovery Plan Testing (ensures that you meet your Audit and Compliance requirements), and best-in-class Security and Privacy features that offer maximum resilience to your business critical applications. All this with minimal cost and without the need to invest in a recovery datacenter. To know more about this announcement and what we have enabled in the Preview, check out ****[**Brad Anderson ’s** In the Cloud blog](https://blogs.technet.com/b/in_the_cloud/archive/2014/06/19/announcing-the-preview-of-disaster-recovery-to-azure-using-azure-site-recovery.aspx). 

We will cover this feature in detail in the coming weeks – stay tuned and try out the feature. We love to hear your feedback!