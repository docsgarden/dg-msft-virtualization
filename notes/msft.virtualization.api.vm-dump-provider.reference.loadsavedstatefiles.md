---
id: KF62E52ZHDBb45NVt5YscWa
title: >-
    The LoadSavedStateFiles function
desc: >-
    Loads the given saved state files and creates an instance of VmSavedStateDump. This instance can be referenced on the other methods with the returned UINT64 Id.
canonicalUrl: https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/LoadSavedStateFiles
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 41d28fd810e8d7344fe611cc34c84bc63703fa6613f76763c0d004595e55def8
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/LoadSavedStateFiles.md
    ms.date: "04/19/2022"
    author: "mattbriggs"
    ms.author: "mabrigg"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/LoadSavedStateFiles.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/LoadSavedStateFiles).

# LoadSavedStateFiles function

Loads the given saved state files and creates an instance of VmSavedStateDump. This instance can be referenced on the other methods with the returned UINT64 Id.

## Syntax

```C
HRESULT
WINAPI
LoadSavedStateFiles(
    _In_    LPCWSTR                         BinFile,
    _In_    LPCWSTR                         VsvFile,
    _Out_   VM_SAVED_STATE_DUMP_HANDLE*     VmSavedStateDumpHandle
    );
```

## Parameters

`BinFile`

Supplies the path to the BIN file to load.

`VsvFile`

Supplies the path to the VSV file to load.

`VmSavedStateDumpHandle`

Returns the ID for the dump provider instance created.

## Return Value

If the operation completes successfully, the return value is `S_OK`.

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |