---
id: 9RHZHgU8UUHq2KtLehNGkiC
title: >-
    HcnReleaseGuestNetworkServicePortReservationHandle
desc: >-
    HcnReleaseGuestNetworkServicePortReservationHandle
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnReleaseGuestNetworkServicePortReservationHandle
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 9295eae1e1c04a544391e804b92bb0b6410be383ee0219ebe072368c3c108cfe
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnReleaseGuestNetworkServicePortReservationHandle.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnReleaseGuestNetworkServicePortReservationHandle']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnReleaseGuestNetworkServicePortReservationHandle.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnReleaseGuestNetworkServicePortReservationHandle).

# HcnReleaseGuestNetworkServicePortReservationHandle

## Description

Releases a port reservation handle.

## Syntax

```cpp
HRESULT
WINAPI
HcnReleaseGuestNetworkServicePortReservationHandle(
    _In_ HANDLE PortReservationHandle
    );
```

## Parameters

`PortReservationHandle`

The handle to the reservations to release.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |