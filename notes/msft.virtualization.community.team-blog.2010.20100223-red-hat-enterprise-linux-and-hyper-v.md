---
id: 8DqAzAocgiFdPi2CTTT5qrH
title: >-
    Red Hat Enterprise Linux and Hyper-V
desc: >-
    Red Hat Enterprise Linux and Hyper-V
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100223-red-hat-enterprise-linux-and-hyper-v
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 328b9b1bebfea64140b0c648c0ddd73fca59a823bda5c2f7d90dadb3715e7501
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100223-red-hat-enterprise-linux-and-hyper-v.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "02/23/2010"
    date: "2010-02-23 10:08:00"
    categories: "hyper-v-linux"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100223-red-hat-enterprise-linux-and-hyper-v.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100223-red-hat-enterprise-linux-and-hyper-v).

# Red Hat Enterprise Linux and Hyper-V

Hyper-V customers are running both SUSE Linux Enterprise Server and Red Hat Enterprise Linux as guests. We have provided Linux integration components for SUSE Linux Enterprise Server, but customers did not have the same level of performance with Red Hat Enterprise Linux as a guest since the IC’s were not supported for RHEL. 

 

We are excited to announce the availability of Linux integration components for Red Hat Enterprise Linux (RHEL 5.2, 5.3, and 5.4) which provides synthetic network and storage drivers enabling RHEL to work with the optimized devices provided by Hyper-V.  We’ve already submitted these drivers to the upstream Linux kernel in July 2009 (read [here](https://blogs.technet.com/virtualization/archive/2009/07/20/linux-ics-for-hyper-v-and-gplv2.aspx) for more information) and are looking forward to these being integrated with a future version of RHEL.  In the meantime, Microsoft will provide full support for these drivers.  Red Hat provides best effort support for these components. Customers interested in understanding how these are supported by Red Hat prior to their inclusion natively into to their distribution can read the details at the [Red Hat Knowledge Base article](http://kbase.redhat.com/faq/docs/DOC-1068).

 

To download this new version of the Linux Integration Components, visit this [link](https://www.microsoft.com/downloads/details.aspx?displaylang=en&FamilyID=c299d675-bb9f-41cf-b5eb-74d0595ccc5c) on the Microsoft Download Center.

 

Mike Sterling

Hyper-V Program Manager, Microsoft