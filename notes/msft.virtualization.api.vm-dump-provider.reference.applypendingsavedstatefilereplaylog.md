---
id: AcFYwebdQGAQCeteGSGoR8k
title: >-
    The ApplyPendingSavedStateFileReplayLog function
desc: >-
    The ApplyPendingSavedStateFileReplayLog function opens the given saved state file in read-write exclusive mode so that it applies any pending replay logs to the contents.
canonicalUrl: https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/ApplyPendingSavedStateFileReplayLog
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: c54845e4d7b8a2b4b13fa863ca2af7994c17d26860cc2b9b56e1b10060dda605
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/ApplyPendingSavedStateFileReplayLog.md
    ms.date: "04/18/2022"
    author: "mattbriggs"
    ms.author: "mabrigg"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/ApplyPendingSavedStateFileReplayLog.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/ApplyPendingSavedStateFileReplayLog).

# ApplyPendingSavedStateFileReplayLog function

Opens the given saved state file in read-write exclusive mode so that it applies any pending replay logs to the contents. This method doesn't loads the saved state file into the library and can't be used to get content data; function LoadSavedStateFile must be used instead.

## Syntax

```C
HRESULT
WINAPI
ApplyPendingSavedStateFileReplayLog(
    _In_    LPCWSTR                         VmrsFile
    );
```

## Parameters

`VmrsFile`

Supplies the path to the VMRS file whose any pending replay log will be applied.

## Return Value

If the operation completes successfully, the return value is `S_OK`.

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |