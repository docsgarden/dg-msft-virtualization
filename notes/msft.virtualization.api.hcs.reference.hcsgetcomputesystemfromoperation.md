---
id: V32PodAb22GsrWzVpmqAUQw
title: >-
    HcsGetComputeSystemFromOperation
desc: >-
    HcsGetComputeSystemFromOperation
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetComputeSystemFromOperation
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: ebcf017c30947f406a37034f6bafa0b89752eb64a589aaf556a2f1030abff3ab
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetComputeSystemFromOperation.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsGetComputeSystemFromOperation']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetComputeSystemFromOperation.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetComputeSystemFromOperation).

# HcsGetComputeSystemFromOperation

## Description

Get the compute system associated with operation.

## Syntax

```cpp
HCS_SYSTEM WINAPI
HcsGetComputeSystemFromOperation(
    _In_ HCS_OPERATION operation
    );

```

## Parameters

`operation`

The handle to an active operation.

## Return Values

Returns the `HCS_SYSTEM` handle to the compute system used by active operation, returns `NULL` if the operation is not active.

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |