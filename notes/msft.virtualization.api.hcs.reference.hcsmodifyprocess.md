---
id: 6P3T5iFmo3b5cZtqsZjkMiV
title: >-
    HcsModifyProcess
desc: >-
    HcsModifyProcess
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsModifyProcess
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 72fe48c950da0d2ea56514e0967bea673db644c80f46bacbc1b7c861ab0434ef
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsModifyProcess.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsModifyProcess']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsModifyProcess.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsModifyProcess).

# HcsModifyProcess

## Description

Modifies the parameters of a process in a compute system.

## Syntax

```cpp
HRESULT WINAPI
HcsModifyProcess(
    _In_ HCS_PROCESS process,
    _In_ HCS_OPERATION operation,
    _In_ PCWSTR settings
    );
```

## Parameters

`process`

Handle to the process to modify.

`operation`

Handle to the operation that tracks the process.

`settings`

JSON document of [[ProcessModifyRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ProcessModifyRequest]] specifying the settings of process to modify.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

If the return value is `S_OK`, it means the operation started successfully. Callers are expected to get the operation's result using [[`HcsWaitForOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresultandprocessinfo]] or [[`HcsGetOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresultandprocessinfo]].


## Operation Results

The return value of [[`HcsWaitForOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresultandprocessinfo]] or [[`HcsGetOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresultandprocessinfo]] based on current operation listed as below.

| Operation Result Value | Description |
| -- | -- |
| `S_OK` | The process parameters changed successfully |

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |