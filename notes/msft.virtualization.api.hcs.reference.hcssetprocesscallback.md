---
id: anJsvhFEayjuGvaSXNZ3TFH
title: >-
    HcsSetProcessCallback
desc: >-
    HcsSetProcessCallback
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsSetProcessCallback
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 8a5dbb5563ad35bce518747357d37130bb0b5b80c87d4227af03e941d7beac19
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsSetProcessCallback.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsSetProcessCallback']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsSetProcessCallback.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsSetProcessCallback).

# HcsSetProcessCallback

## Description

Registers a callback function to receive notifications for a process in a compute system.

## Syntax

```cpp
HRESULT WINAPI
HcsSetProcessCallback(
    _In_ HCS_PROCESS process,
    _In_ HCS_EVENT_OPTIONS callbackOptions,
    _In_ void* context,
    _In_ HCS_EVENT_CALLBACK callback
    );
```

## Parameters

`process`

The handle to the process for that the callback is registered.

`callbackOptions`

The option for callback, using [[HCS_EVENT_OPTIONS|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_event_options]].

`context`

Optional pointer to a context that is passed to the callback.

`callback`

Callback function that is invoked for events on the process.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |