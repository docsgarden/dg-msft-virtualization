---
id: AdmngezvprEg8zUHuQtE4FV
title: >-
    Partition
desc: >-
    Hypervisor Partition Interface
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/partition-properties
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: b6c7de5986117e5420983be67bbed0d48ecec7f2366a2f76682aabcc38a30c3e
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/partition-properties.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/partition-properties.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/partition-properties).

# Partitions

The hypervisor supports isolation in terms of a partition. A partition is a logical unit of isolation, supported by the hypervisor, in which operating systems execute.

## Partition Privilege Flags

Each partition has a set of privileges assigned by the hypervisor. Privileges control access to synthetic MSRs or hypercalls.

A partition can query its privileges through the “Hypervisor Feature Identification” CPUID Leaf (0x40000003). See [[HV_PARTITION_PRIVILEGE_MASK|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.datatypes.hv_partition_privilege_mask]] for a description of all privileges.

## Partition Crash Enlightenment

The hypervisor provides guest partitions with a crash enlightenment facility. This interface allows the operating system running in a guest partition the option of providing forensic information about fatal OS conditions to the hypervisor as part of its crash dump procedure. Options include preserving the contents of the guest crash parameter MSRs and specifying a crash message. The hypervisor then makes this information available to the root partition for logging. This mechanism allows the virtualization host administrator to gather information about the guest OS crash event without needing to inspect persistent storage attached to the guest partition for crash dump or core dump information that may be stored there by the crashing guest OS.

The availability of this mechanism is indicated via `CPUID.0x400003.EDX:10`, the GuestCrashMsrsAvailable flag; refer to [[feature discovery|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.feature-discovery]].

### Guest Crash Enlightenment Interface

The guest crash enlightenment interface is provided through six synthetic MSRs, as defined below.

 ```c
#define HV_X64_MSR_CRASH_P0 0x40000100
#define HV_X64_MSR_CRASH_P1 0x40000101
#define HV_X64_MSR_CRASH_P2 0x40000102
#define HV_X64_MSR_CRASH_P3 0x40000103
#define HV_X64_MSR_CRASH_P4 0x40000104
#define HV_X64_MSR_CRASH_CTL 0x40000105
 ```

#### Guest Crash Control MSR

The guest crash control MSR HV_X64_MSR_CRASH_CTL may be used by guest partitions to determine the hypervisor’s guest crash capabilities, and to invoke the specified action to take. The [[HV_CRASH_CTL_REG_CONTENTS|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.datatypes.hv_crash_ctl_reg_contents]] data structure defines the contents of the MSR.

##### Determining Guest Crash Capabilities

To determine the guest crash capabilities, guest partitions may read the HV_X64_MSR_CRASH_CTL register. The supported set of actions and capabilities supported by the hypervisor is reported.

##### Invoking Guest Crash Capabilities

To invoke a supported hypervisor guest crash action, a guest partition writes to the HV_X64_MSR_CRASH_CTL register, specifying the desired action. Two variations are supported: CrashNotify by itself, and CrashMessage in combination with CrashNotify. For each occurrence of a guest crash, at most a single write to MSR HV_X64_MSR_CRASH_CTL should be performed, specifying one of the two variations.

| Guest Crash Action  | Description                                                 |
|---------------------|-------------------------------------------------------------|
| CrashMessage        | This action is used in combination with CrashNotify to specify a crash message to the hypervisor. When selected, the values of P3 and P4 are treated as the location and size of the message. HV_X64_MSR_CRASH_P3 is the guest physical address of the message, and HV_X64_MSR_CRASH_P4 is the length in bytes of the message (maximum of 4096 bytes). |
| CrashNotify         | This action indicates to the hypervisor that the guest partition has completed writing the desired data into the guest crash parameter MSRs (i.e., P0 thru P4), and the hypervisor should proceed with logging the contents of these MSRs. |