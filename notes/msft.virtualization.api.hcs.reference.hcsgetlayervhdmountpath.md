---
id: 7hemTpRCAAJXRrDnA7HEw57
title: >-
    HcsGetLayerVhdMountPath
desc: >-
    HcsGetLayerVhdMountPath
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetLayerVhdMountPath
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 0491149cbc20a2ab70a4a375baa5ecbe7a62ef2778b61690538e2af0e14d137b
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetLayerVhdMountPath.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsGetLayerVhdMountPath']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetLayerVhdMountPath.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetLayerVhdMountPath).

# HcsGetLayerVhdMountPath

## Description

Returns the volume path for a virtual disk of a writable container layer.

## Syntax

```cpp
HRESULT WINAPI
HcsGetLayerVhdMountPath(
    _In_     HANDLE vhdHandle,
    _Outptr_ PWSTR* mountPath
    );
```

## Parameters

`vhdHandle`

The handle to a mounted virtual hard disk on the host.

`mountPath`

Receives the volume path for the layer. It is the caller's responsibility to release the returned string buffer using [`LocalFree`](https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-localfree).

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeStorage.h |
| **Library** | ComputeStorage.lib |
| **Dll** | ComputeStorage.dll |