---
id: 8f8MCgdLktG6gyuakAaJS9y
title: >-
    HCN_PORT_RANGE_RESERVATION
desc: >-
    HCN_PORT_RANGE_RESERVATION
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_PORT_RANGE_RESERVATION
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 2bb285556c5eca28cce3400fdf5d0d9023d106179618076cf60515178e8fdb4f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_PORT_RANGE_RESERVATION.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HCN_PORT_RANGE_RESERVATION']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_PORT_RANGE_RESERVATION.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_PORT_RANGE_RESERVATION).

# HCN\_PORT\_RANGE\_RESERVATION

## Description

A range of ports for a reservation.

## Syntax

```cpp
typedef struct tagHCN_PORT_RANGE_RESERVATION
{
    // start and end are inclusive
    USHORT startingPort;
    USHORT endingPort;
} HCN_PORT_RANGE_RESERVATION;

```


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |