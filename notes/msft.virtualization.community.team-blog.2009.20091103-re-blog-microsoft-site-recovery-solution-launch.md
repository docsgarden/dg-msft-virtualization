---
id: AmeCthvBFbW7oVujJBH5vbd
title: >-
    Re-blog&#58; Microsoft Site Recovery Solution Launch
desc: >-
    Re-blog Microsoft Site Recovery Solution Launch
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20091103-re-blog-microsoft-site-recovery-solution-launch
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 7376be658e689e6847abcbb848c31e2c50b6bf8790edccaa8c78270ec4c6b4ad
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091103-re-blog-microsoft-site-recovery-solution-launch.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "11/03/2009"
    date: "2009-11-03 23:27:00"
    categories: "disaster-recovery"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091103-re-blog-microsoft-site-recovery-solution-launch.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20091103-re-blog-microsoft-site-recovery-solution-launch).

# Re-blog Microsoft Site Recovery Solution Launch

I'm re-blogging here. Over at [Virt Planet blog](https://blogs.technet.com/virtplanet/ "Virt Planet Blog"), Jim wrote the following:

> This week Microsoft is launching a comprehensive solution to help customers implement cost effective, end-to-end site recovery programs. Built on proven capabilities in [Windows Server 2008 R2](https://www.microsoft.com/windowsserver2008/en/us/default.aspx) and the [System Center](https://www.microsoft.com/systemcenter/en/us/default.aspx) management suite, Microsoft is helping IT Professionals leverage Windows Server [Hyper-V](https://www.microsoft.com/windowsserver2008/en/us/hyperv-main.aspx) and [Failover Clustering](https://www.microsoft.com/Windowsserver2008/en/us/failover-clustering-main.aspx) along with tools like [Virtual Machine Manager](https://www.microsoft.com/systemcenter/virtualmachinemanager/en/us/default.aspx) to deliver cost effective site recovery.
> 
> The **Microsoft Site Recovery Solution** ecosystem is ramping with a broad range of storage replication partners like Double-Take Software, EMC, HDS, HP delivering solutions that take advantage of the [Microsoft Cluster Resource DLL](https://msdn.microsoft.com/library/aa372239\(VS.85\).aspx). With cluster integration IT Professionals can deploy streamlined and operationally effective site recovery.
> 
> You can learn more about the Microsoft Site Recovery Solution by joining the Microsoft team and Enterprise Strategy Group on Thursday, November 5th at 10:30am Pacific for a webcast [Building Effective and Highly Available Disaster Recovery Solutions Using Microsoft Virtualization](https://searchwindowsserver.bitpipe.com/data/document.do;jsessionid=A62722F1FCBFABA0BBCFDCF69D5AE73A?res_id=1256150149_996) __This webcast looks at key drivers for site recovery solutions and reviews practical deployment considerations (you can view the recorded version of the webcast after the 5 th). Microsoft and select partners will also be demonstrating Site Recovery Solutions at TechEd, so if you plan to be in Berlin during the week of November 9th, make sure to stop by the Virtualization Solutions kiosk in the Technical Learning Center.

[Patrick](https://blogs.technet.com/virtplanet/)