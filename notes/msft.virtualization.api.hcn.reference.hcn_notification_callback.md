---
id: 5jLity9qenUPud7aNBhWuLs
title: >-
    HCN_NOTIFICATION_CALLBACK
desc: >-
    HCN_NOTIFICATION_CALLBACK
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_NOTIFICATION_CALLBACK
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 470b6c4f65f49ebdc21be2bb70903040273a96df36c508237e8a991898b73ab4
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_NOTIFICATION_CALLBACK.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HCN_NOTIFICATION_CALLBACK']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_NOTIFICATION_CALLBACK.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_NOTIFICATION_CALLBACK).

# HCN\_NOTIFICATION\_CALLBACK

## Description

Function type for HNS notification callbacks.

## Syntax

```cpp
typedef void (CALLBACK *HCN_NOTIFICATION_CALLBACK)(
    _In_            DWORD  NotificationType,
    _In_opt_        void*  Context,
    _In_            HRESULT NotificationStatus,
    _In_opt_        PCWSTR NotificationData
    );
```

## Parameters

`NotificationType`

The type of notification [[`HCN_NOTIFICATIONS`|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_notifications]].

`Context`

Handle for context of callback.

`NotificationStatus`

Notification Status.

`NotificationData`

Data associated with the notification.


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |