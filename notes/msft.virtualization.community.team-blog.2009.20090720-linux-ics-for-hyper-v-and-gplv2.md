---
id: 3K5sto9FepKPr9rCLTYff46
title: >-
    Linux ICs for Hyper-V and GPLv2
desc: >-
    Learn about Linux ICs for Hyper-V and GPLv2
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090720-linux-ics-for-hyper-v-and-gplv2
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: e568d2d6ff7dc151ce3aa5c62b5859a9f662c7d43c008e8053d777cb3f4b874e
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090720-linux-ics-for-hyper-v-and-gplv2.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "07/20/2009"
    date: "2009-07-20 13:06:00"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090720-linux-ics-for-hyper-v-and-gplv2.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090720-linux-ics-for-hyper-v-and-gplv2).

# Linux ICs for Hyper-V and GPLv2

A funny thing happened on the way to work this morning ... the Hyper-V Linux integration components (ICs) appeared in [Greg Kroah-Hartman's](http://en.wikipedia.org/wiki/Greg_Kroah-Hartman "Wikipedia") tree (aka, the [Linux Driver Project](http://www.linuxdriverproject.org/twiki/bin/view "Linux Driver Project twiki")) of the Linux community. This is the first time Microsoft is contributing code to the Linux kernel; see the Q&A announcement [here](https://www.microsoft.com/presspass/features/2009/Jul09/07-20LinuxQA.mspx "PressPass Q&A article"). The Hyper-V Linux device drivers will be licensed under GPLv2. That's 20,000 lines of code that provide the synthetic device drivers and VM bus implementation needed for a Linux guest OS to run "enlightened" on either Windows Server 2008 Hyper-V or Microsoft Hyper-V Server 2008. Greg's tree is for all Linux device drivers being contributed to the community. I'm told that within 24-48 hours it will begin to be picked up by other developers in the community, and that it won't land in the mainline tree (Torvald's tree) until it has been generally accepted in other trees along the way. So what does this mean? Here are a few thoughts: 

  * I'll be waiting to see the reaction from the Linux community and commercial Linux companies. There should be some positive comments in there along with the expected conspiracy theories ;-)
  * there's a mutual benefit for Linux distis who want to broaden their work with Windows Server, and for customers to broaden the opportunity for Linux as a guest OS running on Hyper-V
  * Customers will have another cost-cutting tool because Linux OS will run as a first-class citizen on Hyper-V, and they be able to manage Windows and non-Windows applications and hypervisors using System Center. IT system consolidation, reduce heat in the data center, optimize power draw.
  * Two Microsoft employees are listed as the maintainers of the Linux ICs, and will continue to enhance the ICs and contribute to the code. I'm sure SMP support will be high on the list.
  * Microsoft currently distirbutes ICs for SLES 10 sp2. With the release of WS08 R2 version of the ICs, we'll also add support for SLES 11 and RHEL 5.2 and 5.3. A list of suppoted products (via SVVP) can be seen [here](http://www.windowsservercatalog.com/results.aspx?&bCatID=1521&cpID=0&avc=0&ava=0&avq=0&OR=1&PGS=25 "SVVP products page").

You can watch/listen to Sam and Tom discuss today's news on Channel 9 [here](https://channel9.msdn.com/posts/NicFill/Microsoft-Contributes-Code-to-the-Linux-Kernel/ "Channel 9 interview"). 

Patrick