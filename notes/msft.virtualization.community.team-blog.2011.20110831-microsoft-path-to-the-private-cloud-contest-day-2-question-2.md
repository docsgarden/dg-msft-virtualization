---
id: 6P9zPbWrcGCitWJ7NY4mLVQ
title: >-
    Microsoft Path to the Private Cloud Contest&#58; Day 2, Question 2
desc: >-
    Microsoft Path to the Private Cloud Contest; Day 2, Question 2
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110831-microsoft-path-to-the-private-cloud-contest-day-2-question-2
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 2adac08cf0a58f694efdd2aa3a3013cabeb408965f65ac3e3985e52fe2040129
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110831-microsoft-path-to-the-private-cloud-contest-day-2-question-2.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "08/31/2011"
    date: "2011-08-31 10:44:23"
    categories: "private-cloud"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110831-microsoft-path-to-the-private-cloud-contest-day-2-question-2.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110831-microsoft-path-to-the-private-cloud-contest-day-2-question-2).

# Microsoft Path to the Private Cloud Contest; Day 2, Question 2

Yesterday we kicked off the “[Microsoft Path to the Private Cloud”](http://bit.ly/q1kpZg) Contest, read official [rules](http://bit.ly/pGlJmT).  Today we have another Xbox to give away, so see below for second question in the series of three for the day. A reminder on how the contest works: We’ll be posting three (3) questions related to Microsoft’s virtualization, system management and private cloud offerings today (8/31), one per hour from 9:00 a.m. – 11:00 a.m. PST.  You’ll be able to find the answers on the [Microsoft.com](http://bit.ly/roPfDJ) sites.  Once you have the answers to all three questions, you send us single Twitter message (direct tweet) with all three answers to the questions to the [@MSServerCloud](http://bit.ly/ouOZlj) Twitter handle.  The person that sends us the first Twitter message with all three correct answers in it will win an Xbox 360 4GB Kinect Game Console bundle for the day. And the 8/31, 10 am. Question is: Q:  What is the name of the Microsoft program which provides an infrastructure as a service private cloud with a pre-validated configuration from server partners through the combination of Microsoft software; consolidated guidance; validated configurations from OEM partners for compute; network; and storage; and value-added software components? Remember to gather all three answers before you direct tweet us the message. Be sure to check back here at 11:00 a.m. PST as well for the next question. Reminder - the 8/31, 9 am. Question was: 

Q: Which Microsoft offering provides deep visibility into the health, performance and availability of your datacenter, private cloud and public cloud environments (such as Windows Azure) – across applications, operating systems, hypervisors and even hardware – through a single, familiar and easy to use interface?