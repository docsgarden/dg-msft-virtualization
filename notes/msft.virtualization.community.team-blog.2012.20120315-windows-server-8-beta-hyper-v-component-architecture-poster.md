---
id: AVGNgbhdHNre96moAMPj6G4
title: >-
    Windows Server “8” Beta Hyper-V Component Architecture Poster
desc: >-
    Windows Server 8 Beta Hyper-V Component Architecture Poster
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2012/20120315-windows-server-8-beta-hyper-v-component-architecture-poster
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 78515f1bbc96c47ec8001d983aaf240afc999cb9f504d1bbffb1f4432be103dc
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2012/20120315-windows-server-8-beta-hyper-v-component-architecture-poster.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "03/15/2012"
    date: "2012-03-15 12:49:00"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2012/20120315-windows-server-8-beta-hyper-v-component-architecture-poster.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2012/20120315-windows-server-8-beta-hyper-v-component-architecture-poster).

# Windows Server 8 Beta Hyper-V Component Architecture Poster

The new Windows Server “8” Beta Hyper-V Component Architecture poster is now available from the [Microsoft Download Center](https://www.microsoft.com/download/en/details.aspx?id=29189).

![Windows Server 8 Beta Hyper-V Component Architecture](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/metablogapi/2211.Windows-Server-8-Beta-Hyper-V-Component-Architecture_0FEF9E6A.jpg)](https://www.microsoft.com/download/en/details.aspx?id=29189)

 

This poster provides a visual reference for understanding key Hyper-V technologies in Windows Server “8” Beta. This new Hyper-V poster focuses on Hyper-V Replica, networking, virtual machine mobility (live migration), storage, failover clustering and scalability.

If you haven’t seen them previously, the following posters are also available:

  * [Windows Server 2008 R2 Hyper-V Component Architecture (with Service Pack 1)](https://www.microsoft.com/download/en/details.aspx?id=2688)
  * [Windows Server 2008 R2 Hyper-V Component Architecture](https://www.microsoft.com/download/en/details.aspx?displaylang=en&id=3501)
  * [Windows Server 2008 R2 Feature Components poster](https://www.microsoft.com/download/en/details.aspx?displaylang=en&id=7002)
  * [Windows Server 2008 Component Posters](https://www.microsoft.com/download/en/details.aspx?displaylang=en&id=17881)



 

Cheers,   
John.