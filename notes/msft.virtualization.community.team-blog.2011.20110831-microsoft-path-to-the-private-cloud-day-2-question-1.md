---
id: 5jbwWUsM9JLfPMkubraNWsP
title: >-
    Microsoft Path to the Private Cloud&#58; Day 2, Question 1
desc: >-
    Microsoft Path to the Private Cloud; Day 2, Question 1
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110831-microsoft-path-to-the-private-cloud-day-2-question-1
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: cc48e2a507550b71acadb647075a4575a5129b1e723cc78091137710da930e91
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110831-microsoft-path-to-the-private-cloud-day-2-question-1.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "08/31/2011"
    date: "2011-08-31 09:41:52"
    categories: "private-cloud"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110831-microsoft-path-to-the-private-cloud-day-2-question-1.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110831-microsoft-path-to-the-private-cloud-day-2-question-1).

# Microsoft Path to the Private Cloud; Day 2, Question 1

Yesterday we kicked off the “[Microsoft Path to the Private Cloud”](http://bit.ly/q1kpZg) Contest, read official [rules](http://bit.ly/pGlJmT).  As of the posting of this blog, we still have not received a tweet with the winning answers.  That means there are still two Xboxes on the table. But we're keeping things moving and today is the final day of the contest so see below for first question in the series of three for the day. A reminder on how the contest works: We’ll be posting three (3) questions related to Microsoft’s virtualization, system management and private cloud offerings today (8/31), one per hour from 9:00 a.m. – 11:00 a.m. PST.  You’ll be able to find the answers on the [Microsoft.com](http://bit.ly/roPfDJ) sites.  Once you have the answers to all three questions, you send us single Twitter message (direct tweet) with all three answers to the questions to the [@MSServerCloud](http://bit.ly/ouOZlj) Twitter handle.  The person that sends us the first Twitter message with all three correct answers in it will win an Xbox 360 4GB Kinect Game Console bundle for the day. And the 8/31, 9 am. Question is: Q: Which Microsoft offering provides deep visibility into the health, performance and availability of your datacenter, private cloud and public cloud environments (such as Windows Azure) – across applications, operating systems, hypervisors and even hardware – through a single, familiar and easy to use interface? 

Let the contest begin!!  Remember to gather all three answers before you direct tweet us the message. Be sure to check back here at 10:00 a.m. and 11:00 a.m. PST as well for the next two questions.