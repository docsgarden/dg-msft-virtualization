---
id: 3reBnPPmUSURseVYSr5LBD6
title: >-
    HcnSchema
desc: >-
    HcnSchema
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/HNS_Schema
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: cb59103721b36276b1f9657cec236ea6c506d34c69de1fd40b6468266b3538c8
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/HNS_Schema.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.date: "10/31/2021"
    api_name: "['HcnSchema']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/HNS_Schema.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/HNS_Schema).

# Hcn Agenda
- [[Enums|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#enums]]
- [[Structs|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#structs]]
- [[JSON type table|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]
- [[Version Map|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]
---
<a name = "enums"></a>
## Enums
Note: all variants listed should be used as string
<a name = "ActionType"></a>
### ActionType
Referenced by: [[CommonAclPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#commonaclpolicysetting]]; [[TierAclRule|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#tieraclrule]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Allow"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Block"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Pass"`<br>|[[2.10|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "AuthenticationType"></a>
### AuthenticationType
Referenced by: [[PrioritizedAuthenticationMethod|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#prioritizedauthenticationmethod]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"PresharedKey"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Certificate"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "DirectionType"></a>
### DirectionType
Referenced by: [[CommonAclPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#commonaclpolicysetting]]; [[TierAclPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#tieraclpolicysetting]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"In"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Out"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "EncryptionMethod"></a>
### EncryptionMethod
Referenced by: [[EncryptionPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#encryptionpolicysetting]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Ipsec"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "EndpointFlags"></a>
### EndpointFlags
Referenced by: [[HostComputeEndpoint|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputeendpoint]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"RemoteEndpoint"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"DisableInterComputeCommunication"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"EnableMirroring"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"EnableLowInterfaceMetric"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"OverrideDNSServerOrder"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"EnableDhcp"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "EndpointPolicyType"></a>
### EndpointPolicyType
Referenced by: [[EndpointPolicy|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#endpointpolicy]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"PortMapping"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"ACL"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"QOS"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"L2Driver"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"OutBoundNAT"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"SDNRoute"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"L4Proxy"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"L4WFPPROXY"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"ProviderAddress"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Maps to VNET policy with PA|
|`"PortName"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"EncapOverhead"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"InterfaceConstraint"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Encryption"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"VLAN"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"InterfaceParameters"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Iov"`<br>|[[2.9|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"TierAcl"`<br>|[[2.10|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "EndpointResourceType"></a>
### EndpointResourceType
Referenced by: [[ModifyEndpointSettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifyendpointsettingrequest]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Port"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Policy"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "EntityFlags"></a>
### EntityFlags


|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"EnableNonPersistent"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "FirewallPolicyFlags"></a>
### FirewallPolicyFlags
Referenced by: [[FirewallPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#firewallpolicysetting]]

|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"ConstrainedInterface"`<br>|[[2.16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "FirewallVMType"></a>
### FirewallVMType
Referenced by: [[FirewallPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#firewallpolicysetting]]

|Variants|NewInVersion|Description|
|---|---|---|
|`"Invalid"`<br>|[[2.16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Host"`<br>|[[2.16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"WSA"`<br>|[[2.16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"WSL"`<br>|[[2.16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestEndpointResourceType"></a>
### GuestEndpointResourceType
Referenced by: [[ModifyGuestEndpointSettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifyguestendpointsettingrequest]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Interface"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Route"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"IPAddress"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"DNS"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"RegistryKey"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Encryption"`<br>|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"MacAddress"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"L4Proxy"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"L4WFPPROXY"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Xlat"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Neighbor"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestNamespaceResourceType"></a>
### GuestNamespaceResourceType
Referenced by: [[ModifyGuestNamespaceSettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifyguestnamespacesettingrequest]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Container"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Endpoint"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestNetworkServiceFlags"></a>
### GuestNetworkServiceFlags
Referenced by: [[GuestNetworkService|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnetworkservice]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.12|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"IsFlowsteered"`<br>|[[2.12|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"IsFlowsteeredSelfManaged"`<br>|[[2.14|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestNetworkServiceInterfaceState"></a>
### GuestNetworkServiceInterfaceState
Referenced by: [[GuestNetworkServiceInterface|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnetworkserviceinterface]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Created"`<br>|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Bootstrapping"`<br>|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Synchronized"`<br>|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Desynchronized"`<br>|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Paused"`<br>|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestNetworkServiceNotificationType"></a>
### GuestNetworkServiceNotificationType
Referenced by: [[GuestNetworkServiceInterface|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnetworkserviceinterface]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"DNSCacheParam"`<br>|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"DHCPParam"`<br>|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"InterfaceParam"`<br>|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"AddressParam"`<br>|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Route"`<br>|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"DNSParam"`<br>|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"XlatParam"`<br>|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Neighbor"`<br>|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestNetworkServiceResourceType"></a>
### GuestNetworkServiceResourceType
Referenced by: [[ModifyGuestNetworkServiceSettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifyguestnetworkservicesettingrequest]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"State"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestNetworkServiceState"></a>
### GuestNetworkServiceState
Referenced by: [[GuestNetworkServiceNotificationData|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnetworkservicenotificationdata]]; [[GuestNetworkServiceStateRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnetworkservicestaterequest]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Created"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Bootstrapping"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Synchronized"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Paused"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Desynchronized"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Rehydrating"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Degraded"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Destroyed"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestResourceType"></a>
### GuestResourceType
Referenced by: [[GuestModifySettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestmodifysettingrequest]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Endpoint"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Namespace"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Service"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Firewall"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "HostComputeQueryFlags"></a>
### HostComputeQueryFlags
Referenced by: [[HostComputeQuery|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputequery]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Detailed"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "HostResourceType"></a>
### HostResourceType


|Variants|NewInVersion|Description|
|---|---|---|
|`"Network"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Endpoint"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Container"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Namespace"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"PolicyList"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "IovInterruptModerationType"></a>
### IovInterruptModerationType
Referenced by: [[IovPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#iovpolicysetting]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"IovInterruptModerationDefault"`<br>|[[2.9|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"IovInterruptModerationAdaptive"`<br>|[[2.9|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"IovInterruptModerationOff"`<br>|[[2.9|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"IovInterruptModerationLow"`<br>|[[2.9|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"IovInterruptModerationMedium"`<br>|[[2.9|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"IovInterruptModerationHigh"`<br>|[[2.9|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "IpamType"></a>
### IpamType
Referenced by: [[Ipam|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#ipam]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Static"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Dhcp"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "IPSubnetFlags"></a>
### IPSubnetFlags
Referenced by: [[IpSubnet|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#ipsubnet]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"EnableBroadcast"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"ReserveNetworkAddress"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "LoadBalancerDistribution"></a>
### LoadBalancerDistribution
Referenced by: [[LoadBalancerPortMapping|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#loadbalancerportmapping]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"SourceIPProtocol"`<br>|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"SourceIP"`<br>|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "LoadBalancerFlags"></a>
### LoadBalancerFlags
Referenced by: [[HostComputeLoadBalancer|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputeloadbalancer]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"EnableDirectServerReturn"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"IPv6"`<br>|[[2.8|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "LoadBalancerPortMappingFlags"></a>
### LoadBalancerPortMappingFlags
Referenced by: [[LoadBalancerPortMapping|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#loadbalancerportmapping]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"EnableInternalLoadBalancer"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"LocalRoutedVip"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"NotUsed"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"EnablePreserveDip"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "ModifyRequestType"></a>
### ModifyRequestType
Referenced by: [[ModifySettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifysettingrequest]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Add"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Remove"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Update"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Refresh"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Reset"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NamespaceResourceType"></a>
### NamespaceResourceType
Referenced by: [[ModifyNamespaceSettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifynamespacesettingrequest]]; [[NamespaceResource|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#namespaceresource]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Container"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Endpoint"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NamespaceType"></a>
### NamespaceType
Referenced by: [[HostComputeNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputenamespace]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Host"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"HostDefault"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Guest"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"GuestDefault"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NatFlags"></a>
### NatFlags
Referenced by: [[OutboundNatPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#outboundnatpolicysetting]]; [[PortMappingPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#portmappingpolicysetting]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"LocalRoutedVip"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"IPv6"`<br>|[[2.8|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NetworkFlags"></a>
### NetworkFlags
Referenced by: [[HostComputeNetwork|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputenetwork]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"EnableDnsProxy"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"EnableDhcpServer"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"EnableMirroring"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"EnableNonPersistent"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"EnablePersistent"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"IsolateVSwitch"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"EnableFlowSteering"`<br>|[[2.11|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"DisableSharing"`<br>|[[2.14|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"EnableFirewall"`<br>|[[2.14|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NetworkMode"></a>
### NetworkMode
Referenced by: [[HostComputeNetwork|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputenetwork]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"NAT"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"ICS"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Transparent"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"L2Bridge"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"L2Tunnel"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Overlay"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Private"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Internal"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Mirrored"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Flow Steering Engine|
|`"Infiniband"`<br>|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"ConstrainedICS"`<br>|[[2.10|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NetworkPolicyType"></a>
### NetworkPolicyType
Referenced by: [[NetworkPolicy|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#networkpolicy]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"SourceMacAddress"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"NetAdapterName"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"InterfaceConstraint"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"VSwitchExtension"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"ProviderAddress"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"DrMacAddress"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"AutomaticDNS"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"RemoteSubnetRoute"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"VxlanPort"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"HostRoute"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"SetPolicy"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"L4Proxy"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"LayerConstraint"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"NetworkACL"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NetworkResourceType"></a>
### NetworkResourceType
Referenced by: [[ModifyNetworkSettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifynetworksettingrequest]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"DNS"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Extension"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Policy"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"Subnet"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"IPSubnet"`<br>|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "ProtocolType"></a>
### ProtocolType
Referenced by: [[CommonL4ProxyPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#commonl4proxypolicysetting]]; [[LoadBalancerPortMapping|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#loadbalancerportmapping]]; [[PortMappingPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#portmappingpolicysetting]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Unknown"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"ICMPv4"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"IGMP"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"TCP"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"UDP"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"ICMPv6"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "ProxyType"></a>
### ProxyType


|Variants|NewInVersion|Description|
|---|---|---|
|`"VFP"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Virtual Filtering Platform|
|`"WFP"`<br>|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Windows Filtering Platform|

---

<a name = "RpcEndpointType"></a>
### RpcEndpointType
Referenced by: [[RpcConnectionInformation|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#rpcconnectioninformation]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"HvSocket"`<br>|[[2.13|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"LRpc"`<br>|[[2.13|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "RuleType"></a>
### RuleType
Referenced by: [[CommonAclPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#commonaclpolicysetting]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"Host"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|WFP|
|`"Switch"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|VFP|

---

<a name = "SetPolicyTypes"></a>
### SetPolicyTypes
Referenced by: [[SetPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#setpolicysetting]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"IPSET"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"NESTEDIPSET"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "SubnetFlags"></a>
### SubnetFlags
Referenced by: [[Subnet|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#subnet]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"None"`<br>|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"DoNotReserveGatewayAddress"`<br>|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "SubnetPolicyType"></a>
### SubnetPolicyType
Referenced by: [[SubnetPolicy|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#subnetpolicy]]



|Variants|NewInVersion|Description|
|---|---|---|
|`"VLAN"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|`"VSID"`<br>|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "structs"></a>
## Structs
<a name = "AclPolicySetting"></a>
### AclPolicySetting


Derived from parent class: [[FiveTuple|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#fivetuple]]; [[CommonAclPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#commonaclpolicysetting]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Protocols**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LocalAddresses**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RemoteAddresses**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LocalPorts**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RemotePorts**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Priority**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Id**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Action**<br>|[[ActionType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#actiontype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Direction**<br>|[[DirectionType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#directiontype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RuleType**<br>|[[RuleType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#ruletype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "AuthenticationCertificate"></a>
### AuthenticationCertificate


Derived from parent class: [[AuthenticationMethod|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#authenticationmethod]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**CertificateName**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "AuthenticationMethod"></a>
### AuthenticationMethod



**Note:** This is an empty struct with no fields, and to be used in the JSON document must be specified as an empty object: `"{}"`.

---

<a name = "AuthenticationPresharedKey"></a>
### AuthenticationPresharedKey


Derived from parent class: [[AuthenticationMethod|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#authenticationmethod]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Key**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "AutomaticDNSNetworkPolicySetting"></a>
### AutomaticDNSNetworkPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Enable**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "Base"></a>
### Base


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Version**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdditionalParams**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Health**<br>|[[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Schema version should be present in all objects|
|**Telemetry**<br>|[[Telemetry|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#telemetry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "BasePolicy"></a>
### BasePolicy


Derived from parent class: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Version**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdditionalParams**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Health**<br>|[[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Schema version should be present in all objects|
|**Telemetry**<br>|[[Telemetry|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#telemetry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Type**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Data**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "CommonAclPolicySetting"></a>
### CommonAclPolicySetting


Derived from parent class: [[FiveTuple|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#fivetuple]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Protocols**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LocalAddresses**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RemoteAddresses**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LocalPorts**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RemotePorts**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Priority**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Id**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Action**<br>|[[ActionType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#actiontype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Direction**<br>|[[DirectionType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#directiontype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RuleType**<br>|[[RuleType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#ruletype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "CommonL4ProxyPolicySetting"></a>
### CommonL4ProxyPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**IP**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Port**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Protocol**<br>|[[ProtocolType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#protocoltype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ExceptionList**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Destination**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**OutboundNat**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "DNS"></a>
### DNS
Referenced by: [[HostComputeEndpoint|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputeendpoint]]; [[HostComputeNetwork|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputenetwork]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Domain**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Search**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ServerList**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Options**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "DNS_2"></a>
### DNS_2
Referenced by: [[GuestEndpointState|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestendpointstate]]



Derived from parent class: [[InterfaceNotificationMessage|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#interfacenotificationmessage]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**MessageNumber**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Family**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Domain**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Search**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ServerList**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Options**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "DrMacAddressNetworkPolicySetting"></a>
### DrMacAddressNetworkPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Address**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "EncapOverheadEndpointPolicySetting"></a>
### EncapOverheadEndpointPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Overhead**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "EncryptionPolicySetting"></a>
### EncryptionPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**EncryptionMethodType**<br>|[[EncryptionMethod|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#encryptionmethod]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AuthenticationMethods**<br>|<[[PrioritizedAuthenticationMethod|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#prioritizedauthenticationmethod]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Endpoints1**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|default Any|
|**Endpoints2**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|default Any|
|**ProtocolType**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|default Any|
|**Endpoint1Ports**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|default All ports|
|**Endpoint2Ports**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|default All ports|

---

<a name = "EndpointAdditionalParams"></a>
### EndpointAdditionalParams


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**SwitchId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SwitchPortId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "EndpointPolicy"></a>
### EndpointPolicy
Referenced by: [[HostComputeEndpoint|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputeendpoint]]; [[PolicyEndpointRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#policyendpointrequest]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Type**<br>|[[EndpointPolicyType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#endpointpolicytype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Settings**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "Extension"></a>
### Extension


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Id**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**IsEnabled**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "ExtraParams"></a>
### ExtraParams
Referenced by: [[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Resources**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SharedContainers**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LayeredOn**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SwitchGuid**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**UtilityVM**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**VirtualMachine**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "Feature"></a>
### Feature


Derived from parent class: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Version**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdditionalParams**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Health**<br>|[[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Schema version should be present in all objects|
|**Telemetry**<br>|[[Telemetry|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#telemetry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Enabled**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Data**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "FirewallPolicySetting"></a>
### FirewallPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**VmType**<br>|[[FirewallVMType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#firewallvmtype]]|[[2.16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**FirewallPolicyFlags**<br>|[[FirewallPolicyFlags|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#firewallpolicyflags]]|[[2.16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "FiveTuple"></a>
### FiveTuple
Referenced by: [[L4WfpProxyPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#l4wfpproxypolicysetting]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Protocols**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LocalAddresses**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RemoteAddresses**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LocalPorts**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RemotePorts**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Priority**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "FiveTuple_2"></a>
### FiveTuple_2


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Protocols**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LocalAddresses**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RemoteAddresses**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LocalPorts**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RemotePorts**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Priority**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestEndpoint"></a>
### GuestEndpoint


Derived from parent class: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Version**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdditionalParams**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Health**<br>|[[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Schema version should be present in all objects|
|**Telemetry**<br>|[[Telemetry|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#telemetry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**NamespaceId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestEndpointState"></a>
### GuestEndpointState


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**NetworkInterfaces**<br>|<[[NetworkInterface|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#networkinterface]]> array|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Routes**<br>|<[[Route_2|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#route_2]]> array|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**IPAddresses**<br>|<[[IPAddress|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#ipaddress]]> array|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**DNS**<br>|<[[DNS_2|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#dns_2]]> array|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Xlat**<br>|<[[Xlat|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#xlat]]> array|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestFirewall"></a>
### GuestFirewall


Derived from parent class: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Version**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdditionalParams**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Health**<br>|[[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Schema version should be present in all objects|
|**Telemetry**<br>|[[Telemetry|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#telemetry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestModifySettingRequest"></a>
### GuestModifySettingRequest


Derived from parent class: [[ModifySettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifysettingrequest]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ResourceUri**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RequestType**<br>|[[ModifyRequestType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifyrequesttype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ResourceType**<br>|[[GuestResourceType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestresourcetype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Settings**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestNamespace"></a>
### GuestNamespace


Derived from parent class: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Version**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdditionalParams**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Health**<br>|[[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Schema version should be present in all objects|
|**Telemetry**<br>|[[Telemetry|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#telemetry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**CompartmentId**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Resources**<br>|<[[NamespaceResource_2|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#namespaceresource_2]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestNetworkService"></a>
### GuestNetworkService
Schema to hold the GNS info in HNS

Derived from parent class: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Version**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdditionalParams**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Health**<br>|[[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Schema version should be present in all objects|
|**Telemetry**<br>|[[Telemetry|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#telemetry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**VirtualMachineId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**MirroredInterfaces**<br>|<[[GuestNetworkServiceInterface|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnetworkserviceinterface]]> array|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**MirrorHostNetworking**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**GnsRpcServerInformation**<br>|[[RpcConnectionInformation|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#rpcconnectioninformation]]|[[2.11|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Flags**<br>|[[GuestNetworkServiceFlags|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnetworkserviceflags]]|[[2.12|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestNetworkServiceInterface"></a>
### GuestNetworkServiceInterface
Referenced by: [[GuestNetworkService|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnetworkservice]]; [[GuestNetworkServiceNotificationData|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnetworkservicenotificationdata]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**EndpointId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**InterfaceGuid**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[GuestNetworkServiceInterfaceState|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnetworkserviceinterfacestate]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**MissedNotifications**<br>|[[GuestNetworkServiceNotificationType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnetworkservicenotificationtype]]|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestNetworkServiceNotificationData"></a>
### GuestNetworkServiceNotificationData


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**State**<br>|[[GuestNetworkServiceState|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnetworkservicestate]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Interfaces**<br>|<[[GuestNetworkServiceInterface|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnetworkserviceinterface]]> array|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestNetworkServiceStateRequest"></a>
### GuestNetworkServiceStateRequest


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**State**<br>|[[GuestNetworkServiceState|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnetworkservicestate]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "GuestService"></a>
### GuestService


Derived from parent class: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Version**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdditionalParams**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Health**<br>|[[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Schema version should be present in all objects|
|**Telemetry**<br>|[[Telemetry|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#telemetry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ServiceId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Base64EncodedData**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "Health"></a>
### Health
Referenced by: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Data**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Extra**<br>|[[ExtraParams|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#extraparams]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "HostComputeEndpoint"></a>
### HostComputeEndpoint


Derived from parent class: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Version**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdditionalParams**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Health**<br>|[[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Schema version should be present in all objects|
|**Telemetry**<br>|[[Telemetry|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#telemetry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**HostComputeNetwork**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**HostComputeNamespace**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Policies**<br>|<[[EndpointPolicy|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#endpointpolicy]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**IpConfigurations**<br>|<[[IpConfig|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#ipconfig]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Dns**<br>|[[DNS|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#dns]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Routes**<br>|<[[Route|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#route]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**MacAddress**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Flags**<br>|[[EndpointFlags|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#endpointflags]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "HostComputeLoadBalancer"></a>
### HostComputeLoadBalancer


Derived from parent class: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Version**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdditionalParams**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Health**<br>|[[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Schema version should be present in all objects|
|**Telemetry**<br>|[[Telemetry|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#telemetry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**HostComputeNetwork**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**HostComputeEndpoints**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SourceVIP**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**FrontendVIPs**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**PortMappings**<br>|<[[LoadBalancerPortMapping|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#loadbalancerportmapping]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Flags**<br>|[[LoadBalancerFlags|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#loadbalancerflags]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "HostComputeNamespace"></a>
### HostComputeNamespace


Derived from parent class: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Version**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdditionalParams**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Health**<br>|[[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Schema version should be present in all objects|
|**Telemetry**<br>|[[Telemetry|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#telemetry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**NamespaceId**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**NamespaceGuid**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Type**<br>|[[NamespaceType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#namespacetype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Resources**<br>|<[[NamespaceResource|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#namespaceresource]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "HostComputeNetwork"></a>
### HostComputeNetwork


Derived from parent class: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Version**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdditionalParams**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Health**<br>|[[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Schema version should be present in all objects|
|**Telemetry**<br>|[[Telemetry|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#telemetry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Type**<br>|[[NetworkMode|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#networkmode]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Policies**<br>|<[[NetworkPolicy|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#networkpolicy]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**MacPool**<br>|[[MacPool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#macpool]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Dns**<br>|[[DNS|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#dns]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Ipams**<br>|<[[Ipam|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#ipam]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Flags**<br>|[[NetworkFlags|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#networkflags]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "HostComputePolicyList"></a>
### HostComputePolicyList


Derived from parent class: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Version**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdditionalParams**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Health**<br>|[[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Schema version should be present in all objects|
|**Telemetry**<br>|[[Telemetry|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#telemetry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**References**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Policies**<br>|<[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "HostComputeQuery"></a>
### HostComputeQuery


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Flags**<br>|[[HostComputeQueryFlags|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputequeryflags]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Filter**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "HostComputeRoute"></a>
### HostComputeRoute


Derived from parent class: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Version**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdditionalParams**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Health**<br>|[[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Schema version should be present in all objects|
|**Telemetry**<br>|[[Telemetry|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#telemetry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**HostComputeNetwork**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**HostComputeEndpoints**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Routes**<br>|<[[SDNRoutePolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#sdnroutepolicysetting]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "HostRoutePolicySetting"></a>
### HostRoutePolicySetting



**Note:** This is an empty struct with no fields, and to be used in the JSON document must be specified as an empty object: `"{}"`.

---

<a name = "InterfaceConstraintEndpointPolicySetting"></a>
### InterfaceConstraintEndpointPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**InterfaceGuid**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**InterfaceLuid**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**InterfaceIndex**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**InterfaceMediaType**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**InterfaceAlias**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**InterfaceDescription**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "InterfaceConstraintNetworkPolicySetting"></a>
### InterfaceConstraintNetworkPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**InterfaceGuid**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**InterfaceLuid**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**InterfaceIndex**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**InterfaceMediaType**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**InterfaceAlias**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**InterfaceDescription**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "InterfaceNotificationMessage"></a>
### InterfaceNotificationMessage


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**MessageNumber**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Family**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "InterfaceParametersPolicySetting"></a>
### InterfaceParametersPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Mtu**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.4|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "IovPolicySetting"></a>
### IovPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**IovOffloadWeight**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.9|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**QueuePairsRequested**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.9|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**InterruptModeration**<br>|[[IovInterruptModerationType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#iovinterruptmoderationtype]]|[[2.9|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "IPAddress"></a>
### IPAddress
Referenced by: [[GuestEndpointState|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestendpointstate]]



Derived from parent class: [[InterfaceNotificationMessage|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#interfacenotificationmessage]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**MessageNumber**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Family**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Address**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**PrefixOrigin**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SuffixOrigin**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ValidLifetime**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**PreferredLifetime**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**OnLinkPrefixLength**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SkipAsSource**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "Ipam"></a>
### Ipam
Referenced by: [[HostComputeNetwork|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputenetwork]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Type**<br>|[[IpamType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#ipamtype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Type : dhcp|
|**Subnets**<br>|<[[Subnet|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#subnet]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "IpConfig"></a>
### IpConfig
Referenced by: [[HostComputeEndpoint|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputeendpoint]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**IpAddress**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**PrefixLength**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**IpSubnetId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "IpSubnet"></a>
### IpSubnet
Referenced by: [[IPSubnetNetworkRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#ipsubnetnetworkrequest]]; [[Subnet|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#subnet]]



Derived from parent class: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Version**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdditionalParams**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Health**<br>|[[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Schema version should be present in all objects|
|**Telemetry**<br>|[[Telemetry|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#telemetry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**IpAddressPrefix**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Flags**<br>|[[IPSubnetFlags|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#ipsubnetflags]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "IPSubnetNetworkRequest"></a>
### IPSubnetNetworkRequest


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**SubnetId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**IpSubnets**<br>|<[[IpSubnet|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#ipsubnet]]> array|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "L4ProxyPolicySetting"></a>
### L4ProxyPolicySetting


Derived from parent class: [[CommonL4ProxyPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#commonl4proxypolicysetting]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**IP**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Port**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Protocol**<br>|[[ProtocolType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#protocoltype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ExceptionList**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Destination**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**OutboundNat**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "L4ProxyPolicySetting_2"></a>
### L4ProxyPolicySetting_2


Derived from parent class: [[CommonL4ProxyPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#commonl4proxypolicysetting]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**IP**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Port**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Protocol**<br>|[[ProtocolType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#protocoltype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ExceptionList**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Destination**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**OutboundNat**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "L4WfpProxyPolicySetting"></a>
### L4WfpProxyPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**InboundProxyPort**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.11|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Port to deliver inbound proxied traffic to.|
|**OutboundProxyPort**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.11|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Port to deliver outbound proxied traffic to.|
|**FilterTuple**<br>|[[FiveTuple|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#fivetuple]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Matching conditions traffic filtering.|
|**UserSID**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|User account of the Proxy container.|
|**InboundExceptions**<br>|[[ProxyExceptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#proxyexceptions]]|[[2.14|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|IP Addresses or ports to exempt from redirection on inbound traffic.|
|**OutboundExceptions**<br>|[[ProxyExceptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#proxyexceptions]]|[[2.14|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|IP Addresses or ports to exempt from redirection on oubound traffic.|

---

<a name = "LayerConstraintNetworkPolicySetting"></a>
### LayerConstraintNetworkPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**LayerId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "LoadBalancerPortMapping"></a>
### LoadBalancerPortMapping
Referenced by: [[HostComputeLoadBalancer|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputeloadbalancer]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Protocol**<br>|[[ProtocolType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#protocoltype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**InternalPort**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ExternalPort**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**DistributionType**<br>|[[LoadBalancerDistribution|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#loadbalancerdistribution]]|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Flags**<br>|[[LoadBalancerPortMappingFlags|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#loadbalancerportmappingflags]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "MacAddress"></a>
### MacAddress


Derived from parent class: [[InterfaceNotificationMessage|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#interfacenotificationmessage]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**MessageNumber**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Family**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**PhysicalAddress**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "MacPool"></a>
### MacPool
Referenced by: [[HostComputeNetwork|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputenetwork]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Ranges**<br>|<[[MacRange|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#macrange]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "MacRange"></a>
### MacRange
Referenced by: [[MacPool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#macpool]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**StartMacAddress**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**EndMacAddress**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "ModifyEndpointSettingRequest"></a>
### ModifyEndpointSettingRequest


Derived from parent class: [[ModifySettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifysettingrequest]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ResourceUri**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RequestType**<br>|[[ModifyRequestType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifyrequesttype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ResourceType**<br>|[[EndpointResourceType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#endpointresourcetype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Settings**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "ModifyGuestEndpointSettingRequest"></a>
### ModifyGuestEndpointSettingRequest


Derived from parent class: [[ModifySettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifysettingrequest]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ResourceUri**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RequestType**<br>|[[ModifyRequestType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifyrequesttype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ResourceType**<br>|[[GuestEndpointResourceType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestendpointresourcetype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Settings**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "ModifyGuestNamespaceSettingRequest"></a>
### ModifyGuestNamespaceSettingRequest


Derived from parent class: [[ModifySettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifysettingrequest]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ResourceUri**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RequestType**<br>|[[ModifyRequestType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifyrequesttype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ResourceType**<br>|[[GuestNamespaceResourceType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnamespaceresourcetype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Settings**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "ModifyGuestNetworkServiceSettingRequest"></a>
### ModifyGuestNetworkServiceSettingRequest


Derived from parent class: [[ModifySettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifysettingrequest]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ResourceUri**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RequestType**<br>|[[ModifyRequestType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifyrequesttype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ResourceType**<br>|[[GuestNetworkServiceResourceType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnetworkserviceresourcetype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Settings**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "ModifyNamespaceSettingRequest"></a>
### ModifyNamespaceSettingRequest


Derived from parent class: [[ModifySettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifysettingrequest]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ResourceUri**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RequestType**<br>|[[ModifyRequestType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifyrequesttype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ResourceType**<br>|[[NamespaceResourceType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#namespaceresourcetype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Settings**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "ModifyNamespaceSettingResponse"></a>
### ModifyNamespaceSettingResponse
Find out if Modify can return with a response?

Derived from parent class: [[Response|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#response]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Success**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Error**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ErrorCode**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**CompartmentId**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "ModifyNetworkSettingRequest"></a>
### ModifyNetworkSettingRequest


Derived from parent class: [[ModifySettingRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifysettingrequest]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ResourceUri**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RequestType**<br>|[[ModifyRequestType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifyrequesttype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ResourceType**<br>|[[NetworkResourceType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#networkresourcetype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Settings**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "ModifySettingRequest"></a>
### ModifySettingRequest


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ResourceUri**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RequestType**<br>|[[ModifyRequestType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#modifyrequesttype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "ModifySettingResponse"></a>
### ModifySettingResponse


Derived from parent class: [[Response|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#response]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Success**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Error**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ErrorCode**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Data**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NamespaceContainerRequest"></a>
### NamespaceContainerRequest


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ContainerId**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NamespaceEndpointRequest"></a>
### NamespaceEndpointRequest


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**EndpointId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NamespaceResource"></a>
### NamespaceResource
Referenced by: [[HostComputeNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputenamespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Type**<br>|[[NamespaceResourceType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#namespaceresourcetype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Data**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NamespaceResource_2"></a>
### NamespaceResource_2
Referenced by: [[GuestNamespace|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnamespace]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**EndpointId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ContainerId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NamespaceResourceContainer"></a>
### NamespaceResourceContainer


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Id**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NamespaceResourceEndpoint"></a>
### NamespaceResourceEndpoint


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Id**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "Neighbor"></a>
### Neighbor


Derived from parent class: [[InterfaceNotificationMessage|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#interfacenotificationmessage]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**MessageNumber**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Family**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Address**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**InterfaceLuid**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**InterfaceIndex**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**PhysicalAddress**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**PhysicalAddressLength**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**IsRouter**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**IsUnreachable**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Flags**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LastReachable**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LastUnreachable**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NetAdapterNameNetworkPolicySetting"></a>
### NetAdapterNameNetworkPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**NetworkAdapterName**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NetworkAclPolicySetting"></a>
### NetworkAclPolicySetting


Derived from parent class: [[FiveTuple|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#fivetuple]]; [[CommonAclPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#commonaclpolicysetting]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Protocols**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LocalAddresses**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RemoteAddresses**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LocalPorts**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RemotePorts**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Priority**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Id**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Action**<br>|[[ActionType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#actiontype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Direction**<br>|[[DirectionType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#directiontype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RuleType**<br>|[[RuleType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#ruletype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NetworkAdditionalParams"></a>
### NetworkAdditionalParams


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ICSFlags**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NetworkInterface"></a>
### NetworkInterface
Referenced by: [[GuestEndpointState|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestendpointstate]]

Derived from parent class: [[InterfaceNotificationMessage|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#interfacenotificationmessage]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**MessageNumber**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Family**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdvertisingEnabled**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ForwardingEnabled**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**WeakHostSend**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**WeakHostReceive**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**UseAutomaticMetric**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**UseNeighborUnreachabilityDetection**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ManagedAddressConfigurationSupported**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**OtherStatefulConfigurationSupported**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdvertiseDefaultRoute**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RouterDiscoveryBehavior**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**DadTransmits**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|DupAddrDetectTransmits in RFC 2462.|
|**BaseReachableTime**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RetransmitTime**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**PathMtuDiscoveryTimeout**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Path MTU discovery timeout (in ms).|
|**LinkLocalAddressBehavior**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LinkLocalAddressTimeout**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|In ms.|
|**ZoneIndices**<br>|<[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Zone part of a SCOPE_ID.|
|**SitePrefixLength**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Metric**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**NlMtu**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Connected**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SupportsWakeUpPatterns**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SupportsNeighborDiscovery**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SupportsRouterDiscovery**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ReachableTime**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**TransmitOffload**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ReceiveOffload**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**DisableDefaultRoutes**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NetworkPolicy"></a>
### NetworkPolicy
Referenced by: [[HostComputeNetwork|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputenetwork]]; [[PolicyNetworkRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#policynetworkrequest]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Type**<br>|[[NetworkPolicyType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#networkpolicytype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Settings**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "NotificationBase"></a>
### NotificationBase


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Flags**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Data**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.2|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "OutboundNatPolicySetting"></a>
### OutboundNatPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**VirtualIP**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Destinations**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Exceptions**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Flags**<br>|[[NatFlags|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#natflags]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "PolicyEndpointRequest"></a>
### PolicyEndpointRequest


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Policies**<br>|<[[EndpointPolicy|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#endpointpolicy]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "PolicyNetworkRequest"></a>
### PolicyNetworkRequest


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Policies**<br>|<[[NetworkPolicy|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#networkpolicy]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "PortMappingPolicySetting"></a>
### PortMappingPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Protocol**<br>|[[ProtocolType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#protocoltype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**InternalPort**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ExternalPort**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**VIP**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Flags**<br>|[[NatFlags|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#natflags]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "PortnameEndpointPolicySetting"></a>
### PortnameEndpointPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "PrioritizedAuthenticationMethod"></a>
### PrioritizedAuthenticationMethod
Referenced by: [[EncryptionPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#encryptionpolicysetting]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Type**<br>|[[AuthenticationType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#authenticationtype]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Settings**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "ProviderAddressEndpointPolicySetting"></a>
### ProviderAddressEndpointPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ProviderAddress**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "ProviderAddressNetworkPolicySetting"></a>
### ProviderAddressNetworkPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ProviderAddress**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "ProxyExceptions"></a>
### ProxyExceptions
Referenced by: [[L4WfpProxyPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#l4wfpproxypolicysetting]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**IpAddressExceptions**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.14|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**PortExceptions**<br>|<[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]> array|[[2.14|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "QosPolicySetting"></a>
### QosPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**MaximumOutgoingBandwidthInBytes**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "RegistryKey"></a>
### RegistryKey


Derived from parent class: [[InterfaceNotificationMessage|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#interfacenotificationmessage]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**MessageNumber**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Family**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RegKeys**<br>|<[[RegKey|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#regkey]]> array|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Keyword**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdapterCLSID**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "RegKey"></a>
### RegKey
Referenced by: [[RegistryKey|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#registrykey]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Path**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Key**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Type**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Value**<br>|[[ByteArray|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.1|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "RemoteSubnetRoutePolicySetting"></a>
### RemoteSubnetRoutePolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**DestinationPrefix**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**IsolationId**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ProviderAddress**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**DistributedRouterMacAddress**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "Response"></a>
### Response


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Success**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Error**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ErrorCode**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "Route"></a>
### Route
Referenced by: [[HostComputeEndpoint|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputeendpoint]]; [[Subnet|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#subnet]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**NextHop**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**DestinationPrefix**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Metric**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "Route_2"></a>
### Route_2
Referenced by: [[GuestEndpointState|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestendpointstate]]



Derived from parent class: [[InterfaceNotificationMessage|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#interfacenotificationmessage]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**MessageNumber**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Family**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**NextHop**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**DestinationPrefix**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SitePrefixLength**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ValidLifetime**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**PreferredLifetime**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Metric**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Protocol**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Loopback**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AutoconfigureAddress**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Publish**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Immortal**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "RpcConnectionInformation"></a>
### RpcConnectionInformation
Referenced by: [[GuestNetworkService|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestnetworkservice]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**NetworkAddress**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.11|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**EndpointAddress**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.11|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**EndpointType**<br>|[[RpcEndpointType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#rpcendpointtype]]|[[2.13|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**ObjectUuid**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.13|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "SDNRoutePolicySetting"></a>
### SDNRoutePolicySetting
Referenced by: [[HostComputeRoute|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputeroute]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**DestinationPrefix**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**NextHop**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**NeedEncap**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AutomaticEndpointMonitor**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "SetPolicySetting"></a>
### SetPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Id**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**PolicyType**<br>|[[SetPolicyTypes|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#setpolicytypes]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Values**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "SourceMacAddressNetworkPolicySetting"></a>
### SourceMacAddressNetworkPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**SourceMacAddress**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "Subnet"></a>
### Subnet
Referenced by: [[Ipam|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#ipam]]; [[SubnetNetworkRequest|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#subnetnetworkrequest]]



Derived from parent class: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Owner**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Version**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**AdditionalParams**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Health**<br>|[[Health|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#health]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SchemaVersion**<br>|[[Version|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#version]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]|Schema version should be present in all objects|
|**Telemetry**<br>|[[Telemetry|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#telemetry]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**State**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**IpAddressPrefix**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Policies**<br>|<[[SubnetPolicy|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#subnetpolicy]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Routes**<br>|<[[Route|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#route]]> array|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**IpSubnets**<br>|<[[IpSubnet|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#ipsubnet]]> array|[[2.3|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Flags**<br>|[[SubnetFlags|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#subnetflags]]|[[2.7|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "SubnetNetworkRequest"></a>
### SubnetNetworkRequest


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Subnets**<br>|<[[Subnet|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#subnet]]> array|[[2.6|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "SubnetPolicy"></a>
### SubnetPolicy
Referenced by: [[Subnet|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#subnet]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Type**<br>|[[SubnetPolicyType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#subnetpolicytype]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Settings**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "Telemetry"></a>
### Telemetry
Referenced by: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Data**<br>|[[Any|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "TierAclPolicySetting"></a>
### TierAclPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Name**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.10|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Direction**<br>|[[DirectionType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#directiontype]]|[[2.10|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Order**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.10|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**TierAclRules**<br>|<[[TierAclRule|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#tieraclrule]]> array|[[2.10|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "TierAclRule"></a>
### TierAclRule
Referenced by: [[TierAclPolicySetting|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#tieraclpolicysetting]]



Derived from parent class: [[FiveTuple|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#fivetuple]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Protocols**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LocalAddresses**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RemoteAddresses**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LocalPorts**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RemotePorts**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Priority**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Id**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.10|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**TierAclRuleAction**<br>|[[ActionType|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#actiontype]]|[[2.10|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "Version"></a>
### Version
Referenced by: [[Base|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#base]]; [[HostComputeQuery|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#hostcomputequery]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Major**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Minor**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "VlanPolicySetting"></a>
### VlanPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**IsolationId**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "VmEndpointRequest"></a>
### VmEndpointRequest


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**PortId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**VirtualNicName**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**VirtualMachineId**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "VsidPolicySetting"></a>
### VsidPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**IsolationId**<br>|[[uint32|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "VSwitchExtensionNetworkPolicySetting"></a>
### VSwitchExtensionNetworkPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**ExtensionID**<br>|[[Guid|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Enable**<br>|[[bool|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "VxlanPortPolicySetting"></a>
### VxlanPortPolicySetting


|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**Port**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "Xlat"></a>
### Xlat
Referenced by: [[GuestEndpointState|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#guestendpointstate]]



Derived from parent class: [[InterfaceNotificationMessage|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#interfacenotificationmessage]]



|Field|Type|NewInVersion|Description|
|---|---|---|---|
|**MessageNumber**<br>|[[uint64|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**Family**<br>|[[uint16|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.0|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**SyntheticIPv4Address**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**FallbackIPv4Address**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LocalPrefix**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**LocalPrefixLength**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RemotePrefix**<br>|[[string|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||
|**RemotePrefixLength**<br>|[[uint8|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#json-type]]|[[2.5|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.hns_schema#schema-version-map]]||

---

<a name = "JSON-type"></a>
## JSON type

The table shows the mapping from type name for field of classes to JSON type, its format and pattern. See details in [Swagger IO spec](https://swagger.io/specification/#data-types)

|Name|JSON Type|Format|Pattern|
|---|---|---|---|
|Any|object|||
|bool|boolean|||
|ByteArray|string|byte||
|Guid|string||^[0-9A-Fa-f]{8}-([0-9A-Fa-f]{4}-){3}[0-9A-Fa-f]{12}$|
|string|string|||
|uint16|integer|uint16||
|uint32|integer|uint32||
|uint64|integer|uint64||
|uint8|integer|uint8||

---
<a name = "Schema-Version-Map"></a>
## Schema Version Map

|Schema Version|Release Version|
|---|---|
|2.0|Windows 10, version 1809 (10.0.17763.0)|
|2.6|Windows 10, version 2004 (10.0.19041.0)|
|2.11|Windows Server 2022 (10.0.20348.0)|
|2.14|Windows 11, version 2004 (10.0.2200.0)|

---