---
id: 4aR6ScbSSNLceKnPXdmiwD8
title: >-
    Hyper-V virtual machine gallery and networking improvements
desc: >-
    Learn about the the Hyper-V virtual machine gallery and related networking improvements.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2017/20170726-hyper-v-virtual-machine-gallery-and-networking-improvements
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 7b6340b88a3611d145f39d1875399958f970069971ef0c10c0cd536173415a99
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2017/20170726-hyper-v-virtual-machine-gallery-and-networking-improvements.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2017-07-26 01:42:39"
    ms.date: "07/26/2017"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2017/20170726-hyper-v-virtual-machine-gallery-and-networking-improvements.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2017/20170726-hyper-v-virtual-machine-gallery-and-networking-improvements).

# Hyper-V virtual machine gallery and networking improvements

In January, [we added Quick Create](https://blogs.technet.microsoft.com/virtualization/2017/01/10/cool-new-things-for-hyper-v-on-desktop/ "Quick Create") to Hyper-V manager in Windows 10. Quick Create is a single-page wizard for fast, easy, virtual machine creation. Starting in the latest fast-track Windows Insider builds (16237+) we’re expanding on that idea in two ways. Quick Create now includes: 

  1. A virtual machine gallery with downloadable, pre-configured, virtual machines.
  2. A default virtual switch to allow virtual machines to share the host’s internet connection using NAT.

<!--![image](https://msdnshared.blob.core.windows.net/media/2017/07/image_thumb118.png)](https://msdnshared.blob.core.windows.net/media/2017/07/image139.png)--> To launch Quick Create, open Hyper-V Manager and click on the “Quick Create…” button (1). From there you can either create a virtual machine from one of the pre-built images available from Microsoft (2) or use a local installation source. Once you’ve selected an image or chosen installation media, you’re done! The virtual machine comes with a default name and a pre-made network connection using NAT (3) which can be modified in the “more options” menu. Click “Create Virtual Machine” and you’re ready to go – granted downloading the virtual machine will take awhile. 

### Details about the Default Switch

The switch named “Default Switch” or “Layered_ICS”, allows virtual machines to share the host’s network connection. Without getting too deep into networking (saving that for a different post), this switch has a few unique attributes compared to other Hyper-V switches: 

  1. Virtual machines connected to it will have access to the host’s network whether you’re connected to WIFI, a dock, or Ethernet.
  2. It’s available as soon as you enable Hyper-V – you won’t lose internet setting it up.
  3. You can’t delete it.
  4. It has the same name and device ID (GUID c08cb7b8-9b3c-408e-8e30-5e16a3aeb444) on all Windows 10 hosts so virtual machines on recent builds can assume the same switch is present on all Windows 10 Hyper-V host.

I’m really excited by the work we are doing in this area. These improvements make Hyper-V a better tool for people running virtual machines on a laptop. They don’t, however, replace existing Hyper-V tools. If you need to define specific virtual machine settings, New-VM or the new virtual machine wizard are the right tools. For people with custom networks or complicated virtual network needs, continue using Virtual Switch Manager. Also keep in mind that all of this is a work in progress. There are rough edges for the default switch right now and there aren't many images in the gallery. Please give us feedback! Your feedback helps us. Let us know what images you would like to see and share issues by commenting on this blog or submitting feedback through Feedback Hub. Cheers, Sarah