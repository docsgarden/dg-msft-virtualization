---
id: ABkdZ7tzufiktpDWhgjFUhY
title: >-
    HCS_EVENT
desc: >-
    HCS_EVENT
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HCS_EVENT
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 6b1ff0c986d5282fb438bd7dabe64ea2ef225b0ba843c637f6d165e893a6b692
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HCS_EVENT.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HCS_EVENT']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HCS_EVENT.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HCS_EVENT).

# HCS_EVENT

## Description

The struct provides information about an event that occurred on a compute system or process.

## Syntax

```cpp
typedef struct HCS_EVENT
{
    HCS_EVENT_TYPE Type;
    PCWSTR EventData;
    HCS_OPERATION Operation;
} HCS_EVENT;
```

## Members


`Type`

Type of event [[`HCS_EVENT_TYPE`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_event_type]]

`EventData`

Optionally provides additional data for the event as a JSON document. The following table shows expected documents for specific event types.

|Event Type|JSON Document|
|---|---|
|`HcsEventSystemExited`|[[`SystemExitStatus`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#SystemExitStatus]]|
|`HcsEventSystemCrashInitiated`|[[`CrashReport`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#CrashReport]]|
|`HcsEventSystemCrashReport`|[[`CrashReport`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#CrashReport]]|
|`HcsEventProcessExited`|[[`ProcessStatus`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ProcessStatus]]|

`Operation`

Handle to a completed operation, if `Type` is `HcsEventOperationCallback`. This is only possible when [[`HcsSetComputeSystemCallback`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcssetcomputesystemcallback]] has specified event option `HcsEventOptionEnableOperationCallbacks`.


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeDefs.h |