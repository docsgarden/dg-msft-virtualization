---
id: fhYHszxdhVttwZnr2FNwrCZ
title: >-
    HCN_LOADBALANCER
desc: >-
    HCN_LOADBALANCER
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_LOADBALANCER
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: d1f9117b4b6c6b5c9d087f21e439a8b1ec4b1a2a3f63d2d4d8c8a1a80c39903e
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_LOADBALANCER.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HCN_LOADBALANCER']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_LOADBALANCER.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_LOADBALANCER).

# HCN_LOADBALANCER

## Description

Context handle referencing a LoadBalancer in HNS.

## Syntax

```cpp
typedef void* HCN_LOADBALANCER;
```


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |