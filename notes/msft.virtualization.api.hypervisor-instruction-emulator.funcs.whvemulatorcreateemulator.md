---
id: 76hu9xDXkdceHQvjN2ZrmPw
title: >-
    WHvEmulatorCreateEmulator method
desc: >-
    Learn about the WHvEmulatorCreateEmulator method.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WHvEmulatorCreateEmulator
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 4b9c963d4507f6b8e670931ffa69b5e2777401daaae3b3058e1f5e5c426d4364
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WHvEmulatorCreateEmulator.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/19/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WHvEmulatorCreateEmulator.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WHvEmulatorCreateEmulator).

# WHvEmulatorCreateEmulator


## Syntax

```c
HRESULT
WINAPI
WHvEmulatorCreateEmulator(
    _In_ const WHV_EMULATOR_CALLBACKS* Callbacks,
    _Out_ WHV_EMULATOR_HANDLE* Emulator
    );
```
### Parameters

`Callbacks`

The specified callback method

`Emulator`

Receives the handle to the newly created emulator instance

## Remarks
Create an instance of the instruction emulator with the specified callback methods