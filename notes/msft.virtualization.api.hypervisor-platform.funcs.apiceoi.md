---
id: 6L4JRLNr5C7ibCBkfYUotTc
title: >-
    APIC EOI
desc: >-
    Learn about context data for an exit caused by an APIC EOI of a level-triggered interrupt.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/ApicEOI
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 0f9ec6a6b2f8f35cef2769fb0e5b9868d5a884406d1666865c5d7c5a6b70294d
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/ApicEOI.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/20/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/ApicEOI.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/ApicEOI).

# APIC EOI


## Syntax
```C
//
// Context data for an exit caused by an APIC EOI of a level-triggered
// interrupt (WHvRunVpExitReasonX64ApicEoi)
//
typedef struct WHV_X64_APIC_EOI_CONTEXT
{
    UINT32 InterruptVector;
} WHV_X64_APIC_EOI_CONTEXT;
```

## Return Value