---
id: AGWXeXtyFcEiSYZqLidMCM3
title: >-
    WHvSetVirtualProcessorInterruptControllerState
desc: >-
    Description for understanding WHvSetVirtualProcessorInterruptControllerState and its parameters, return value, and syntax.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvSetVirtualProcessorInterruptControllerState
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: a0bf3d6c5159b0f97a88b4d73a5bef0c372639e41a8c175fdacdcb2335c8c88f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvSetVirtualProcessorInterruptControllerState.md
    author: "jstarks"
    ms.author: "jostarks"
    ms.date: "06/06/2019"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvSetVirtualProcessorInterruptControllerState.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvSetVirtualProcessorInterruptControllerState).

# WHvSetVirtualProcessorInterruptControllerState

## Syntax

```
HRESULT
WINAPI
WHvSetVirtualProcessorInterruptControllerState(
    _In_ WHV_PARTITION_HANDLE Partition,
    _In_ UINT32 VpIndex,
    _In_reads_bytes_(StateSize) const VOID* State,
    _In_ UINT32 StateSize
    );
```

### Parameters

`Partition`

Specifies the partition of the virtual processor.

`VpIndex`

Specifies the index of the virtual processor whose interrupt controller should be set.

`State`

Specifies a buffer containing the interrupt controller state.

`StateSize`

Specifies the size of the buffer, in bytes.

## Return Value

If the function succeeds, the return value is `S_OK`.

If the virtual processor is currently running, the return value is `WHV_E_INVALID_VP_STATE`.