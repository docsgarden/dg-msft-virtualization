---
id: FXAv2mn7ikRkX9e2en77Swe
title: >-
    The Hyper-V Team at VMworld 2013 - Fun Times and Frozen Custard
desc: >-
    What the Hyper-V team did at VMworld 2013.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2013/20130828-the-hyper-v-team-at-vmworld-2013-fun-times-and-frozen-custard
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 035703e669b2c6773a8ad853103e751fef7c6346185c659d382be1cbf16c1a6b
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20130828-the-hyper-v-team-at-vmworld-2013-fun-times-and-frozen-custard.md
    ms.author: "mabrigg"
    author: "mattbriggs"
    ms.date: "08/28/2013"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20130828-the-hyper-v-team-at-vmworld-2013-fun-times-and-frozen-custard.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2013/20130828-the-hyper-v-team-at-vmworld-2013-fun-times-and-frozen-custard).

# The Hyper-V Team at VMworld 2013

Many VMworld 2013 attendees looked at Hyper-V a long time ago, and haven’t kept up with the progress our platform has made in recent years. If you count yourself among this group, we at Microsoft would love to show you how far we’ve come. However, you needn’t take my word for it – I encourage you to find out for yourself. The Hyper-V team took their message and information to VMworld 2013 in a fun and creative way. Varun Chhabra, Senior Product Marketing Manager, Server and Tools blogs about the experience in “[Planes, trucks and frozen custard - The Hyper-V Team at VMworld 2013](https://blogs.technet.com/b/windowsserver/archive/2013/08/28/planes-trucks-and-frozen-custard-the-hyper-v-team-at-vmworld-2013.aspx)” where he had the opportunity to talk with customers, potential customers, and members of the VMWare staff.  It is an insightful view.  Check it out! 

And for those of you interested in downloading some of the other products and trying them, here are some resources to help you:

  * Windows Server 2012 R2 Preview [download](https://technet.microsoft.com/evalcenter/dn205286.aspx)
  * System Center 2012 R2 Preview [download](https://technet.microsoft.com/evalcenter/dn205295)
  * SQL Server 2014 Community Technology Preview 1 (CTP1) [download](https://technet.microsoft.com/evalcenter/dn205290.aspx)
  * Windows 8.1 Enterprise Preview [download](https://technet.microsoft.com/windows/hh771457.aspx?ocid=wc-blog-wfyb)