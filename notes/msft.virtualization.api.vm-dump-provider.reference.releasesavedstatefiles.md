---
id: 4uxyEBTYyRrc7wCgyJpARJZ
title: >-
    The ReleaseSavedStateFiles function
desc: >-
    Releases the given VmSavedStateDump provider that matches the supplied ID.
canonicalUrl: https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/ReleaseSavedStateFiles
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 9da88df2492dd760ddd5d8ef5eacfcb6b7c0a7d528a7361e2e339dcc86a24e5d
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/ReleaseSavedStateFiles.md
    ms.date: "04/19/2022"
    author: "mattbriggs"
    ms.author: "mabrigg"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/ReleaseSavedStateFiles.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/ReleaseSavedStateFiles).

# ReleaseSavedStateFiles function

Releases the given VmSavedStateDump provider that matches the supplied ID. Releasing the provider releases the locks to the saved state files. This means that it won't be available for use on other methods.

## Syntax
```C

HRESULT
WINAPI
ReleaseSavedStateFiles(
    _In_    VM_SAVED_STATE_DUMP_HANDLE      VmSavedStateDumpHandle
    );
```

## Parameters

`VmSavedStateDumpHandle`

Supplies the ID of the dump provider instance to release.

## Return Value

If the operation completes successfully, the return value is `S_OK`.

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |