---
id: 3edecaNpRtiwGTkKcqPNVzu
title: >-
    Microsoft’s Hyper-V Cloud Fast Track Accelerates Private Cloud Deployment
desc: >-
    Microsoft’s Hyper-V Cloud Fast Track Accelerates Private Cloud Deployment
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110519-microsofts-hyper-v-cloud-fast-track-accelerates-private-cloud-deployment
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 15942c5dd93f5dd470be864ae43eef42e2e36a1f03cdf6963ccd8b3698fe22ab
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110519-microsofts-hyper-v-cloud-fast-track-accelerates-private-cloud-deployment.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "05/19/2011"
    date: "2011-05-19 10:21:45"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110519-microsofts-hyper-v-cloud-fast-track-accelerates-private-cloud-deployment.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110519-microsofts-hyper-v-cloud-fast-track-accelerates-private-cloud-deployment).

# Microsoft’s Hyper-V Cloud Fast Track Accelerates Private Cloud Deployment

What an exciting week at Tech Ed for [Private Cloud](https://www.microsoft.com/virtualization/en/us/private-cloud.aspx) solutions from Microsoft and our great partners!   It started with the announcement of [NetApp and Cisco joining the Hyper-V Cloud Fast Track program](http://www.netapp.com/us/company/news/news-rel-20110516-918927.html) and bringing their solution to market immediately.  We had a session where Alex Jauch from NetApp did a very cool demo. He showed [provisioning](https://www.youtube.com/watch?v=rdjCBTTXAx8) of Cisco UCS blades via an Opalis workflow and PowerShell. He followed that with a [Disaster Recovery](https://www.youtube.com/watch?v=jkXz7pouFhY) scenario – bringing down a private cloud in Seattle and bringing the infrastructure back up in Tacoma without losing connectivity to the hosted applications.        Next, [HP’s private cloud offering in the Fast Track](http://h71028.www7.hp.com/enterprise/us/en/partners/microsoft-cloud-foundation.html?jumpid=ex_r2858_us/en/large/tsg/microsoft_cloud) program provided an incredible display of power – supporting thousands of VM’s on just a 16-node configuration.   It was amazing to see this system in action, specifically the quick provisioning and de-provisioning of virtual machines, automating the process of workload balancing and the ability to keep the infrastructure available through advanced monitoring and automation.   This live Fast Track implementation clearly demonstrates the benefit of shared resources pools with advanced automation and management.   And to top it all off, Fujitsu announced on Wednesday that their [Fast Track](http://www.fujitsu.com/global/news/pr/archives/month/2011/20110518-01.html) offering is coming to market.  Fujitsu’s is based on their Fujitsu PRIMERGY BX900 blade server system and ETERNUS storage systems. 

Keep looking [here](https://blogs.technet.com/b/virtualization/) for updates on how to implement private clouds in your organization – today - with [Hyper-V Cloud Fast Track](https://www.microsoft.com/virtualization/en/us/hyperv-cloud-fasttrack.aspx)  offerings from partners around the globe.