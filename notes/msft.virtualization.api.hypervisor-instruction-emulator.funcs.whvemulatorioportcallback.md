---
id: AueGCvo4HUXp37g3sCUAbhK
title: >-
    WHV_EMULATOR_IO_PORT_CALLBACK method
desc: >-
    Learn about the WHV_EMULATOR_IO_PORT_CALLBACK method.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorIOPortCallback
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 80a16dd83fb9f584d6bb5d122ac5e6692f2fca274aed91c8f81d50370879dfef
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorIOPortCallback.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/19/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorIOPortCallback.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorIOPortCallback).

# WHV_EMULATOR_IO_PORT_CALLBACK


## Syntax

```c
typedef HRESULT (CALLBACK *WHV_EMULATOR_IO_PORT_CALLBACK)(
    _In_ VOID* Context,
    _Inout_ WHV_EMULATOR_IO_ACCESS_INFO* IoAccess
    );
```

## Return Value
The callback should return `S_OK` on success, and some error value on failure. Any error value returned here will terminate emulation and return from the corresponding emulation call.

## Remarks
Callback notifying the virtualization stack that the current instruction has
modified the IO Port specified in the IoAccess structure. Context is the value
specified in the emulation call which identifies this current instance of the emulation.