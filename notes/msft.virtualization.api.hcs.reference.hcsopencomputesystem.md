---
id: AX6DKYNZrekuwoPgRHM3rvb
title: >-
    HcsOpenComputeSystem
desc: >-
    HcsOpenComputeSystem
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/reference/HcsOpenComputeSystem
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 0ddec4828fe84e8b3e3da3ab13597198090a1a6df2c91bdf1a856f3a12f59c72
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/reference/HcsOpenComputeSystem.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsOpenComputeSystem']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/reference/HcsOpenComputeSystem.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/reference/HcsOpenComputeSystem).

# HcsOpenComputeSystem

## Description

Opens a handle to an existing compute system, see [[sample code|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.computesystemsample#openvm]].

## Syntax

```cpp
HRESULT WINAPI
HcsOpenComputeSystem(
    _In_  PCWSTR id,
    _In_  DWORD requestedAccess,
    _Out_ HCS_SYSTEM* computeSystem
    );
```

## Parameters

`id`

Unique Id identifying the compute system.

`requestedAccess`

Reserved for future use, must be `GENERIC_ALL`.

`computeSystem`

Receives a handle to the compute system. It is the responsibility of the caller to release the handle using [[HcsCloseComputeSystem|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsclosecomputesystem]] once it is no longer in use.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |