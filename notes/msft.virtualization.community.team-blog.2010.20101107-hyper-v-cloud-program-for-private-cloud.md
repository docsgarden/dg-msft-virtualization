---
id: AgLaBMGNH4fZJPnXcdjHM3c
title: >-
    Hyper-V Cloud Program for private cloud
desc: >-
    Several items from Microsoft TechEd Europe that you'll be interested to know if interested in private cloud computing, virtualization and systems management.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20101107-hyper-v-cloud-program-for-private-cloud
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 9e665eef13f36eacfc583c16547136f982482066f66fd70a0337b51704ad87e6
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20101107-hyper-v-cloud-program-for-private-cloud.md
    author: "scooley"
    ms.author: "scooley"
    date: "2010-11-07 23:30:00"
    ms.date: "11/07/2010"
    categories: "dell"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20101107-hyper-v-cloud-program-for-private-cloud.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20101107-hyper-v-cloud-program-for-private-cloud).

# Hyper-V Cloud Program for private cloud

Hello from Berlin. [Microsoft TechEd Europe](http://www.msteched.com/) started today with Brad Anderson's keynote. Aside from the keynote, you can read the news release [here](https://www.microsoft.com/presspass/press/NewsArchive.mspx?cmbContentType=PressRelease). There are several items that you'll be interested to know if interested in private cloud computing, virtualization and systems management. The [Hyper-V Cloud program](https://www.microsoft.com/privatecloud) is the main item. Here's a few of the several components of the program. **[Hyper-V Cloud Fast Track program](https://www.microsoft.com/virtualization/en/us/hyperv-cloud-fasttrack.aspx)**. Oliver wrote about is at the [Windows Server Division blog](https://blogs.technet.com/b/windowsserver/archive/2010/11/08/hyper-v-cloud-fast-track.aspx). This reference architecture, available from any of the 6 major OEMs, reduces the risk and time to private cloud creation and deployment. These OEM partners represent more than 80% of the worldwide server market, so we likely have you covered. The Microsoft components to the reference architecture include Windows Server 2008 R2 Hyper-V, System Center Operations Manager, System Center Virtual Machine Manager, and options include SC Configuration Manager, SC Data Protection Manager and SC Virtual Machine Manager Self-Service Portal 2.0. Adam Fazio, an architect within Microsoft Consulting Services, will have a blog post tomorrow to detail the reference architecture. To whet your appetite, here's some scoop: 

  * Each OEM vendor has pre-defined compute, network and storage configurations. Dell blogged [here](http://en.community.dell.com/dell-blogs/enterprise/b/tech-center/archive/2010/11/08/architecting-the-dell-desktop-virtualization-solution-powered-by-microsoft-windows-server-2008-r2-sp1.aspx); NEC blogged [here](http://www.nectoday.com/index.php/straighttalk); Hitachi Data Systems blogged [here](http://blogs.hds.com/miki/2010/11/participation-hyper-v-cloud-fast-track.html).
  * HP's configuration is called [HP CloudStart for Hyper-V](http://www.hp.com/go/microsoft/hyperv/cloud) and is based on HP BladeSystem Matrix and EVA storage.  They'll get you deployed within 30 days starting at $250,000. HP blog is [here](http://h30507.www3.hp.com/t5/Converged-Infrastructure/Taking-practical-steps-to-the-cloud-with-HP-and-Microsoft-Part/ba-p/83055).
  * HP, Dell and IBM have reference architectures available today, and the other 3 will be available shortly. 

**Hyper-V Cloud Deployment Guides**. For people who want to build their own private clouds on Microsoft technology. These guides are based on over 1,000 Microsoft Consulting Services engagements over the past couple years. These guides are for you that want the highest levels of flexibility, control and customization. See more [here](https://www.microsoft.com/virtualization/en/us/private-cloud-get-started.aspx#Hyper-V-Cloud-Deployment-Guides). **Hyper-V Cloud Service Provider program** : [This program](https://www.microsoft.com/virtualization/en/us/hyperv-cloud-service-providers.aspx) is the next version of the [Dynamic Datacenter Alliance](https://www.microsoft.com/hosting/dynamicdatacenter/Alliance.html), which we introduced to service providers a couple years ago. More than 70 service providers covering more than 30 countries (and there's nearly 100 more coming) offer infrastructure as a fully-hosted service built on Windows Server Hyper-V and System Center, and the Dynamic Datacenter toolkit. This toolkit provides on-demand VM provisioning, sample portal and prescriptive guidance. Some service providers already on board include Terremark, Hosting.com, Hostway, Outsourcery, Strato. Some of you might be wondering ... how is this different than vBlock? I'd say three reasons: 

·        There’s proven private cloud solutions from 6 different system vendors, using an open framework.

·        Customers can get to market quickly – we’ve heard of the laborious deployments with v1 of vBlock. 

·        Customers will use consistent and familiar technologies, and can manage IT services and applications with a common management suite. 

Beyond these three reasons, we also deliver identity, database tools, application development tools, and management tools that span across private cloud and public cloud, to include Windows Azure. You have the control to build, run, migrate and extend apps to Windows Azure. 

Patrick