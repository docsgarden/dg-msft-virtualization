---
id: 3sW4XpcCeJubeyKHbL6oh5e
title: >-
    HvFlushVirtualAddressListEx
desc: >-
    HvFlushVirtualAddressListEx hypercall
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallFlushVirtualAddressListEx
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: ce0fbd4d9f09a4dd0fb2932cc586fcebfb4aa4e40e461a1bbb86603083de6d5f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallFlushVirtualAddressListEx.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallFlushVirtualAddressListEx.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallFlushVirtualAddressListEx).

# HvFlushVirtualAddressListEx

The HvFlushVirtualAddressListEx hypercall is similar to [[HvCallFlushVirtualAddressList|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallflushvirtualaddresslist]], but can take a variably-sized sparse VP set as an input.
The following checks should be used to infer the availability of this hypercall:

- ExProcessorMasks must be indicated via CPUID leaf 0x40000004.

## Interface

 ```c
HV_STATUS
HvCallFlushVirtualAddressListEx(
    _In_ HV_ADDRESS_SPACE_ID AddressSpace,
    _In_ HV_FLUSH_FLAGS Flags,
    _In_ HV_VP_SET ProcessorSet,
    _Inout_ PUINT32 GvaCount,
    _In_reads_(GvaCount) PCHV_GVA GvaRangeList
    );
 ```

## Call Code
`0x0014` (Rep)

## Input Parameters

| Name                    | Offset     | Size     | Information Provided                      |
|-------------------------|------------|----------|-------------------------------------------|
| `AddressSpace`          | 0          | 8        | Specifies an address space ID (a CR3 value). |
| `Flags`                 | 8          | 8        | Set of flag bits that modify the operation of the flush. |
| `ProcessorSet`          | 16         | Variable | Processor set indicating which processors should be affected by the flush operation. |

## Input List Element

| Name                    | Offset     | Size     | Information Provided                      |
|-------------------------|------------|----------|-------------------------------------------|
| `GvaRange`              | 0          | 8        | GVA range                                 |

## See also

[[HV_VP_SET|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.datatypes.HV_VP_SET]]