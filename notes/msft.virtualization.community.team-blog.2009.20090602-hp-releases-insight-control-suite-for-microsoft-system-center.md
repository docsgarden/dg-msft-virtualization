---
id: 3Ro5iPPMmK5Zev5YQngFE59
title: >-
    HP releases Insight Control suite for Microsoft System Center
desc: >-
    Some exciting news related to Microsoft’s virtualization and System Center offerings and HP’s Insight Control Suite for System Center.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090602-hp-releases-insight-control-suite-for-microsoft-system-center
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 5535d1af41fa5dd54b8dbd3a1bbd33be3f6584c7a82ac6b26c611f7cc22898da
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090602-hp-releases-insight-control-suite-for-microsoft-system-center.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-06-02 05:00:00"
    ms.date: "06/02/2009"
    categories: "hp"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090602-hp-releases-insight-control-suite-for-microsoft-system-center.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090602-hp-releases-insight-control-suite-for-microsoft-system-center).

# HP releases Insight Control suite for Microsoft System Center

Hello - I’m Zane Adam, senior director of System Center and Virtualization marketing at Microsoft. I wanted to share some exciting news related to Microsoft’s virtualization and System Center offerings and HP’s Insight Control Suite for System Center.  [With the official release today of Insight Control Suite for System Center](https://blogs.technet.com/systemcenter/archive/2009/06/02/guest-blog-post-hp-delivers-industry-first-management-capabilities-for-microsoft-system-center.aspx "HP Insight Control suite for System Center announcement"), HP has taken a significant step in advancing the dynamic datacenter for customers with HP ProLiant and HP BladeSystem platforms by enabling HP Insight Control capabilities to be invoked directly from the Microsoft System Center consoles.

 

We believe the combination of the System Center suite, Microsoft’s virtualization offerings and Insight Control suite for System Center will help further ease the customer experience by reducing complexity and the requirements on administrator’s time, delivering integrated management of virtual machines, the underlying host systems and optimizing the utilization of resources, power and space. Today’s release of ICE-SC includes a combination of functionality delivered seamlessly through System Center and HP Systems Insight Manager.  At a high level, the fully integrated components include:

 

}  Bare Metal deployment – delivered via an Operating System Deployment (OSD) toolkit that plugs directly into System Center Configuration Manager

}  PRO pack integration – the ability to manage virtual machines running on ProLiant host systems through System Center consoles

}  Lights out remote management – via an iLO Advanced license

}  Enhanced configuration and inventory information for ProLiant systems surfaced in the System Center Configuration Manager console

}  ProLiant system health monitoring from the System Center Operations Manager console

}  All backed by the power of the HP Services organization with 30,000 Microsoft-trained professionals

Congratulations to HP on bringing this offering to market and providing these powerful capabilities to our mutual customers.  Be sure to check out the [guest blog post on the System Center blog](https://blogs.technet.com/systemcenter/archive/2009/06/02/guest-blog-post-hp-delivers-industry-first-management-capabilities-for-microsoft-system-center.aspx "HP Insight Control suite for System Center guest blog post") from Scott Farrand of HP.

 

Thanks - Zane