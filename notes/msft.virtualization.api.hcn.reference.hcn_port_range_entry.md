---
id: AUHPbH4n9QvrjGJerQq8uDn
title: >-
    HCN_PORT_RANGE_ENTRY
desc: >-
    HCN_PORT_RANGE_ENTRY
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_PORT_RANGE_ENTRY
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 7df2faac092d0fa942a76ad01f8861174dfed9afc226a424c5cb8eb95da3c41f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_PORT_RANGE_ENTRY.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HCN_PORT_RANGE_ENTRY']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_PORT_RANGE_ENTRY.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_PORT_RANGE_ENTRY).

# HCN\_PORT\_RANGE\_ENTRY

## Description

A range of ports for a reservation.

## Syntax

```cpp
typedef struct tagHCN_PORT_RANGE_ENTRY {
    GUID OwningPartitionId;
    GUID TargetPartitionId;
    HCN_PORT_PROTOCOL Protocol;
    UINT64 Priority;
    UINT32 ReservationType;
    UINT32 SharingFlags;
    UINT32 DeliveryMode;
    UINT16 StartingPort;
    UINT16 EndingPort;
} HCN_PORT_RANGE_ENTRY, *PHCN_PORT_RANGE_ENTRY;
```

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |