---
id: f8tmP4XWBFonKpqp5FhAqaa
title: >-
    Azure Site Recovery adds InMage Scout to Its Portfolio for Any Virtual and Physical Workload Disaster Recovery
desc: >-
    This article shares how Azure Site Recovery acquired InMage Systems Inc.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2014/20140717-azure-site-recovery-adds-inmage-scout-to-its-portfolio-for-any-virtual-and-physical-workload-disaster-recovery
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: e11e7247aafd78d4ae7d8dccb9a77ee3982f7581956f33a32c665bca0e3925fe
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140717-azure-site-recovery-adds-inmage-scout-to-its-portfolio-for-any-virtual-and-physical-workload-disaster-recovery.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "07/17/2014"
    date: "2014-07-17 01:14:44"
    categories: "asr"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140717-azure-site-recovery-adds-inmage-scout-to-its-portfolio-for-any-virtual-and-physical-workload-disaster-recovery.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2014/20140717-azure-site-recovery-adds-inmage-scout-to-its-portfolio-for-any-virtual-and-physical-workload-disaster-recovery).

# InMage Systems Inc Acquisition

Azure Site Recovery with Hyper-V Replica already supports the ability to set up disaster recovery between your two Windows Server 2012+ Hyper-V data centers, or between your Windows Server 2012 R2 Hyper-V data center and Microsoft Azure. Recently we acquired [**InMage Systems Inc**.](https://blogs.microsoft.com/blog/2014/07/11/microsoft-acquires-inmage-better-business-continuity-with-azure/), an innovator in the emerging area of cloud-based business continuity. InMage offers migration and disaster recovery capabilities for heterogeneous IT environments with workloads running on **any hypervisor (e.g. VMware) or even on physical servers**. InMage ’s flagship product Scout is now available as a limited period **free trial** from the Azure Site Recovery management portal. 

To learn more, download and try out Azure Site Recovery with InMage Scout, please read my colleague Gaurav Daga’s blog [Azure Site Recovery Now Offers Disaster Recovery for Any Physical or Virtualized IT Environment with InMage Scout](https://azure.microsoft.com/blog/2014/07/16/azure-site-recovery-now-offers-disaster-recovery-for-any-physical-or-virtualized-it-environment-with-inmage-scout-2). 

You can find more details about Azure Site Recovery @ <https://azure.microsoft.com/services/site-recovery/>. If you have questions when using the product, post them @ <https://social.msdn.microsoft.com/Forums/windowsazure/en-US/home?forum=hypervrecovmgr> or in this blog. You can also share your feedback on your favorite features/gaps @ <https://feedback.azure.com/forums/256299-site-recovery>