---
id: 7cgs36gP8sgsRQ4m6tAWNaz
title: >-
    New Hyper-V Survey
desc: >-
    Send us some feedback using this survey about shielded virtual machines and troubleshooting using Hyper-V.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2016/20160216-new-hyper-v-survey
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: ef20eda38fe8f22dcaa0d45054cbe7e362984b2e7c86f9b48ad6f4b3266fa55a
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2016/20160216-new-hyper-v-survey.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2016-02-16 23:31:44"
    ms.date: "02/16/2016"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2016/20160216-new-hyper-v-survey.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2016/20160216-new-hyper-v-survey).

# New Hyper-V Survey

We want to hear from you regarding shielded VMs and troubleshooting! When you have about 5 to 10 minutes, please take [this short survey](https://www.instant.ly/s/t5uu5?s=vbl "Shielded VM Survey"). The survey will close on February 23rd 2016, please submit your answers by then. It is recommended to take the survey on a desktop browser so the questions show up properly. Thank you and have a great day. Survey URL: <https://www.instant.ly/s/t5uu5?s=vbl>   Thank you for your participation! Lars Iwer