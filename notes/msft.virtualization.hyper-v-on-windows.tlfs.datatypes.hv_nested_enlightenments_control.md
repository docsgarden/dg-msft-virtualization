---
id: LKohX4Bx7WfNPxTJiEYogH8
title: >-
    HV_NESTED_ENLIGHTENMENTS_CONTROL
desc: >-
    HV_NESTED_ENLIGHTENMENTS_CONTROL
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_nested_enlightenments_control
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 1f4f8ba7acedda26dc82a1f25706e6874cd3c042de2959dac9dd7adae20a6a3d
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_nested_enlightenments_control.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_nested_enlightenments_control.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_nested_enlightenments_control).

# HV_NESTED_ENLIGHTENMENTS_CONTROL

Control structure that allows a hypervisor to indicate to its parent hypervisor which nested enlightenment privileges are to be granted to the current nested guest context.

## Syntax

```c
typedef struct
{
    union {
        UINT32 AsUINT32;
        struct
        {
            UINT32 DirectHypercall:1;
            UINT32 VirtualizationException:1;
            UINT32 Reserved:30;
        };
    } Features;

    union
    {
        UINT32 AsUINT32;
        struct
        {
            UINT32 InterPartitionCommunication:1;
            UINT32 Reserved:31;
        };
     } HypercallControls;
} HV_NESTED_ENLIGHTENMENTS_CONTROL, *PHV_NESTED_ENLIGHTENMENTS_CONTROL;
 ```

## See also

[[HV_VP_ASSIST_PAGE|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.datatypes.hv_vp_assist_page]]