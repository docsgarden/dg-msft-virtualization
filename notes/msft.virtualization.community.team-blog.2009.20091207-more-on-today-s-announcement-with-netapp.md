---
id: 4oCJg8h9nfUmYDrNLGtReVL
title: >-
    More on today's announcement with NetApp
desc: >-
    More on today's announcement with NetApp
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20091207-more-on-today-s-announcement-with-netapp
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 4d6151e9c1262a76d45cd284f7a6ff2525e259462ccd544ad692dcd6816e3867
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091207-more-on-today-s-announcement-with-netapp.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "12/07/2009"
    date: "2009-12-07 22:06:00"
    categories: "cloud-computing"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091207-more-on-today-s-announcement-with-netapp.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20091207-more-on-today-s-announcement-with-netapp).

# More on today's announcement with NetApp

We published some news today with NetApp; see [here](https://www.microsoft.com/presspass/press/2009/dec09/12-08netapppr.mspx "news release"). In short, Microsoft and NetApp... 

> today announced a new three-year agreement that deepens product collaboration and technical integration, and extends joint sales and marketing activities to customers worldwide. Under the new agreement, the two companies will collaborate and deliver technology solutions that span virtualization, private cloud computing, and storage and data management, enabling customers to increase data center management efficiencies, reduce costs, and improve business agility.

 As [NetApp's partner site](https://cloud.netapp.com/blog/ma-anf-blg-windows-virtual-desktop-implementation "NetApp site on MS.com/virtualization") states, we'll work with NetApp to sell/market disaster recovery solutions using NetApp HA and DR storage with Hyper-V and System Center. There's a sample customer blueprint (.pdf) [here](https://www-download.netapp.com/edm/TT/docs/Seattle_Retail_Report_040909.pdf "pdf ") with datacenter configs. And there's datacenter consolidation solutions, which are discussed in this white paper ([here](http://www-download.netapp.com/edm/gen/20090129_TOT_Articles/TOT_Virtualize_Hyper-V_13JAN09-CB.pdf "white paper")). 

 Above and beyond this, examples of technical integration include

  * the creation of NetApp's SnapManager for Hyper-V, which integrates their storage with Hyper-V and supports CSV.
  * NetApp management pack for System Center Operations Manager



In addition, NetApp is working with the [Dynamic Datacenter Toolkit for enterprises](https://blogs.technet.com/ddcalliance/archive/2009/10/16/dynamic-data-center-toolkit-for-the-enterprise-extensibility-story-and-opportunities-for-hardware-partners.aspx "blog post") and Powershell commandlets to rapidly provision VMs as the foundation for private cloud computing environments. 

Patrick O'Rourke