---
id: A4ewPSWeporM4scd3kquDD5
title: >-
    Hey channel partners&#58; Microsoft and Citrix V-Alliance
desc: >-
    For you channel partners out there, if you don't know about the Microsoft and Citrix V-Alliance, then speak with either your Microsoft or Citrix contacts.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20101013-hey-channel-partners-microsoft-and-citrix-v-alliance
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: f1d7fb0b220cb2accd81744b02e0d8ec1a463250186db3905b0954014f99fbb8
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20101013-hey-channel-partners-microsoft-and-citrix-v-alliance.md
    author: "scooley"
    ms.author: "scooley"
    date: "2010-10-13 06:05:53"
    ms.date: "10/13/2010"
    categories: "citrix"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20101013-hey-channel-partners-microsoft-and-citrix-v-alliance.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20101013-hey-channel-partners-microsoft-and-citrix-v-alliance).

# Microsoft and Citrix V-Alliance

For you channel partners out there, if you don't know about the Microsoft and Citrix V-Alliance, then speak with either your Microsoft or Citrix contacts. The program is rolled out in Europe, is rolling out in the U.S., and will kick-off soon in Asia. The website is [here](http://www.v-alliance.net/index.aspx).  Here's a brief video that I recorded with Citrix's Klaus Oestermann today from VMworld Europe 2010. If you're a channel partner in Copenhagen, and want to learn more, stop by MIcrosoft booth #69, Citrix booth #80, or tonight at the Microsoft Tweetup [here](http://twtvite.com/msvirt). [note: O’Learys Sports Bar is in Copenhagen’s Central Station. On S Train route maps, look for København H, which is Central Station. Once you’ve arrive Central Station, O’Learys is in the far corner on the right when you enter the main hall from the train platforms. [View:https://www.youtube.com/watch?v=HpLmj4-lfAg]  

Patrick