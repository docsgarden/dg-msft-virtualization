---
id: 6yfpr4vF8UTHM7YLqNcDEki
title: >-
    HCS_EVENT_CALLBACK
desc: >-
    HCS_EVENT_CALLBACK
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HCS_EVENT_CALLBACK
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 7ca6b49038e59d3b18d3f354a5bdc2a6bf078c096704d25f9bff038211bdb6ff
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HCS_EVENT_CALLBACK.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HCS_EVENT_CALLBACK']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HCS_EVENT_CALLBACK.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HCS_EVENT_CALLBACK).

# HCS_EVENT_CALLBACK

## Description

Function type for compute system event callbacks.

## Syntax

```cpp
typedef void (CALLBACK *HCS_EVENT_CALLBACK)(
    _In_ HCS_EVENT* event,
    _In_opt_ void* context
    );
```

## Parameters

`event`

Handle to the pointer of [[`HCS_EVENT`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_event]].

`context`

Handle for context of callback.

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeDefs.h |