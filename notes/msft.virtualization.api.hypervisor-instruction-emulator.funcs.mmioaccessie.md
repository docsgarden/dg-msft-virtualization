---
id: 3BuF5heangFDQWHRNZJ8Rqp
title: >-
    MMIO Access for QEMU
desc: >-
    Learn about the MMIO Access for QEMU.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/MMIOAccessIE
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 5950f7bd52ec11056ff7c8b8e211140bbfcf6398f281c5da6e85f91976e60be5
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/MMIOAccessIE.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/19/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/MMIOAccessIE.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/MMIOAccessIE).

# MMIO Access for QEMU


## Syntax
```C
typedef struct { 
    UINT64 GpaAddress; // GPA address of the memory access 
    UINT8 Direction; // Read or write 
    UINT8 AccessSize; // 1, 2, 4, or 8 bytes 
    union { 
        UINT64 Value; // Input value (for write), output value (for read) 
        UINT64 GpaAddress2; // GPA address of the second instruction operand 
    }; 
} MMIO_ACCESS_INFO; 
```

## Remarks
This information is derived by decoding the instruction that caused an `RunVpExitMemoryAccess` exit for a virtual processor.