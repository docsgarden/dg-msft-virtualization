---
id: 4Cvira8pfYRjbRjCUbSMoEV
title: >-
    Get the Scoop - FREE Frozen Custard and Hyper-V
desc: >-
    Learn about Hyper-V while attending VMWorld.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2013/20130826-get-the-scoop-free-frozen-custard-and-hyper-v
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 075d35ae2a94be9b9f8edc1c0ee5e779038828b768ca3838ddd9000ccb736d07
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20130826-get-the-scoop-free-frozen-custard-and-hyper-v.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2013-08-26 10:05:51"
    ms.date: "08/26/2013"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20130826-get-the-scoop-free-frozen-custard-and-hyper-v.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2013/20130826-get-the-scoop-free-frozen-custard-and-hyper-v).

# Get the Scoop - FREE Frozen Custard and Hyper-V

<!-- ![](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/50/45/6663.custard%20truck.jpg)] -->
[https://www.virtualizationsquared.com/](https://www.virtualizationsquared.com/)VMworld is in full swing and Microsoft is there to actively participate.  One obvious question that VMworld attendees (and IT professionals that are familiar with VMware) are probably asking right now – “why would I want to learn about Hyper-V while attending VMworld?” The answer is simple – to help their careers as technology professionals. Research shows that over 70% of businesses now have more than one virtualization platform in their IT environment. As you can imagine, this trend is opening up opportunities for IT professionals that are familiar with more than one virtualization platform. And if you look at the market data, it is clear that Hyper-V is the one to watch (and try!) .

Sound interesting? Get more information on our activities at VMworld 2013 from Microsoft Senior PMM Varun Chhabra in his post, “[Get the “Scoop” on Hyper-V During VMworld](https://blogs.technet.com/b/windowsserver/archive/2013/08/26/get-the-scoop-on-hyper-v-during-vmworld.aspx)”. Well worth the read on how we compare.

For those of you interested in downloading some of the products and trying them, here are some resources to help you:

  * Windows Server 2012 R2 Preview [download](https://technet.microsoft.com/evalcenter/dn205286.aspx)
  * System Center 2012 R2 Preview [download](https://technet.microsoft.com/evalcenter/dn205295)
  * SQL Server 2014 Community Technology Preview 1 (CTP1) [download](https://technet.microsoft.com/evalcenter/dn205290.aspx)
  * Windows 8.1 Enterprise Preview [download](https://technet.microsoft.com/windows/hh771457.aspx?ocid=wc-blog-wfyb)



As always, follow us on Twitter via @MSCloud!  And if you would like to follow Microsoft VP Brad Anderson, do that via @InTheCloudMSFT !