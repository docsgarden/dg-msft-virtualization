---
id: AhACSANNo3yuL9Dm6P7oo9Y
title: >-
    HCN_ENDPOINT
desc: >-
    HCN_ENDPOINT
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_ENDPOINT
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: e42ee22b42332227239813189d1bc790e54ae76fa0ea67554ef39bfa08ce03e1
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_ENDPOINT.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HCN_ENDPOINT']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_ENDPOINT.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_ENDPOINT).

# HCN\_ENDPOINT

## Description

Context handle referencing an Endpoint in HNS.


## Syntax

```cpp
typedef void* HCN_ENDPOINT;
```

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |