---
id: AtfpkVxiSkWJ3A869ZVesVx
title: >-
    HcsInitializeLegacyWritableLayer
desc: >-
    HcsInitializeLegacyWritableLayer
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsInitializeLegacyWritableLayer
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 2a042410c753c6bf8d3f5e281581104bb5d6e1cfd2cfced0d805bebb11b2df65
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsInitializeLegacyWritableLayer.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsInitializeLegacyWritableLayer']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsInitializeLegacyWritableLayer.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsInitializeLegacyWritableLayer).

# HcsInitializeLegacyWritableLayer

## Description

This function initializes the writable layer for a container (e.g. the layer that captures the filesystem and registry changes caused by executing the container).

## Syntax

```cpp
HRESULT WINAPI
HcsInitializeLegacyWritableLayer(
    _In_ PCWSTR writableLayerMountPath,
    _In_ PCWSTR writableLayerFolderPath,
    _In_ PCWSTR layerData,
    _In_opt_ PCWSTR options
    );

```

## Parameters

`writableLayerMountPath`

Full path to the root directory of the writable layer.

`writableLayerFolderPath`

The legacy hive folder with the writable layer.

`layerData`

JSON document of [[layerData|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#LayerData]] providing the locations of the antecedent layers that are used by teh writable layer.

`options`

Optional JSON document specifying the options for how to initialize the sandbox (e.g. which filesystem paths should be pre-expanded in the sandbox).

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeStorage.h |
| **Library** | ComputeStorage.lib |
| **Dll** | ComputeStorage.dll |