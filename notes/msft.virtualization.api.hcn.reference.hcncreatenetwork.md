---
id: 4MVrniuoDgUP2KoXfzefVrj
title: >-
    HcnCreateNetwork
desc: >-
    HcnCreateNetwork
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnCreateNetwork
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: fc9bd532a54a94e7f3fd2b18e34a84769e16e4fa49c84321053dd79f0ee967a2
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnCreateNetwork.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnCreateNetwork']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnCreateNetwork.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnCreateNetwork).

# HcnCreateNetwork

## Description

Creates a network.

## Syntax

```cpp
HRESULT
WINAPI
HcnCreateNetwork(
    _In_ REFGUID Id,
    _In_ PCWSTR Settings,
    _Out_ PHCN_NETWORK Network,
    _Outptr_opt_ PWSTR* ErrorRecord
    );
```

## Parameters

`Network`

Network for the new network.

`Id`

Id for the new network.

`Settings`

JSON document specifying the settings of the [[HostComputeNetwork|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.HNS_Schema#HostComputeNetwork]].

`Network`

Receives a handle to the newly created network. It is the responsibility of the caller to release the handle using [[HcnCloseNetwork|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnclosenetwork]] once it is no longer in use.

`ErrorRecord`

Receives a JSON document with extended errorCode information. The caller must release the buffer using CoTaskMemFree.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |