---
id: 6bohmwGJkntFsJ6qDo4C9LE
title: >-
    Windows Server 8&#58; Standards-Based Storage Management
desc: >-
    Windows Server Standards-Based Storage Management
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20111014-windows-server-8-standards-based-storage-management
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: f418eae43a1647d0c0354a97a51da47af67f5f11b54e63eea59c0d488b355925
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20111014-windows-server-8-standards-based-storage-management.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "10/14/2011"
    date: "2011-10-14 11:49:00"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20111014-windows-server-8-standards-based-storage-management.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20111014-windows-server-8-standards-based-storage-management).

# Windows Server Standards-Based Storage Management

Take a look at Jeffrey Snover's blog post, Distinguished Engineer and Lead Architect for Window Server, where he discusses standards-based storage management in the next release of Windows Server codenamed "Windows Server 8".

  
Read Jeffrey's post on the [Microsoft Server and Cloud Platform blog](http://bit.ly/pxC85J).