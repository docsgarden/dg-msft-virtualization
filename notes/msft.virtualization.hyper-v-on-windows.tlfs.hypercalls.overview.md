---
id: 5HsVAJSJqurg8gXium3ApZJ
title: >-
    Hypercall Reference
desc: >-
    List of supported hypercalls
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/overview
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 119cc06c819ba3593ad2e39909d4c3d297aa6363ebaf84b6d11bdab248639dba
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/overview.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/overview.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/overview).

# Hypercall Reference

The following table lists supported hypercalls by call code.

| Call Code | Type    | Hypercall                                                                           |
|-----------|---------|-------------------------------------------------------------------------------------|
| 0x0001    | Simple  | HvCallSwitchVirtualAddressSpace                                                     |
| 0x0002    | Simple  | [[HvCallFlushVirtualAddressSpace|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallflushvirtualaddressspace]]                 |
| 0x0003    | Rep     | [[HvCallFlushVirtualAddressList|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallflushvirtualaddresslist]]                   |
| 0x0008    | Simple  | [[HvCallNotifyLongSpinWait|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallnotifylongspinwait]]                             |
| 0x000b    | Simple  | [[HvCallSendSyntheticClusterIpi|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallsendsyntheticclusteripi]]                   |
| 0x000c    | Rep     | [[HvCallModifyVtlProtectionMask|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallmodifyvtlprotectionmask]]                   |
| 0x000d    | Simple  | [[HvCallEnablePartitionVtl|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallenablepartitionvtl]]                             |
| 0x000f    | Simple  | [[HvCallEnableVpVtl|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallenablevpvtl]]                                           |
| 0x0011    | Simple  | [[HvCallVtlCall|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallvtlcall]]                                                   |
| 0x0012    | Simple  | [[HvCallVtlReturn|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallvtlreturn]]                                               |
| 0x0013    | Simple  | [[HvCallFlushVirtualAddressSpaceEx|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallflushvirtualaddressspaceex]]             |
| 0x0014    | Rep     | [[HvCallFlushVirtualAddressListEx|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallflushvirtualaddresslistex]]               |
| 0x0015    | Simple  | [[HvCallSendSyntheticClusterIpiEx|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallsendsyntheticclusteripiex]]               |
| 0x0050    | Rep     | [[HvCallGetVpRegisters|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallgetvpregisters]]                                     |
| 0x0051    | Rep     | [[HvCallSetVpRegisters|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallsetvpregisters]]                                     |
| 0x005C    | Simple  | [[HvCallPostMessage|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallpostmessage]]                                           |
| 0x005D    | Simple  | [[HvCallSignalEvent|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallsignalevent]]                                           |
| 0x007e    | Simple  | [[HvCallRetargetDeviceInterrupt|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallretargetdeviceinterrupt]]                   |
| 0x0099    | Simple  | [[HvCallStartVirtualProcessor|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallstartvirtualprocessor]]                       |
| 0x009A    | Rep     | [[HvCallGetVpIndexFromApicId|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallgetvpindexfromapicid]]                         |
| 0x00AF    | Simple  | [[HvCallFlushGuestPhysicalAddressSpace|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallflushguestphysicaladdressspace]]     |
| 0x00B0    | Rep     | [[HvCallFlushGuestPhysicalAddressList|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvcallflushguestphysicaladdresslist]]       |

The following table lists supported extended hypercalls by call code.

| Call Code | Type    | Hypercall                                                                           |
|-----------|---------|-------------------------------------------------------------------------------------|
| 0x8001    | Simple  | [[HvExtCallQueryCapabilities|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.hypercalls.hvextcallquerycapabilities]]                         |
| 0x8002    | Simple  | HvExtCallGetBootZeroedMemory                                                        |
| 0x8003    | Simple  | HvExtCallMemoryHeatHint                                                             |
| 0x8004    | Simple  | HvExtCallEpfSetup                                                                   |
| 0x8006    | Simple  | HvExtCallMemoryHeatHintAsync                                                        |