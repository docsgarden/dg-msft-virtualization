---
id: 7n5JcfodfX5kxvBZiXybrjX
title: >-
    TechEd Europe Days 2-4&#58; Blogs, Tweets, and Videos
desc: >-
    Blogs, tweets, and videos from TechEd Europe 2010 days 2-4
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20101112-teched-europe-days-2-4-blogs-tweets-and-videos
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: c36b0fcb68b15db0cadb060846798c8e648277f16b9f6180aee91b6d4423f3dd
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20101112-teched-europe-days-2-4-blogs-tweets-and-videos.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2010-11-12 02:54:04"
    ms.date: "11/12/2010"
    categories: "blogs"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20101112-teched-europe-days-2-4-blogs-tweets-and-videos.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20101112-teched-europe-days-2-4-blogs-tweets-and-videos).

# TechEd Europe 2010 Days 2-4 
The first day’s keynote out of the way, online chatter about TechEd Europe 2010 turned to session reporting, the [announced](https://blogs.technet.com/b/systemcenter/archive/2010/11/09/announcing-configuration-manager-v-next-official-name.aspx) naming of Configuration Manager 2012 (formerly “v.Next”), and continued absorption of Microsoft’s initiatives in [cloud](https://www.microsoft.com/cloud) computing, [Hyper-V Cloud](https://www.microsoft.com/privatecloud) in particular. To give you a kind of wrap up of the goings-on of the past several days, and, [as previously](https://blogs.technet.com/b/systemcenter/archive/2010/11/10/teched-europe-2010-day-1-voices-from-the-community.aspx), to give a virtual shout-out to some of the more notable voices from the community, here are some blog posts we found interesting, along with a small selection of Twitter personalities. I’m also including an aggregation of the related videos that we’ve put together, including those that TechNet Edge’s David Tesar, Joey Snow, and Adam Carter (BOMB) shot on site in Berlin…. 

[See the System Center Team Blog for the full post](https://blogs.technet.com/b/systemcenter/archive/2010/11/12/teched-europe-2010-days-2-4-blogs-tweets-and-videos.aspx).