---
id: 77fuhGMnpCj2Dkku3eFzMHR
title: >-
    City of Frisco (Texas) saves big with Hyper-V and NetApp
desc: >-
    City of Frisco (Texas) saves big with Hyper-V and NetApp
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20091201-city-of-frisco-texas-saves-big-with-hyper-v-and-netapp
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 15c20c27c31ae562c1bb5f884fe6b27731afd947c29d67ca3338005a01be6336
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091201-city-of-frisco-texas-saves-big-with-hyper-v-and-netapp.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "12/01/2009"
    date: "2009-12-01 15:52:00"
    categories: "events"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091201-city-of-frisco-texas-saves-big-with-hyper-v-and-netapp.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20091201-city-of-frisco-texas-saves-big-with-hyper-v-and-netapp).

# City of Frisco (Texas) saves big with Hyper-V and NetApp

Hi, I'm Jim Schwartz, a director of virtualization solutions marketing in Redmond. 

A great example of why companies are adopting virtualization and the resulting benefits can be seen in Texas, at the [City of Frisco](http://en.wikipedia.org/wiki/Frisco,_Texas "Wikipedia entry"). The city, one of the fastest growing in America, needed to scale a siloed IT infrastructure to accommodate data growth of 200-300% year. 

Check out this [NetApp blog](http://blogs.netapp.com/msenviro/2009/11/city-in-texas-saves-big-with-hyper-v-netapp.html) to learn how the City’s move toward virtualization resulted in substantial savings and maximized uptime for Exchange, SharePoint, and other apps.

 

Also don’t miss the Microsoft and NetApp live webcast on Thursday, December 3 at 11am Pacific/2pm Eastern: [Virtualize with Microsoft and NetApp: Consolidate and Increase Uptime with Windows Server 2008 R2 Hyper-V and NetApp](http://communicate.netapp.com/forms/verify?seminarID=20091203WL&REF_SOURCE=microsoft_blogs). City of Frisco Enterprise Technology Manager Tim Yarbrough will join the discussion to talk about his environment and Windows Server 2008 R2 upgrade. You will have the opportunity to submit live questions to Microsoft and NetApp experts during the webcast.

 

Thanks,

 

 

Jim