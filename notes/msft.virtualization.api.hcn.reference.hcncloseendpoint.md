---
id: 7dAP786RgzNn6h7SkgFAnmb
title: >-
    HcnCloseEndpoint
desc: >-
    HcnCloseEndpoint
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnCloseEndpoint
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 49628ba10fecf95d767c4b191e63250d48bdaf6038d0a638972967c70f2e4972
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnCloseEndpoint.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnCloseEndpoint']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnCloseEndpoint.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnCloseEndpoint).

# HcnCloseEndpoint

## Description

Close a handle to an Endpoint.

## Syntax

```cpp
HRESULT
WINAPI
HcnCloseEndpoint(
    _In_ HCN_ENDPOINT Endpoint
    );
```

## Parameters

`Endpoint`

Handle to an endpoint [[`HCN_ENDPOINT`|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_endpoint]]

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |