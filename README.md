[![Converted for Dendron](https://img.shields.io/badge/Converted%20for-Dendron-brightgreen)](https://dendron.so)
[![Published with GitLab Pages](https://img.shields.io/badge/Published%20with-GitLab%20Pages-blue)](https://docsgarden.gitlab.io/dg-msft-virtualization/)
[![CC-BY 4.0 License](https://img.shields.io/badge/License-Creative%20Commons%20Attribution%204.0%20International-orange)](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE)

# Microsoft Virtualization Docs Vault

Dendron docs vault of Microsoft Virtualization converted from [official Microsoft source docs](https://github.com/MicrosoftDocs/Virtualization-Documentation).

- [View the published Dendron vault on GitLab Pages](https://docsgarden.gitlab.io/dg-msft-virtualization/)

## Licensing

- Everything under `notes/*` is released under the same license as the source content. Learn more in the `LICENSE` file of this repo.
- Everything else is released as MIT Licensed code. Learn more in the `LICENSE-CODE` file of this repo.

## Dendron Vault Visualization

> Generated with `dendron visualize` via [Dendron CLI](https://wiki.dendron.so/notes/RjBkTbGuKCXJNuE4dyV6G/).

![Packed circles visualization](./notes/assets/diagram-dg-msft-virtualization.svg)

## About Dendron

Interested in creating your own knowledge base using markdown, git, and [Visual Studio Code (VS Code)](https://code.visualstudio.com/) (or [VSCodium](https://github.com/VSCodium/vscodium/))? Take a look at the [Dendron Getting Started Guide](https://wiki.dendron.so/notes/678c77d9-ef2c-4537-97b5-64556d6337f1/).

A majority of the community interaction currently happens on the [Dendron Discord](https://link.dendron.so/discord). 🌱 The community talks about things related to Dendron, PKM, and making sense of the world around us!

- [Subscribe to the newsletter](https://link.dendron.so/newsletter)
- Follow [Dendron on Twitter](https://link.dendron.so/twitter)
- Follow [Dendron on Mastodon](https://link.dendron.so/mastodon)
- Subscribe to [Dendron on YouTube](https://link.dendron.so/youtube)
- Checkout [Dendron on GitHub](https://link.dendron.so/github)
- Read the [Dendron Blog](https://blog.dendron.so/)
- Visit and bookmark the [Dendron Smartpage](https://link.dendron.so/smartpage)

Dendron has a bunch of community events that they host throughout the week. You can stay up to date on what's happening by taking a look at their community calendar, and registering for events!

- [Dendron Community Events on Luma](https://link.dendron.so/luma)