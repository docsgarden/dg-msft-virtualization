---
id: 9c4UKLA7wkvizELFuTZ4wyk
title: >-
    HDV_PCI_READ_INTERCEPTED_MEMORY
desc: >-
    HDV_PCI_READ_INTERCEPTED_MEMORY
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciReadInterceptedMemory
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 9de6bb33cc0c45261d7f121cc36361fb92756beb15f71f0ff4e9aec4685a18d8
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciReadInterceptedMemory.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HDV_PCI_READ_INTERCEPTED_MEMORY']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciReadInterceptedMemory.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciReadInterceptedMemory).

# HDV_PCI_READ_INTERCEPTED_MEMORY

## Description

Function called to execute an intercepted MMIO read for the emulated device.

## Syntax

```C++
typedef HRESULT (CALLBACK *HDV_PCI_READ_INTERCEPTED_MEMORY)(
    _In_opt_             void*                DeviceContext,
    _In_                 HDV_PCI_BAR_SELECTOR BarIndex,
    _In_                 UINT64               Offset,
    _In_                 UINT64               Length,
    _Out_writes_(Length) BYTE*                Value
    );
```

## Parameters

|Parameter|Description|
|---|---|---|---|---|---|---|---|
|`DeviceContext` |Context pointer that was supplied to HdvCreateDeviceInstance.|
|`BarIndex` |Index to the BAR the read operation pertains to.|
|`Offset` |Offset in bytes from the base of the BAR to read.|
|`Length` |Length in bytes to read (1 / 2 / 4 / 8 bytes).|
|`Value` |Receives the read value.|
|    |    |

## Return Values

|Return Value     |Description|
|---|---|
|`S_OK` | Returned if function succeeds.|
|`HRESULT` | An error code is returned if the function fails.
|     |     |

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |