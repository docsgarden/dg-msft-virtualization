---
id: A8WtSRncFgArHuDhZfq7UKc
title: >-
    Hyper-V, your journey starts here!
desc: >-
    Hyper-V, your journey starts here!
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090903-hyper-v-your-journey-starts-here
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 1f3c92b41da84bc1a70594aeff75fb7889ad1f4a62c44ff3ce5947cc1259aa42
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090903-hyper-v-your-journey-starts-here.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "09/03/2009"
    date: "2009-09-03 12:59:00"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090903-hyper-v-your-journey-starts-here.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090903-hyper-v-your-journey-starts-here).

# Hyper-V, your journey starts here!

I have been inundated by people here at VMworld 2009 with tons of questions on Hyper-V and who want to see demos of Hyper-V. This blog post attempts to put together a list material for folks. Given the large amount of material this list is only a snapshot but one which will provide you enough to get started. 

First off there is a huge amount of technical articles, documentation, whitepapers, case studies, API documentation that’s available. Your start point should be the Virtualization page [here](https://www.microsoft.com/virtualization/default.mspx). That site provides links to numerous [case studies](https://www.microsoft.com/virtualization/casestudies/default.mspx) about customers who are using Hyper-V and Microsoft Virtualization solutions. Next I suggest looking at the [solutions site](https://www.microsoft.com/virtualization/solutions/datacenter-consolidation/default.mspx) that provides information on the customer focused scenarios. Following this I encourage you to delve into the [products](https://www.microsoft.com/virtualization/products/server/default.mspx) that are used to build these solutions. 

For the geeks like me who are really looking to understand how to get started and the technical details you should start your journey at the [Virtualization TechCenter](https://technet.microsoft.com/virtualization/default.aspx). For those of you who wanted to just get to the nuts and bolts go straight to the [Hyper-V Getting Started Guide.](https://technet.microsoft.com/library/cc732470\(WS.10\).aspx) For folks wanting to understand the architecture in some depth you want to start [here](https://msdn.microsoft.com/library/cc768520\(BTS.10\).aspx). Developers can jump straight to the [WMI API’s](https://msdn.microsoft.com/library/cc136992\(VS.85\).aspx). 

Finally for folks who want to see a collection of videos with great demos of the products I’d like to point to [Tony Sopers blog](https://blogs.technet.com/tonyso/archive/2009/09/03/hyper-v-tv.aspx) which contains a huge collection of these. 

Enjoy.

Vijay Tewari

Principal Program Manager, Windows Server Virtualization