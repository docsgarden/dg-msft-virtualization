---
id: 6VgDFNNndBzM3EyBA3AyfoZ
title: >-
    What to expect in the System Center Orchestrator Beta!
desc: >-
    What to expect in the System Center Orchestrator Beta!
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110518-what-to-expect-in-the-system-center-orchestrator-beta
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: cb54da74c06824f40ed8d6923f9e3248476f7880a4989b6b56b7a84b3b1cfc90
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110518-what-to-expect-in-the-system-center-orchestrator-beta.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "05/18/2011"
    date: "2011-05-18 09:22:10"
    categories: "system-center"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110518-what-to-expect-in-the-system-center-orchestrator-beta.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110518-what-to-expect-in-the-system-center-orchestrator-beta).

# What to expect in the System Center Orchestrator Beta!

Hi everyone

Today at TechEd North America we disclosed a lot of information about, and took System Center Orchestrator for a spin in it’s first public outing!

I have posted an article over on the Orchestrator Blog site with the details about the Beta and what to expect. You can view the article here [**https://blogs.technet.com/b/scorch/archive/2011/05/17/what-to-expect-in-the-system-center-orchestrator-beta.aspx**](https://blogs.technet.com/b/scorch/archive/2011/05/17/what-to-expect-in-the-system-center-orchestrator-beta.aspx)

In addition, we will be running an Evaluation Program as announced [**here**](https://blogs.technet.com/b/systemcenter/archive/2011/05/16/community-evaluation-program-now-accepting-applications.aspx). If you fill out the form for the [**Orchestrator CEP**](https://connect.microsoft.com/site1211/Survey/Survey.aspx?SurveyID=12773) you will not only be part of the program which includes guided tours of the new features, ready made environments and calls with the Product Group, but you will also be notified the minute that the Beta is available to download!

We look forward to engaging with you in the Evaluation Program.

Adam

![Adam_thumb5](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/59/30/metablogapi/4885.Adam_thumb5_thumb_31706152.jpg)](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/59/30/metablogapi/2248.Adam_thumb5_603619F4.jpg) | Adam Hall   
Senior Technical Product Manager   
  
![image](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/59/30/metablogapi/5277.image_thumb_57BA9374.png)](https://msdnshared.blob.core.windows.net/media/TNBlogsFS/prod.evol.blogs.technet.com/CommunityServer.Blogs.Components.WeblogFiles/00/00/00/59/30/metablogapi/2047.image_6D847BD1.png)  
---|---  
  
**[Send me an e-mail](mailto:adhall@microsoft.com) **

| 

**[Follow me on Twitter](https://twitter.com/adman_nz) [ Connect with me on LinkedIn](http://www.linkedin.com/in/adamhall)**