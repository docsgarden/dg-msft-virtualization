---
id: 9Hszqfaz7DZTZcHpPVncQM3
title: >-
    WinHEC 2006 Presentation slides are available online
desc: >-
    post id 3973
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2006/20060614-winhec-2006-presentation-slides-are-available-online
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 9773f325cbde0190afaffeec690de2e90df4c28ca2c96de29eeae6c5094fe2b5
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2006/20060614-winhec-2006-presentation-slides-are-available-online.md
    keywords: "virtualization, virtual server, blog"
    author: "scooley"
    ms.author: "scooley"
    ms.date: "6/14/2006"
    ms.topic: "article"
    ms.prod: "virtualization"
    ms.assetid: "165262c0-7981-4e1c-b1b5-9a51f88543d1"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2006/20060614-winhec-2006-presentation-slides-are-available-online.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2006/20060614-winhec-2006-presentation-slides-are-available-online).

# WinHEC 2006 Presentation slides are available online

The Microsoft WinHEC 2006 presentation slides are now available to view online under the [WHDC](https://www.microsoft.com/whdc/winhec/pres06.mspx) (Windows Hardware Developer Central) area on microsoft.com. The Windows Virtualization related sessions are near the bottom of the page and are listed below for reference.

* [Device Virtualization Architecture](https://download.microsoft.com/download/5/b/9/5b97017b-e28a-4bae-ba48-174cf47d23cd/VIR040_WH06.ppt)
* [How to Use the WMI Interfaces with Windows Virtualization](https://download.microsoft.com/download/5/b/9/5b97017b-e28a-4bae-ba48-174cf47d23cd/VIR043_WH06.ppt)
* [HyperCall APIs Explained](https://download.microsoft.com/download/5/b/9/5b97017b-e28a-4bae-ba48-174cf47d23cd/VIR046_WH06.ppt)
* [Hypervisor, Virtualization Stack, and Device Virtualization Architectures](https://download.microsoft.com/download/5/b/9/5b97017b-e28a-4bae-ba48-174cf47d23cd/VIR046_WH06.ppt)
* [I/O Memory Management Hardware goes Mainstream](https://download.microsoft.com/download/5/b/9/5b97017b-e28a-4bae-ba48-174cf47d23cd/VIR048_WH06.ppt)
* [Inside Microsoft's Network and Storage VSP/VSC](https://download.microsoft.com/download/5/b/9/5b97017b-e28a-4bae-ba48-174cf47d23cd/VIR049_WH06.ppt)
* [Intel Virtualization Technology: Strategy and Evolution](https://download.microsoft.com/download/5/b/9/5b97017b-e28a-4bae-ba48-174cf47d23cd/VIR054_WH06.ppt)
* [Microsoft Server Virtualization Strategy and Virtual Hard Disk Directions](https://download.microsoft.com/download/5/b/9/5b97017b-e28a-4bae-ba48-174cf47d23cd/VIR065_WH06.ppt)
* [PCIe Address Translation Services and I/O Virtualization](https://download.microsoft.com/download/5/b/9/5b97017b-e28a-4bae-ba48-174cf47d23cd/VIR071_WH06.ppt)
* [Windows Virtualization Best Practices and Future Hardware Directions](https://download.microsoft.com/download/5/b/9/5b97017b-e28a-4bae-ba48-174cf47d23cd/VIR124_WH06.ppt)
* [Windows Virtualization: Scenarios and Features](https://download.microsoft.com/download/5/b/9/5b97017b-e28a-4bae-ba48-174cf47d23cd/SER125_WH06.ppt)
* [Windows Virtualization: Strategy and Roadmap](https://download.microsoft.com/download/5/b/9/5b97017b-e28a-4bae-ba48-174cf47d23cd/BUS126_WH06.ppt)

John Howard  
Program Manager  
Windows Virtualization

[Original link](https://blogs.technet.microsoft.com/virtualization/2006/06/14/winhec-2006-presentation-slides-are-available-online/
)