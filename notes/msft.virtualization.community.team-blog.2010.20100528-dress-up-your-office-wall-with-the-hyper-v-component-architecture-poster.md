---
id: AECTaX9HCo7mqGaMSU7X3Mr
title: >-
    Dress up your office wall with the Hyper-V component architecture poster
desc: >-
    The poster is a great visual tool to help in the understanding of the key features and components of the Hyper-V in Windows Server 2008 R2.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100528-dress-up-your-office-wall-with-the-hyper-v-component-architecture-poster
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 2b000785a5719d75a799ffa3c9bcecdb39bc986c6c13e464c1d43011b8635db1
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100528-dress-up-your-office-wall-with-the-hyper-v-component-architecture-poster.md
    author: "scooley"
    ms.author: "scooley"
    date: "2010-05-28 11:32:18"
    ms.date: "05/28/2010"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100528-dress-up-your-office-wall-with-the-hyper-v-component-architecture-poster.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100528-dress-up-your-office-wall-with-the-hyper-v-component-architecture-poster).

# Dress up your office wall with the Hyper-V component architecture poster

Hi, this is Bryon Surace again.  I’m a senior program manager on the Windows virtualization team at Microsoft. 

 

I wanted to draw your attention to the new [Hyper-V Component Architecture Poster](https://www.microsoft.com/downloads/details.aspx?displaylang=en&FamilyID=5567b22a-8c47-4840-a88d-23146fd93151).   

[Hyper-V Component Architecture Poster](https://www.microsoft.com/downloads/details.aspx?displaylang=en&FamilyID=5567b22a-8c47-4840-a88d-23146fd93151)

The poster is a great visual tool to help in the understanding of the key features and components of the Hyper-V in Windows Server 2008 R2.  It highlights key Hyper-V components including:

§  Architecture

§  Virtual Networking 

§  Virtual Machine Snapshots 

§  Live Migration

§  Storage Interfaces

§  Storage Types

§  Storage Location and Paths

§  Import and Export

 

This large-format poster provides practical visual depictions of the Windows Hypervisor, live migration process, cluster shared volumes architecture, VMQ data paths, disk storage I/O path, and much more.

 

Download, Print, and Enjoy!

Bryon Surace