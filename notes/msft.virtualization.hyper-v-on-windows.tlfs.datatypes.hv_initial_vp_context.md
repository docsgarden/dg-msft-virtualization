---
id: 6Nnuksh69bZetHDVWr34Xbj
title: >-
    HV_INITIAL_VP_CONTEXT
desc: >-
    HV_INITIAL_VP_CONTEXT
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_initial_vp_context
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 354b55954f900679e8bd773243876e121b5e9eb627d1295e9b636db96246e49c
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_initial_vp_context.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_initial_vp_context.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_initial_vp_context).

# HV_INITIAL_VP_CONTEXT

## Syntax

 ```c
typedef struct
{
    UINT64 Rip;
    UINT64 Rsp;
    UINT64 Rflags;

    // Segment selector registers together with their hidden state.
    HV_X64_SEGMENT_REGISTER Cs;
    HV_X64_SEGMENT_REGISTER Ds;
    HV_X64_SEGMENT_REGISTER Es;
    HV_X64_SEGMENT_REGISTER Fs;
    HV_X64_SEGMENT_REGISTER Gs;
    HV_X64_SEGMENT_REGISTER Ss;
    HV_X64_SEGMENT_REGISTER Tr;
    HV_X64_SEGMENT_REGISTER Ldtr;

    // Global and Interrupt Descriptor tables
    HV_X64_TABLE_REGISTER Idtr;
    HV_X64_TABLE_REGISTER Gdtr;

    // Control registers and MSR's
    UINT64 Efer;
    UINT64 Cr0;
    UINT64 Cr3;
    UINT64 Cr4;
    UINT64 MsrCrPat;
} HV_INITIAL_VP_CONTEXT;
 ```

## See also

[[HV_X64_SEGMENT_REGISTER|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.datatypes.hv_x64_segment_register]]

[[HV_X64_TABLE_REGISTER|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.datatypes.hv_x64_table_register]]