---
id: Abxkc7FQ2KVGWM4MEXSVysC
title: >-
    Announcing the GA of Disaster Recovery to Azure using Azure Site Recovery
desc: >-
    Learn about what additional features and improvements the GA release brings to the Disaster Recovery of Azure Site Recovery.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2014/20141002-announcing-the-ga-of-disaster-recovery-to-azure-using-azure-site-recovery
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 6fb0a4c82d349314fc85a3779043206fd2a23a961ccb7930d3732fb4f09f3d1e
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20141002-announcing-the-ga-of-disaster-recovery-to-azure-using-azure-site-recovery.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2014-10-02 12:48:32"
    ms.date: "10/02/2014"
    categories: "azure-site-recovery"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20141002-announcing-the-ga-of-disaster-recovery-to-azure-using-azure-site-recovery.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2014/20141002-announcing-the-ga-of-disaster-recovery-to-azure-using-azure-site-recovery).

# Announcing the GA of Disaster Recovery to Azure using Azure Site Recovery

I am excited to [announce the GA of the Disaster Recovery to Azure using Azure Site Recovery](https://aka.ms/asr_ga_virtualizationblog). In addition to enabling _replication to_ and _recovery in_ Microsoft Azure, ASR enables automated protection of VMs, remote health monitoring, no-impact recovery plan testing, and single click orchestrated recovery - all backed by an enterprise-grade SLA.  The DR to Azure functionality in ASR builds on top of System Center Virtual Machine Manager, Windows Server Hyper-V Replica, and Microsoft Azure to ensure that our customers can leverage existing IT investments while still helping them optimize precious CAPEX and OPEX spent in building and managing secondary datacenter sites. 

The GA release also brings significant additions to the already expansive list of ASR’s DR to Azure features:

  * **NEW** ** ** **ASR Recovery Plans and Azure Automation** integrate to offer robust and simplified one-click orchestration of your DR plans  

  * **NEW** ** ** **Track Initial Replication Progress** as virtual machine data gets replicated to a customer-owned and managed geo-redundant Azure Storage account. This new feature is also available when configuring DR between on-premises private clouds across enterprise sites
  * **NEW** ** ** **Simplified Setup and Registration** streamlines the DR setup by removing the complexity of generating certificates and integrity keys needed to register your on-premises System Center Virtual Machine Manager server with your Site Recovery vault