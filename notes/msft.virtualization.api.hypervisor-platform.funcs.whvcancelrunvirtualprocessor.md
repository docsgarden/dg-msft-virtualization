---
id: 4YTJbCakSDrQj3VAtL9bSQr
title: >-
    Cancel the run of a virtual processor
desc: >-
    Learn about the WHvCancelRunVirtualProcessor function that cancels the call to run a virtual processor.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvCancelRunVirtualProcessor
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 0861829a85c6eb112e998687af4ccca6c64face600d6370cf7998a54fa306ecc
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvCancelRunVirtualProcessor.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/20/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvCancelRunVirtualProcessor.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvCancelRunVirtualProcessor).

# WHvCancelRunVirtualProcessor

## Syntax

```C
HRESULT
WINAPI
WHvCancelRunVirtualProcessor(
    _In_ WHV_PARTITION_HANDLE Partition,
    _In_ UINT32 VpIndex,
    _In_ UINT32 Flags
    );
```

### Parameters

`Partition`

Handle to the partition object

`VpIndex`

Specifies the index of the virtual processor for which the execution should be stopped

`Flags`

Unused, must be zero 

## Remarks

Canceling the execution of a virtual processor allows an application to abort the call to run the virtual processor ([[`WHvRunVirtualProcessor`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-platform.funcs.whvrunvirtualprocessor]]) by another thread, and to return the control to that thread. The virtualization stack can use this function to return the control of a virtual processor back to the virtualization stack in case it needs to change the state of a VM or to inject an event into the processor.