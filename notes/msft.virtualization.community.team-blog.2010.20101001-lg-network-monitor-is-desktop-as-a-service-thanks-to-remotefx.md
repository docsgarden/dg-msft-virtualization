---
id: ATTv4EovGkJzcDLuTKKRf6m
title: >-
    LG Network Monitor is desktop as a service thanks to RemoteFX
desc: >-
    LG and Microsoft team up to introduce monitors for cloud computing, a technology that allows users to access data and software over the Internet.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20101001-lg-network-monitor-is-desktop-as-a-service-thanks-to-remotefx
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: ec420b6619c6510a70c515225a722ab684faaa7b3bfa487add9da47dac661782
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20101001-lg-network-monitor-is-desktop-as-a-service-thanks-to-remotefx.md
    author: "scooley"
    ms.author: "scooley"
    date: "2010-10-01 17:08:14"
    ms.date: "10/01/2010"
    categories: "calista-technologies"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20101001-lg-network-monitor-is-desktop-as-a-service-thanks-to-remotefx.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20101001-lg-network-monitor-is-desktop-as-a-service-thanks-to-remotefx).

# LG Network Monitor is desktop as a service thanks to RemoteFX

We all love the innovation enabled by virtualization, and the new options of computing form factors is even more interesting (to me). Yesterday saw something cool from LG, the electronics giant who helps me watch hockey, football and MadMen in HD. Allesandro wrote about it yesterday [[here](http://virtualization.info/en/news/2010/09/microsoft-and-lg-partner-on-vdi-desktop-as-a-service-cloud-computing.html)]. [News coverage](http://www.reuters.com/article/idUSTOE68T03K20100930?type=marketsNews) provided some context for the development work: 

LG and Microsoft will also team up to introduce monitors for cloud computing, a technology that allows users to access data and software over the Internet. The cloud computing market is expected to expand to 12 million units in 2012, LG said.

The alliance will primarily target the market for virtualization solutions for educational institutions, aiming to become a leader in the segment with a 25 percent share in 2012. The segment is expected to grow by more than 50 percent every year, reaching 6 million units in 2012 and 20 million units in 2015, LG said More technical meat was provided today by Max over at the [Remote Desktop Services team blog](https://blogs.msdn.com/b/rds/archive/2010/10/01/the-lg-network-monitor-a-fantastic-addition-to-the-growing-list-of-remotefx-enabled-devices.aspx "RDS team blog"). Here are some excerpts:

Yesterday, [LG and Microsoft announced an agreement](http://www.reuters.com/article/idUSTRE68T15U20100930?type=technologyNews)  to collaborate on the development and marketing of the LG Network Monitor, a multi-user computing solution that will integrate with RemoteFX. Under the agreement, LG will introduce a new line of network monitors with RemoteFX in mid-2011.

An LG [Network Monitor](http://networkmonitor.lge.com/us/) is a monitor that connects to a centralized desktop over the network.  With a connected LG Network Monitor, all you need to get to your desktop and applications at work is a keyboard and a mouse – no PC. Multiple LG Network Monitor users can remotely access a complete Windows 7 desktop experience, hosted on the same computer enabled by Remote Desktop Services, running somewhere on the network.

If you have been following my blogs in the past, you know that I have been passionate about RemoteFX; however, I have been even more passionate about the excitement of all of our hardware and software partners for this technology and the efforts they are making to enable powerful, end-to-end RemoteFX solutions for our joint customers. With LG, it is no different: the prospect of seeing LCD displays in the market next year that will be enabled for RemoteFX decoding means that customers will have even more form factors to choose from when determining the right access device for a VDI or session virtualization environment: rich clients, thin clients, ultra-lightweight clients, and now also network monitors (did anybody say no clients ?  ;-)). 

Patrick