---
id: 3kwYmBGUJvQpL8HXjs4DXCR
title: >-
    SQL Server is a Great Workload to Consolidate on Microsoft Virtualization
desc: >-
    SQL Server is a Great Workload to Consolidate on Microsoft Virtualization
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100126-sql-server-is-a-great-workload-to-consolidate-on-microsoft-virtualization
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 2e9bb37779e2a1fbfd93784805a8d53be797d05a9210b1d96070310575e877c9
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100126-sql-server-is-a-great-workload-to-consolidate-on-microsoft-virtualization.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "01/26/2010"
    date: "2010-01-26 05:51:00"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100126-sql-server-is-a-great-workload-to-consolidate-on-microsoft-virtualization.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100126-sql-server-is-a-great-workload-to-consolidate-on-microsoft-virtualization).

# SQL Server is a Great Workload to Consolidate on Microsoft Virtualization

Yesterday, Vipul Shah, a Senior Product Manager with the Virtualization Team, [guest posted](https://blogs.technet.com/virtplanet/archive/2010/01/26/guest-blog-sql-server-consolidation-with-microsoft-virtualization.aspx) on Virtualization Planet about how Windows Server 2008 R2 Hyper-V and System Center is a great platform for running SQL Server consolidated workloads. He pointed to a newly released [video](https://www.microsoft.com/sqlserver/2008/en/us/server-consolidation.aspx) by Ted Kummert, Senior Vice President, Microsoft Business Platform Division that outlines how virtualization enables consolidation. This is exciting as we ran some performance tests against a complex stock trading application using a machine with Second Level Address Translation (SLAT) and saw good performance throughout the tests. We recently discussed in the _Best Practices for SQL Server Virtualization_ webcast (click [here](https://msevents.microsoft.com/CUI/WebCastEventDetails.aspx?EventID=1032428764&EventCategory=5&culture=en-US&CountryCode=US)) and in the _SQL Server Consolidation Guidance_ (click [here](https://msdn.microsoft.com/library/ee819082.aspx)) the results of those tests, and guidance on running SQL as a virtual machine. Take a look at the [post](https://blogs.technet.com/virtplanet/archive/2010/01/26/guest-blog-sql-server-consolidation-with-microsoft-virtualization.aspx) for more details. Kenon Owens Product Marketing Manager 

Integrated Virtualization