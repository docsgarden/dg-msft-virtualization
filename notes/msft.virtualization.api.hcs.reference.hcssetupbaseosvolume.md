---
id: 4MwbBLVW4PpxrbamEAmS93j
title: >-
    HcsSetupBaseOSVolume
desc: >-
    HcsSetupBaseOSVolume
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsSetupBaseOSVolume
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 7b3db7fc4d5a4f8c192f1a1ad4f2bab9e242e5e885f55b33a2ba5483b4b2af3e
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsSetupBaseOSVolume.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "12/21/2021"
    api_name: "['HcsSetupBaseOSVolume']"
    api_location: "['ComputeStorage.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsSetupBaseOSVolume.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsSetupBaseOSVolume).

# HcsSetupBaseOSVolume

## Description

This function sets up the base OS layer for the use on a host based on the mounted volume (VHD).

## Syntax

```cpp
HRESULT WINAPI
HcsSetupBaseOSVolume(
    _In_ PCWSTR layerPath,
    _In_ PCWSTR volumePath,
    _In_ PCWSTR options
    );
```

## Parameters

`layerPath`

Path to the root of the base OS layer.

`vhdHandle`

The handle to a VHD.

`options`

Optional JSON document  of [[OsLayerOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#OsLayerOptions]] describing options for setting up the layer.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 2104|
| **Minimum supported server** | Windows Server 2022 |
| **Target Platform** | Windows |
| **Header** | ComputeStorage.h |
| **Library** | ComputeStorage.lib |
| **Dll** | ComputeStorage.dll |