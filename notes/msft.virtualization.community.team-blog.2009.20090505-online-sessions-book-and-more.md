---
id: 35TSiP3326EveX9FRTofLL3
title: >-
    Online sessions, book and more
desc: >-
    Admittedly this post is a stew and not a meal (if that metaphor works). But you might be interested in the following items.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090505-online-sessions-book-and-more
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 49b30674583304be7726d9ad1e3e1a2420186a3272712a8971531550de218603
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090505-online-sessions-book-and-more.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-05-05 18:56:00"
    ms.date: "05/05/2009"
    categories: "application-virtualization"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090505-online-sessions-book-and-more.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090505-online-sessions-book-and-more).

# Online sessions, book and more

Admittedly this post is a stew and not a meal (if that metaphor works). But you might be interested in the following items. I'll keep it short: 

  * John Kelbley will host a webcast on Friday, May 8 at 8:00am PDT titled, "Running Linux on Hyper-V." The session will discuss install, configure, run, backup and monitor non-Windows systems. See [here](https://msevents.microsoft.com/CUI/EventDetail.aspx?EventID=1032415500&Culture=en-US "TechNet webcast").
  * On May 14, 8am-noon PDT, Edwin Yuen will host a live chat on TechTarget. He'll answer questions about our virt products, be it datacenter, desktop or managemment. See more [here](http://itknowledgeexchange.techtarget.com/microsoft-virtualization-chat/ "TechTarget chat site").
  * Wondering what to read when you're flying to TechEd, or your next trip? The Windows Server 2008 Hyper-V Resource Kit [book](https://www.microsoft.com/learning/en/us/Books/11842.aspx "MS Learning site") is it. One of the authors, Robert Larson, architect in MS Services and [TechNet blogger](https://blogs.technet.com/roblarson/ "Robert Larson blog"), told me that the book is in final formatting and some sample chapters are available to download ([here](http://doingitvirtual.com/media/15/default.aspx "Chapter downloads")). The book will be available via Amazon and Barnes and Noble in June. Read more from one of the authors [here](http://doingitvirtual.com/blogs/virtualzone/archive/2009/04/28/download-samples-of-the-ms-press-windows-server-2008-hyper-v-resource-kit.aspx "Doing IT Virtual blog").
  * Finally 

Enjoy. 

Patrick