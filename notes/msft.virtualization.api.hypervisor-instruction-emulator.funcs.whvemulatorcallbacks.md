---
id: B3dWEXSc7XD3FLXfEryPmKd
title: >-
    WHV_EMULATOR_CALLBACKS method
desc: >-
    Learn about the WHV_EMULATOR_CALLBACKS method.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorCallbacks
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 3c63bd8a8055e64a5ca37e798f5b9053e8367ffc1296b719df71fa81bb35670f
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorCallbacks.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/19/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorCallbacks.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorCallbacks).

# WHV_EMULATOR_CALLBACKS


## Syntax

```c
typedef struct _WHV_EMULATOR_CALLBACKS {
    UINT32 Size;
    UINT32 Reserved;
    WHV_EMULATOR_IO_PORT_CALLBACK WHvEmulatorIoPortCallback;
    WHV_EMULATOR_MEMORY_CALLBACK WHvEmulatorMemoryCallback;
    WHV_EMULATOR_GET_VIRTUAL_PROCESSOR_REGISTERS_CALLBACK       WHvEmulatorGetVirtualProcessorRegisters;
    WHV_EMULATOR_SET_VIRTUAL_PROCESSOR_REGISTERS_CALLBACK               WHvEmulatorSetVirtualProcessorRegisters;
    WHV_EMULATOR_TRANSLATE_GVA_PAGE_CALLBACK WHvEmulatorTranslateGvaPage;
} WHV_EMULATOR_CALLBACKS;
```
## Remarks
Used in [`WHvTranslateGva`](https://docs.microsoft.com/en-us/virtualization/api/hypervisor-platform/funcs/WHvTranslateGva) to specify callback methods needed by the emulator.