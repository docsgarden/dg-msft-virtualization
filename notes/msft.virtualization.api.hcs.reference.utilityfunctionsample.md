---
id: Ag5PrfTmnARpawdiotE4QpM
title: >-
    Virtual Machine Utilities Samples
desc: >-
    Virtual Machine Utilities Samples
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/UtilityFunctionSample
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 8c60be271028a5a6c4e07f1746dc1001f113e0c9c1a675ff69c31c7c501125f8
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/UtilityFunctionSample.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['Virtual Machine Utilities Samples']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/UtilityFunctionSample.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/UtilityFunctionSample).

# Virtual Machine Utilities Samples

<a name = "CreateFilesGrantAccess"></a>
## Create the file and grant vm access to them

```cpp
// This assumes "Sample" has been used as the id for a compute system when created through HcsCreateComputeSystem
THROW_IF_FAILED(HcsCreateEmptyRuntimeStateFile(L"emptyfile.vmrs"));
THROW_IF_FAILED(HcsGrantVmAccess(L"Sample", L"emptyfile.vmrs"));
```