---
id: U82AeWX6DAYxNM57ey2BDPd
title: >-
    Microsoft Acquires Opalis Software
desc: >-
    Microsoft Acquires Opalis Software
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20091211-microsoft-acquires-opalis-software
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: a91123b4c43e937bba84b25a8d3429517de983dda99bb7d47e4a4d4d2cae6d4e
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091211-microsoft-acquires-opalis-software.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "12/11/2009"
    date: "2009-12-11 07:29:00"
    categories: "cloud-computing"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091211-microsoft-acquires-opalis-software.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20091211-microsoft-acquires-opalis-software).

# Microsoft Acquires Opalis Software

Good news for a Friday. [Brad's post](https://blogs.technet.com/systemcenter/archive/2009/12/11/microsoft-acquires-opalis-software.aspx "Brad's blog post") (with video interview) announces the acquisition of Opalis Software. Why is this important to virtualization projects? Thoughts below, but first some background on the business. 

> Microsoft’s Server and Tools Business (STB) is a $14.1 billion business.  
> 
> Within STB, the Management and Services Division is focused on reducing customers' costs and improve IT efficiency to drive businesses forward with a suite of products and services built around the System Center brand. In August 2009, Microsoft reported that the Management and Services Division revenues grew more than 30% from 2008 to 2009, and is now at approximately $1 billion in annual sales.
> 
> Financial terms of the acquisition won’t be disclosed. Opalis [recently reported](http://www.opalis.com/upload/pressreleases/OpalisQ309RecapPR.pdf) 104% increase in new license bookings for calendar Q3 (compared to Q3 2008), selling to enterprise and managed service provider customers. 

So how is Opalis Software used? Here's some examples:  •        [Incident response](http://opalis.com/Solutions_Incident_Response.asp) standardize and automate triage, diagnose and repair processes to reduce the number of incidents. •        [Provisioning](http://opalis.com/Solutions_Provisioning.asp) orchestrate datacenter tools to configure, deploy, and verify IT services in response to an incident or change request. Provision server, storage, or network resources across physical, virtual or cloud environments.  •        [Virtual service management](http://opalis.com/Solutions_Virtualization.asp) automate virtual lifecycle management to control server sprawl and extend management best practices, such as incident management and provisioning, to your virtual environments.  •        [Run book procedures](http://opalis.com/Solutions_Run_Book_Automation.asp) automate re-occurring maintenance and administrative tasks, such as data and file handling, database and application support. •        [Cloud Computing](http://opalis.com/Solutions_Cloud_Computing.asp) automate cloud lifecycle management, to request, provision, release and track costs of cloud resources.

So there's a summary. Brad says much more in his video interview. An analyst at Gartner forecasted that, by end of 2010, at least 50% of the automation and workflow management tasks in support of virtual server infrastructures will be supported by next-gen Run Book Automation-based tools. That's exactly what Opalis has created, and will become part of System Center.

As the CEO of Opalis wrote in [his blog post](http://tdelaughter.blogspot.com/2009/12/opalis-joins-microsoft-system-center.html "Opalis blog") today:

>  I believe, with the Opalis technology, Microsoft will have the most complete virtualization stack available from any single vendor.

Indeed, a good Friday.

Patrick