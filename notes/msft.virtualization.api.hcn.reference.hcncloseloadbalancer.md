---
id: 6iJFCSirZE6Mw4ELKqpvkCr
title: >-
    HcnCloseLoadBalancer
desc: >-
    HcnCloseLoadBalancer
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnCloseLoadBalancer
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 3a2a6e2a2aa8974b451ed309e53cda90bdbe3d1d0a071ceb859af30fe74a5b3b
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnCloseLoadBalancer.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnCloseLoadBalancer']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnCloseLoadBalancer.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnCloseLoadBalancer).

# HcnCloseLoadBalancer

## Description

Close a handle to a load balancer.

## Syntax

```cpp
HRESULT
WINAPI
HcnCloseLoadBalancer(
    _In_ HCN_LOADBALANCER LoadBalancer
    );
```

## Parameters

`LoadBalancer`

Handle to a LoadBalancer [[`HCN_LOADBALANCER`|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_loadbalancer]]

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |