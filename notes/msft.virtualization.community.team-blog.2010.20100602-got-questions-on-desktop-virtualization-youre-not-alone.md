---
id: 95YePecxvc28nPzRLn7WyoF
title: >-
    Got Questions on Desktop Virtualization? You’re Not Alone…
desc: >-
    For those of you who think about desktop management strategies, and how virtualization might fit, I'd recommend reading Edwin Yuen's new blog post.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100602-got-questions-on-desktop-virtualization-youre-not-alone
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: c6256e9dc6cb0fbf61e3bd203e100e1be1a7a1a95b193aa712593f2eb8cd8914
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100602-got-questions-on-desktop-virtualization-youre-not-alone.md
    author: "scooley"
    ms.author: "scooley"
    date: "2010-06-02 12:00:00"
    ms.date: "06/02/2010"
    categories: "application-virtualization"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100602-got-questions-on-desktop-virtualization-youre-not-alone.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100602-got-questions-on-desktop-virtualization-youre-not-alone).

# Got Questions on Desktop Virtualization? You’re Not Alone

For those of you who think about desktop management strategies, and how virtualization might fit, I'd recommend reading Edwin Yuen's [new blog post](https://blogs.technet.com/b/systemcenterexperts/archive/2010/06/01/got-questions-on-desktop-virtualization-you-re-not-alone.aspx "Edwin Yuen's desktop virt post"). Some of you may know Edwin as he's contributed to this blog, now blogs at [Virtualization Planet](https://blogs.technet.com/b/virtplanet/ "Virtualization Planet blog"), and has done lots of demos during keynotes. Edwin's latest post describes our approach to desktop virtualization, the partners we work with on desktop virtualization solutions, a brief video with customers Expedia, Holland America and Group Health. Edwin's conclusion is: 

 I would recommend working on developing your desktop virtualization strategy today.  This means researching and understanding what impacts virtualization can have on your desktops and what the advantages and disadvantages of each solution can be.  Click on the thumbnail below to explore the Microsoft Desktop Virtualization Hour Web site.  And if you are ready to get started with Desktop Virtualization or even if you are already deployed with Desktop Virtualization, you should take advantage of some special, limited-time offers from Microsoft to [kick start your VDI implementation](http://www.citrixandmicrosoft.com/) or [replace your VMware VDI licenses](http://www.citrixandmicrosoft.com/). If you're looking for something more meaty, such as planning and deploying VDI, then I recommend this [on-demand webcast](https://msevents.microsoft.com/CUI/WebCastEventDetails.aspx?EventID=1032447901&EventCategory=4&culture=en-US&CountryCode=US "TechNet Webcast") (registration required) by Michael Kleef. Here's the abstract:

In this webcast, we drill down into the architecture of the Virtual Desktop Infrastructure (VDI) and determine what key questions need to be asked and answered around required components, networking, capacity, and end-user experience. We also discuss how this can also fit within a Remote Desktop Services deployment.   Patrick