---
id: 44kSZyNsFR94ZaW5JchfnSz
title: >-
    HcnEnumerateEndpoints
desc: >-
    HcnEnumerateEndpoints
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnEnumerateEndpoints
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: df6930572453a23750a6f0931edffcdf7963b9724047900a394d3995cceaa6b5
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnEnumerateEndpoints.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnEnumerateEndpoints']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnEnumerateEndpoints.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnEnumerateEndpoints).

# HcnEnumerateEndpoints

## Description

Enumerates the endpoints.

## Syntax

```cpp
HRESULT
WINAPI
HcnEnumerateEndpoints(
    _In_ PCWSTR Query,
    _Outptr_ PWSTR* Endpoints,
    _Outptr_opt_ PWSTR* ErrorRecord
    );
```

## Parameters

`Query`

Optional JSON document of [[HostComputeQuery|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.HNS_Schema#HostComputeQuery]].

`Endpoints`

A list of IDs for each Endpoint.

`ErrorRecord`

Receives a JSON document with extended errorCode information. The caller must release the buffer using CoTaskMemFree.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |