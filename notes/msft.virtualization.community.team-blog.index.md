---
id: iwCwUJyGYds5aipoBf2fR5J
title: >-
    Virtualization Team Blog
desc: >-
    Official team blog for the Microsoft virtualization team and technology.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/index
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: d233b098d285711c3af7bc86c23f5e211f16a9f3e258605bb1b22288bc21f996
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/index.md
    keywords: "windows 10, hyper-v, container, docker, team, windows server, azure, virtualization, blog"
    author: "scooley"
    ms.author: "scooley"
    ms.date: "09/28/2018"
    ms.topic: "article"
    ms.prod: "virtualization"
    ms.assetid: "59950aac-d13b-499c-83b3-5f48f72cdd24"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/index.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/index).

# Virtualization Team Blog

Information and announcements from Program Managers, Product Managers, Developers and Testers in the Microsoft Virtualization team.