---
id: 9EXrBFtVB6cZb4N22hzrffe
title: >-
    HcsCrashComputeSystem
desc: >-
    HcsCrashComputeSystem
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsCrashComputeSystem
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 6bbaa5d81fed694e2266b9c97753f3750c1fd19e1dc9e6a29626dea05afce612
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsCrashComputeSystem.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsCrashComputeSystem']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsCrashComputeSystem.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsCrashComputeSystem).

# HcsCrashComputeSystem

## Description

Requests to crash the guest through an architecture defined mechanism, by sending an NMI (Non Maskable Interrupt).

## Syntax

```cpp
HRESULT WINAPI
HcsCrashComputeSystem(
    _In_ HCS_SYSTEM computeSystem,
    _In_ HCS_OPERATION operation,
    _In_opt_ PCWSTR options
    );
```

## Parameters

`computeSystem`

The handle to the compute system to crash.

`operation`

The handle to the operation that tracks the crash system operation.

`options`

Optional JSON document [[CrashOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#CrashOptions]] specifying terminate options.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

If the return value is `S_OK`, it means the operation started successfully. Callers are expected to get the operation's result using [[`HcsWaitForOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]] or [[`HcsGetOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]].



## Operation Results

The return value of [[`HcsWaitForOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]] or [[`HcsGetOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]] based on current operation listed as below.

| Operation Result Value | Description |
| -- | -- |
| `S_OK` | The guest compute system was crashed successfully |

If the operation's result is not `S_OK`, then it's possible the result document might contain the error message.


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |