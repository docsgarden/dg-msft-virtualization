---
id: AoQSZPcQfUcsLLtzaV5DojU
title: >-
    HcsWaitForOperationResultAndProcessInfo
desc: >-
    HcsWaitForOperationResultAndProcessInfo
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsWaitForOperationResultAndProcessInfo
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 8e36a83174f1d9c38598764fc905d51404875762796bcd99170c4532c22f29f3
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsWaitForOperationResultAndProcessInfo.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsWaitForOperationResultAndProcessInfo']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsWaitForOperationResultAndProcessInfo.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsWaitForOperationResultAndProcessInfo).

# HcsWaitForOperationResultAndProcessInfo

## Description

Similar to [[`HcsGetOperationResultAndProcessInfo`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresultandprocessinfo]], but additionally waits for the operation to complete the specified number of milliseconds. It is intended to be used when synchronous behavior is desired.

## Syntax

```cpp
HRESULT WINAPI
HcsWaitForOperationResultAndProcessInfo(
    _In_ HCS_OPERATION operation,
    _In_ DWORD timeoutMs,
    _Out_opt_ HCS_PROCESS_INFORMATION* processInformation,
    _Outptr_opt_ PWSTR* resultDocument
    );
```

## Parameters

`operation`

The handle to an active operation.

`timeoutMs`

Time to wait in milliseconds for the operation to complete.

`processInformation`

If the return value is `S_OK` and this parameter has been provided a valid pointer by the caller, it returns the [[HCS_PROCESS_INFORMATION|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcs_process_information]] associated to the HCS process created with [[`HcsCreateProcess`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcscreateprocess]].

`resultDocument`

If the operation finished, regardless of success or failure, receives the result document of the operation. The returned result document's JSON document is dependent on the HCS function that was being tracked by this operation. Not all functions that are tracked with operations return a result document. Refer to the remarks on the documentation for the HCS functions that use hcs operations for asynchronous tracking.


On failure, it can optionally receive an error JSON document represented by a [[ResultError|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#ResultError]]; it's not guaranteed to be always returned and depends on the function call the operation was tracking.


The caller is responsible for releasing the returned string using [`LocalFree`](https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-localfree).

## Return Values

|Value|Description|
|---|---|
|`S_OK`|The operation has completed successfully.|
|`HCS_E_OPERATION_NOT_STARTED`|The operation has not been started. This is expected when the operation has not been used yet in an HCS function that expects an `HCS_OPERATION` handle.|
|`HCS_E_OPERATION_TIMEOUT`|The operation didn't complete within the specified wait time.|
|`E_INVALIDARG`|It's possible the operation's type is different from `HcsOperationTypeCreateProcess` and `HcsOperationTypeGetProcessInfo`. Note that invalid argument return code could be also returned by the HCS function that was being tracked by this operation.|
|Any other failure [[`HRESULT`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]] value|The operation completed with failures. The returned `HRESULT` is dependent on the HCS function thas was being tracked.|


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |