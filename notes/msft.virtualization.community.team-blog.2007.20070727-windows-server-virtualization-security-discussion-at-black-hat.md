---
id: 3SqzHq3pczpKb9j4B6xCffK
title: >-
    Windows Server Virtualization Security Discussion at Black Hat
desc: >-
    post id 3933
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2007/20070727-windows-server-virtualization-security-discussion-at-black-hat
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: e1a87e1f70753544a6c2a277f199741859f045b43de3b21bbac54ab5b24b4238
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2007/20070727-windows-server-virtualization-security-discussion-at-black-hat.md
    keywords: "virtualization, virtual server, blog"
    author: "scooley"
    ms.author: "scooley"
    ms.date: "7/27/2007"
    ms.topic: "article"
    ms.prod: "virtualization"
    ms.assetid: "a4998f08-b4f0-475c-a074-989665716bc0"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2007/20070727-windows-server-virtualization-security-discussion-at-black-hat.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2007/20070727-windows-server-virtualization-security-discussion-at-black-hat).

# Windows Server Virtualization Security Discussion at Black Hat

Greetings! An important heads up about Black Hat next week in Las Vegas. For those of you who don’t know what Black Hat is, Black Hat bills itself as a security convention that puts you face to face with people on the cutting edge of network security. Microsoft is an active sponsor in this event and we’re sending one of our lead security developers in the Windows kernel, Brandon Baker, to Black Hat to discuss on Windows Server virtualization security.

The abstract to Brandon’s presentation is [here](http://blackhat.com/html/bh-usa-07/bh-usa-07-speakers.html#Baker) and in summary, Brandon will discuss:

* Windows Server virtualization and Windows Server 2008 architecture and components
* How Windows Server virtualization virtualizes the CPU and enforces virtual machine isolation
* Best practices for Windows Server virtualization deployment
* Hardware futures (e.g., TXP from Intel, SVM from AMD, IOMMU)
* And more…

I got a chance to see what Brandon is presenting and for folks in the security field (who will undoubtedly be at Black Hat) this is a session you don’t want to miss!

Cheers,  
Jeff

[Original post](https://blogs.technet.microsoft.com/virtualization/2007/07/27/windows-server-virtualization-security-discussion-at-black-hat/)