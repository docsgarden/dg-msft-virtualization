---
id: 7GQNAsALgXmMVFVtEikCKYV
title: >-
    WHvMapGpaRange
desc: >-
    Working with WHvMapGpaRange. Description on parameters, return value, and proper syntax.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvMapGpaRange
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 08c750570ecdb9e25959f19c768bfdb31f1ac07fab4c5dad178b934e50e95a78
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvMapGpaRange.md
    author: "Juarezhm"
    ms.author: "hajuarez"
    ms.date: "01/06/2019"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvMapGpaRange.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvMapGpaRange).

# WHvMapGpaRange


## Syntax
```C
// Guest physical or virtual address
typedef UINT64 WHV_GUEST_PHYSICAL_ADDRESS;
typedef UINT64 WHV_GUEST_VIRTUAL_ADDRESS;


// Flags used by WHvMapGpaRange
typedef enum WHV_MAP_GPA_RANGE_FLAGS
{
    WHvMapGpaRangeFlagNone              = 0x00000000,
    WHvMapGpaRangeFlagRead              = 0x00000001,
    WHvMapGpaRangeFlagWrite             = 0x00000002,
    WHvMapGpaRangeFlagExecute           = 0x00000004,
    WHvMapGpaRangeFlagTrackDirtyPages   = 0x00000008,
} WHV_MAP_GPA_RANGE_FLAGS;

DEFINE_ENUM_FLAG_OPERATORS(WHV_MAP_GPA_RANGE_FLAGS);

HRESULT
WINAPI
WHvMapGpaRange(
    _In_ WHV_PARTITION_HANDLE Partition,
    _In_ VOID* SourceAddress,
    _In_ WHV_GUEST_PHYSICAL_ADDRESS GuestAddress,
    _In_ UINT64 SizeInBytes,
    _In_ WHV_MAP_GPA_RANGE_FLAGS Flags
    );
```
### Parameters

`Partition`

Handle to the partition object

`SourceAddress`

Specifies the page-aligned address of the memory region in the caller’s process that is the source of the mapping

`GuestAddress`

Specifies the destination address in the VM’s physical address space

`SizeInBytes`

Specifies the number of bytes that are to be mapped

`Flags`

Specifies the access flags for the mapping

## Remarks

Creating a mapping for a range in the GPA space of a partition sets a region in the caller’s process as the backing memory for that range. The operation replaces any previous mappings for the specified GPA pages.