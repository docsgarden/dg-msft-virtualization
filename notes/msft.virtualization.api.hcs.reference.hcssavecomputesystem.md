---
id: 8Lqa5bqn3DaGT2vCJKAHTYg
title: >-
    HcsSaveComputeSystem
desc: >-
    HcsSaveComputeSystem
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsSaveComputeSystem
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 46860b67416b1a36c99421a41cecc81de57bb0082bb9b1ac040b5c33cc91b1a0
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsSaveComputeSystem.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsSaveComputeSystem']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsSaveComputeSystem.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsSaveComputeSystem).

# HcsSaveComputeSystem

## Description

Saves the state of a compute system, see [[sample code|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.computesystemsample#saveclosecs]].

## Syntax

```cpp
HRESULT WINAPI
HcsSaveComputeSystem(
    _In_ HCS_SYSTEM computeSystem,
    _In_ HCS_OPERATION operation,
    _In_opt_ PCWSTR options
    );
```

## Parameters

`computeSystem`

The handle to the compute system to save.

`operation`

The handle to the operation that tracks the save operation.

`options`

Optional JSON document of [[SaveOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#SaveOptions]] specifying save options.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

If the return value is `S_OK`, it means the operation started successfully. Callers are expected to get the operation's result using [[`HcsWaitForOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]] or [[`HcsGetOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]].


## Operation Results

The return value of [[`HcsWaitForOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]] or [[`HcsGetOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]] based on current operation listed as below.

| Operation Result Value | Description |
| -- | -- |
| `S_OK` | The compute system was saved successfully |
| `HCS_E_INVALID_STATE` | The compute system cannot be saved as it is still running |

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |