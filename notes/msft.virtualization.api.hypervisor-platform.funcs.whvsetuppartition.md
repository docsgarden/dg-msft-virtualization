---
id: 9EWpAfSJCrfNEEupPmpMcCx
title: >-
    WHvSetupPartition
desc: >-
    Description for working with and understanding syntax, parameters, and remarks for WHvSetupPartition
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvSetupPartition
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 169ac12f41e5c4edd3120c83ff6bc50d1ca1a857534cdfe8efb3b33d974916c7
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvSetupPartition.md
    author: "Juarezhm"
    ms.author: "hajuarez"
    ms.date: "05/31/2019"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvSetupPartition.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvSetupPartition).

# WHvSetupPartition

## Syntax

```C
HRESULT
WINAPI
WHvSetupPartition(
    _In_ WHV_PARTITION_HANDLE Partition
    );
```

### Parameters

`Partition`

Handle to the partition object
  

## Remarks

The `WHvSetupPartition` function sets up the parition which causes the actual partition to be created in the hypervisor.

Before setting up the partition with `WHvSetupPartition`, the partition object should be created with [[`WHvCreatePartition`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-platform.funcs.whvcreatepartition]] and the initial properties of the partition object configured with [[`WHvSetPartitionProperty`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-platform.funcs.whvsetpartitionproperty]]. After setting up the partition, the partition object can be passed to the other Windows Hypervisor Platform APIs. Starting in Insider Preview Builds (19H2), the following properties can be modified after the [[`WHvSetupPartition`|dendron://dg-msft-virtualization/msft.virtualization.api.hypervisor-platform.funcs.whvsetuppartition]] function:
`WHvPartitionPropertyCodeExtendedVmExits`
`WHvPartitionPropertyCodeExceptionExitBitmap`
`WHvPartitionPropertyCodeX64MsrExitBitmap`
`WHvPartitionPropertyCodeCpuidExitList`