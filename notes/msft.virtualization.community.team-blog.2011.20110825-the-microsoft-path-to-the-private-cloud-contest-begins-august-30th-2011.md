---
id: 5jW2uTq7wztXskHmyvkcGAR
title: >-
    The Microsoft Path to the Private Cloud Contest - Begins August 30th, 2011!
desc: >-
    The Microsoft Path to the Private Cloud Contest - Begins August 30th, 2011!
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2011/20110825-the-microsoft-path-to-the-private-cloud-contest-begins-august-30th-2011
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: b50e30e1ee07395c904c64447a5d5729849eb77770b6af683d11a07729791539
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110825-the-microsoft-path-to-the-private-cloud-contest-begins-august-30th-2011.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "08/25/2011"
    date: "2011-08-25 11:40:47"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2011/20110825-the-microsoft-path-to-the-private-cloud-contest-begins-august-30th-2011.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2011/20110825-the-microsoft-path-to-the-private-cloud-contest-begins-august-30th-2011).

# The Microsoft Path to the Private Cloud Contest - Begins August 30th, 2011!

**Your  opportunity to win one of two Xbox 360 4GB Kinect Game Console Bundles!**

Starting next week on August 30th at 9:00 a.m. PST, we’re kicking off our  
Microsoft Path to the Private Cloud Contest. During the contest you’ll have the  
opportunity to demonstrate your knowledge of Microsoft virtualization, systems  
management and private cloud offerings by being the first to provide the  
correct answers to three questions posted daily on the [Microsoft Server and Cloud Platform blog](http://bit.ly/o1zBkP).