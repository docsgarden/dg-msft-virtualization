---
id: AFnkmJ4w9G7VdefnmFyQnJv
title: >-
    Hosting Providers and HRM
desc: >-
    This article explains how HRM can help you offer DR as a service.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2014/20140218-hosting-providers-and-hrm
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 3bf58cf15d86f15ccd2ee90df91d9b87b4e1a821011275bdfdb179d399ff5ccd
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140218-hosting-providers-and-hrm.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "02/18/2014"
    categories: "hrm"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2014/20140218-hosting-providers-and-hrm.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2014/20140218-hosting-providers-and-hrm).

# Using HRM to Help Offer DR as a Service

If you are a hosting provider who is interested in offering DR as a service – you should go over this great post by **Gaurav** on how Hyper-V Recovery Manager ( **HRM** ) helps you build this capability <https://blogs.technet.com/b/scvmm/archive/2014/02/18/disaster-recovery-as-a-service-by-hosting-service-providers-using-windows-azure-hyper-v-recovery-manager.aspx>

The post provides a high level overview of the capability and also a detailed FAQ on the common set of queries which we have heard from our customers. If you have any further questions, leave a comment in the blog post.