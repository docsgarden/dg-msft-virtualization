---
id: 8AQ8EWTh23c9Xy7or93nbJv
title: >-
    Linux Integration Services Download 4.1.2-2 hotfix
desc: >-
    Blog post that discusses the release of the Linux Integration Services version 4.1.2-2 hotfix and what issues it addresses.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2016/20161021-linux-integration-services-download-4-1-2-2-hotfix
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 2ad08da68f619daa16c989cbdf598b59a04b6d36a879f244998099b1379c0c94
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2016/20161021-linux-integration-services-download-4-1-2-2-hotfix.md
    author: "scooley"
    ms.author: "scooley"
    date: "2016-10-21 20:29:47"
    ms.date: "03/20/2019"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2016/20161021-linux-integration-services-download-4-1-2-2-hotfix.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2016/20161021-linux-integration-services-download-4-1-2-2-hotfix).

# Linux Integration Services Download 4.1.2-2 hotfix

We've just published a hotfix release of the Linux Integration Services download, version 4.1.2-2. This release addresses two critical issues: "Do not lose pending heartbeat vmbus packets" (for versions 5.x, 6.x, 7.x) Hyper-V hosts can be configures to sent "heartbeat" packets to guests to see if they are active, and reboot them when they do not respond. These heartbeat packets can queue up when a guest is paused expecting a response when the guest is re-activated, for example when a guest is moved by live migration. This fix corrects a problem where some of these packets could be dropped leading the host to reboot an otherwise healthy guest. "Exclude UDP ports in RSS hashing" (for version 6.x, 7.x) While improving network performance by taking advantage of host-supported offloads we had introduced a problem with UDP workloads on Azure. This change fixes excessive UDP packet loss in this scenario. Linux Integration Services 4.1.2-2 can be downloaded [here](https://www.microsoft.com/en-us/download/details.aspx?id=51612).