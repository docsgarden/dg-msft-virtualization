---
id: 7uWVAKkD6y75TLQt54VJANo
title: >-
    HcsPauseComputeSystem
desc: >-
    HcsPauseComputeSystem
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsPauseComputeSystem
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 7dfe6034ab94c63eb5281e5feaba91250d0a023edda145e5fb5b2c3c204fa5c4
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsPauseComputeSystem.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsPauseComputeSystem']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsPauseComputeSystem.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsPauseComputeSystem).

# HcsPauseComputeSystem

## Description

Pauses the execution of a compute system, see [[sample code|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.computesystemsample#pauseresumecs]].

## Syntax

```cpp

HRESULT WINAPI
HcsPauseComputeSystem(
    _In_ HCS_SYSTEM computeSystem,
    _In_ HCS_OPERATION operation,
    _In_opt_ PCWSTR options
    );
```

## Parameters

`computeSystem`

The handle to the compute system to pause.

`operation`

The handle to the operation that tracks the pause operation.

`options`

Optional JSON document of [[PauseOptions|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#PauseOptions]] specifying pause options.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]], refer to hcs operation async model.

If the return value is `S_OK`, it means the operation started successfully. Callers are expected to get the operation's result using [[`HcsWaitForOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]] or [[`HcsGetOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]].

## Operation Results

The return value of [[`HcsWaitForOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcswaitforoperationresult]] or [[`HcsGetOperationResult`|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcsgetoperationresult]] based on current operation listed as below.

| Operation Result Value | Description |
| -- | -- |
| `S_OK` | The compute system was paused successfully |

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |