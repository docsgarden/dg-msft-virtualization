---
id: 8dko447GBAbpM72vy58G86B
title: >-
    Disclaimer
desc: >-
    post id 3993
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2006/20060428-disclaimer
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 08a205ba66e69ef254d1f5eab75a5644bce1e2b48b45c99ee306a1b288dab3ba
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2006/20060428-disclaimer.md
    keywords: "virtualization, blog"
    author: "scooley"
    ms.author: "scooley"
    ms.date: "4/28/2006"
    ms.topic: "article"
    ms.prod: "virtualization"
    ms.assetid: "aff2511e-76f4-48f3-976b-ff0a36b6e3ad"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2006/20060428-disclaimer.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2006/20060428-disclaimer).

# Disclaimer

The information in this weblog is provided "AS IS" with no warranties, and confers no rights. This weblog does not represent the thoughts, intentions, plans or strategies of my employer. It is solely my opinion. Inappropriate comments will be deleted at the authors discretion. All code samples are provided "AS IS" without warranty of any kind, either express or implied, including but not limited to the implied warranties of merchantability and/or fitness for a particular purpose.

John Howard  
Program Manager  
Windows Virtualization

[original link]( https://blogs.technet.microsoft.com/virtualization/2006/04/28/disclaimer/)