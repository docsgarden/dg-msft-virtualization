---
id: 8GNoi8eiDMfUmctkPap22Pr
title: >-
    HV_X64_XMM_CONTROL_STATUS_REGISTER
desc: >-
    HV_X64_XMM_CONTROL_STATUS_REGISTER
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_x64_xmm_control_status_register
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: c6970455122284a944d60dca8eadd2d75f12c8b19ee402a318ef8b7ac9d45635
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_x64_xmm_control_status_register.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_x64_xmm_control_status_register.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_x64_xmm_control_status_register).

# HV_X64_XMM_CONTROL_STATUS_REGISTER

## Syntax

```c
typedef struct
{
    union
    {
        UINT64 LastFpRdp;
        struct
        {
            UINT32 LastFpDp;
            UINT16 LastFpDs;
        };
    };

    UINT32 XmmStatusControl;
    UINT32 XmmStatusControlMask;
} HV_X64_XMM_CONTROL_STATUS_REGISTER;
 ```