---
id: 3eH6xcXDXtSoDxMK3R3yGDC
title: >-
    Mark your calendar&#58; Watch the MS Management Summit keynotes
desc: >-
    Mark your calendar; Watch the MS Management Summit keynotes
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2010/20100416-mark-your-calendar-watch-the-ms-management-summit-keynotes
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 311788c13a2fad9fdc0c4814b41324d627d7fc67edbf575602b005a69648a9ee
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100416-mark-your-calendar-watch-the-ms-management-summit-keynotes.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/16/2010"
    date: "2010-04-16 09:15:00"
    categories: "application-virtualization"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2010/20100416-mark-your-calendar-watch-the-ms-management-summit-keynotes.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2010/20100416-mark-your-calendar-watch-the-ms-management-summit-keynotes).

# Mark your calendar; Watch the MS Management Summit keynotes

Next Tuesday (20th) and Wednesday (21st) be sure to visit the [**Microsoft News Center**](https://www.microsoft.com/presspass/presskits/infrastructure/Default.aspx) to view the Microsoft Management Summit executive keynotes live on the web.  Don’t miss this opportunity to see Bob Muglia and Brad Anderson discuss the latest IT Management news, MS strategies for datacenter and desktop management, and virtualization and cloud solutions from Microsoft. And did I mention demos of new and upcoming products.

·         Tuesday, April 20, 8:30 – 9:45 AM PST:  Managing Systems from the Datacenter to the Cloud,  Bob Muglia, president, Microsoft Server and Tools 

·         Wednesday, April 21, 8:30 – 9:45 AM PST:  User Centric Client Management, Brad Anderson, corporate vice president, Management and Services Division