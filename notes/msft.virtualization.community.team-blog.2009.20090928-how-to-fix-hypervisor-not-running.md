---
id: 74Y3fvasZavAgkLSh62JqyA
title: >-
    How to fix&#58; Hypervisor not running
desc: >-
    How to fix Hypervisor not running
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090928-how-to-fix-hypervisor-not-running
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 61fb5cda82f1cdcd4124b2c54e73916364fe236c1853628ae406466e5addcb4e
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090928-how-to-fix-hypervisor-not-running.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "09/28/2009"
    date: "2009-09-28 22:04:08"
    categories: "uncategorized"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090928-how-to-fix-hypervisor-not-running.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090928-how-to-fix-hypervisor-not-running).

# How to fix Hypervisor not running

This is a Hypervisor 101 case that keeps showing itself every so often the forums, mailing lists, etc: A message to the effect of “The Hypervisor is not running” when a user tries to launch a VM. Felipe Ayora, one of our awesome UA people, has created this video that steps through how to investigate such a failure and fix it

 _</troubleshoot/windows-server/virtualization/virtual-machine-not-started-hypervisor-not-running>_