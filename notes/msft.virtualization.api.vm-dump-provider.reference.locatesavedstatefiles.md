---
id: A59sfYSmQKCFPcQke9DFnJ4
title: >-
    The LocateSavedStateFiles function
desc: >-
    Locates the saved state file(s) for a given VM and/or snapshot.
canonicalUrl: https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/LocateSavedStateFiles
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 866c5a67e02ba8602208949e0b7777d353d8488bba794b408bb2f05051e114b0
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/LocateSavedStateFiles.md
    ms.date: "04/19/2022"
    author: "mattbriggs"
    ms.author: "mabrigg"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/LocateSavedStateFiles.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/LocateSavedStateFiles).

# LocateSavedStateFiles function

Locates the saved state file(s) for a given VM and/or snapshot. This function uses WMI and the V1 or V2 virtualization namespace. So this is expected to fail if ran on a machine without Hyper-V installed.
- If the given VM has a VMRS file, parameters BinPath and VsvPath will be a single null terminator character.
- If the given VM has BIN and VSV files, parameter VmrsPath will be a single null terminator character.
- If no saved state files are found, all three returned string parameters will be single null terminator characters.

## Syntax
```C
HRESULT
WINAPI
LocateSavedStateFiles(
    _In_        LPCWSTR                     VmName,
    _In_opt_    LPCWSTR                     SnapshotName,
    _Out_       LPWSTR*                     BinPath,
    _Out_       LPWSTR*                     VsvPath,
    _Out_       LPWSTR*                     VmrsPath
    );
```
## Parameters

`VmName`

Supplies the VM name for which the saved state file will be located.

`SnapshotName`

Supplies an optional snapshot name to locate its saved state file on relation to the given VM name.

`BinPath`

Returns a pointer to a NULL-terminated string containing the full path name to the BIN file. The caller must call LocalFree on the returned pointer in order to release the memory occupied by the string.

`VsvPath`

Returns a pointer to a NULL-terminated string containing the full path name to the VSV file. The caller must call LocalFree on the returned pointer in order to release the memory occupied by the string.

`VmrsPath`

Returns a pointer to a NULL-terminated string containing the full path name to the VMRS file. The caller must call LocalFree on the returned pointer in order to release the memory occupied by the string.

## Return Value

If the operation completes successfully, the return value is `S_OK`. Otherwise, `E_OUTOFMEMORY` indicates there was insufficient memory to return the full path(s) or other `HRESULT` failure codes might be returned.

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |