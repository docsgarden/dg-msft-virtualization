---
id: 9UExbQpF7vR7PMPRScSoADJ
title: >-
    HCN_GUESTNETWORKSERVICE
desc: >-
    HCN_GUESTNETWORKSERVICE
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_GUESTNETWORKSERVICE
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: cb3c15320759b32bf28c185a230726c7ec7ee2a87733b5c65a27be454a0888e8
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_GUESTNETWORKSERVICE.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HCN_GUESTNETWORKSERVICE']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_GUESTNETWORKSERVICE.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_GUESTNETWORKSERVICE).

# HCN\_GUESTNETWORKSERVICE

## Description

Context handle referencing a GuestNetworkService in HNS.


## Syntax

```cpp
typedef void* HCN_GUESTNETWORKSERVICE;
```


## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |