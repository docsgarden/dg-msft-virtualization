---
id: i6fKaEuiQmFAQULrrm7NLFx
title: >-
    HDV_PCI_PNP_ID structure
desc: >-
    HDV_PCI_PNP_ID structure
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciPnpId
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 2971a7d9bbb868ac2327c021294b2eb20a04fc8e05b46eb5e5abcea9704ae228
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciPnpId.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HDV_PCI_PNP_ID structure']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/hdv/HdvPciPnpId.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/hdv/HdvPciPnpId).

# HDV_PCI_PNP_ID structure

PnP ID definition for a virtual device.

## Syntax

```C++
typedef struct HDV_PCI_PNP_ID
{
    UINT16 VendorID;
    UINT16 DeviceID;
    UINT8  RevisionID;
    UINT8  ProgIf;
    UINT8  SubClass;
    UINT8  BaseClass;
    UINT16 SubVendorID;
    UINT16 SubSystemID;

} HDV_PCI_PNP_ID, *PHDV_PCI_PNP_ID;
```

## Members

`VendorID`

Vendor ID (lowest two types of config space).

`DeviceID`

Device ID (offset 0x4 of config space).

`RevisionID`

Device Revision ID (offset 0x8 of config space).

`ProgIf`

Programable interface ID (offset 0x9 of config space).

`SubClass`

Sub-class code of device (offset 0xA of config space).

`BaseClass`

Base class code of device (offset 0xB of config space).

`SubVendorID`

Subsystem vendor ID of device (offset 0x2C of config space).

`SubSystemID`

Subsystem ID of device (offset 0x2E of config space).

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |