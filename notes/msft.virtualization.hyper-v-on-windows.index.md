---
id: 9xnAD32pgQwYNzd6XC2G46A
title: >-
    Hyper-V on Windows 10
desc: >-
    Hyper-V on Windows 10
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/index
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: e2cd7fb01c17f65aa488228d1c1e36ceb68edaa5de45fd9a1685ed34d5eab99e
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/index.md
    keywords: "windows 10, hyper-v"
    author: "scooley"
    ms.author: "scooley"
    ms.date: "05/02/2016"
    ms.topic: "article"
    ms.prod: "windows-10-hyperv"
    ms.assetid: "05269ce0-a54f-4ad8-af75-2ecf5142b866"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/index.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/index).

# Hyper-V on Windows 10 

Many versions of Windows 10 include the Hyper-V virtualization technology. Hyper-V enables running virtualized computer systems on top of a physical host. These virtualized systems can be used and managed just as if they were physical computer systems, however they exist in virtualized and isolated environment. Special software called a hypervisor manages access between the virtual systems and the physical hardware resources. Virtualization enables quick deployment of computer systems, a way to quickly restore systems to a previously known good state, and the ability to migrate systems between physical hosts.

The following documents detail the Hyper-V feature in Windows 10, provide a guided quick start, and also contain links to further resources and community forums. 

## About Hyper-V on Windows
The following articles provide an introduction to and information about Hyper-V on Windows.

* [[Introduction to Hyper-V|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.about.index]]
* [[Supported Guest Operating Systems|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.about.supported-guest-os]]

## Get started with Hyper-V
The following documents provide a quick and guided introduction to Hyper-V on Windows 10.

* [[Install Hyper-V|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.quick-start.enable-hyper-v]]
* [[Create a Virtual Machine|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.quick-start.create-virtual-machine]]
* [[Create a Virtual Switch|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.quick-start.connect-to-network]]
* [[Hyper-V and PowerShell|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.quick-start.try-hyper-v-powershell]]

## Connect with Community and Support
Additional technical support and community resources.

* [Hyper-V forums](https://social.technet.microsoft.com/Forums/windowsserver/home?forum=winserverhyperv)
* [Community Resources for Hyper-V and Windows Containers](https://docs.microsoft.com/en-us/virtualization/community/)