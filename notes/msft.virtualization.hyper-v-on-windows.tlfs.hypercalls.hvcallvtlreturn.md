---
id: AkJCX3AAK2FKMU4K9VtMQvw
title: >-
    HvCallVtlReturn
desc: >-
    HvCallVtlReturn hypercall
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallVtlReturn
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 9dffba6215367fccef7a16c27ed91cccaa5fc08982f25082880adf193f88a6c0
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallVtlReturn.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallVtlReturn.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/hypercalls/HvCallVtlReturn).

# HvCallVtlReturn

HvCallVtlReturn initiates a "[[VTL Return|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.vsm#vtl-return]]" and switches into the next lowest VTL enabled on the VP.

## Interface

 ```c
HV_STATUS
HvCallVtlReturn(
    VOID
    );
 ```

## Call Code

`0x0012` (Simple)