---
id: 5fuiYFCbpCUoXiZaXT2c9sy
title: >-
    WHV_EMULATOR_GET_VIRTUAL_PROCESSOR_REGISTERS_CALLBACK method
desc: >-
    Learn about the WHV_EMULATOR_GET_VIRTUAL_PROCESSOR_REGISTERS_CALLBACK method.
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorGetVirtualProcessorRegistersCallback
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 427c01aca32bfa0e6792cf2ecc2c589c7c6f13c4519028c73aeeced9de61eef4
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorGetVirtualProcessorRegistersCallback.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "04/19/2022"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorGetVirtualProcessorRegistersCallback.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-instruction-emulator/funcs/WhvEmulatorGetVirtualProcessorRegistersCallback).

# WHV_EMULATOR_GET_VIRTUAL_PROCESSOR_REGISTERS_CALLBACK


## Syntax

```c
typedef HRESULT (CALLBACK *WHV_EMULATOR_GET_VIRTUAL_PROCESSOR_REGISTERS_CALLBACK)(
    _In_ VOID* Context,
    _In_reads_(RegisterCount) const WHV_REGISTER_NAME* RegisterNames,
    _In_ UINT32 RegisterCount,
    _Out_writes_(RegisterCount) WHV_REGISTER_VALUE* RegisterValues
);
```

## Remarks
Callback requesting VP register state, similar to the WinHv API