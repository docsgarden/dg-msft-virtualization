---
id: 8gceCP7kdxVVhTz7B69gQsr
title: >-
    Hyper-V Replica Runbooks
desc: >-
    Hyper-V Replica Runbooks
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2013/20130220-hyper-v-replica-runbooks
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 5cfef5d7e64c7af6f82443d19b753eb01abe620aac2d9cdd6d4cf7f5b24c0b1e
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20130220-hyper-v-replica-runbooks.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "02/20/2013"
    date: "2013-02-20 06:21:43"
    categories: "hvr"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2013/20130220-hyper-v-replica-runbooks.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2013/20130220-hyper-v-replica-runbooks).

# Runbooks in Hyper-V Replica

System Center 2012 Orchestrator provides a powerful workflow management solution which allows IT administrators to automate different tasks exposed by the platform. At the heart of the Orchestrator is a set of **Runbooks** which outline the sequence of activities for a given task. A number of resources are available on Technet which explain the concept and provide guidance on building these Runbooks.  

**Charles Joy** is a Senior Program Manager with the Windows Server & System Center group in Microsoft who has authored a set of Hyper-V Replica sample Runbooks which can be downloaded for free from [TechNet Galleries](https://gallery.technet.microsoft.com/Orchestrated-HVR-Planned-5ebecfc1). In his blog post <https://blogs.technet.com/b/building_clouds/archive/2013/02/11/automation-orchestrating-hyper-v-replica-with-system-center-for-planned-failover.aspx> he explains the high level concepts and also shows a Planned Failover in action. 

Download the runbook and share your experience with us – we would love to hear your feedback!