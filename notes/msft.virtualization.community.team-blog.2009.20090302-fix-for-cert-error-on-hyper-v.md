---
id: 8krTXKSQvnos5pcTb6rVycr
title: >-
    Fix for Cert Error on Hyper-V
desc: >-
    A certificate error can occur for those of you running WS08 Hyper-V and connect using vmconnect. This error results in the inability to start or connect to VMs running on WS08 Hyper-V or MS Hyper-V Server 2008.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090302-fix-for-cert-error-on-hyper-v
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 5012c9c7013dcdfacb1b18d54b7b05a7c56b9406bc7895fa9956b8c1c99f0ecf
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090302-fix-for-cert-error-on-hyper-v.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-03-02 20:08:00"
    ms.date: "03/02/2009"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090302-fix-for-cert-error-on-hyper-v.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090302-fix-for-cert-error-on-hyper-v).

# Fix for Cert Error on Hyper-V

Bryon over at the [Windows Server Division blog](https://blogs.technet.com/windowsserver/ "Windows Server Div blog") pointed out a certificate error that can occur for those of you running WS08 Hyper-V and connect using vmconnect. The cert error results in the inability to start or connect to VMs running on WS08 Hyper-V or MS Hyper-V Server 2008. Here's an excerpt: 

**Symptoms and resolution:**

§  You may be unable to start or connect to virtual machines running on Windows Server 2008 or Microsoft Hyper-V Server 2008. This occurs when connecting using vmconnect.  Connections made using Remote Desktop won't be affected.

§  KB Article [967902](https://support.microsoft.com/default.aspx?scid=kb;EN-US;967902) has been created that details the symptoms and resolution.  This KB article provides a direct link to download the quickfix to resolve this error.

 

**Important Notes:**

§  Though this error may occur, the Hyper-V service will continue to operate.   Neither the Hyper-V host nor the running virtual machines will go offline.

§  It is not expected that this issue can be exploited for malicious purposes.

§  Customers running Windows Server 2008 R2 Hyper-V beta won’t experience this error.

  Patrick