---
id: 7ncvrjj8iN7784Bi9h9ruCe
title: >-
    WHvGetVirtualProcessorRegisters
desc: >-
    Description on working with parameters, return value, and syntax for WHvGetVirtualProcessorRegisters
canonicalUrl: https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvGetVirtualProcessorRegisters
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 5da9b95f1b8ab1292b9ba4ecbba84b90400b67ed125f6a4ab3546ea57ce8e7e4
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvGetVirtualProcessorRegisters.md
    author: "Juarezhm"
    ms.author: "hajuarez"
    ms.date: "06/22/2018"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hypervisor-platform/funcs/WHvGetVirtualProcessorRegisters.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hypervisor-platform/funcs/WHvGetVirtualProcessorRegisters).

# WHvGetVirtualProcessorRegisters

## Syntax

```C
HRESULT
WINAPI
WHvGetVirtualProcessorRegisters(
    _In_ WHV_PARTITION_HANDLE Partition,
    _In_ UINT32 VpIndex,
    _In_reads_(RegisterCount) const WHV_REGISTER_NAME* RegisterNames,
    _In_ UINT32 RegisterCount,
    _Out_writes_(RegisterCount) WHV_REGISTER_VALUE* RegisterValues
    );
```

### Parameters

`Partition`

Handle to the partition object

`VpIndex`

Specifies the index of the virtual processor whose registers are queried

`RegisterNames`

Array specifying the names of the registers that are queried

`RegisterCount`

Specifies the number of elements in the `RegisterNames` array

`RegisterValues`

Specifies the output buffer that receives the values of the request registers