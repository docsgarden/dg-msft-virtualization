---
id: 96GWhwJjTmbRxRkxHwVo6ai
title: >-
    Run containers on Windows Admin Center
desc: >-
    Run a container on Windows Admin Center
canonicalUrl: https://docs.microsoft.com/virtualization/windowscontainers/wac-tooling/wac-containers
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 8b98822ac7d2e6e22f8732d68d520df504433c5256e49456a08aee76b39a9b64
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/windowscontainers/wac-tooling/wac-containers.md
    keywords: "docker, containers, Windows Admin Center"
    author: "vrapolinario"
    ms.author: "viniap"
    ms.date: "12/23/2020"
    ms.topic: "quickstart"
    ms.assetid: "bb9bfbe0-5bdc-4984-912f-9c93ea67105f"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/windowscontainers/wac-tooling/wac-containers.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/windowscontainers/wac-tooling/wac-containers).

# Run new containers using Windows Admin Center

Windows Admin Center helps you run containers locally on your container host. 

1. In Windows Admin Center with the **Containers** extension installed, open the container host you want to manage.
2. Under **Tools** in the left pane, select the **Containers** extension. 
3. Select the **Images** tab under Container host.

    ![Images tab]([Images tab|dendron://dg-msft-https://docs.microsoft.com/virtualization/msft.https://docs.microsoft.com/virtualization/windowscontainers/wac-tooling/media/wac-images.png]])

4. Select the image you want to run and click **Run**.

    ![Run image]([Run image|dendron://dg-msft-https://docs.microsoft.com/virtualization/msft.https://docs.microsoft.com/virtualization/windowscontainers/wac-tooling/media/wac-run-containers.png]])

5. Under **Run image**, you can specify the container-related configuration:

    - Container name: This is an optional input. You can provide a container name to help you track the container you created - otherwise, Docker will provide a generic name to it.
    - Isolation type: You can choose to use hypervisor instead of the default process isolation. For more information on isolation modes for Windows Containers, see [[Isolation Modes|dendron://dg-msft-virtualization/msft.virtualization.windowscontainers.manage-containers.hyperv-container]].
    - Publish ports: By default, Windows containers use NAT for the networking mode. This means a port on the container host will be mapped to the container. This option allows you to specify that mapping.
    - Memory allocation and CPU count: You can specify how much memory and how many CPUs that a container should be able to use. This option does not allocate the assigned memory or CPU to the container. Rather, this option specifies the maximum amount a container will be able to allocate.
    - Add: You can also append Docker run parameters that are not in the UI, such as -v for persistent volume. For more information on available Docker run parameters, see [Docker documentation](https://docs.docker.com/engine/reference/commandline/run/).

6. Once you've finished configuring the container, select **Run**. You can view the status of the running containers on the **Containers** tab:

    ![Containers tab]([Containers tab|dendron://dg-msft-https://docs.microsoft.com/virtualization/msft.https://docs.microsoft.com/virtualization/windowscontainers/wac-tooling/media/wac-containers.png]])

## Next steps

> [[!div class="nextstepaction"]
> [Manage Azure Container Registry on Windows Admin Center|dendron://dg-msft-virtualization/msft.virtualization.windowscontainers.wac-tooling.wac-acr]]