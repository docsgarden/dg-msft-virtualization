---
id: 63DLdiiyAUtcbUB2KKK7evV
title: >-
    HcnModifyEndpoint
desc: >-
    HcnModifyEndpoint
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnModifyEndpoint
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 94fbad2d9ac49d96867c91ba9d1a1cdac9620011a03dfa12feed379e6a4ef891
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnModifyEndpoint.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HcnModifyEndpoint']"
    api_location: "['computenetwork.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HcnModifyEndpoint.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HcnModifyEndpoint).

# HcnModifyEndpoint

## Description

Modifies an endpoint.

## Syntax

```cpp
HRESULT
WINAPI
HcnModifyEndpoint(
    _In_ HCN_ENDPOINT Endpoint,
    _In_ PCWSTR Settings,
    _Outptr_opt_ PWSTR* ErrorRecord
    );
```

## Parameters

`Endpoint`

The [[HCN\_ENDPOINT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcn_endpoint]] to modify.

`Settings`

JSON document specifying the settings of the [[endpoint|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.HNS_Schema#HostComputeEndpoint]].

`ErrorRecord`

Receives a JSON document with extended errorCode information. The caller must release the buffer using CoTaskMemFree.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcn.reference.hcnhresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |
| **Library** | ComputeNetwork.lib |
| **Dll** | ComputeNetwork.dll |