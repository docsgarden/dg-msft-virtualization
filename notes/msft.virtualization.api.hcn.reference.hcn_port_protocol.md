---
id: 99owcBm6VqYPpzoo6svR7Di
title: >-
    HCN_PORT_PROTOCOL
desc: >-
    HCN_PORT_PROTOCOL
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_PORT_PROTOCOL
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 10319f49abf83e144a52b9aa39f153d06d6b811a1dd15cf980831b1630f4d1d5
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_PORT_PROTOCOL.md
    author: "Keith-Mange"
    ms.author: "kemange"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "10/31/2021"
    api_name: "['HCN_PORT_PROTOCOL']"
    api_location: "['computenetwork.dlll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcn/Reference/HCN_PORT_PROTOCOL.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcn/Reference/HCN_PORT_PROTOCOL).

# HCN\_PORT\_PROTOCOL

## Description

The protocol for a reservation.

## Syntax

```cpp
typedef enum tagHCN_PORT_PROTOCOL
{
    HCN_PORT_PROTOCOL_TCP = 0x01,
    HCN_PORT_PROTOCOL_UDP = 0x02,
    HCN_PORT_PROTOCOL_BOTH = 0x03
} HCN_PORT_PROTOCOL;

```

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeNetwork.h |