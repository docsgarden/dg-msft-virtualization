---
id: 9sTv4cWswEXaPBk8FQicrgv
title: >-
    HcsExportLegacyWritableLayer
desc: >-
    HcsExportLegacyWritableLayer
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsExportLegacyWritableLayer
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 651f536bb66a470258c8bcea3d81ad74d8864da791264686c1fc150b0095e107
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsExportLegacyWritableLayer.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsExportLegacyWritableLayer']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsExportLegacyWritableLayer.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsExportLegacyWritableLayer).

# HcsExportLegacyWritableLayer

## Description

This function exports a legacy container writable layer.

## Syntax

```cpp
HRESULT WINAPI
HcsExportLegacyWritableLayer(
    _In_ PCWSTR writableLayerMountPath,
    _In_ PCWSTR writableLayerFolderPath,
    _In_ PCWSTR exportFolderPath,
    _In_ PCWSTR layerData
    );
```

## Parameters

`writableLayerMountPath`

Path of the writable layer to export.

`writableLayerFolderPath`

Folder of the writable layer to export.

`exportFolderPath`

Destination folder for the exported layer.

`layerData`

JSON document of [[layerData|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.SchemaReference#LayerData]] providing the locations of the antecedent layers that are used by the exported layer.

## Return Values

The function returns [[HRESULT|dendron://dg-msft-virtualization/msft.virtualization.api.hcs.reference.hcshresult]].

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeStorage.h |
| **Library** | ComputeStorage.lib |
| **Dll** | ComputeStorage.dll |