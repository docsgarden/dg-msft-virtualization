---
id: cxnt4fRDihTButkR6WifHcA
title: >-
    RT &#34;Cutting through the FUD&#58; Facts you should know about Hyper-V and System Center&#34;
desc: >-
    Cutting through the FUD; Facts you should know about Hyper-V and System Center
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20091218-rt-cutting-through-the-fud-facts-you-should-know-about-hyper-v-and-system-center
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 61266782bad4ef0ea8f72fcb072d188fa1ec94e06756174e0506080a780a0544
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091218-rt-cutting-through-the-fud-facts-you-should-know-about-hyper-v-and-system-center.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    ms.date: "12/18/2009"
    date: "2009-12-18 23:30:00"
    categories: "esx"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20091218-rt-cutting-through-the-fud-facts-you-should-know-about-hyper-v-and-system-center.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20091218-rt-cutting-through-the-fud-facts-you-should-know-about-hyper-v-and-system-center).

# Cutting through the FUD; Facts you should know about Hyper-V and System Center

I'm not sure the retweet tag (RT) also applies to blogs or not, but I thought I'd give it a try. Now to my point. Edwin posted a [good blog](https://jamesone111.wordpress.com/category/windows-server-2008-r2/page/2/ "Edwin's blog") this evening about re-occurring Fear, Uncertainty, Doubt the spreads via traditional journalism or blogs. I don't know for sure, but I suspect his post had something to do with an InformationWeek blog posted by an [SI partner of VMware](http://www.artemistechnology.com/partner.aspx?id=57 "Artemis website"). I recommend reading Edwin's post. Here's a summary of his points: 

  * **You do not need to remove or replace your existing VMware installation to deploy Hyper-V**
  * **Hyper-V and the rest of Microsoft Virtualization is “Enterprise ready” for hundreds of companies today**.
  * **Virtual Machine Manager is not a replacement for vCenter**  
  * **Hyper-V is a mature, safe, secure hypervisor**

Have a great holiday. 

Patrick 

 **UPDATE** : James also [blogged about this topic](https://jamesone111.wordpress.com/category/windows-server-2008-r2/page/2/x "James O'Neill's blog"), with video and picture too.