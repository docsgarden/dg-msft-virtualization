---
id: 84MpGgnmMzT2baaJDZRaahM
title: >-
    HcsGetOperationId
desc: >-
    HcsGetOperationId
canonicalUrl: https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetOperationId
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 3dcd694bc01b4ad3697a27a32e5e65049e76be00e74397e2d2ccad488dc1e8b5
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetOperationId.md
    author: "faymeng"
    ms.author: "mabrigg"
    ms.topic: "reference"
    ms.prod: "virtualization"
    ms.date: "06/09/2021"
    api_name: "['HcsGetOperationId']"
    api_location: "['computecore.dll']"
    api_type: "['DllExport']"
    topic_type: "['apiref']"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/hcs/Reference/HcsGetOperationId.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/hcs/Reference/HcsGetOperationId).

# HcsGetOperationId

## Description

Returns the Id that uniquely identifies an operation.

## Syntax

```cpp
UINT64 WINAPI
HcsGetOperationId(
    _In_ HCS_OPERATION operation
    );
```

## Parameters

`operation`

The handle to an active operation.

## Return Values

If the function succeeds, the return value is the operation's Id.

If the operation is invalid, the return value is `HCS_INVALID_OPERATION_ID`.

## Requirements

|Parameter|Description|
|---|---|
| **Minimum supported client** | Windows 10, version 1809 |
| **Minimum supported server** | Windows Server 2019 |
| **Target Platform** | Windows |
| **Header** | ComputeCore.h |
| **Library** | ComputeCore.lib |
| **Dll** | ComputeCore.dll |