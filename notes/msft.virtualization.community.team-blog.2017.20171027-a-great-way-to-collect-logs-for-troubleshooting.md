---
id: 8DX9aC2drkPUHdxf6eaL3KP
title: >-
    A great way to collect logs for troubleshooting
desc: >-
    Learn how to optimize your use of event logs to troubleshoot issues within a Hyper-V cluster.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2017/20171027-a-great-way-to-collect-logs-for-troubleshooting
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 12279a580534faa19639762513c9ba910a10362cdd85d4b312b06a1eecfa858b
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2017/20171027-a-great-way-to-collect-logs-for-troubleshooting.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2017-10-27 23:30:39"
    ms.date: "10/27/2017"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2017/20171027-a-great-way-to-collect-logs-for-troubleshooting.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2017/20171027-a-great-way-to-collect-logs-for-troubleshooting).

# A great way to collect logs for troubleshooting

Did you ever have to troubleshoot issues within a Hyper-V cluster or standalone environment and found yourself switching between different event logs? Or did you repro something just to find out not all of the important Windows event channels had been activated? To make it easier to collect the right set of event logs into a single evtx file to help with troubleshooting we have published a [HyperVLogs PowerShell module](https://github.com/MicrosoftDocs/Virtualization-Documentation/tree/live/hyperv-tools/HyperVLogs) on GitHub.  In this blog post I am sharing with you how to get the module and how to gather event logs using the functions provided. 

### Step 1: Download and import the PowerShell module

First of all you need to download the PowerShell module and import it. 

### Step 2: Reproduce the issue and capture logs

Now, you can use the functions provided as part of the module to collect logs for different situations. For example, to investigate an issue on a single node, you can collect events with the following steps:  Using this module and its functions made it a lot easier for me to collect the right event data to help with investigations. Any feedback or suggestions are highly welcome. Cheers, Lars