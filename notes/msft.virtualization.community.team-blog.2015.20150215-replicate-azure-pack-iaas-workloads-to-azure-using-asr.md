---
id: 6q6eTguDyPbyTaku5Z5xqHp
title: >-
    Replicate Azure Pack IaaS Workloads to Azure using ASR
desc: >-
    Learn about how you can use Azure Site Recovery to replicate Azure Pack IaaS workloads.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2015/20150215-replicate-azure-pack-iaas-workloads-to-azure-using-asr
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 0585590a8c54e4f8269af7efd2d1f185cbf4015a30ebe6c9c0b35d421bc27f2c
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2015/20150215-replicate-azure-pack-iaas-workloads-to-azure-using-asr.md
    author: "mattbriggs"
    ms.author: "mabrigg"
    date: "2015-02-15 22:58:27"
    ms.date: "02/16/2015"
    categories: "asr"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2015/20150215-replicate-azure-pack-iaas-workloads-to-azure-using-asr.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2015/20150215-replicate-azure-pack-iaas-workloads-to-azure-using-asr).

# Replicate Azure Pack IaaS Workloads to Azure using ASR

A few months back, we announced [Azure Site Recovery](https://azure.microsoft.com/services/site-recovery/) (ASR)’s integration with Azure Pack, which enabled our Service Providers to start offering [Managed Disaster Recovery for IaaS Workloads using ASR and Azure Pack](https://azure.microsoft.com/blog/2014/11/12/offering-managed-dr-for-iaas-workloads-with-asr-and-azure-pack/). The response, since we announced the integration, has been phenomenal - customers are appreciating the simplicity of ASR and are adopting ASR as the standard for offering DR solution in their environments. The integration of ASR and Azure Pack also enabled Service Providers to offer DR as a value added service, opening up new review streams and the ability to offer better Service Level Agreements (SLA) to their end customers. A key ask that our early adopters have expressed – and that we are delivering on today – is the ability to use Microsoft Azure as a Disaster Recovery Site for IaaS workloads when using Azure Pack. The new features in the ASR – Azure Pack integration will enable Service Providers to offer Azure as a DR site or to an on-premise secondary site using their Azure Pack UR 4.0 deployments. To enable these capabilities you can [download the latest ASR runbooks](https://gallery.technet.microsoft.com/scriptcenter/Azure-Recovery-integration-0ed7a9a3) and import them into your WAP environments. To know more about the integration, check out our [WAP deployment guide](https://technet.microsoft.com/library/dn850370.aspx). Getting started with [Azure Site Recovery](https://aka.ms/asr_wap_annoucement_landingpage) is easy – simply check out the [pricing information](https://aka.ms/asr_wap_annoucement_pricing), and [sign up for a free Microsoft Azure trial](https://aka.ms/asr_wap_annoucement_trial).