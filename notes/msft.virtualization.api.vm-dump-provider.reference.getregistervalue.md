---
id: 72gPqeK2ar6rSov9qa6WD68
title: >-
    The GetRegisterValue function
desc: >-
    Queries for a specific register value for a given VP in a VmSavedStateDump.
canonicalUrl: https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/GetRegisterValue
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: a78d9be309316fee20f51d16eeca6d1b570bc5427396d25fbfab756333e12b0a
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/GetRegisterValue.md
    ms.date: "04/19/2022"
    author: "mattbriggs"
    ms.author: "mabrigg"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/GetRegisterValue.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/GetRegisterValue).

# GetRegisterValue function

Queries for a specific register value for a given VP in a VmSavedStateDump. Callers must specify architecture and register ID in paremeter Register, and this function returns the regsiter value through it.

## Syntax

```C
HRESULT
WINAPI
GetRegisterValue(
    _In_    VM_SAVED_STATE_DUMP_HANDLE      VmSavedStateDumpHandle,
    _In_    UINT32                          VpId,
    _Inout_ VIRTUAL_PROCESSOR_REGISTER*     Register
    );
```

## Parameters

`VmSavedStateDumpHandle`

Supplies a handle to a dump provider instance.

`VpId`

Supplies the Virtual Processor Id.

`Register`

Supplies an enumeration value with the register being queried. Returns the queried register value.

## Return Value

If the operation completes successfully, the return value is `S_OK`.

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |