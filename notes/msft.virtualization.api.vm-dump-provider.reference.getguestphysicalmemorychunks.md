---
id: 9izKsYhkv2oNkaohTa6RaZy
title: >-
    The GetGuestPhysicalMemoryChunks function
desc: >-
    Returns the layout of the physical memory of the guest.
canonicalUrl: https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/GetGuestPhysicalMemoryChunks
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 5d188a4ae27442ee2441c752aed1208d26bd1bde829fa4853b4a3302153897bf
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/GetGuestPhysicalMemoryChunks.md
    ms.date: "04/18/2022"
    author: "mattbriggs"
    ms.author: "mabrigg"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966270000
created: 1658966270000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/api/vm-dump-provider/reference/GetGuestPhysicalMemoryChunks.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/api/vm-dump-provider/reference/GetGuestPhysicalMemoryChunks).

# GetGuestPhysicalMemoryChunks function

Returns the layout of the physical memory of the guest. This information contains the chunks of memory with consecutive pages and from where each one starts. If the supplied count is less than the amount of chunks for this guest, then this function returns the expected chunk count.

## Syntax

```C
HRESULT
WINAPI
GetGuestPhysicalMemoryChunks(
    _In_    VM_SAVED_STATE_DUMP_HANDLE      VmSavedStateDumpHandle,
    _Out_   UINT64*                         MemoryChunkPageSize,
    _Out_   GPA_MEMORY_CHUNK*               MemoryChunks,
    _Inout_ UINT64*                         MemoryChunkCount
    );
```

## Parameters

`VmSavedStateDumpHandle`

Supplies a handle to a dump provider instance.

`MemoryChunkPageSize`

Returns the size of a page in the memory chunk layout.

`MemoryChunks`

Supplies a buffer of memory chunk structures that are filled up with the requested information if the buffer size is the same or bigger than the memory chunks count for this guest.

`MemoryChunkCount`

Supplies the size of the MemoryChunks buffer. If this count is lower than what the guest really has, then it returns the expected count. If it was higher than what the guest has, then it returns the exact count.

## Return Value

If the operation completes successfully, the return value is `S_OK`.

## Requirements

|Parameter|Description|
|---|---|---|---|---|---|---|---|
| **Minimum supported client** | Windows 10, version 1607 |
| **Minimum supported server** | Windows Server 2016 |
| **Target Platform** | Windows |
| **Library** | ComputeCore.ext |
| **Dll** | ComputeCore.ext |
|    |    |