---
id: 3yVWdLjizM4ayYocbXZijCU
title: >-
    Windows Server 2008 Hyper-V Security Guide – beta now available
desc: >-
    The guide provides authoritative guidance that relates to the following strategies for securing virtualized environments.
canonicalUrl: https://docs.microsoft.com/virtualization/community/team-blog/2009/20090206-windows-server-2008-hyper-v-security-guide-beta-now-available
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: 902466e5573b6000526245358dea9172e9a4a225f3e8301fc4dba96bb9426151
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090206-windows-server-2008-hyper-v-security-guide-beta-now-available.md
    author: "scooley"
    ms.author: "scooley"
    date: "2009-02-06 01:32:00"
    ms.date: "02/06/2009"
    categories: "hyper-v"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966271000
created: 1658966271000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/community/team-blog/2009/20090206-windows-server-2008-hyper-v-security-guide-beta-now-available.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/community/team-blog/2009/20090206-windows-server-2008-hyper-v-security-guide-beta-now-available).

# Windows Server 2008 Hyper-V Security Guide – beta now available

**The Hyper-V Security Guide can help you elevate the security of virtualized Windows Server environments to meet your business-critical needs. This[accelerator](https://technet.microsoft.com/solutionaccelerators/default.aspx "MS.com site") provides IT professionals like you with recommendations to address your key security concerns around server virtualization. The guide provides authoritative guidance that relates to the following strategies for securing virtualized environments: 

  * Hardening Hyper-V. The guide provides prescriptive guidance for hardening the Hyper-V server role, including several best practices for installing and configuring Hyper-V with a focus on security. These best practices include measures for reducing the attack surface of Hyper-V as well as recommendations for properly configuring secure virtual networks and storage devices on a Hyper-V host server. 
  * Virtual machine management and delegation. The ability to safely and securely delegate administrative access to virtual machine resources within an organization is essential. The guide highlights several available methods to administer different aspects of a virtual machine infrastructure and ways to control administrative access to different servers and at different levels. 
  * Protecting virtual machines. The guide also provides prescriptive guidance for securing virtual machine resources, including best practices and detailed steps for protecting virtual machines by using a combination of file system permissions, encryption, and auditing.

Join the [beta](https://connect.microsoft.com/InvitationUse.aspx?ProgramID=2699&InvitationID=TET-THVC-6CWK&SiteID=715) program [Live ID required].  Then [bookmark this link](https://connect.microsoft.com/content/content.aspx?SiteID=715&ContentID=10340) to the program site to get the latest information about upcoming events. Patrick **