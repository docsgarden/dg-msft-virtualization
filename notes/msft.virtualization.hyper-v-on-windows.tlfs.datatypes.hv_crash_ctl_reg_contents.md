---
id: 845iLaYXUWB4tpgjHDTe4E9
title: >-
    HV_CRASH_CTL_REG_CONTENTS
desc: >-
    HV_CRASH_CTL_REG_CONTENTS
canonicalUrl: https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_crash_ctl_reg_contents
license: Creative Commons Attribution 4.0 International
source:
    sourceFileHash: e0f0db38a71c3f1d505b047d7f8ffecfe49c48cea8c62a4cf5c28ccdf4b1f000
    sourceFileUrl: https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_crash_ctl_reg_contents.md
    keywords: "hyper-v"
    author: "alexgrest"
    ms.author: "hvdev"
    ms.date: "10/15/2020"
    ms.topic: "reference"
    ms.prod: "windows-10-hyperv"
frontmatterVersion: 1.0.0
importScriptVersion: 1.0.0
importModuleVersion: 1.0.0
generatorType: Python
updated: 1658966269000
created: 1658966269000
---

> The [original source of this page](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_crash_ctl_reg_contents.md) was released by [Microsoft](https://github.com/MicrosoftDocs/Virtualization-Documentation) under the [Creative Commons Attribution 4.0 International](https://github.com/MicrosoftDocs/Virtualization-Documentation/blob/main/LICENSE) license. If wanting to contribute/update/fix the content, contribute to the source! This page uses the `canonicalUrl` HTML attribute to ensure SEO is uninterrupted for the original [published page](https://docs.microsoft.com/virtualization/hyper-v-on-windows/tlfs/datatypes/hv_crash_ctl_reg_contents).

# HV_CRASH_CTL_REG_CONTENTS

The following data structure is used to define the contents of the guest crash enlightenment control register (HV_X64_MSR_CRASH_CTL).

## Syntax

 ```c
typedef union
{
    UINT64 AsUINT64;
    struct
    {
        UINT64 Reserved : 62;         // Reserved bits
        UINT64 CrashMessage : 1;      // P3 is the PA of the message
                                      // P4 is the length in bytes
        UINT64 CrashNotify : 1;       // Log contents of crash parameter
    };
} HV_CRASH_CTL_REG_CONTENTS;
 ```

## See also

 [[Partition Crash Enlightenment|dendron://dg-msft-virtualization/msft.virtualization.hyper-v-on-windows.tlfs.partition-properties#partition-crash-enlightenment]]